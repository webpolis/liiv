<?php
/**
 * @property RequestHandler $RequestHandler
 * @property Auth $Auth
 */
class AppController extends Controller {
	var $helpers = array('Asset.asset', 'Html', 'Form', 'Javascript');
	var $components = array('Acl','Auth','RequestHandler');
	var $cacheAction = true;
	var $additionalJs = null;

	function beforeFilter() {
		/* Store to be available on all controllers */
		$controller = (isset($this->params['controller'])) ? $this->params['controller'] : null;
		$action = (isset($this->params['action'])) ? $this->params['action'] : null;
		/* Fields used for authorization, username is mapped to email column
		 * (users use email to log in) */
		$this->Auth->fields = array("username" => "email","password" => "password");
		/* Allow by default display, view and index methods */
		$this->Auth->allow(array("display","view","index","tag_cloud","featured"));
		/* Always allow people to subscribe */
		if($controller == 'subscribers' && $action = 'add') $this->Auth->allow('add');
		/* Is requested page in backend? */
		$adminPage = (isset($this->params['admin'])&&$this->params['admin'])? true: false;
		/* Log into admin section if logging in at /admin/users/login */
		$this->Auth->loginAction = array("controller"=>"users","action"=>"login","admin"=>$adminPage);
		$this->Auth->loginRedirect = ($adminPage)?"/admin":"/";
		/* Always redirect to / on logout */
		$this->Auth->logoutRedirect = "/";
		if(low($controller) == "users" && low($action) !== "add")  $this->Auth->authenticate =& ClassRegistry::init('User');// this gives problem on controllers' add methods
		$sess = $this->Session->read("Auth.User.email");
		if(isset($sess)){ //Is logged in
			$email = $sess;
			/* Log user's IP */
			$proxy = $ip = $proxy_text = $ip_text = '';
			if (getenv('HTTP_X_FORWARDED_FOR')) {
				if (getenv('HTTP_CLIENT_IP')) {
					$proxy = ip2long(getenv('HTTP_CLIENT_IP'));
					$proxy_text = getenv('HTTP_CLIENT_IP');
				} else {
					$proxy = ip2long(getenv('REMOTE_ADDR'));
					$proxy_text = getenv('REMOTE_ADDR');
				}
				$ip = ip2long(getenv('HTTP_X_FORWARDED_FOR'));
				$ip_text = getenv('HTTP_X_FORWARDED_FOR');
			} else {
				if (getenv('HTTP_CLIENT_IP')) {
					$ip = ip2long(getenv('HTTP_CLIENT_IP'));
					$ip_text = getenv('HTTP_CLIENT_IP');
				} else {
					$ip = ip2long(getenv('REMOTE_ADDR'));
					$ip_text = getenv('REMOTE_ADDR');
				}
			}
			$userId = $this->Session->read("Auth.User.id");

			$sql = "INSERT INTO iplogs (user_id, ip, proxy, ip_text, proxy_text, created, updated)
					VALUES ('".$userId."', '".$ip."', '".$proxy."','".$ip_text."','".$proxy_text."', NOW(), NOW())
					ON DUPLICATE KEY UPDATE updated = NOW()";
			$this->{$this->modelClass}->query($sql);
			/* END user's IP log */
		} else {
			$email = 'anonymous';
		}
		/* Check if user has permission */
		$acoAlias = $this->Auth->action();
		if($this->Acl->check($email,$acoAlias,"read")) {
			$this->set('read', $this->Acl->check($email, $acoAlias, 'read'));
			$this->set('delete', $this->Acl->check($email, $acoAlias, 'delete'));
			$this->set('create', $this->Acl->check($email, $acoAlias, 'create'));
			$this->set('update', $this->Acl->check($email, $acoAlias, 'update'));
		}else {
			if ($sess) {
				if(preg_match("/^.*(jpg|gif|bmp|png)$/si",$acoAlias)){
					$this->redirect("/img/unavailable.png");
				}
				$this->redirect(array('controller' => 'users', 'action' => 'unauthorized', 'admin' => true));
			} else {
				$this->redirect(array('controller' => 'users', 'action' => 'login','admin' => true));
			}
		}
	}

	function beforeRender () {
		$this->set("userInfo",$this->Auth->user());
		$admin = Configure::read('Routing.admin');
		if (isset($this->params[$admin]) && $this->params[$admin] && $this->params['controller'] != 'users' && $this->params['action'] != 'login') {
			$this->layout = 'admin';
		}
		if(isset($this->modelClass)) {
			$this->set("modelName",$this->modelClass);
		}
		$js = $this->_habtmEditHack();
		$this->set("additionalJs",$js);
	}

	protected function generateTagCloud(){
		$visits = array();
		$custom = preg_match("/condition|therapy|symptom|tradition/si",$this->modelClass);
		$model = ($custom) ? $this->modelClass : "Condition";
		$Visit = ClassRegistry::init(array("class" => "Visit", "table" =>
            "visits", "alias" => "Visit"));
		$Visit->bindModel(array(
			"belongsTo"=>array(
				"Entity"
				)
				));
				$Visit->bindModel(array(
			"belongsTo" => array(
				$model => array(
							"foreignKey" => "item_id"
							)
							)
							));
							$Visit->recursive = 1;
							$visits = $Visit->find("all",array("conditions"=>array($model.".published"=>true,"Entity.name"=>$model),
			"fields"=>array($model.".id",$model.".title",$model.".path","SUM(Visit.counter) AS `counter`"),
			"order" => array("Visit.counter DESC",$model.".title ASC"),
			"group" => array("Visit.entity_id","Visit.item_id")
							));
							return $visits;
	}

	function afterFilter(){
		/* Only increment visits when view action is called */
		if($this->params['action'] == 'view'){
			$this->addVisit();
		}
	}

	/*
	 * This function will add a visit to the object's record
	 * Visits are useful for statistics or tag cloud
	 */
	function addVisit(){
		$admin = Configure::read('Routing.admin');
		if(@!$this->params['admin']){
			$referer = Controller::referer();
			App::import("Model","Entity");
			$e =& new Entity();
			$edata = $e->find("first",array("fields"=>array("id"),"conditions"=>array("name"=>$this->modelClass)));
			$entity_id = (isset($edata['Entity']['id']))?$edata['Entity']['id']:null;
			if(!empty($entity_id)){
				$item_id = $this->{$this->modelClass}->id;
				$Visit = ClassRegistry::init( // Create a temporary Visit model
						array(
							  "class" => "Visit", 
							  "table" => "visits", 
							  "alias" => "Visit"
						));
				$ret = $Visit->find("first",array("conditions"=>array("entity_id"=>$entity_id,"item_id"=>$item_id,"referer"=>$referer)));
				if(empty($ret)){
					/* Initalize counter for current item
					 * Will be called also if it's the first time
					 * it's called from a particular referer
					 */
					$Visit->create();
					$Visit->save(array("entity_id"=>$entity_id,"item_id"=>$item_id,"counter"=>1,"referer"=>$referer));
				}else{
					//update counter on current item
					if(isset($ret['Visit']['id']) && !empty($ret['Visit']['id'])){
						$Visit->updateAll(array("counter"=>"counter+1"),array("Visit.id"=>$ret['Visit']['id']));
					}
				}
			}
		}
	}

	/**
	 * This is a hack for a self referenced HABTM relationship (ex. Article => Article).
	 * In the EDIT view, the multi-select box is not properly set, so we need to write a jQuery code snippet
	 * that will properly choose the linked items.
	 */
	private function _habtmEditHack() {
		$js = null;
		if(isset($this->params['action']) && $this->params['action'] == "admin_edit") {
			if(isset($this->data[$this->modelClass."Related"])) {
				$multiBoxId = $this->modelClass.$this->modelClass;
				$selected = array();
				if(is_array($this->data[$this->modelClass."Related"])) {
					foreach($this->data[$this->modelClass."Related"] as $rel) {
						if(isset($rel['id'])) {
							array_push($selected,$rel['id']);
						}
					}
					$js = "
                    j(function(){
                        var related = ".json_encode($selected).";
                        if(j('#$multiBoxId').is('select')){
                            j('#$multiBoxId option').each(function(i,e){
                                if(typeof(j(e).attr('selected')) != 'undefined'){
                                    j(e).attr('selected',false);
                                }
                            });
                            j('#$multiBoxId option').each(function(i,e){
                                if(typeof(j(e).attr('selected')) != 'undefined' && (jQuery.inArray(j(e).attr('value'),related)!= -1)){
                                    j(e).attr('selected',true);
                                }
                            });
                        }
                    });
                        ";
				}

			}
		}
		return $js;
	}

	function admin_relate($action="search",$search=null,$filter="all",$limit=50,$offset=0) {
		$this->render("admin_relate","ajax");
		if($this->RequestHandler->isAjax()){
			uses("sanitize");
			$clean = new Sanitize();
			$data = array();
			$this->{$this->modelClass}->contain();
			$associated = array_keys($this->{$this->modelClass}->getAssociated());
			$allowed = explode(",","Article,ArticleRelated,Authority,AuthorityRelated,Publication,PublicationRelated,Video,VideoRelated,Condition,ConditionRelated,Therapy,TherapyRelated,Tradition,TraditionRelated,Organization,OrganizationRelated");
			$diff = array_intersect($associated,$allowed);
			$associated =& $diff;
			switch($action) {
				case "search":
					if(!empty($search)) {
						$search = low(trim($clean->escape($search)));
						$assoc = ($this->modelClass=="Article")?"ArticleRelated":"Article";
						$articles = (in_array($assoc,$associated)&& ($filter=="all"||stristr($filter,"articles")))?$this->{$this->modelClass}->{$assoc}->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE($assoc.title) LIKE"=>"%".$search."%",
                        "LCASE($assoc.content) LIKE"=>"%".$search."%",
                        "LCASE($assoc.description) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "$assoc.title","$assoc.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $assoc = ($this->modelClass=="Condition")?"ConditionRelated":"Condition";
                        $conditions = (in_array($assoc,$associated)&& ($filter=="all"||stristr($filter,"conditions")))?$this->{$this->modelClass}->{$assoc}->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE($assoc.title) LIKE"=>"%".$search."%",
                        "LCASE($assoc.definition_short) LIKE"=>"%".$search."%",
                        "LCASE($assoc.definition_long) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "$assoc.title","$assoc.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $assoc = ($this->modelClass=="Tradition")?"TraditionRelated":"Tradition";
                        $traditions = (in_array($assoc,$associated)&& ($filter=="all"||stristr($filter,"traditions")))?$this->{$this->modelClass}->{$assoc}->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE($assoc.title) LIKE"=>"%".$search."%",
                        "LCASE($assoc.definition_short) LIKE"=>"%".$search."%",
                        "LCASE($assoc.definition_long) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "$assoc.title","$assoc.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $assoc = ($this->modelClass=="Audio")?"AudioRelated":"Audio";
                        $audios = (in_array($assoc,$associated)&&($filter=="all"||stristr($filter,"audios")))?$this->{$this->modelClass}->{$assoc}->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE($assoc.title) LIKE"=>"%".$search."%",
                        "LCASE($assoc.description_short) LIKE"=>"%".$search."%",
                        "LCASE($assoc.content) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "$assoc.title","$assoc.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        /**
                         * Primary Symptoms
                         */
                        $assoc = ($this->modelClass=="PrimarySymptom")?"PrimarySymptomRelated":"PrimarySymptom";
                        $prisymptoms = (in_array($assoc,$associated)&&(($filter=="all"||stristr($filter,"symptoms"))))?$this->{$this->modelClass}->{$assoc}->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE($assoc.title) LIKE"=>"%".$search."%",
                        "LCASE($assoc.definition) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "$assoc.title","$assoc.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        /**
                         * Secondary Symptoms
                         */
                        $assoc = ($this->modelClass=="SecondarySymptom")?"SecondarySymptomRelated":"SecondarySymptom";
                        $secsymptoms = (in_array($assoc,$associated)&&($filter=="all"||stristr($filter,"symptoms")))?$this->{$this->modelClass}->{$assoc}->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE($assoc.title) LIKE"=>"%".$search."%",
                        "LCASE($assoc.definition) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "$assoc.title","$assoc.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $assoc = ($this->modelClass=="Authority")?"AuthorityRelated":"Authority";
                        $authorities = (in_array($assoc,$associated)&&($filter=="all"||stristr($filter,"authorities")))?$this->{$this->modelClass}->{$assoc}->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE($assoc.name) LIKE"=>"%".$search."%",
                        "LCASE($assoc.title) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "$assoc.name","$assoc.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $discussions = (in_array("Discussion",$associated)&&($filter=="all"||stristr($filter,"discussions")))?$this->{$this->modelClass}->Discussion->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE(Discussion.title) LIKE"=>"%".$search."%",
                        "LCASE(Discussion.content) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "Discussion.title","Discussion.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $events = (in_array("Event",$associated)&&($filter=="all"||stristr($filter,"events")))?$this->{$this->modelClass}->Event->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE(Event.name) LIKE"=>"%".$search."%",
                        "LCASE(Event.description_short) LIKE"=>"%".$search."%",
                        "LCASE(Event.description_long) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "Event.name","Event.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $organizations = (in_array("Organization",$associated)&&($filter=="all"||stristr($filter,"organizations")))?$this->{$this->modelClass}->Organization->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE(Organization.name) LIKE"=>"%".$search."%",
                        "LCASE(Organization.website) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "Organization.name","Organization.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $publications = (in_array("Publication",$associated)&&($filter=="all"||stristr($filter,"publications")))?$this->{$this->modelClass}->Publication->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE(Publication.title) LIKE"=>"%".$search."%",
                        "LCASE(Publication.description_short) LIKE"=>"%".$search."%",
                        "LCASE(Publication.content) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "Publication.title","Publication.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $stories = (in_array("Story",$associated)&&($filter=="all"||stristr($filter,"stories")))?$this->{$this->modelClass}->Story->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE(Story.title) LIKE"=>"%".$search."%",
                        "LCASE(Story.content) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "Story.title","Story.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $therapies = (in_array("Therapy",$associated)&&($filter=="all"||stristr($filter,"therapies")))?$this->{$this->modelClass}->Therapy->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE(Therapy.title) LIKE"=>"%".$search."%",
                        "LCASE(Therapy.short_definition) LIKE"=>"%".$search."%",
                    	"LCASE(Therapy.long_definition) LIKE"=>"%".$search."%"
                    	)
                    	)
                    	,
                        "fields" => array(
                        "Therapy.title","Therapy.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $assoc = ($this->modelClass=="Video")?"VideoRelated":"Video";
                        $videos = (in_array($assoc,$associated)&&($filter=="all"||stristr($filter,"videos")))?$this->{$this->modelClass}->{$assoc}->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE($assoc.title) LIKE"=>"%".$search."%",
                        "LCASE($assoc.description_short) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "$assoc.title","$assoc.id","$assoc.content"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $assoc = ($this->modelClass=="Workshop")?"WorkshopRelated":"Workshop";
                        $workshops = (in_array($assoc,$associated)&&($filter=="all"||stristr($filter,"workshops")))?$this->{$this->modelClass}->{$assoc}->find("all",array(
                        "conditions" => array(
                        "or" => array(
                        "LCASE($assoc.title) LIKE"=>"%".$search."%"
                        )
                        )
                        ,
                        "fields" => array(
                        "$assoc.title","$assoc.id"
                        ),
                        "recursive" => 0,
                        "limit" => $limit,
                        "offset" => $offset
                        )):null;
                        $data = compact("traditions","conditions","articles","audios","authorities","discussions","events","organizations","publications","stories","prisymptoms","secsymptom","therapies","videos","workshops");
					}
					break;
			}
			Configure::write('debug', 0);
			$this->autoRender = false;
			$this->RequestHandler->respondAs("json");
			echo json_encode($data);
		}
	}

	/*
	 * Called before rendering, generates and attaches the page title
	 * must be called manually on each view method
	 */
	function generatePageTitle($content = null, $return = false){
		$pTitle = 'Liiv.com';
		if ($content){
			$pTitle = $content.' - Liiv.com';
		}
		if($return){
			return $pTitle;
		} else {
			$this->pageTitle = $pTitle;
			return true;
		}
	}
}
?>

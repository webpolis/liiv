<?php
/* SVN FILE: $Id$ */

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.app
 */
class AppModel extends Model {
	var $actsAs = array('Containable');

	function beforeSave() {
		$this->_habtmHack();

		/* Creating/updating
		 *
		 * "Created by and modified by records"
		 */
		if (empty($this->data) && !$this->hasField(array('createdby', 'updatedby', 'modifiedby'))) {
			return false;
		}

		/* Copied from core code */
		foreach (array('createdby', 'updatedby', 'modifiedby') as $field) {
			$keyPresentAndEmpty = (
				isset($this->data[$this->alias]) &&
				array_key_exists($field, $this->data[$this->alias]) &&
				$this->data[$this->alias][$field] === null
			);
			if ($keyPresentAndEmpty) {
				unset($this->data[$this->alias][$field]);
			}
		}

		$this->exists();
		$userFields = array('modifiedby', 'updatedby');

		if (!$this->__exists) {
			$userFields[] = 'createdby';
		}
		$userID = (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['User'])? $_SESSION['Auth']['User']['id']: 0);
		if (!($userID || $userID == 0)) {
			$this->redirect(array('controller' => 'users', 'action' => 'unauthorized', 'admin' => true));
		}
		foreach ($userFields as $field) {
			$this->data[$this->alias][$field] = $userID;
		}

		return true;
	}

	// Called after any model is saved
	function afterSave($created) {
		if ($created) {
			/*
			 * For tag cloud query to work correctly,
			 * when a new record is created, a first visit
			 * must be inserted in visits table (if there is
			 * none, it won't show up.
			 * Referer "/" is used by default
			 */
			App::import('Model', 'Entity');
			$e =& new Entity();
			$edata = $e->find('first', array('fields'=>array('id'), 'conditions'=>array('name'=>$this->name)));
			$entity_id = (isset($edata['Entity']['id'])? $edata['Entity']['id']: null);
			if (!empty($entity_id)) {
				$this->generateFirstVisit($entity_id, $this->id);
			}
		}
		return true;
	}

	/**
	 * The following method overrides the $data array since CakePHP doesn't handle very well
	 * a self-referenced HABTM.
	 */
	private function _habtmHack() {
		$data =& $this->data;
		if (isset($data[$this->name][$this->name])) {
			$related = $data[$this->name][$this->name];
			$data[$this->name.'Related'] = array($this->name.'Related' => $related);
			unset($data[$this->name][$this->name]);
		}
	}

	/**
	 * @param  string $string
	 * @param  string $field
	 * @return string
	 */
	function getUniqueUrl($string, $field) {
		// Build URL
		$url = str_replace('_', '-', Inflector::slug(strtolower($string), '_'));
		// Look for same URL, if so try until we find a unique one
		$column = $this->name.'.'.$field;
		$conditions = array($column => $url);
		$result = $this->findAll($conditions, $column, null);
		if ($result !== false && count($result) > 0) {
			$sameUrls = array();
			foreach ($result as $record) {
				$sameUrls[] = $record[$this->name][$field];
			}

			if (count($sameUrls) > 0) {
				$i = 1;
				while (in_array($url.'_'.$i, $sameUrls)) {
					++$i;
				}
				$url .= '_'.$i;
			}
		}

		return $url;
	}

	/**
	 * @param  string $string
	 * @return string
	 */
	function _getStringAsURL($string) {
		// Define the maximum number of characters allowed as part of the URL
		static $currentMaximumURLLength = 100;
		$string = strtolower($string);
		// Any non valid characters will be treated as _, also remove duplicate _
		$string = preg_replace('/[^a-z0-9\\-]|-{2,}/', '-', $string);
		// Remove beggining and ending signs
		$string = preg_replace('/^-+|-+$/', '', $string);
		// Cut to specified maximum length
		if (strlen($string) > $currentMaximumURLLength) {
			$string = substr($string, 0, $currentMaximumURLLength);
		}

		return $string;
	}

	/*
	 * Insert first visit to initialize a new object in tag cloud.
	 * Called in afterSave
	 */
	private function generateFirstVisit($entity_id, $item_id) {
		$Visit = ClassRegistry::init(
			array(
				"class" => "Visit",
				"table" => "visits",
				"alias" => "Visit"
			));
		$data = array(
				'entity_id' => $entity_id,
				'item_id' => $item_id,
				'counter' => 1,
				'referer' => NULL
		);
		//initalize counter for current item
		$Visit->create();
		$Visit->save($data);
		return true;
	}
}
?>
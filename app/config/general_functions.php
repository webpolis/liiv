<?php
/**
 * Converts a text into a UL
 * @param $txt Text
 * @param $max Maximum items per list portion
 * @param $obj Array. A reference to the original array that generated the $txt.
 * Line number corresponds to the index in the array.  
 * @param $controller Name of the model's controller name that is being listed.
 * If it's specified, the LI items will link to its corresponding "path"
 * @return unknown_type
 */
if(!function_exists('textToList')){
	function textToList($txt=null, $class=null, $max=8,&$obj=array(),$controller=null){
		$out = null;
		if(!is_array($obj)) $obj= array();
		$arr = preg_split("/\n+/s",$txt,0,PREG_SPLIT_NO_EMPTY);
		if(!empty($arr)){
			$arr = array_chunk($arr,$max);
			foreach($arr as $v){
				if($class){
					$out .= '<ul class="'.$class.'">';
				} else {
					$out .= '<ul>';
				}
				foreach($v as $k => $item){
					$link = $item;
					if(isset($obj[$k]) && !empty($controller)){
						$link = '<a href="/'.$controller.'/'.((isset($obj[$k]['path']))?$obj[$k]['path']:null).'">'.$item.'</a>';
					}
					$out .= '<li>'.$link.'</li>';
				}
				$out .= "</ul>";
			}
		}
		return (!empty($out))? $out : false;
	}
}

/**
 * @param array $arr
 * @param string $class
 * @return string
 * 
 * Convert a single level array into a list
 */
function arrayToUl(array $arr, $class = NULL){
	$class = !empty($class)? ' class="'.$class.'" ': '';
	$out= '<ul'.$class.'>';
	foreach($arr as $item){
		$out.='<li>'.$item.'</li>';
	}
	$out.= '</ul>';
	return $out;
}

/*
 * Divide text in two columns breaking at \n
 * Left column will fill till $leftColAmmount
 * Then, right column will start to fill until 
 * both have the same ammount.
 * After that, left column will contain half the items,
 * and then right column will be filled with more.
 * 
 */
function twoColsTextToList($txt= NULL, $class = NULL){
	$out = NULL;
	$arr = preg_split("/\n+/s",$txt,0,PREG_SPLIT_NO_EMPTY);
	$leftColAmmount = 6;
	if(!empty($arr)){
		$count = count($arr);
		$leftCol = array();
		$rightCol = array();
		$leftCount = ceil(($count) / 2);
		if($count < ($leftColAmmount * 2)) $leftCount = $leftColAmmount;
		for ($c = 0; $c < $count; $c++ ){
			if ($c < $leftCount) {
				$leftCol[] = $arr[$c];
			} else {
				$rightCol[] = $arr[$c];
			}
		}
		if(!count($rightCol)){
			$out = arrayToUl($leftCol, $class);
		} else {
			if($class) $class = $class.' ';
			$out = arrayToUl($leftCol, $class.'leftList');
			$out .= arrayToUl($rightCol, $class.'rightList');
		}
		return $out;
	}
	return false;
}

//function twoColsItemList(&$items, $itemName )
?>
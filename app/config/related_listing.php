<?php
/*
 * This file contains functions that generate the related article tables when an
 * item is viewed.
 *
 * This was done in order to avoid code duplication (or rather, code multiplication,
 * as the code is the same for any view).
 */

/* Duplication of the truncate function */

function related_articles_table($article_input, &$html, &$text){ 
if (!empty($article_input)):?>
<div class="related">
	<h3><?php __('Related Articles');?></h3>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Title'); ?></th>
		<th><?php __('Author'); ?></th>
		<th><?php __('Description'); ?></th>
		<th><?php __('Content'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($article_input as $article):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $article['title'];?></td>
			<td><?php echo $article['author'];?></td>
			<td><?php echo $text->truncate($article['description'], ADMIN_TEXT_TRIM, '...', true);?></td>
			<td><?php echo $text->truncate($article['content'], ADMIN_TEXT_TRIM, '...', true);?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'articles', 'action' => 'view', $article['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'articles', 'action' => 'edit', $article['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'articles', 'action' => 'delete', $article['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $article['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
</div>
<?php
endif;
}

function related_audios_table($audio_input, &$html, &$text){
if (!empty($audio_input)):
?>
<div class="related">
	<h3><?php __('Related Audios');?></h3>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Title'); ?></th>
		<th><?php __('Author'); ?></th>
		<th><?php __('Short Description'); ?></th>
		<th><?php __('Content'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($audio_input as $audio):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $audio['title'];?></td>
			<td><?php echo $audio['author'];?></td>
			<td><?php echo $text->truncate($audio['description_short'], ADMIN_TEXT_TRIM, '...', true);?></td>
			<td><?php echo $audio['content'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'audios', 'action' => 'view', $audio['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'audios', 'action' => 'edit', $audio['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'audios', 'action' => 'delete', $audio['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $audio['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
</div>
<?php
endif;
}

function related_authorities_table($authority_input, &$html, &$text){
if (!empty($authority_input)): ?>
<div class="related">
	<h3><?php __('Related Authorities');?></h3>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Name'); ?></th>
		<th><?php __('Title'); ?></th>
		<th><?php __('Bio'); ?></th>
		<th><?php __('Image'); ?></th>
		<th><?php __('Url'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($authority_input as $authority):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $authority['name'];?></td>
			<td><?php echo $authority['title'];?></td>
			<td><?php echo $text->truncate($authority['bio'], ADMIN_TEXT_TRIM, '...', true);?></td>
			<td><?php echo $authority['image'];?></td>
			<td><?php echo $authority['url'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'authorities', 'action' => 'view', $authority['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'authorities', 'action' => 'edit', $authority['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'authorities', 'action' => 'delete', $authority['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $authority['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
</div>
<?php
endif;
}

function related_conditions_table($condition_input, &$html, &$text ){
if (!empty($condition_input)): ?>
<div class="related">
	<h3><?php __('Related Conditions');?></h3>
	<?php ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Title'); ?></th>
		<th><?php __('Short Definition'); ?></th>
		<th><?php __('Long Definition'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($condition_input as $condition):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $condition['title'];?></td>
			<td><?php echo $text->truncate(strip_tags($condition['definition_short']), ADMIN_TEXT_TRIM, '...', true);?></td>
			<td><?php echo $text->truncate(strip_tags($condition['definition_long']), ADMIN_TEXT_TRIM, '...', true);?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'conditions', 'action' => 'view', $condition['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'conditions', 'action' => 'edit', $condition['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'conditions', 'action' => 'delete', $condition['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $condition['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>

</div>
<?php
endif;
}

function related_events_table($event_input, &$html, &$text){
if (!empty($event_input)):?>
<div class="related">
	<h3><?php __('Related Events');?></h3>
	<?php ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Name'); ?></th>
		<th><?php __('Short Description'); ?></th>
		<th><?php __('Long Description'); ?></th>
		<th><?php __('Event Date'); ?></th>
		<th><?php __('Link'); ?></th>
		<th><?php __('Event Blob'); ?></th>
		<th><?php __('Tags'); ?></th>
		<th><?php __('Location'); ?></th>
		<th><?php __('Teacher'); ?></th>
		<th><?php __('Subject'); ?></th>
		<th><?php __('Source'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($event_input as $event):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $event['name'];?></td>
			<td><?php echo $text->trim($event['description_short'], ADMIN_TEXT_TRIM, '...', true);?></td>
			<td><?php echo $text->trim($event['description_long'], ADMIN_TEXT_TRIM, '...', true);?></td>
			<td><?php echo $event['event_date'];?></td>
			<td><?php echo $event['link'];?></td>
			<td><?php echo $text->trim($event['event_blob'], ADMIN_TEXT_TRIM, '...', true);?></td>
			<td><?php echo $text->trim($event['tags'], ADMIN_TEXT_TRIM, '...', true);?></td>
			<td><?php echo $event['location'];?></td>
			<td><?php echo $event['teacher'];?></td>
			<td><?php echo $event['subject'];?></td>
			<td><?php echo $event['source'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'events', 'action' => 'view', $event['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'events', 'action' => 'edit', $event['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'events', 'action' => 'delete', $event['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $event['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>

</div>
<?php
endif;
}

function related_publications_table($publication_input, &$html, &$text){
if (!empty($publication_input)):?>
<div class="related">
	<h3><?php __('Related Publications');?></h3>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Title'); ?></th>
		<th><?php __('Author'); ?></th>
		<th><?php __('Short Description'); ?></th>
		<th><?php __('Content'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($publication_input as $publication):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $publication['title'];?></td>
			<td><?php echo $publication['author'];?></td>
			<td><?php echo $text->truncate($publication['description_short'], ADMIN_TEXT_TRIM, '...', true);?></td>
			<td><?php echo $text->truncate($publication['content'], ADMIN_TEXT_TRIM, '...', true);?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'publications', 'action' => 'view', $publication['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'publications', 'action' => 'edit', $publication['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'publications', 'action' => 'delete', $publication['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $publication['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>

</div>
<?php
endif;
}

function related_stories_table($stories_input, &$html, &$text){
if (!empty($stories_input)):?>
    <div class="related">
            <h3><?php __('Related Stories');?></h3>
            <table cellpadding = "0" cellspacing = "0">
            <tr>
                    <th><?php __('Title'); ?></th>
                    <th><?php __('Content'); ?></th>
                    <th><?php __('Video'); ?></th>
                    <th><?php __('Photo'); ?></th>
                    <th><?php __('User Id'); ?></th>
                    <th class="actions"><?php __('Actions');?></th>
            </tr>
            <?php
                    $i = 0;
                    foreach ($stories_input as $story):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
                    <tr<?php echo $class;?>>
                            <td><?php echo $story['title'];?></td>
                            <td><?php echo $text->trim(strip_tags($story['content']), ADMIN_TEXT_TRIM, '...', true);?></td>
                            <td><?php echo $story['media'];?></td>
                            <td><?php echo $story['photo'];?></td>
                            <td><?php echo $story['user_id'];?></td>
                            <td class="actions">
                                    <?php echo $html->link(__('View', true), array('controller' => 'stories', 'action' => 'view', $story['id'])); ?>
                                    <?php echo $html->link(__('Edit', true), array('controller' => 'stories', 'action' => 'edit', $story['id'])); ?>
                                    <?php echo $html->link(__('Delete', true), array('controller' => 'stories', 'action' => 'delete', $story['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $story['id'])); ?>
                            </td>
                    </tr>
            <?php endforeach; ?>
            </table>
    </div>
<?php
endif;
}

function related_therapies_table($therapies_input, &$html, &$text){
if (!empty($therapies_input)):?>
    <div class="related">
            <h3><?php __('Related Therapies');?></h3>
            <table cellpadding = "0" cellspacing = "0">
            <tr>
                    <th><?php __('Title'); ?></th>
                    <th><?php __('Short Definition'); ?></th>
                    <th><?php __('Long Definition'); ?></th>
                    <th class="actions"><?php __('Actions');?></th>
            </tr>
            <?php
                    $i = 0;
                    foreach ($therapies_input as $therapy):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
                    <tr<?php echo $class;?>>
                            <td><?php echo $therapy['title'];?></td>
                            <td><?php echo $text->truncate(strip_tags($therapy['short_definition']), ADMIN_TEXT_TRIM, '...', true);?></td>
                            <td><?php echo $text->truncate(strip_tags($therapy['long_definition']), ADMIN_TEXT_TRIM, '...', true);?></td>
                            <td class="actions">
                                    <?php echo $html->link(__('View', true), array('controller' => 'therapies', 'action' => 'view', $therapy['id'])); ?>
                                    <?php echo $html->link(__('Edit', true), array('controller' => 'therapies', 'action' => 'edit', $therapy['id'])); ?>
                                    <?php echo $html->link(__('Delete', true), array('controller' => 'therapies', 'action' => 'delete', $therapy['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $therapy['id'])); ?>
                            </td>
                    </tr>
            <?php endforeach; ?>
            </table>
    </div>
<?php
endif;
}

function related_videos_table ($videos_input, &$html, &$text) { 
if (!empty($videos_input)):?>
    <div class="related">
            <h3><?php __('Related Videos');?></h3>
            <table cellpadding = "0" cellspacing = "0">
            <tr>
                    <th><?php __('Title'); ?></th>
                    <th><?php __('Author'); ?></th>
                    <th><?php __('Short Description'); ?></th>
                    <th><?php __('Content'); ?></th>
                    <th class="actions"><?php __('Actions');?></th>
            </tr>
            <?php
                    $i = 0;
                    foreach ($videos_input as $video):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
                    <tr<?php echo $class;?>>
                            <td><?php echo $video['title'];?></td>
                            <td><?php echo $video['author'];?></td>
                            <td><?php echo $text->trim($video['description_short'], ADMIN_TEXT_TRIM, '...', true);?></td>
                            <td><?php echo $text->trim($video['content'], ADMIN_TEXT_TRIM, '...', true);?></td>
                            <td class="actions">
                                    <?php echo $html->link(__('View', true), array('controller' => 'videos', 'action' => 'view', $video['id'])); ?>
                                    <?php echo $html->link(__('Edit', true), array('controller' => 'videos', 'action' => 'edit', $video['id'])); ?>
                                    <?php echo $html->link(__('Delete', true), array('controller' => 'videos', 'action' => 'delete', $video['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $video['id'])); ?>
                            </td>
                    </tr>
            <?php endforeach; ?>
            </table>
    </div>
<?php
endif;
}

function related_discussions_table($discussions_input, &$html, &$text){
?>
    <div class="related">
            <h3><?php __('Related Discussions');?></h3>
            <?php if (!empty($discussions_input)):?>
            <table cellpadding = "0" cellspacing = "0">
            <tr>
                    <th><?php __('Id'); ?></th>
                    <th><?php __('Condition Id'); ?></th>
                    <th><?php __('Title'); ?></th>
                    <th><?php __('Content'); ?></th>
                    <th><?php __('Therapy Id'); ?></th>
                    <th><?php __('Updated'); ?></th>
                    <th><?php __('Created'); ?></th>
                    <th class="actions"><?php __('Actions');?></th>
            </tr>
            <?php
                    $i = 0;
                    foreach ($discussions_input as $discussion):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
                    <tr<?php echo $class;?>>
                            <td><?php echo $discussion['id'];?></td>
                            <td><?php echo $discussion['condition_id'];?></td>
                            <td><?php echo $discussion['title'];?></td>
                            <td><?php echo $text->trim($discussion['content'], ADMIN_TEXT_TRIM, '...', true);?></td>
                            <td><?php echo $discussion['therapy_id'];?></td>
                            <td><?php echo $discussion['updated'];?></td>
                            <td><?php echo $discussion['created'];?></td>
                            <td class="actions">
                                    <?php echo $html->link(__('View', true), array('controller' => 'discussions', 'action' => 'view', $discussion['id'])); ?>
                                    <?php echo $html->link(__('Edit', true), array('controller' => 'discussions', 'action' => 'edit', $discussion['id'])); ?>
                                    <?php echo $html->link(__('Delete', true), array('controller' => 'discussions', 'action' => 'delete', $discussion['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $discussion['id'])); ?>
                            </td>
                    </tr>
            <?php endforeach; ?>
            </table>
    <?php endif; ?>

            <div class="actions">
                    <ul>
                            <li><?php echo $html->link(__('New Discussion', true), array('controller' => 'discussions', 'action' => 'add'));?> </li>
                    </ul>
            </div>
    </div>
<?php
}

function related_organizations_table($organizations_input, &$html, &$text){
if (!empty($organizations_input)):?>
    <div class="related">
            <h3><?php __('Related Organizations');?></h3>
            <table cellpadding = "0" cellspacing = "0">
            <tr>
                    <th><?php __('Name'); ?></th>
                    <th><?php __('Website'); ?></th>
                    <th class="actions"><?php __('Actions');?></th>
            </tr>
            <?php
                    $i = 0;
                    foreach ($organizations_input as $organization):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
                    <tr<?php echo $class;?>>
                            <td><?php echo $organization['name'];?></td>
                            <td><?php echo $organization['website'];?></td>
                            <td class="actions">
                                    <?php echo $html->link(__('View', true), array('controller' => 'organizations', 'action' => 'view', $organization['id'])); ?>
                                    <?php echo $html->link(__('Edit', true), array('controller' => 'organizations', 'action' => 'edit', $organization['id'])); ?>
                                    <?php echo $html->link(__('Delete', true), array('controller' => 'organizations', 'action' => 'delete', $organization['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $organization['id'])); ?>
                            </td>
                    </tr>
            <?php endforeach; ?>
            </table>
    </div>
<?php
endif;
}

function related_products_table($product_input, &$html, &$text){
if (!empty($product_input)):?>
    <div class="related">
            <h3><?php __('Related Products');?></h3>
            <table cellpadding = "0" cellspacing = "0">
            <tr>
                    <th><?php __('Id'); ?></th>
                    <th class="actions"><?php __('Actions');?></th>
            </tr>
            <?php
                    $i = 0;
                    foreach ($product_input as $product):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
                    <tr<?php echo $class;?>>
                            <td><?php echo $product['id'];?></td>
                            <td class="actions">
                                    <?php echo $html->link(__('View', true), array('controller' => 'products', 'action' => 'view', $product['id'])); ?>
                                    <?php echo $html->link(__('Edit', true), array('controller' => 'products', 'action' => 'edit', $product['id'])); ?>
                                    <?php echo $html->link(__('Delete', true), array('controller' => 'products', 'action' => 'delete', $product['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $product['id'])); ?>
                            </td>
                    </tr>
            <?php endforeach; ?>
            </table>
    </div>
<?php
endif;
}

function related_symptoms_table($symptoms_input, &$html, &$text){
if (!empty($symptoms_input)):?>
    <div class="related">
            <h3><?php __('Related Symptoms');?></h3>
            <table cellpadding = "0" cellspacing = "0">
            <tr>
                    <th><?php __('Title'); ?></th>
                    <th><?php __('Definition'); ?></th>
                    <th class="actions"><?php __('Actions');?></th>
            </tr>
            <?php
                    $i = 0;
                    foreach ($symptoms_input as $symptom):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
                    <tr<?php echo $class;?>>
                            <td><?php echo $symptom['title'];?></td>
                            <td><?php echo $text->trim($symptom['definition'], ADMIN_TEXT_TRIM, '...', true);?></td>
                            <td class="actions">
                                    <?php echo $html->link(__('View', true), array('controller' => 'symptoms', 'action' => 'view', $symptom['id'])); ?>
                                    <?php echo $html->link(__('Edit', true), array('controller' => 'symptoms', 'action' => 'edit', $symptom['id'])); ?>
                                    <?php echo $html->link(__('Delete', true), array('controller' => 'symptoms', 'action' => 'delete', $symptom['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $symptom['id'])); ?>
                            </td>
                    </tr>
            <?php endforeach; ?>
            </table>

    </div>
<?php
endif;
}
?>
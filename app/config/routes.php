<?php
/* SVN FILE: $Id$ */
/**
 * Short description for file.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
Router::connect('/about', array('controller' => 'pages', 'action' => 'display', 'about'));
Router::connect('/terms-and-conditions', array('controller' => 'pages', 'action' => 'display', 'terms-and-conditions'));
Router::connect('/privacy-policy', array('controller' => 'pages', 'action' => 'display', 'privacy-policy'));
Router::connect('/contact', array('controller' => 'pages', 'action' => 'display', 'contact'));
Router::connect('/partnership-opportunities', array('controller' => 'pages', 'action' => 'display', 'partnership-opportunities'));

Router::connect('/admin', array("admin" => true,'controller' => 'pages', 'action' => 'welcome'));
Router::connect('/admin/login', array('controller' => 'users', 'action' => 'login', "admin" => true));
Router::connect('/admin/logout', array('controller' => 'users', 'action' => 'logout', 'admin' => true));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
Router::connect('/accesses/add', array("controller" => "access","action"=>"add"));

/*
 * Custom pages urls
 */
Router::connect('/health', array('controller' => 'pages', 'action' => 'display',  'health'));
Router::connect('/stress', array('controller' => 'pages', 'action' => 'display',  'stress'));
Router::connect('/unauthorized', array('controller' => 'users', 'action' => 'unauthorized',  'admin'=>true));
//Router::connect('/featured/story', array('controller' => 'stories', 'action' => 'featured'));
//Router::connect('/featured/video', array('controller' => 'videos', 'action' => 'featured'));

/*
 * Custom conditions urls
 */
Router::connect('/conditions', array('controller' => 'conditions', 'action' => 'index'));
Router::connect('/conditions/*', array('controller' => 'conditions', 'action' => 'view'));

/*
 * Custom therapy urls
 */
Router::connect('/therapies', array('controller' => 'therapies', 'action' => 'index'));
Router::connect('/therapies/*', array('controller' => 'therapies', 'action' => 'view'));

/*
 * Custom spiritual traditions urls
 */
Router::connect('/traditions', array('controller' => 'traditions', 'action' => 'index'));
Router::connect('/traditions/*', array('controller' => 'traditions', 'action' => 'view'));

/*
 * Custom Video urls
 */
Router::connect('/video', array('controller' => 'videos', 'action' => 'index'));
Router::connect('/video/*', array('controller' => 'videos', 'action' => 'view'));

/*
 * Custom Stories urls
 */
//Router::connect('/stories', array('controller' => 'videos', 'action' => 'index'));
Router::connect('/stories/featured', array('controller' => 'stories', 'action' => 'featured'));
Router::connect('/stories/*', array('controller' => 'stories', 'action' => 'view'));

/* Store urls */
Router::connect('/store', array('controller' => 'pages', 'action' => 'display', 'store'));
Router::connect('/store/*', array('controller' => 'pages', 'action' => 'display', 'store'));

/*
 * Articles urls
 */
Router::connect('/articles/*', array('controller' => 'articles', 'action' => 'view'));
?>
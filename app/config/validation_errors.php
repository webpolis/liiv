<?php
if(function_exists('__')) {
    define ('VALIDATION_NOT_EMPTY', __('This field cannot be left blank', true));
    define ('VALIDATION_URL', __('This field must be a valid url.', true));
    define ('VALIDATION_EXISTING_PATH', __('This path is already in use.', true));
    define ('VALIDATION_LENGTH', __('This field must be at least %s characters long.', true));
    define ('VALIDATION_NUMBER', __('You must enter a valid number', true));
}
?>
<?php
/**
 * @title ACL Management Controller by Nicolas Iglesias
 * @copyright 2008
 */

class AccessController extends AppController {
    var $layout = "admin";
    var $name = "Access";
    var $uses = array("User");
    var $pageTitle = "Access Control Manager";
    var $components = array("RequestHandler", "Session");

    function index() {
        $aro =& $this->Acl->Aro;
        $aco =& $this->Acl->Aco;
        $this->set("aros", $aro->children());
        $this->set("acos", $aco->children());
    }

    function add() {
        $aro =& $this->Acl->Aro;
        $aco =& $this->Acl->Aco;
        if(isset($this->data) && !empty($this->data['Access']['Name'])) {
        // call sanitization feature
            uses("sanitize");
            $clean = new Sanitize();
            switch($this->data['Access']['Type']) {
                case "aro":
                    $aroAlias = low($this->data['Access']['Name']);
                    $arr = $aro->find("first",array("conditions"=>array("alias"=>$aroAlias)));
                    if(!empty($arr)){
                    	$aro->id = $arr['Aro']['id'];
                    	$aro->delete();
                    }
                    $aro->create();
                    $t = ($this->data['Access']['Parent'] == 0) ? "Group" : "User";
                    $parentName = $aro->field("alias", array("id" => $this->data['Access']['Parent']));
                    $s = $aro->save(array('model' => $t, 'parent_id' => $this->data['Access']['Parent'],
                        'alias' => $aroAlias));
                    if($s) $this->Session->setFlash("Saved");
                    break;
                case "aco":
                    $parentName = ($this->data['Access']['Parent']==0)?$this->data['Access']['Name']:$aco->field("alias", array("id" => $this->data['Access']['Parent']));
                    $aco->create();
                    if($this->data['Access']['Parent'] != 0) {
                        $alias = $clean->escape($this->data['Access']['Name']);
                    }
                    else {
                        $alias = $clean->escape($parentName);
                    }
                    $s = $aco->save(array("model" => $parentName, 'parent_id' => $this->data['Access']['Parent'],
                        'alias' => $alias));
                    if($s) $this->Session->setFlash("Saved");
                    break;
            }
        }else{
            $this->Session->setFlash("Cannot save an empty registry.");
        }
        $this->redirect(array("action" => "index"));
    }

    function delete($id = null, $type = null) {
        $aro =& $this->Acl->Aro;
        $aco =& $this->Acl->Aco;
        if(!empty($id) && $type == "aro") {
            $aro->delete($id);
            $this->redirect(array("controller" => "access", "action" => "index"));
        }
        if(!empty($id) && $type == "aco") {
            $aco->delete($id);
            $this->redirect(array("controller" => "access", "action" => "index"));
        }
    }

    function setPermission($aroId = null, $acoId = null, $permission=null) {
        $aro =& $this->Acl->Aro;
        $aro->id = $aroId;
        $arodata = $aro->read(array("Aro.model","Aro.alias"));
        $aroAlias = $arodata['Aro']['alias'];
        $aco =& $this->Acl->Aco;
        $aco->id = $acoId;
        $acodata = $aco->read(array("Aco.model","Aco.alias"));
        $acoParent = $acodata['Aco']['model'];
        $action = ($acodata['Aco']['model'] === $acodata['Aco']['alias']) ? null : "/".$acodata['Aco']['alias'];
        // call sanitization feature
        uses("sanitize");
        $clean = new Sanitize();
        if(!empty($aroAlias) && !empty($permission)) {
            switch($permission) {
                case "Allow":
                    $this->Acl->allow($clean->escape($aroAlias),$clean->escape($acoParent.$action),"*");
                    break;
                case "Deny":
                    $this->Acl->deny($clean->escape($aroAlias),$clean->escape($acoParent.$action),"*");
                    break;
            }
            $this->Session->setFlash("The permissions have been changed.");
            $this->redirect(array("action" => "index"));
        }
    }
}
?>
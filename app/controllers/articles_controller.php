<?php
class ArticlesController extends AppController {
	var $name = 'Articles';
	var $helpers = array('Text', 'Wyswig');
	var $paginate = array('order'=>'Article.created DESC');

	function index() {
		$this->Article->recursive = 0;
		$this->set('articles', $this->paginate());
	}

	function view($id=null) {
		if (!$id || !($article = $this->Article->read(null, $id)) || !$article['Article']['published']) {
			$this->cakeError('error404');
//			$this->autoRender = false;
//			$this->render(null, null, '/errors/error404');
		}
		else {
			$this->set('article', $article);
		}
	}

	function add() {
		if (!empty($this->data)) {
			$this->Article->create();
			if ($this->Article->save($this->data)) {
				$this->Session->setFlash(__('The Article has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Article could not be saved. Please, try again.', true));
			}
		}
		$articles = $this->Article->Article->find('list');
		$audios = $this->Article->Audio->find('list');
		$authorities = $this->Article->Authority->find('list');
		$conditions = $this->Article->Condition->find('list');
		$events = $this->Article->Event->find('list');
		$organizations = $this->Article->Organization->find('list');
		$publications = $this->Article->Publication->find('list');
		$stories = $this->Article->Story->find('list');
		$therapies = $this->Article->Therapy->find('list');
		$videos = $this->Article->Video->find('list', array('fields'=>array('Video.id', 'Video.title', 'Video.content')));
		$organizations = $this->Article->Organization->find('list');
		$areas = $this->Article->Area->find('list');
		$this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'publications', 'stories', 'therapies', 'videos', 'organizations', 'areas'));
	}

	function edit($id=null) {
		if (empty($this->data)) {
			if (!$id) {
				$this->Session->setFlash(__('Invalid Article', true));
				$this->redirect(array('action'=>'index'));
			}
			else {
				$this->data = $this->Article->read(null, $id);
			}
		}
		elseif ($this->Article->save($this->data)) {
			$this->Session->setFlash(__('The Article has been saved', true));
			$this->redirect(array('action'=>'index'));
		}
		else {
			$this->Session->setFlash(__('The Article could not be saved. Please, try again.', true));
		}

		$articles = $this->Article->ArticleRelated->find('list');
		$audios = $this->Article->Audio->find('list');
		$authorities = $this->Article->Authority->find('list');
		$conditions = $this->Article->Condition->find('list');
		$events = $this->Article->Event->find('list');
		$organizations = $this->Article->Organization->find('list');
		$publications = $this->Article->Publication->find('list');
		$stories = $this->Article->Story->find('list');
		$therapies = $this->Article->Therapy->find('list');
		$videos = $this->Article->Video->find('list',array('fields'=>array('Video.id','Video.title','Video.content')));
		$organizations = $this->Article->Organization->find('list');
		$areas = $this->Article->Area->find('list');
		$this->set(compact('articles','audios','authorities','conditions','events','organizations','publications','stories','therapies','videos','organizations','areas'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Article', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Article->del($id)) {
			$this->Session->setFlash(__('Article deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

	function admin_index($search=null) {
		$filter = (isset($search)) ? array('or'=>array('Article.title LIKE' =>'%$search%','Article.description LIKE'=>'%$search','Article.content LIKE'=>'%$search%')) : array('1=1');
		$this->Article->recursive = 0;
		$this->set('articles', $this->paginate(null,$filter));
		$this->set('filter',$search);
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Article.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('article', $this->Article->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Article->create();
			if ($this->Article->save($this->data)) {
				$this->Session->setFlash(__('The Article has been saved', true));
				$this->redirect(array('action'=>'index'));
			}
			else {
				$this->Session->setFlash(__('The Article could not be saved. Please, try again.', true));
			}
		}

		$articles = $this->Article->ArticleRelated->find('list');
		$audios = $this->Article->Audio->find('list');
		$authorities = $this->Article->Authority->find('list');
		$conditions = $this->Article->Condition->find('list');
		$events = $this->Article->Event->find('list');
		$organizations = $this->Article->Organization->find('list');
		$publications = $this->Article->Publication->find('list');
		$stories = $this->Article->Story->find('list');
		$therapies = $this->Article->Therapy->find('list');
		$videos = $this->Article->Video->find('list',array('fields'=>array('Video.id','Video.title','Video.content')));
		$organizations = $this->Article->Organization->find('list');
		$areas = $this->Article->Area->find('list');
		$this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'publications', 'stories',  'therapies', 'videos', 'organizations', 'areas'));
	}

	function admin_edit($id = null) {
		if (empty($this->data)) {
			if (!$id) {
				$this->Session->setFlash(__('Invalid Article', true));
				$this->redirect(array('action'=>'index'));
			}
			else {
				$this->data = $this->Article->read(null, $id);
			}
		}
		else {
			if ($this->Article->save($this->data)) {
				$this->Session->setFlash(__('The Article has been saved', true));
				$this->redirect(array('action'=>'index'));
			}
			else {
				$this->Session->setFlash(__('The Article could not be saved. Please, try again.', true));
			}
		}

		$articles = $this->Article->ArticleRelated->find('list');
		$audios = $this->Article->Audio->find('list');
		$authorities = $this->Article->Authority->find('list');
		$conditions = $this->Article->Condition->find('list');
		$events = $this->Article->Event->find('list');
		$organizations = $this->Article->Organization->find('list');
		$publications = $this->Article->Publication->find('list');
		$stories = $this->Article->Story->find('list');
		$therapies = $this->Article->Therapy->find('list');
		$videos = $this->Article->Video->find('list',array('fields'=>array('Video.id','Video.title','Video.content')));
		$organizations = $this->Article->Organization->find('list');
		$areas = $this->Article->Area->find('list');
		$this->set(compact('articles','audios','authorities','conditions','events','organizations','publications','stories','therapies','videos','organizations','areas'));
	}

	function admin_delete($id=null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Article', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Article->del($id)) {
			$this->Session->setFlash(__('Article deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>
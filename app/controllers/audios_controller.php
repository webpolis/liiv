<?php
class AudiosController extends AppController {

    var $name = 'Audios';
    var $helpers = array('Text');
    var $paginate = array("order"=>"Audio.created DESC");

    function index() {
        $this->Audio->recursive = 0;
        $this->set('audios', $this->paginate());
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Audio.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('audio', $this->Audio->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            $this->Audio->create();
            if ($this->Audio->save($this->data)) {
                $this->Session->setFlash(__('The Audio has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Audio could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Audio->Article->find('list');
        $audios = $this->Audio->Audio->find('list');
        $authorities = $this->Audio->Authority->find('list');
        $conditions = $this->Audio->Condition->find('list');
        $events = $this->Audio->Event->find('list');
        $organizations = $this->Audio->Organization->find('list');
        $publications = $this->Audio->Publication->find('list');
        $stories = $this->Audio->Story->find('list');
        $therapies = $this->Audio->Therapy->find('list');
        $videos = $this->Audio->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $organizations = $this->Audio->Organization->find('list');
        $areas = $this->Audio->Area->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'publications', 'stories', 'therapies', 'videos', 'organizations', 'areas'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Audio', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Audio->save($this->data)) {
                $this->Session->setFlash(__('The Audio has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Audio could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Audio->read(null, $id);
        }
        $articles = $this->Audio->Article->find('list');
        $audios = $this->Audio->Audio->find('list');
        $authorities = $this->Audio->Authority->find('list');
        $conditions = $this->Audio->Condition->find('list');
        $events = $this->Audio->Event->find('list');
        $organizations = $this->Audio->Organization->find('list');
        $publications = $this->Audio->Publication->find('list');
        $stories = $this->Audio->Story->find('list');
        $symptoms = $this->Audio->Symptom->find('list');
        $therapies = $this->Audio->Therapy->find('list');
        $videos = $this->Audio->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $organizations = $this->Audio->Organization->find('list');
        $areas = $this->Audio->Area->find('list');
        $this->set(compact('articles','audios','authorities','conditions','events','organizations','publications','stories','symptoms','therapies','videos','organizations','areas'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Audio', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Audio->del($id)) {
            $this->Session->setFlash(__('Audio deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }


    function admin_index($search=null) {
        $filter = (isset($search)) ? array("or"=>array("Audio.title LIKE" =>"%$search%","Audio.description_short LIKE"=>"%$search","Article.content LIKE"=>"%$search%")) : array("1=1");
        $this->Audio->recursive = 0;
        $this->set('audios', $this->paginate(null,$filter));
        $this->set("filter",$search);
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Audio.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('audio', $this->Audio->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Audio->create();
            if ($this->Audio->save($this->data)) {
                $this->Session->setFlash(__('The Audio has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Audio could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Audio->Article->find('list');
        $audios = $this->Audio->AudioRelated->find('list');
        $authorities = $this->Audio->Authority->find('list');
        $conditions = $this->Audio->Condition->find('list');
        $events = $this->Audio->Event->find('list');
        $organizations = $this->Audio->Organization->find('list');
        $publications = $this->Audio->Publication->find('list');
        $stories = $this->Audio->Story->find('list');
        $therapies = $this->Audio->Therapy->find('list');
        $videos = $this->Audio->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $organizations = $this->Audio->Organization->find('list');
        $areas = $this->Audio->Area->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'publications', 'stories', 'therapies', 'videos', 'organizations', 'areas'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Audio', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Audio->save($this->data)) {
                $this->Session->setFlash(__('The Audio has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Audio could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Audio->read(null, $id);
        }
        $articles = $this->Audio->Article->find('list');
        $audios = $this->Audio->AudioRelated->find('list');
        $authorities = $this->Audio->Authority->find('list');
        $conditions = $this->Audio->Condition->find('list');
        $events = $this->Audio->Event->find('list');
        $organizations = $this->Audio->Organization->find('list');
        $publications = $this->Audio->Publication->find('list');
        $stories = $this->Audio->Story->find('list');
        $therapies = $this->Audio->Therapy->find('list');
        $videos = $this->Audio->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $organizations = $this->Audio->Organization->find('list');
        $areas = $this->Audio->Area->find('list');
        $this->set(compact('articles','audios','authorities','conditions','events','organizations','publications','stories','therapies','videos','organizations','areas'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Audio', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Audio->del($id)) {
            $this->Session->setFlash(__('Audio deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

}
?>
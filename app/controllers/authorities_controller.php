<?php
class AuthoritiesController extends AppController {

    var $name = 'Authorities';
    var $helpers = array('Text', 'Wyswig');
    var $paginate = array("order"=>"Authority.created DESC");

    function index() {
        $this->Authority->recursive = 0;
        $this->set('authorities', $this->paginate());
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Authority.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('authority', $this->Authority->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            $this->Authority->create();
            if ($this->Authority->save($this->data)) {
                $this->Session->setFlash(__('The Authority has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Authority could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Authority->Article->find('list');
        $audios = $this->Authority->Audio->find('list');
        $authorities = $this->Authority->Authority->find('list');
        $conditions = $this->Authority->Condition->find('list');
        $events = $this->Authority->Event->find('list');
        $organizations = $this->Authority->Organization->find('list');
        $stories = $this->Authority->Story->find('list');
        $symptoms = $this->Authority->Symptom->find('list');
        $therapies = $this->Authority->Therapy->find('list');
        $publications = $this->Authority->Publication->find('list');
        $videos = $this->Authority->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Authority->Area->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'stories', 'symptoms', 'therapies', 'publications', 'videos', 'areas'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Authority', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Authority->save($this->data)) {
                $this->Session->setFlash(__('The Authority has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Authority could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Authority->read(null, $id);
        }
        $articles = $this->Authority->Article->find('list');
        $audios = $this->Authority->Audio->find('list');
        $authorities = $this->Authority->Authority->find('list');
        $conditions = $this->Authority->Condition->find('list');
        $events = $this->Authority->Event->find('list');
        $organizations = $this->Authority->Organization->find('list');
        $stories = $this->Authority->Story->find('list');
        $symptoms = $this->Authority->Symptom->find('list');
        $therapies = $this->Authority->Therapy->find('list');
        $publications = $this->Authority->Publication->find('list');
        $videos = $this->Authority->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Authority->Area->find('list');
        $this->set(compact('articles','audios','authorities','conditions','events','organizations','stories','symptoms','therapies','publications','videos','areas'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Authority', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Authority->del($id)) {
            $this->Session->setFlash(__('Authority deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }


    function admin_index($search=null) {
        $filter = (isset($search)) ? array("or"=>array("Authority.title LIKE" =>"%$search%","Authority.name LIKE"=>"%$search","Authority.overview LIKE"=>"%$search%")) : array("1=1");
        $this->Authority->recursive = 0;
        $this->set('authorities', $this->paginate(null,$filter));
        $this->set("filter",$search);
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Authority.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('authority', $this->Authority->read(null, $id));
    }
    /**
     * The following method overrides the $data array since CakePHP doesn't handle very well
     * a HABTM to the same model.
     */
    private function _habtmHack() {
        $data =& $this->data;
        $related = $data[$this->modelClass][$this->modelClass];
        $data[$this->modelClass.'Related'] = array($this->modelClass."Related" => $related);
        unset($data[$this->modelClass][$this->modelClass]);
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->_habtmHack();
            $this->Authority->create();
            if ($this->Authority->save($this->data)) {
                $this->Session->setFlash(__('The Authority has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Authority could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Authority->Article->find('list');
        $audios = $this->Authority->Audio->find('list');
        $authorities = $this->Authority->AuthorityRelated->find('list');
        $conditions = $this->Authority->Condition->find('list');
        $events = $this->Authority->Event->find('list');
        $organizations = $this->Authority->Organization->find('list');
        $stories = $this->Authority->Story->find('list');
        $therapies = $this->Authority->Therapy->find('list');
        $publications = $this->Authority->Publication->find('list');
        $videos = $this->Authority->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Authority->Area->find('list');
        $types = $this->Authority->AuthorityType->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'stories', 'therapies', 'publications', 'videos', 'areas',"types"));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Authority', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Authority->save($this->data)) {
                $this->Session->setFlash(__('The Authority has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Authority could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Authority->read(null, $id);
        }
        $articles = $this->Authority->Article->find('list');
        $audios = $this->Authority->Audio->find('list');
        $authorities = $this->Authority->AuthorityRelated->find('list');
        $conditions = $this->Authority->Condition->find('list');
        $events = $this->Authority->Event->find('list');
        $organizations = $this->Authority->Organization->find('list');
        $stories = $this->Authority->Story->find('list');
        $therapies = $this->Authority->Therapy->find('list');
        $publications = $this->Authority->Publication->find('list');
        $videos = $this->Authority->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Authority->Area->find('list');
        $types = $this->Authority->AuthorityType->find('list');
        $this->set(compact('articles','audios','authorities','conditions','events','organizations','stories','therapies','publications','videos','areas',"types"));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Authority', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Authority->del($id)) {
            $this->Session->setFlash(__('Authority deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

}
?>
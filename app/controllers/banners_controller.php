<?php
class BannersController extends AppController {

	var $name = 'Banners';

	function index() {
		$this->Banner->recursive = 0;
		$this->set('banners', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Banner.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('banner', $this->Banner->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Banner->create();
			if ($this->Banner->save($this->data)) {
				$this->Session->setFlash(__('The Banner has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Banner could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Banner', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Banner->save($this->data)) {
				$this->Session->setFlash(__('The Banner has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Banner could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Banner->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Banner', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Banner->del($id)) {
			$this->Session->setFlash(__('Banner deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function admin_index($search=null) {
		$filter = (isset($search)) ? array("or"=>array("Banner.title LIKE" =>"%$search%","Banner.link LIKE" =>"%$search%")) : array("1=1");
		/*if($search != null){
			if(is_numeric($search)){
				$filter = $position_id = $search;
			} else {
				$filter = array("or"=>array("Banner.title LIKE" =>"%$search%","Banner.content LIKE"=>"%$search"));
				if($search == 'all') $filter = array("1=1");
			}
		} else {
			$filter = array("1=1"); 
		}*/
		// Fetch all banner positions
		App::import('Model','Position');
		$positionModel = new Position();
		//$positionSearch = $positionModel->findById($search);
		$positionModel->contain();
		$positionModel->recursive = 0;
		// Find & process for the form helper
		$positions = $this->generatePositionsArray($positionModel->find('all'));
		$banners = $this->paginate(null,$filter); 
		//debug($banners);
		$this->set(compact('banners','positions','filter'));
		//$this->set("filter",((isset($search)&&!is_numeric($search))?"all":$search));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Banner.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('banner', $this->Banner->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Banner->create();
			if ($this->Banner->save($this->data)) {
				$this->Session->setFlash(__('The Banner has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Banner could not be saved. Please, try again.', true));
			}
		}
		$positions = $this->Banner->Position->find('list');
		$this->set(compact('positions'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Banner', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Banner->save($this->data)) {
				$this->Session->setFlash(__('The Banner has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Banner could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Banner->read(null, $id);
		}
		$positions = $this->Banner->Position->find('list');
		$this->set(compact('positions'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Banner', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Banner->del($id)) {
			$this->Session->setFlash(__('Banner deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

	/**
	 * @param array $positions
	 * @return array:
	 * 
	 * Generates an array that can be used to build the 
	 * Filter on banners page
	 */
	function generatePositionsArray($positions){
		$pArray = array();
		$pArray['all'] = 'All';
		foreach ($positions as $position){
			$pArray[$position['Position']['id']] = $position['Position']['name'];
		}
		return $pArray;
	}
}
?>
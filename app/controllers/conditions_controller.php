<?php
class ConditionsController extends AppController {

	var $name = 'Conditions';
	var $helpers = array('Text', 'Wyswig', 'Banner');
	var $paginate = array("order"=>"Condition.created DESC");

	function index($params = null) {
		App::import('Model','Page');
		$pageModel = new Page();
		$conditionsHomepage = $pageModel->find('first', array('conditions' => array('Page.id' => '5')));
		$this->Condition->recursive = 0;
		$conditions = $this->paginate();
		/*App::import("Model","Poll");
        $p = new Poll();
        $polls = $p->find("first",array("condition"=>array("Poll.published"=>true),
	        "order"=>array("RAND()")
    	));
    	$this->set("polls",$polls);*/
		$this->set(compact('conditions', 'conditionsHomepage'));
	}

	function tag_cloud($param=null){
		$this->autoRender = false;
		return $this->generateTagCloud();
	}
	
	function view($path = null) {
		/* Modified to use custom URLs */
		if (!$path) {/* Can't actually happen */
			$this->redirect(array('action' => 'index'));
		}
		/*App::import("Model","Poll");
        $p = new Poll();
        $polls = $p->find("first",array("condition"=>array("Poll.published"=>true),
	        "order"=>array("RAND()")
    	));*/
		$condition = $this->Condition->findByPath($path);
		if(!$condition || !$condition['Condition']['published']){
			$this->cakeError('error404');
		}
		$this->Condition->id = (isset($condition['Condition']['id']))?$condition['Condition']['id']:0;
		$this->generatePageTitle($condition['Condition']['title']);
		//$this->set("polls",$polls);
		$this->set('condition', $condition);
	}

	function add() {
		if (!empty($this->data)) {
			$this->Condition->create();
			if ($this->Condition->save($this->data)) {
				$this->Session->setFlash(__('The Condition has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Condition could not be saved. Please, try again.', true));
			}
		}
		$articles = $this->Condition->Article->find('list');
		$audios = $this->Condition->Audio->find('list');
		$authorities = $this->Condition->Authority->find('list');
		$organizations = $this->Condition->Organization->find('list');
		$products = $this->Condition->Product->find('list');
		$therapies = $this->Condition->Therapy->find('list');
		$events = $this->Condition->Event->find('list');
		$publications = $this->Condition->Publication->find('list');
		$stories = $this->Condition->Story->find('list');
		$videos = $this->Condition->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$areas = $this->Condition->Area->find('list');
		$this->set(compact('articles', 'audios', 'authorities', 'organizations', 'products', 'therapies', 'events', 'publications', 'stories', 'videos', 'areas'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Condition', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Condition->save($this->data)) {
				$this->Session->setFlash(__('The Condition has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Condition could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Condition->read(null, $id);
		}
		$articles = $this->Condition->Article->find('list');
		$audios = $this->Condition->Audio->find('list');
		$authorities = $this->Condition->Authority->find('list');
		$organizations = $this->Condition->Organization->find('list');
		$products = $this->Condition->Product->find('list');
		$therapies = $this->Condition->Therapy->find('list');
		$events = $this->Condition->Event->find('list');
		$publications = $this->Condition->Publication->find('list');
		$stories = $this->Condition->Story->find('list');
		$videos = $this->Condition->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$areas = $this->Condition->Area->find('list');
		$this->set(compact('articles','audios','authorities','organizations','products','therapies','events','publications','stories','videos','areas'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Condition', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Condition->del($id)) {
			$this->Session->setFlash(__('Condition deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function admin_index($search=null) {
		$filter = (isset($search)) ? array("or"=>array("Condition.title LIKE" =>"%$search%","Condition.definition_short LIKE"=>"%$search","Condition.definition_long LIKE"=>"%$search%")) : array("1=1");
		$this->Condition->recursive = 0;
		$this->set('conditions', $this->paginate(null,$filter));
		$this->set("filter",$search);
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Condition.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('condition', $this->Condition->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Condition->create();
			if ($this->Condition->save($this->data)) {
				$this->_symptomsSave();
				$this->_therapiesSave();
				$this->Session->setFlash(__('The Condition has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Condition could not be saved. Please, try again.', true));
			}
		}
		$articles = $this->Condition->Article->find('list');
		$audios = $this->Condition->Audio->find('list');
		$authorities = $this->Condition->Authority->find('list');
		$organizations = $this->Condition->Organization->find('list');
		$products = $this->Condition->Product->find('list');
		$therapies = $this->Condition->Therapy->find('list');
		$events = $this->Condition->Event->find('list');
		$publications = $this->Condition->Publication->find('list');
		$stories = $this->Condition->Story->find('list');
		$videos = $this->Condition->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$areas = $this->Condition->Area->find('list');
		$this->set(compact('articles', 'audios', 'authorities', 'organizations', 'products',  'therapies', 'events', 'publications', 'stories', 'videos', 'areas'));
	}

	private function _symptomsSave() {
		if(isset($this->data['PrimarySymptom']) && is_array($this->data['PrimarySymptom']['PrimarySymptom'])) {
			$pri = $this->data['PrimarySymptom']['PrimarySymptom'];
			unset($this->data['PrimarySymptom']);
			if(count($pri)>0) {
				$sql = "DELETE FROM conditions_symptoms WHERE classification IN (1,2) AND condition_id = ".$this->Condition->id." AND symptom_id IN (".implode(",",$pri).")";
				$ret = $this->Condition->query($sql);
			}
			$sql = "INSERT INTO conditions_symptoms (id,condition_id,symptom_id,classification) VALUES ";
			foreach($pri as $id) {
				$sql .= "(NULL,".$this->Condition->id.",".$id.",1),";
			}
			$sql = preg_replace("/^(.*)\,$/s","$1",$sql);
			$this->Condition->query($sql);
		}
		if(isset($this->data['SecondarySymptom']) && is_array($this->data['SecondarySymptom']['SecondarySymptom'])) {
			$sec = $this->data['SecondarySymptom']['SecondarySymptom'];
			unset($this->data['SecondarySymptom']);
			if(count($sec)>0) {
				$sql = "DELETE FROM conditions_symptoms WHERE classification IN (1,2) AND condition_id = ".$this->Condition->id." AND symptom_id IN (".implode(",",$sec).")";
				$ret = $this->Condition->query($sql);
			}
			$sql = "INSERT INTO conditions_symptoms (condition_id,symptom_id,classification) VALUES ";
			foreach($sec as $id) {
				$sql .= "(".$this->Condition->id.",".$id.",2),";
			}
			$sql = preg_replace("/^(.*)\,$/s","$1",$sql);
			$this->Condition->query($sql);
		}
	}

	function _therapiesSave() {
		if(isset($this->data['Therapy']) && !empty($this->data['Therapy']['Therapy'])) {
			$therapies =& $this->data['Therapy']['Therapy'];
			$pro = (isset($this->data['Therapy']['Pro'])&&!empty($this->data['Therapy']['Pro'])) ?$this->data['Therapy']['Pro']:array();
			$con = (isset($this->data['Therapy']['Con'])&&!empty($this->data['Therapy']['Con'])) ? $this->data['Therapy']['Con']:array();
			foreach($therapies as $k => $tid) {
				$protext = (isset($pro[$k]))?$pro[$k]:null;
				$context = (isset($con[$k]))?$con[$k]:null;
				$this->Condition->ConditionsTherapy->updateAll(
				array("pro" => "'$protext'","con" => "'$context'"),
				array("ConditionsTherapy.condition_id"=>$this->Condition->id,"ConditionsTherapy.therapy_id"=>$tid)
				);
			}

		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Condition', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			$this->Condition->id = $this->data['Condition']['id'];
			if ($this->Condition->save($this->data)) {
				$this->_symptomsSave();
				$this->_therapiesSave();
				$this->Session->setFlash(__('The Condition has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Condition could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Condition->read(null, $id);
		}
		$articles = $this->Condition->Article->find('list');
		$audios = $this->Condition->Audio->find('list');
		$authorities = $this->Condition->Authority->find('list');
		$organizations = $this->Condition->Organization->find('list');
		$products = $this->Condition->Product->find('list');
		$traditions = $this->Condition->Tradition->find('list');
		$therapies = $this->Condition->Therapy->find('list',array("order"=>array("Therapy.id ASC")));
		$events = $this->Condition->Event->find('list');
		$publications = $this->Condition->Publication->find('list');
		$stories = $this->Condition->Story->find('list');
		$videos = $this->Condition->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$areas = $this->Condition->Area->find('list');
		$conditionsTherapies = $this->Condition->ConditionsTherapy->find("all",array("order"=>array("ConditionsTherapy.therapy_id ASC"),"conditions"=>array("ConditionsTherapy.condition_id"=>$id)));
		$this->set(compact('conditionsTherapies','traditions','articles','audios','authorities','organizations','products','therapies','events','publications','stories','videos','areas'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Condition', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Condition->del($id)) {
			$this->Session->setFlash(__('Condition deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
<?php
class EntitiesController extends AppController {

	var $name = 'Entities';

	function index() {
		$this->Entity->recursive = 0;
		$this->set('entities', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Entity.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('entity', $this->Entity->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Entity->create();
			if ($this->Entity->save($this->data)) {
				$this->Session->setFlash(__('The Entity has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Entity could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Entity', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Entity->save($this->data)) {
				$this->Session->setFlash(__('The Entity has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Entity could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Entity->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Entity', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Entity->del($id)) {
			$this->Session->setFlash(__('Entity deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function admin_index() {
		$this->Entity->recursive = 0;
		$this->set('entities', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Entity.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('entity', $this->Entity->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Entity->create();
			if ($this->Entity->save($this->data)) {
				$this->Session->setFlash(__('The Entity has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Entity could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Entity', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Entity->save($this->data)) {
				$this->Session->setFlash(__('The Entity has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Entity could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Entity->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Entity', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Entity->del($id)) {
			$this->Session->setFlash(__('Entity deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
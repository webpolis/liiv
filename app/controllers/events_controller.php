<?php
class EventsController extends AppController {

    var $name = 'Events';
    var $helpers = array('Text', 'wyswig');
    var $paginate = array("order"=>"Event.created DESC");

    function index() {
        $this->Event->recursive = 0;
        $this->set('events', $this->paginate());
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Event.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('event', $this->Event->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            $this->Event->create();
            if ($this->Event->save($this->data)) {
                $this->Session->setFlash(__('The Event has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Event could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Event->Article->find('list');
        $audios = $this->Event->Audio->find('list');
        $authorities = $this->Event->Authority->find('list');
        $conditions = $this->Event->Condition->find('list');
        $organizations = $this->Event->Organization->find('list');
        $stories = $this->Event->Story->find('list');
        $therapies = $this->Event->Therapy->find('list');
        $publications = $this->Event->Publication->find('list');
        $videos = $this->Event->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Event->Area->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'organizations', 'stories', 'symptoms', 'therapies', 'publications', 'videos', 'areas'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Event', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Event->save($this->data)) {
                $this->Session->setFlash(__('The Event has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Event could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Event->read(null, $id);
        }
        $articles = $this->Event->Article->find('list');
        $audios = $this->Event->Audio->find('list');
        $authorities = $this->Event->Authority->find('list');
        $conditions = $this->Event->Condition->find('list');
        $organizations = $this->Event->Organization->find('list');
        $stories = $this->Event->Story->find('list');
        $symptoms = $this->Event->Symptom->find('list');
        $therapies = $this->Event->Therapy->find('list');
        $publications = $this->Event->Publication->find('list');
        $videos = $this->Event->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Event->Area->find('list');
        $this->set(compact('articles','audios','authorities','conditions','organizations','stories','symptoms','therapies','publications','videos','areas'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Event', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Event->del($id)) {
            $this->Session->setFlash(__('Event deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }


    function admin_index($search=null) {
        $filter = (isset($search)) ? array("or"=>array("Event.name LIKE" =>"%$search%","Event.description_short LIKE"=>"%$search","Event.description_long LIKE"=>"%$search%")) : array("1=1");
        $this->Event->recursive = 0;
        $this->set('events', $this->paginate(null,$filter));
        $this->set("filter",$search);
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Event.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('event', $this->Event->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Event->create();
            if ($this->Event->save($this->data)) {
                $this->Session->setFlash(__('The Event has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Event could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Event->Article->find('list');
        $audios = $this->Event->Audio->find('list');
        $authorities = $this->Event->Authority->find('list');
        $conditions = $this->Event->Condition->find('list');
        $organizations = $this->Event->Organization->find('list');
        $stories = $this->Event->Story->find('list');
        $therapies = $this->Event->Therapy->find('list');
        $publications = $this->Event->Publication->find('list');
        $videos = $this->Event->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Event->Area->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'organizations', 'stories', 'therapies', 'publications', 'videos', 'areas'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Event', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Event->save($this->data)) {
                $this->Session->setFlash(__('The Event has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Event could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Event->read(null, $id);
        }
        $articles = $this->Event->Article->find('list');
        $audios = $this->Event->Audio->find('list');
        $authorities = $this->Event->Authority->find('list');
        $conditions = $this->Event->Condition->find('list');
        $organizations = $this->Event->Organization->find('list');
        $stories = $this->Event->Story->find('list');
        $therapies = $this->Event->Therapy->find('list');
        $publications = $this->Event->Publication->find('list');
        $videos = $this->Event->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Event->Area->find('list');
        $this->set(compact('articles','audios','authorities','conditions','organizations','stories','therapies','publications','videos','areas'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Event', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Event->del($id)) {
            $this->Session->setFlash(__('Event deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

}
?>
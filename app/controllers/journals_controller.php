<?php
class JournalsController extends AppController {

	var $name = 'Journals';

	function index() {
		$this->Journal->recursive = 0;
		$this->set('journals', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Journal.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('journal', $this->Journal->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Journal->create();
			if ($this->Journal->save($this->data)) {
				$this->Session->setFlash(__('The Journal has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Journal could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Journal', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Journal->save($this->data)) {
				$this->Session->setFlash(__('The Journal has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Journal could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Journal->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Journal', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Journal->del($id)) {
			$this->Session->setFlash(__('Journal deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function admin_index() {
		$this->Journal->recursive = 0;
		$this->set('journals', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Journal.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('journal', $this->Journal->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Journal->create();
			if ($this->Journal->save($this->data)) {
				$this->Session->setFlash(__('The Journal has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Journal could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Journal', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Journal->save($this->data)) {
				$this->Session->setFlash(__('The Journal has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Journal could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Journal->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Journal', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Journal->del($id)) {
			$this->Session->setFlash(__('Journal deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
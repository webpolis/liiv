<?php
/**
 * @property LiivToday $LiivToday
 * @property Story $Story
 */
class LiivTodaysController extends AppController {

    var $name = 'LiivTodays';
    var $helpers = array('Text','Wyswig');
    var $pageTitle = "Liiv Today";

    function index() {
        $this->LiivToday->recursive = 0;
        $this->set('liivTodays', $this->paginate());
    }

    function view($id = null,$action=null) {
        $this->layout = "ajax";
        if($this->RequestHandler->isAjax()) {
            $this->RequestHandler->respondAs("json");
            if (!$id) {
                $this->Session->setFlash(__('Invalid LiivToday.', true));
            }
            Configure::write("debug",0);
            if(!empty($id)&&!empty($action)&&$action=="neighbors") {
                $this->LiivToday->id = $id;
                $out = $this->LiivToday->find('neighbors',array(
                        "fields"=>array(
                                "(select count(*) from liiv_today lt where lt.`when` > `LiivToday`.`when` and lt.type_id = `LiivToday`.`type_id` and `lt`.`when` <= NOW()) as next_num",
                                "(select count(*) from liiv_today lt where lt.`when` < `LiivToday`.`when` and lt.type_id = `LiivToday`.`type_id`) as prev_num",
                                "LiivToday.id","LiivToday.type_id","LiivToday.image","LiivToday.when","LiivToday.content","LiivToday.link","LiivToday.topic"
                        ),
                        "field"=>"when",
                        "value"=>$this->LiivToday->field("when"),
                        "conditions"=>array(
                                "LiivToday.type_id"=>$this->LiivToday->field("type_id")
                        ),
                        "contain"=>array()
                ));
                $this->set('liivToday', $out);
            }else {
                $this->LiivToday->contain();
                $this->set('liivToday', $this->LiivToday->read(null, $id));
            }
        }
    }

    function add() {
        if (!empty($this->data)) {
            $this->LiivToday->create();
            if ($this->LiivToday->save($this->data)) {
                $this->Session->setFlash(__('The LiivToday has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The LiivToday could not be saved. Please, try again.', true));
            }
        }
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid LiivToday', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->LiivToday->save($this->data)) {
                $this->Session->setFlash(__('The LiivToday has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The LiivToday could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->LiivToday->read(null, $id);
        }
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for LiivToday', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->LiivToday->del($id)) {
            $this->Session->setFlash(__('LiivToday deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

    function admin_index($search=null) {
        if(isset($search)&&$search==4) {
            $this->LiivToday->bind("Story", array("type"=>"hasMany"));
            $this->LiivToday->contain();
            $this->LiivToday->Story->contain();
            $this->paginate = array("fields"=>array("Story.when","Story.id","Story.title","Story.content","Story.updated","Story.created","User.email"),"order"=>array("Story.when DESC"));
            $data = $this->paginate("LiivToday.Story",array("Story.when REGEXP"=>"^[1-9]{1}[0-9]{3}\-[0-9]{2}\-[0-9]{2}$"));
        }else {
            $this->LiivToday->recursive = 0;
            $isNum = (preg_match("/^\d+$/s",$search))?true:false;
            $filter = (isset($search) && $isNum)? array("LiivTodayType.id" =>$search):
                    (($search!=="all"&&!$isNum) ? array("or"=>array("LiivToday.topic LIKE" =>"%$search%","LiivToday.content LIKE"=>"%$search%")) : array("1=1"));
            $data = $this->paginate(null,$filter);
        }
        if(!isset($search)||$search=="all") {
            $this->LiivToday->bind("Story", array("type"=>"hasMany"));
            $this->LiivToday->contain();
            $this->LiivToday->Story->bind("LiivTodayType",
                    array(
                    "belongsTo"=>array(
                            "LiivTodayType"=>array(
                                    "className" => "LiivTodayType"
                            )
                    )
            ));
            $this->LiivToday->Story->contain();
            $ret = $this->paginate("LiivToday.Story",array("Story.when REGEXP"=>"^[1-9]{1}[0-9]{3}\-[0-9]{2}\-[0-9]{2}$"));
            $data = array_merge($data,$ret);
        }
        $this->Session->write("LiivType",$search);
        $this->set('liivTodays', $data);
        $this->set("filter",((isset($search)&&!is_numeric($search))?"all":$search));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid LiivToday.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('liivToday', $this->LiivToday->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->LiivToday->create();
            if ($this->LiivToday->save($this->data)) {
                $this->Session->setFlash(__('The Liiv Today has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Liiv Today could not be saved. Please, try again.', true));
            }
        }
        $type = ($this->Session->check("LiivType"))?$this->Session->read("LiivType"):1;
        $used = json_encode($this->LiivToday->find("all",array("fields"=>array("LiivToday.topic","LiivToday.id","LiivToday.when","LiivTodayType.title"),"conditions"=>array("LiivTodayType.id"=>$type,"LiivToday.when REGEXP"=>"^[1-9]{1}[0-9]{3}\-[0-9]{2}\-[0-9]{2}$"))));
        $users = $this->LiivToday->User->find('list');
        $types = $this->LiivToday->LiivTodayType->find('list');
        $this->set(compact("users","types","used"));
        $this->set("selectedType",$type);
    }

    function admin_edit($id = null) {
        if(isset($this->data['LiivToday'])) {
            $this->data['LiivToday']['when'] = date("Y-m-d",strtotime($this->data['LiivToday']['when']));
        }
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid LiivToday', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->LiivToday->save($this->data)) {
                $this->Session->setFlash(__('The LiivToday has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The LiivToday could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->LiivToday->contain(false,array("LiivTodayType.id","LiivTodayType.title","User.id"));
            $this->data = $this->LiivToday->read(null, $id);
        }
        $type = ($this->Session->check("LiivType"))?$this->Session->read("LiivType"):1;
        $used = json_encode($this->LiivToday->find("all",array("fields"=>array("LiivToday.topic","LiivToday.id","LiivToday.when","LiivTodayType.title"),"conditions"=>array("LiivTodayType.id"=>$type,"LiivToday.when REGEXP"=>"^[1-9]{1}[0-9]{3}\-[0-9]{2}\-[0-9]{2}$"))));
        $users = $this->LiivToday->User->find('list');
        $types = $this->LiivToday->LiivTodayType->find('list');
        $this->set(compact("users","types","used"));
        $this->set("selectedType",$type);
    }

    function admin_delete($id = null) {
    	$type = ($this->Session->check("LiivType"))?$this->Session->read("LiivType"):1;
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for LiivToday', true));
            $this->redirect(array('action'=>'index',$type));
        }
        if ($this->LiivToday->del($id)) {
            $this->Session->setFlash(__('LiivToday deleted', true));
            $this->redirect(array('action'=>'index',$type));
        }
    }
}
?>
<?php
class OrganizationsController extends AppController {

	var $name = 'Organizations';
	var $helpers = array('Text');
        var $paginate = array("order"=>"Organization.created DESC");

	function index() {
		$this->Organization->recursive = 0;
		$this->set('organizations', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Organization.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('organization', $this->Organization->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Organization->create();
			if ($this->Organization->save($this->data)) {
				$this->Session->setFlash(__('The Organization has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Organization could not be saved. Please, try again.', true));
			}
		}
		$articles = $this->Organization->Article->find('list');
		$audios = $this->Organization->Audio->find('list');
		$authorities = $this->Organization->Authority->find('list');
		$conditions = $this->Organization->Condition->find('list');
		$events = $this->Organization->Event->find('list');
		$publications = $this->Organization->Publication->find('list');
		$stories = $this->Organization->Story->find('list');
		$therapies = $this->Organization->Therapy->find('list');
		$videos = $this->Organization->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'publications', 'stories', 'therapies', 'videos'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Organization', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Organization->save($this->data)) {
				$this->Session->setFlash(__('The Organization has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Organization could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Organization->read(null, $id);
		}
		$articles = $this->Organization->Article->find('list');
		$audios = $this->Organization->Audio->find('list');
		$authorities = $this->Organization->Authority->find('list');
		$conditions = $this->Organization->Condition->find('list');
		$events = $this->Organization->Event->find('list');
		$publications = $this->Organization->Publication->find('list');
		$stories = $this->Organization->Story->find('list');
		$therapies = $this->Organization->Therapy->find('list');
		$videos = $this->Organization->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$this->set(compact('articles','audios','authorities','conditions','events','publications','stories','therapies','videos'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Organization', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Organization->del($id)) {
			$this->Session->setFlash(__('Organization deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function admin_index($search=null) {
        $filter = (isset($search)) ? array("or"=>array("Organization.name LIKE" =>"%$search%","Organization.website LIKE" =>"%$search%")) : array("1=1");
		$this->Organization->recursive = 0;
		$this->set('organizations', $this->paginate(null,$filter));
		$this->set("filter",$search);
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Organization.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('organization', $this->Organization->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Organization->create();
			if ($this->Organization->save($this->data)) {
				$this->Session->setFlash(__('The Organization has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Organization could not be saved. Please, try again.', true));
			}
		}
		$articles = $this->Organization->Article->find('list');
		$audios = $this->Organization->Audio->find('list');
		$authorities = $this->Organization->Authority->find('list');
		$conditions = $this->Organization->Condition->find('list');
		$events = $this->Organization->Event->find('list');
		$publications = $this->Organization->Publication->find('list');
		$stories = $this->Organization->Story->find('list');
		$therapies = $this->Organization->Therapy->find('list');
		$videos = $this->Organization->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'publications', 'stories', 'therapies', 'videos'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Organization', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Organization->save($this->data)) {
				$this->Session->setFlash(__('The Organization has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Organization could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Organization->read(null, $id);
		}
		$articles = $this->Organization->Article->find('list');
		$audios = $this->Organization->Audio->find('list');
		$authorities = $this->Organization->Authority->find('list');
		$conditions = $this->Organization->Condition->find('list');
		$events = $this->Organization->Event->find('list');
		$publications = $this->Organization->Publication->find('list');
		$stories = $this->Organization->Story->find('list');
		$therapies = $this->Organization->Therapy->find('list');
		$videos = $this->Organization->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$this->set(compact('articles','audios','authorities','conditions','events','publications','stories','therapies','videos'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Organization', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Organization->del($id)) {
			$this->Session->setFlash(__('Organization deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
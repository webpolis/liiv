<?php
class PagesController extends AppController {

	var $name = 'Pages';
	var $helpers = array('Text', 'Wyswig', 'Banner');
	var $dbPages = array('health', 'stress');
	var $dbPagesOneColumn = array('terms-and-conditions','privacy-policy','partnership-opportunities','about','contact');
	
	function tag_cloud($param=null){
		$this->autoRender = false;
		return $this->generateTagCloud();
	}
	
	function display() {
		$path = func_get_args();
		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			if(strtolower($path[0]) == 'store') {
				$title = 'Store';
			} else if(strtolower($path[$count - 1]) == 'home'){
					$title = 'Liiv';
			} else {
				$title = $this->generatePageTitle(Inflector::humanize($path[$count - 1]), true);
			}
		}
		if(in_array($page, $this->dbPages) || in_array($page, $this->dbPagesOneColumn)) {
			$id = $this->_getDbPageId($page);
			if($id <= 0){
				$this->cakeError('error404');
			}
			App::import("Model","Poll");
			$p = new Poll();
			$polls = $p->find("first",array("condition"=>array("Poll.published"=>true),
	        "order"=>array("RAND()")
			));
			$data = $this->_getData($id);
			$data['elements'] = $this->_buildPageElements($page);
			$this->set("polls",$polls);
			$this->generatePageTitle($data['title']);
			$this->set('titleq', $data['title']);
			$this->set('content', $data['content']);
			if(in_array($page, $this->dbPagesOneColumn)) {
				$this->render('onecol');
			} else {
				$this->render('view');
			}
			return;
		}
		$elements = $this->_buildPageElements($page);
		$this->set(compact('page', 'subpage', 'title','elements'));
		if($path[0] == 'store'){
			$this->render('store');
		} else {
			$this->render(join('/', $path));
		}
	}

	function _getData($id){
		$this->Page->id = $id;
		$data = $this->Page->read();
		if(empty ($data)){
			return false;
		}
		return array('content' => $data['Page']['content'], 'title' => $data['Page']['title']);
	}

	function _getDbPageId($page) {
		$id = 0;
		switch($page) {
			case 'about': $id = 1;break;
			case 'health': $id = 2;break;
			case 'stress': $id = 3; break;
			case 'terms-and-conditions'; $id = 7;break;
			case 'privacy-policy'; $id = 8;break;
			case 'partnership-opportunities'; $id = 9;break;
			case 'contact'; $id = 10;break;
		}
		return $id;
	}

	function _buildPageElements($page=null) {
		$out = null;
		$this->set("story",$this->requestAction("/stories/featured",array("return"=>true)));
		$this->set("video",$this->requestAction("/videos/featured",array("return"=>true)));
		if(!empty($page)) {
			switch($page) {
				case "home":
					App::import("Model","LiivToday");
					App::import("Model","LiivToday");
					App::import("Model","Slide");
					$now = date("Y-m-d H:i:s");
					$lt =& new LiivToday();
					$lt->contain();
					/**
					 * fetch those items that were intended to be shown today or earlier
					 */
					$liivTodays = $lt->find("all",array(
                        "fields"=>array(
                        "(select count(*) from liiv_today lt where lt.`when` > `LiivToday`.`when` and lt.type_id = `LiivToday`.`type_id` and `lt`.`when` <= NOW()) as next_num",
                        "(select count(*) from liiv_today lt where lt.`when` < `LiivToday`.`when` and lt.type_id = `LiivToday`.`type_id`) as prev_num",
                        "LiivToday.id","LiivToday.type_id","LiivToday.image","LiivToday.when","LiivToday.content","LiivToday.link","LiivToday.topic"
                        ),
                        "conditions"=>array(
                        "AND"=>array(
                        //"LiivToday.when = (select max(ll.`when`) from liiv_today ll where ll.`type_id` = `LiivToday`.`type_id`)",
                        "LiivToday.when < NOW()"
                        )
                        ),
                        "order"=>array("LiivToday.type_id ASC","LiivToday.when DESC")
                        ));
                    		$sl =& new Slide();
                    		$sl->contain();
                    		$slides = $sl->find("all",array("fields"=> array("id","title","content","link","link_title","description"),
                    "limit" => 7,"order" => array("updated DESC")));
                    		$out = array_merge_recursive($liivTodays, $slides);
                    		break;
			}
		}
		return $out;
	}

	function index() {
		$this->Page->recursive = 0;
		$this->set('pages', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Page.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('page', $this->Page->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Page->create();
			if ($this->Page->save($this->data)) {
				$this->Session->setFlash(__('The Page has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Page could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Page', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Page->save($this->data)) {
				$this->Session->setFlash(__('The Page has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Page could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Page->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Page', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Page->del($id)) {
			$this->Session->setFlash(__('Page deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function admin_index($search=null) {
		$filter = (isset($search)) ? array("or"=>array("Page.title LIKE" =>"%$search%","Page.content LIKE" =>"%$search%")) : array("1=1");
		$this->Page->recursive = 0;
		$this->set('pages', $this->paginate(null,$filter));
		$this->set("filter",$search);
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Page.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('page', $this->Page->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Page->create();
			if ($this->Page->save($this->data)) {
				$this->Session->setFlash(__('The Page has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Page could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Page', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Page->save($this->data)) {
				$this->Session->setFlash(__('The Page has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Page could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Page->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Page', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Page->del($id)) {
			$this->Session->setFlash(__('Page deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

	function admin_welcome() {

	}

}
?>
<?php
class PolloptionsController extends AppController {

	var $name = 'Polloptions';
	var $helpers = array('Text');

	function index() {
		$this->Polloption->recursive = 0;
		$this->set('polloptions', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Polloption.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('polloption', $this->Polloption->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Polloption->create();
			if ($this->Polloption->save($this->data)) {
				$this->Session->setFlash(__('The Polloption has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Polloption could not be saved. Please, try again.', true));
			}
		}
		$polls = $this->Polloption->Poll->find('list');
		$this->set(compact('polls'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Polloption', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Polloption->save($this->data)) {
				$this->Session->setFlash(__('The Polloption has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Polloption could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Polloption->read(null, $id);
		}
		$polls = $this->Polloption->Poll->find('list');
		$this->set(compact('polls'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Polloption', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Polloption->del($id)) {
			$this->Session->setFlash(__('Polloption deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	function admin_index() {
		$this->Polloption->recursive = 0;
		$this->set('polloptions', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Polloption.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('polloption', $this->Polloption->read(null, $id));
	}

	function admin_add() {
		if(! $this->RequestHandler->isAjax()){
			if (!empty($this->data)) {
				$this->Polloption->create();
				if ($this->Polloption->save($this->data)) {
					$this->Session->setFlash(__('The Polloption has been saved', true));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash(__('The Polloption could not be saved. Please, try again.', true));
				}
			}
			$polls = $this->Polloption->Poll->find('list');
			$this->set(compact('polls'));
		} else {
			if (!empty ($this->data)) {
				if(!$this->data['Polloption']['poll_id']) $this->data['Polloption']['poll_id'];
				if ($this->Polloption->save($this->data)) {
					echo json_encode(array('code' => 'ok', 'message' => '', 'id' => $this->Polloption->id));
				} else {
					echo json_encode(array('code' => 'error','message' => 'Error adding Poll Option'));
				}
			}
		    Configure::write('debug', 0);
		    $this->autoRender = false;
		    exit();
		}
	}

	function admin_edit($id = null) {
		if(!$this->RequestHandler->isAjax()){
			if (!$id && empty($this->data)) {
				$this->Session->setFlash(__('Invalid Polloption', true));
				$this->redirect(array('action'=>'index'));
			}
			if (!empty($this->data)) {
				if ($this->Polloption->save($this->data)) {
					$this->Session->setFlash(__('The Polloption has been saved', true));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash(__('The Polloption could not be saved. Please, try again.', true));
				}
			}
			if (empty($this->data)) {
				$this->data = $this->Polloption->read(null, $id);
			}
			$polls = $this->Polloption->Poll->find('list');
			$this->set(compact('polls'));
		} else {
			if (!$id && empty($this->data)) {
				$errors = __('Invalid Polloption', true);
				echo json_encode(array('code' => 'error', 'message' => $errors));
				Configure::write('debug', 0);
				$this->autoRender = false;
				exit();
			} else {
				if (!empty($this->data)) {
					if ($this->Polloption->save($this->data)) {
						echo json_encode(array('code' => 'ok', 'message' => __('The poll has been saved', true)));
						
					} else {
						echo json_encode(array('code' => 'error', 'message' => __('The Polloption could not be saved. Please, try again.', true)));
					}
					Configure::write('debug', 0);
					$this->autoRender = false;
					exit();
				}
				if (empty($this->data)) {
					$this->data = $this->Polloption->read(null, $id);
				}
				$polls = $this->Polloption->Poll->find('list');
				$this->set(compact('polls'));
				$this->render('admin_ajaxedit', 'ajax');
				$this->autoRender = false;
			}
		}
	}

	function admin_delete($id = null) {
		if($this->RequestHandler->isAjax()){
			if($this->data['Polloption']['poll_id']) {
				if($this->Polloption->del($this->data['Polloption']['poll_id'])){
					echo json_encode(array('code' => 'ok'));
				}
			}
			Configure::write('debug', 0);
			$this->autoRender = false;
			exit();
		} else {
			if (!$id) {
				$this->Session->setFlash(__('Invalid id for Polloption', true));
				$this->redirect(array('action'=>'index'));
			}
			if ($this->Polloption->del($id)) {
				$this->Session->setFlash(__('Polloption deleted', true));
				$this->redirect(array('action'=>'index'));
			}
		}
	}
	
	function admin_saveorder($order = null) {
		if($this->RequestHandler->isAjax()){
			if($this->data['order']){
				if(strlen($this->data['order']) > 5){
					$data = substr($this->data['order'], 5);
					$polloptions = explode("&id[]=", $data);
					$sql = '';
					$order = 0;
					foreach ($polloptions as $item){
						$sql ="UPDATE polloptions a SET a.order='".$order."' WHERE a.id='".$item."'";
						$this->Polloption->Query($sql);
						$order++;
					}
				}
			}
			Configure::write('debug', 0);
		    $this->autoRender = false;
		    exit();
		}	
	}
}
?>
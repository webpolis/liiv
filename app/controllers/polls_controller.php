<?php
class PollsController extends AppController {
    var $name = 'Polls';
    var $helpers = array('Text');
    var $paginate = array("order"=>"Poll.created DESC");

    function index() {
        $this->Poll->recursive = 0;
        $this->set('polls', $this->paginate());
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Poll.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('poll', $this->Poll->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            $this->Poll->create();
            if ($this->Poll->save($this->data)) {
                $this->Session->setFlash(__('The Poll has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Poll could not be saved. Please, try again.', true));
            }
        }
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Poll', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Poll->save($this->data)) {
                $this->Session->setFlash(__('The Poll has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Poll could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Poll->read(null, $id);
        }
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Poll', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Poll->del($id)) {
            $this->Session->setFlash(__('Poll deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }
    function admin_index($search=null) {
        $filter = (isset($search)) ? array("or"=>array("Poll.question LIKE" =>"%$search%")) : array("1=1");
        $this->Poll->recursive = 0;
        $this->set('polls', $this->paginate(null,$filter));
        $this->set("filter",$search);
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Poll.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('poll', $this->Poll->read(null, $id));
    }

    function admin_add() {
        if(!$this->RequestHandler->isAjax()) {
            if (!empty($this->data)) {
                $this->Poll->create();
                if ($this->Poll->save($this->data)) {
                    $this->Session->setFlash(__('The Poll has been saved', true));
                    $this->redirect(array('action'=>'index'));
                } else {
                    $this->Session->setFlash(__('The Poll could not be saved. Please, try again.', true));
                }
            }
        } else {
            if (!empty($this->data)) {
                $this->Poll->create();
                if ($this->Poll->save($this->data)) {
                    $pollid = $this->Poll->id;
                    if(!empty($this->data['Poll']['Polloption'])) {
                        foreach ($this->data['Poll']['Polloption'] as $poption) {
                            $tempOption = $this->Poll->Polloption->find(array('Polloption.id' => $poption));
                            $tempOption['Polloption']['poll_id'] = $pollid;
                            $poptionsarray[] = $tempOption['Polloption'];
                        }
                        $this->Poll->Polloption->deleteAll(array('Polloption.poll_id' => null));
                        if (isset ($poptionsarray)) {
                            if(!$this->Poll->Polloption->saveAll($poptionsarray)) {
                                $this->Poll->delete($pollid);
                                echo json_encode (array('code'=> 'error','message' => 'Error saving Poll options. Please try again.'));
                                Configure::write('debug', 0);
                                $this->autoRender = false;
                                exit();
                            }
                        }
                    }
                    echo json_encode(array('code' => 'ok'));
                    $this->Session->setFlash(__('The Poll has been saved.', true));
                } else {
                    echo json_encode (array('code'=> 'error','message' => 'The Poll could not be saved. Please, try again.'));
                }
            } else {
                echo json_encode (array('code'=> 'error','message' => 'Invalid Data'));
            }
            Configure::write('debug', 0);
            $this->autoRender = false;
            exit();
        }
    }

    function admin_edit($id = null) {
        if(!$this->RequestHandler->isAjax()) {
            if (!$id && empty($this->data)) {
                $this->Session->setFlash(__('Invalid Poll', true));
                $this->redirect(array('action'=>'index'));
            }
            if (!empty($this->data)) {
                if ($this->Poll->save($this->data)) {
                    $this->Session->setFlash(__('The Poll has been saved', true));
                    $this->redirect(array('action'=>'index'));
                } else {
                    $this->Session->setFlash(__('The Poll could not be saved. Please, try again.', true));
                }
            }
            if (empty($this->data)) {
                $this->data = $this->Poll->read(null, $id);
            }
            $polloptions = $this->Poll->Polloption->find('list', array('conditions' => array('Polloption.poll_id' => $this->data['Poll']['id']), 'order' => 'Polloption.order'));
            $this->set(compact('polloptions'));
        } else {
            if (!$id && empty($this->data)) {
                $response = json_encode(array('code' => 'error', 'message' => 'Invalid Poll'));
            } else {
                if (!empty($this->data)) {
                    if ($this->Poll->save($this->data)) {
                        $response = json_encode(array('code' => 'ok'));
                    } else {
                        $response = json_encode(array('code' => 'error', 'message' => 'The Poll could not be saved. Please, try again.'));
                    }
                }
            }
            echo $response;
            Configure::write('debug', 0);
            $this->autoRender = false;
            exit();
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Poll', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Poll->del($id)) {
            $this->Session->setFlash(__('Poll deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }
}
?>
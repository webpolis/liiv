<?php
class PublicationsController extends AppController {

    var $name = 'Publications';
    var $helpers = array('Text', 'wyswig');
    var $paginate = array("order"=>"Publication.created DESC");

    function index() {
        $this->Publication->recursive = 0;
        $this->set('publications', $this->paginate());
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Publication.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('publication', $this->Publication->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            $this->Publication->create();
            if ($this->Publication->save($this->data)) {
                $this->Session->setFlash(__('The Publication has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Publication could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Publication->Article->find('list');
        $audios = $this->Publication->Audio->find('list');
        $authorities = $this->Publication->Authority->find('list');
        $organizations = $this->Publication->Organization->find('list');
        $conditions = $this->Publication->Condition->find('list');
        $events = $this->Publication->Event->find('list');
        $organizations = $this->Publication->Organization->find('list');
        $publications = $this->Publication->Publication->find('list');
        $stories = $this->Publication->Story->find('list');
        $therapies = $this->Publication->Therapy->find('list');
        $videos = $this->Publication->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Publication->Area->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'publications', 'stories', 'therapies', 'videos', 'organizations', 'areas'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Publication', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Publication->save($this->data)) {
                $this->Session->setFlash(__('The Publication has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Publication could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Publication->read(null, $id);
        }
        $articles = $this->Publication->Article->find('list');
        $audios = $this->Publication->Audio->find('list');
        $authorities = $this->Publication->Authority->find('list');
        $conditions = $this->Publication->Condition->find('list');
        $events = $this->Publication->Event->find('list');
        $organizations = $this->Publication->Organization->find('list');
        $publications = $this->Publication->Publication->find('list');
        $stories = $this->Publication->Story->find('list');
        $therapies = $this->Publication->Therapy->find('list');
        $videos = $this->Publication->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $organizations = $this->Publication->Organization->find('list');
        $areas = $this->Publication->Area->find('list');
        $this->set(compact('articles','audios','authorities','conditions','events','organizations','publications','stories','therapies','videos','organizations','areas'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Publication', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Publication->del($id)) {
            $this->Session->setFlash(__('Publication deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }


    function admin_index($search=null) {
        $filter = (isset($search)) ? array("or"=>array("Publication.title LIKE" =>"%$search%","Publication.description_short LIKE" =>"%$search%","Publication.content LIKE"=>"%$search%")) : array("1=1");
        $this->Publication->recursive = 0;
        $this->set('publications', $this->paginate(null,$filter));
        $this->set("filter",$search);
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Publication.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('publication', $this->Publication->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Publication->create();
            if ($this->Publication->save($this->data)) {
                $this->Session->setFlash(__('The Publication has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Publication could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Publication->Article->find('list');
        $audios = $this->Publication->Audio->find('list');
        $authorities = $this->Publication->Authority->find('list');
        $organizations = $this->Publication->Organization->find('list');
        $conditions = $this->Publication->Condition->find('list');
        $events = $this->Publication->Event->find('list');
        $publications = $this->Publication->PublicationRelated->find('list');
        $stories = $this->Publication->Story->find('list');
        $therapies = $this->Publication->Therapy->find('list');
        $videos = $this->Publication->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Publication->Area->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'publications', 'stories', 'therapies', 'videos', 'organizations', 'areas'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Publication', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Publication->save($this->data)) {
                $this->Session->setFlash(__('The Publication has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Publication could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Publication->read(null, $id);
        }
        $articles = $this->Publication->Article->find('list');
        $audios = $this->Publication->Audio->find('list');
        $authorities = $this->Publication->Authority->find('list');
        $conditions = $this->Publication->Condition->find('list');
        $events = $this->Publication->Event->find('list');
        $organizations = $this->Publication->Organization->find('list');
        $publications = $this->Publication->PublicationRelated->find('list');
        $stories = $this->Publication->Story->find('list');
        $therapies = $this->Publication->Therapy->find('list');
        $videos = $this->Publication->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $organizations = $this->Publication->Organization->find('list');
        $areas = $this->Publication->Area->find('list');
        $this->set(compact('articles','audios','authorities','conditions','events','organizations','publications','stories','therapies','videos','organizations','areas'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Publication', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Publication->del($id)) {
            $this->Session->setFlash(__('Publication deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

}
?>
<?php
class SlidesController extends AppController {

	var $name = 'Slides';
	var $helpers = array('Wyswig');

	function index() {
		$this->Slide->recursive = 0;
		$this->set('slides', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Slide.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('slide', $this->Slide->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Slide->create();
			if ($this->Slide->save($this->data)) {
				$this->Session->setFlash(__('The Slide has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Slide could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Slide', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Slide->save($this->data)) {
				$this->Session->setFlash(__('The Slide has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Slide could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Slide->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Slide', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Slide->del($id)) {
			$this->Session->setFlash(__('Slide deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function admin_index($search=null) {
		$filter = (isset($search)) ? array("or"=>array("Slide.title LIKE" =>"%$search%","Slide.description LIKE" =>"%$search%")) : array("1=1");
		$this->Slide->recursive = 0;
		$this->set('slides', $this->paginate(null,$filter));
		$this->set("filter",$search);
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Slide.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('slide', $this->Slide->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Slide->create();
			if ($this->Slide->save($this->data)) {
				$this->Session->setFlash(__('The Slide has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Slide could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Slide', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Slide->save($this->data)) {
				$this->Session->setFlash(__('The Slide has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Slide could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Slide->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Slide', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Slide->del($id)) {
			$this->Session->setFlash(__('Slide deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
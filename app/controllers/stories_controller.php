<?php
class StoriesController extends AppController {

	var $name = 'Stories';
	var $helpers = array('Text','Wyswig','Banner');
	var $paginate = array("order"=>"Story.created DESC");

	function index() {
		$this->Story->recursive = 0;
		$this->set('stories', $this->paginate());
	}

	function tag_cloud($param=null){
		$this->autoRender = false;
		return $this->generateTagCloud();
	}

	function view($path = null) {
		if (!$path) {
			$this->Session->setFlash(__('Invalid Story.', true));
			$this->redirect(array('action'=>'index'));
		}
		$story = $this->Story->findByPath($path);
		if (empty($story)){
			$this->cakeError('error404');
		}
		$this->generatePageTitle($story['Story']['title']);
		$this->set(compact('story'));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Story->create();
			if ($this->Story->save($this->data)) {
				$this->Session->setFlash(__('The Story has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Story could not be saved. Please, try again.', true));
			}
		}
		$articles = $this->Story->Article->find('list');
		$audios = $this->Story->Audio->find('list');
		$authorities = $this->Story->Authority->find('list');
		$events = $this->Story->Event->find('list');
		$publications = $this->Story->Publication->find('list');
		$conditions = $this->Story->Condition->find('list');
		$organizations = $this->Story->Organization->find('list');
		$products = $this->Story->Product->find('list');
		$stories = $this->Story->Story->find('list');
		$therapies = $this->Story->Therapy->find('list');
		$videos = $this->Story->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$areas = $this->Story->Area->find('list');
		$users = $this->Story->User->find('list');
		$this->set(compact('articles', 'audios', 'authorities', 'events', 'publications', 'conditions', 'organizations', 'products', 'stories', 'therapies', 'videos', 'areas', 'users'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Story', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Story->save($this->data)) {
				$this->Session->setFlash(__('The Story has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Story could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Story->read(null, $id);
		}
		$articles = $this->Story->Article->find('list');
		$audios = $this->Story->Audio->find('list');
		$authorities = $this->Story->Authority->find('list');
		$events = $this->Story->Event->find('list');
		$publications = $this->Story->Publication->find('list');
		$conditions = $this->Story->Condition->find('list');
		$organizations = $this->Story->Organization->find('list');
		$products = $this->Story->Product->find('list');
		$stories = $this->Story->Story->find('list');
		$symptoms = $this->Story->Symptom->find('list');
		$therapies = $this->Story->Therapy->find('list');
		$videos = $this->Story->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$areas = $this->Story->Area->find('list');
		$users = $this->Story->User->find('list');
		$this->set(compact('articles','audios','authorities','events','publications','conditions','organizations','products','stories','symptoms','therapies','videos','areas','users'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Story', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Story->del($id)) {
			$this->Session->setFlash(__('Story deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function admin_index($search=null) {
		$filter = (isset($search)) ? array("or"=>array("Story.content LIKE" =>"%$search%","Story.title LIKE" =>"%$search%")) : array("1=1");
		$this->Story->recursive = 0;
		$this->set('stories', $this->paginate(null,$filter));
		$this->set('filter',$search);
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Story.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('story', $this->Story->read(null, $id));
	}

	function featured(){
		$story = $this->Story->find("first",array("conditions"=>array("Story.featured"=>true),"order"=>array("RAND()")));
		$story = (!empty($story)&&isset($story['Story']))? $story : array("Story"=>array("created"=>"","content"=>"","path"=>""));
		return $story;
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Story->create();
			if ($this->Story->save($this->data)) {
				$this->Session->setFlash(__('The Story has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Story could not be saved. Please, try again.', true));
			}
		}
		$used = json_encode($this->Story->find("all",array("fields"=>array("Story.title","Story.id","Story.when"),"conditions"=>array("Story.when REGEXP"=>"^[1-9]{1}[0-9]{3}\-[0-9]{2}\-[0-9]{2}$"))));
		$articles = $this->Story->Article->find('list');
		$audios = $this->Story->Audio->find('list');
		$authorities = $this->Story->Authority->find('list');
		$events = $this->Story->Event->find('list');
		$publications = $this->Story->Publication->find('list');
		$conditions = $this->Story->Condition->find('list');
		$organizations = $this->Story->Organization->find('list');
		$products = $this->Story->Product->find('list');
		$stories = $this->Story->StoryRelated->find('list');
		$therapies = $this->Story->Therapy->find('list');
		$videos = $this->Story->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$areas = $this->Story->Area->find('list');
		$users = $this->Story->User->find('list');
		$isLiivToday = ($this->Session->check("LiivType")&&$this->Session->read("LiivType"))?true:false;
		$this->set(compact('isLiivToday','articles', 'audios', 'authorities', 'events', 'publications', 'conditions', 'organizations', 'products', 'stories', 'therapies', 'videos', 'areas', 'users','used'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Story', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Story->save($this->data)) {
				$this->Session->setFlash(__('The Story has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Story could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Story->read(null, $id);
		}
		/**
		 * get reserved dates
		 */
		$this->Story->recursive = 0;
		$used = json_encode($this->Story->find("all",array("fields"=>array("Story.title","Story.id","Story.when"),"conditions"=>array("Story.when REGEXP"=>"^[1-9]{1}[0-9]{3}\-[0-9]{2}\-[0-9]{2}$"))));
		$articles = $this->Story->Article->find('list');
		$audios = $this->Story->Audio->find('list');
		$authorities = $this->Story->Authority->find('list');
		$events = $this->Story->Event->find('list');
		$publications = $this->Story->Publication->find('list');
		$conditions = $this->Story->Condition->find('list');
		$organizations = $this->Story->Organization->find('list');
		$products = $this->Story->Product->find('list');
		$stories = $this->Story->StoryRelated->find('list');
		$therapies = $this->Story->Therapy->find('list');
		$videos = $this->Story->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
		$areas = $this->Story->Area->find('list');
		$users = $this->Story->User->find('list');
		$isLiivToday = ($this->Session->check("LiivType")&&$this->Session->read("LiivType"))?true:false;
		$this->set(compact('isLiivToday','articles','audios','authorities','events','publications','conditions','organizations','products','stories','therapies','videos','areas','users',"used"));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Story', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Story->del($id)) {
			$this->Session->setFlash(__('Story deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
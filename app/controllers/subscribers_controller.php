<?php
class SubscribersController extends AppController {

	var $name = 'Subscribers';

	function index() {
		$this->Subscriber->recursive = 0;
		$this->set('subscribers', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Subscriber.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('subscriber', $this->Subscriber->read(null, $id));
	}

	function add() {
		if($this->RequestHandler->isAjax()){
			if (!empty($this->data)) {
				/* Check to see if already registered */
				$email = $this->Subscriber->findByEmail($this->data['Subscriber']['email']);
				if(!isset($email['Subscriber'])){
					$this->Subscriber->create();
					if ($this->Subscriber->save($this->data)) {
						echo json_encode(array('code' => 'ok','message' => 'You have been signed up, thank you!'));
					} else {
						echo json_encode(array('code' => 'error', 'message' => 'Error signing up, please try again later.'));				
					}
				} else {
					echo json_encode(array('code' => 'error', 'message' => 'Your email is already registered.'));
				}
				Configure::write('debug', 0);
		    	$this->autoRender = false;
		    	exit();
			}
		} else {
			die();
		}
	}
	
	function suscribe() {
		echo 'got';
		if($this->RequestHandler->isAjax()){
			if (!empty($this->data)) {
				/* Check to see if already registered */
				$email = $this->Subscriber->findByEmail($this->data['Subscriber']['email']);
				if(!isset($email['Subscriber'])){
					$this->Subscriber->create();
					if ($this->Subscriber->save($this->data)) {
						echo json_encode(array('code' => 'ok','message' => 'You have been signed up, thank you!'));
					} else {
						echo json_encode(array('code' => 'error', 'message' => 'Error signing up, please try again later.'));				
					}
				} else {
					echo json_encode(array('code' => 'error', 'message' => 'Your email is already registered.'));
				}
				Configure::write('debug', 0);
		    	$this->autoRender = false;
		    	exit();
			}
		} else {
			die();
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Subscriber', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Subscriber->save($this->data)) {
				$this->Session->setFlash(__('The Subscriber has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Subscriber could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Subscriber->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Subscriber', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Subscriber->del($id)) {
			$this->Session->setFlash(__('Subscriber deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
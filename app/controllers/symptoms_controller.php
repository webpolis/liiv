<?php
class SymptomsController extends AppController {

    var $name = 'Symptoms';
    var $helpers = array('Text');
    var $paginate = array("order"=>"Symptom.created DESC");

    function index() {
        $this->Symptom->recursive = 0;
        $this->set('symptoms', $this->paginate());
    }
    
	function tag_cloud($param=null){
		$this->autoRender = false;
		return $this->generateTagCloud();
	}

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Symptom.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('symptom', $this->Symptom->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            $this->Symptom->create();
            if ($this->Symptom->save($this->data)) {
                $this->Session->setFlash(__('The Symptom has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Symptom could not be saved. Please, try again.', true));
            }
        }
        $conditions = $this->Symptom->Condition->find('list');
        $areas = $this->Symptom->Area->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'publications', 'stories', 'therapies', 'videos', 'areas'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Symptom', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Symptom->save($this->data)) {
                $this->Session->setFlash(__('The Symptom has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Symptom could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Symptom->read(null, $id);
        }
//        $articles = $this->Symptom->Article->find('list');
//        $audios = $this->Symptom->Audio->find('list');
//        $authorities = $this->Symptom->Authority->find('list');
        $conditions = $this->Symptom->Condition->find('list');
//        $events = $this->Symptom->Event->find('list');
//        $publications = $this->Symptom->Publication->find('list');
//        $stories = $this->Symptom->Story->find('list');
//        $therapies = $this->Symptom->Therapy->find('list');
//        $videos = $this->Symptom->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
//        $areas = $this->Symptom->Area->find('list');
        $this->set(compact('conditions','areas'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Symptom', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Symptom->del($id)) {
            $this->Session->setFlash(__('Symptom deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }


    function admin_index($search=null) {
        $filter = (isset($search)) ? array("or"=>array("Symptom.title LIKE" =>"%$search%","Symptom.definition LIKE" =>"%$search%")) : array("1=1");
        $this->Symptom->contain();
        $this->layout = "admin";
        $this->Symptom->recursive = 0;
        $this->set('symptoms', $this->paginate(null,$filter));
        $this->set("filter",$search);
    }

    function admin_view($id = null) {
        $this->layout = "admin";
        if (!$id) {
            $this->Session->setFlash(__('Invalid Symptom.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('symptom', $this->Symptom->read(null, $id));
    }

    function admin_add() {
        $this->layout = "admin";
        if (!empty($this->data)) {
            $this->Symptom->create();
            if ($this->Symptom->save($this->data)) {
                $this->Session->setFlash(__('The Symptom has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Symptom could not be saved. Please, try again.', true));
            }
        }
        $conditions = $this->Symptom->Condition->find('list');
        $areas = $this->Symptom->Area->find('list');
        $this->set(compact('conditions', 'areas'));
    }

    function admin_edit($id = null) {
        $this->layout = "admin";
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Symptom', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Symptom->save($this->data)) {
                $this->Session->setFlash(__('The Symptom has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Symptom could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Symptom->read(null, $id);
        }
//        $articles = $this->Symptom->Article->find('list');
//        $audios = $this->Symptom->Audio->find('list');
//        $authorities = $this->Symptom->Authority->find('list');
        $conditions = $this->Symptom->Condition->find('list');
//        $events = $this->Symptom->Event->find('list');
//        $publications = $this->Symptom->Publication->find('list');
//        $stories = $this->Symptom->Story->find('list');
//        $therapies = $this->Symptom->Therapy->find('list');
//        $videos = $this->Symptom->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
//        $areas = $this->Symptom->Area->find('list');
        $this->set(compact('articles','audios','authorities','conditions','events','publications','stories','therapies','videos','areas'));
    }
    
    function admin_delete($id = null) {
        $this->layout = "admin";
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Symptom', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Symptom->del($id)) {
            $this->Session->setFlash(__('Symptom deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

}
?>
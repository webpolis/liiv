<?php
class TherapiesController extends AppController {

    var $name = 'Therapies';
    var $helpers = array('Text', 'Wyswig', 'Banner');
    var $paginate = array("order"=>"Therapy.created DESC");

    function index($params = null) {
		App::import('Model','Page');
		$pageModel = new Page();
		$therapiesHomepage = $pageModel->find('first', array('conditions' => array('Page.id' => '6')));
        $this->Therapy->recursive = 0;
        $therapies = $this->paginate();
		App::import("Model","Poll");
        $p = new Poll();
        $polls = $p->find("first",array("condition"=>array("Poll.published"=>true),
	        "order"=>array("RAND()")
    	));
    	$this->set("polls",$polls);
    	$this->set(compact('therapies','therapiesHomepage'));
    }
    
	function tag_cloud($param=null){
		$this->autoRender = false;
		return $this->generateTagCloud();
	}

    function view($path = null) {
        if (!$path) {/* Can't actually happen */
			$this->redirect(array('action' => 'index'));
        }
		/*App::import("Model","Poll");
        $p = new Poll();
        $polls = $p->find("first",array("condition"=>array("Poll.published"=>true),
	        "order"=>array("RAND()")
    	));*/
    	$therapy = $this->Therapy->findByPath($path);
		if(!$therapy || !$therapy['Therapy']['published']){
			$this->cakeError('error404');
		}
		$this->Therapy->id = (isset($therapy['Therapy']['id']))?$therapy['Therapy']['id']:0;
		//$this->set("polls",$polls);
		$this->generatePageTitle($therapy['Therapy']['title']);
        $this->set('therapy', $therapy);
    }

    function add() {
        if (!empty($this->data)) {
            $this->Therapy->create();
            if ($this->Therapy->save($this->data)) {
                $this->Session->setFlash(__('The Therapy has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Therapy could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Therapy->Article->find('list');
        $audios = $this->Therapy->Audio->find('list');
        $authorities = $this->Therapy->Authority->find('list');
        $conditions = $this->Therapy->Condition->find('list');
        $events = $this->Therapy->Event->find('list');
        $publications = $this->Therapy->Publication->find('list');
        $stories = $this->Therapy->Story->find('list');
        $organizations = $this->Therapy->Organization->find('list');
        $products = $this->Therapy->Product->find('list');
        $symptoms = $this->Therapy->Symptom->find('list');
        $videos = $this->Therapy->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Therapy->Area->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'publications', 'stories', 'organizations', 'products', 'symptoms', 'videos', 'areas'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Therapy', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Therapy->save($this->data)) {
                $this->Session->setFlash(__('The Therapy has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Therapy could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Therapy->read(null, $id);
        }
        $articles = $this->Therapy->Article->find('list');
        $audios = $this->Therapy->Audio->find('list');
        $authorities = $this->Therapy->Authority->find('list');
        $conditions = $this->Therapy->Condition->find('list');
        $events = $this->Therapy->Event->find('list');
        $publications = $this->Therapy->Publication->find('list');
        $stories = $this->Therapy->Story->find('list');
        $organizations = $this->Therapy->Organization->find('list');
        $products = $this->Therapy->Product->find('list');
        $symptoms = $this->Therapy->Symptom->find('list');
        $videos = $this->Therapy->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Therapy->Area->find('list');
        $this->set(compact('articles','audios','authorities','conditions','events','publications','stories','organizations','products','symptoms','videos','areas'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Therapy', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Therapy->del($id)) {
            $this->Session->setFlash(__('Therapy deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }


    function admin_index($search=null) {
        $filter = (isset($search)) ? array("or"=>array("Therapy.title LIKE" =>"%$search%","Therapy.definition LIKE" =>"%$search%","Therapy.beliefs LIKE"=>"%$search%")) : array("1=1");
        $this->layout = 'admin';
        $this->Therapy->recursive = 0;
        $this->set('therapies', $this->paginate(null,$filter));
        $this->set("filter",$search);
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Therapy.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('therapy', $this->Therapy->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Therapy->create();
            if ($this->Therapy->save($this->data)) {
                $this->Session->setFlash(__('The Therapy has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Therapy could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Therapy->Article->find('list');
        $audios = $this->Therapy->Audio->find('list');
        $authorities = $this->Therapy->Authority->find('list');
        $conditions = $this->Therapy->Condition->find('list');
        $events = $this->Therapy->Event->find('list');
        $publications = $this->Therapy->Publication->find('list');
        $stories = $this->Therapy->Story->find('list');
        $organizations = $this->Therapy->Organization->find('list');
        $products = $this->Therapy->Product->find('list');
        $videos = $this->Therapy->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Therapy->Area->find('list');
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'publications', 'stories', 'organizations', 'products','videos', 'areas'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Therapy', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Therapy->save($this->data)) {
                $this->Session->setFlash(__('The Therapy has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Therapy could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Therapy->read(null, $id);
        }
        $articles = $this->Therapy->Article->find('list');
        $audios = $this->Therapy->Audio->find('list');
        $authorities = $this->Therapy->Authority->find('list');
        $conditions = $this->Therapy->Condition->find('list');
        $events = $this->Therapy->Event->find('list');
        $publications = $this->Therapy->Publication->find('list');
        $stories = $this->Therapy->Story->find('list');
        $organizations = $this->Therapy->Organization->find('list');
        $products = $this->Therapy->Product->find('list');
        $videos = $this->Therapy->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $areas = $this->Therapy->Area->find('list');
        $this->set(compact('articles','audios','authorities','conditions','events','publications','stories','organizations','products','videos','areas'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Therapy', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Therapy->del($id)) {
            $this->Session->setFlash(__('Therapy deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

}
?>
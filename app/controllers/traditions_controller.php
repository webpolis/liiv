<?php
class TraditionsController extends AppController {

    var $name = 'Traditions';
    var $helpers = array('Text', 'Wyswig', 'Banner');
    var $paginate = array("order"=>"Tradition.created DESC");

    function index() {
		App::import('Model','Page');
		$pageModel = new Page();
		$traditionsHomepage = $pageModel->find('first', array('conditions' => array('Page.id' => '4')));
        $this->Tradition->recursive = 0;
        $traditions = $this->paginate();
        $this->set(compact('traditions', 'traditionsHomepage'));
    }
    
	function tag_cloud($param=null){
		$this->autoRender = false;
		return $this->generateTagCloud();
	}

    function view($path = null) {
        if (!$path) {/* Can't actually happen */
			$this->redirect(array('action' => 'index'));
        }
		/*App::import("Model","Poll");
        $p = new Poll();
        $polls = $p->find("first",array("condition"=>array("Poll.published"=>true),
	        "order"=>array("RAND()")
    	));*/
    	$tradition = $this->Tradition->findByPath($path);
		if(!$tradition || !$tradition['Tradition']['published']){
			$this->cakeError('error404');
		}
		$this->Tradition->id = (isset($tradition['Tradition']['id']))?$tradition['Tradition']['id']:0;
        $this->set('tradition', $tradition);
        //$this->set("polls",$polls);
        $this->generatePageTitle($tradition['Tradition']['title']);
    }

    function add() {
        if (!empty($this->data)) {
            $this->Tradition->create();
            if ($this->Tradition->save($this->data)) {
                $this->Session->setFlash(__('The Tradition has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Tradition could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Tradition->Article->find('list');
        $audios = $this->Tradition->Audio->find('list');
        $authorities = $this->Tradition->Authority->find('list');
        $conditions = $this->Tradition->Condition->find('list');
        $events = $this->Tradition->Event->find('list');
        $organizations = $this->Tradition->Organization->find('list');
        $products = $this->Tradition->Product->find('list');
        $publications = $this->Tradition->Publication->find('list');
        $stories = $this->Tradition->Story->find('list');
        $videos = $this->Tradition->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'products', 'publications', 'stories', 'videos'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Tradition', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Tradition->save($this->data)) {
                $this->Session->setFlash(__('The Tradition has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Tradition could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Tradition->read(null, $id);
        }
        $articles = $this->Tradition->Article->find('list');
        $audios = $this->Tradition->Audio->find('list');
        $authorities = $this->Tradition->Authority->find('list');
        $conditions = $this->Tradition->Condition->find('list');
        $events = $this->Tradition->Event->find('list');
        $organizations = $this->Tradition->Organization->find('list');
        $products = $this->Tradition->Product->find('list');
        $publications = $this->Tradition->Publication->find('list');
        $stories = $this->Tradition->Story->find('list');
        $videos = $this->Tradition->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $this->set(compact('articles','audios','authorities','conditions','events','organizations','products','publications','stories','videos'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Tradition', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Tradition->del($id)) {
            $this->Session->setFlash(__('Tradition deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }
    function admin_index($search=null) {
    	$filter = (isset($search)) ? array("or"=>array("Tradition.title LIKE" =>"%$search%","Tradition.definition_long LIKE" =>"%$search%","Tradition.definition_short LIKE"=>"%$search%")) : array("1=1");
        $this->Tradition->recursive = 0;
        $this->set('traditions', $this->paginate(null,$filter));
        $this->set("filter",$search);
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Tradition.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('tradition', $this->Tradition->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Tradition->create();
            if ($this->Tradition->save($this->data)) {
                $this->Session->setFlash(__('The Tradition has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Tradition could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Tradition->Article->find('list');
        $audios = $this->Tradition->Audio->find('list');
        $authorities = $this->Tradition->Authority->find('list');
        $conditions = $this->Tradition->Condition->find('list');
        $events = $this->Tradition->Event->find('list');
        $organizations = $this->Tradition->Organization->find('list');
        $products = $this->Tradition->Product->find('list');
        $publications = $this->Tradition->Publication->find('list');
        $stories = $this->Tradition->Story->find('list');
        $videos = $this->Tradition->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $this->set(compact('articles', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'products', 'publications', 'stories', 'videos'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Tradition', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Tradition->save($this->data)) {
                $this->Session->setFlash(__('The Tradition has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Tradition could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Tradition->read(null, $id);
        }
        $articles = $this->Tradition->Article->find('list');
        $audios = $this->Tradition->Audio->find('list');
        $authorities = $this->Tradition->Authority->find('list');
        $conditions = $this->Tradition->Condition->find('list');
        $events = $this->Tradition->Event->find('list');
        $organizations = $this->Tradition->Organization->find('list');
        $products = $this->Tradition->Product->find('list');
        $publications = $this->Tradition->Publication->find('list');
        $stories = $this->Tradition->Story->find('list');
        $videos = $this->Tradition->Video->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $this->set(compact('articles','audios','authorities','conditions','events','organizations','products','publications','stories','videos'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Tradition', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Tradition->del($id)) {
            $this->Session->setFlash(__('Tradition deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }
}
?>
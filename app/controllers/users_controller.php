<?php
/**
 * @property User $User
 */
class UsersController extends AppController {
    var $name = 'Users';
    var $components = array('Session');
    /**
     * @var User
     */
    var $User;
    /**
     * @var AuthComponent
     */
    var $Auth;

    function logout() {
        $this->redirect($this->Auth->logout());
    }

    function login() {
        $this->layout = 'login';
    }

    function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    function view($id=null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid User.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('user', $this->User->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            $this->User->create();
            if ($this->User->save($this->data)) {
                $this->Session->setFlash(__('The User has been saved', true));
                $this->redirect(array('action'=>'index'));
            }
            else {
                $this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
            }
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

    function edit($id=null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid User', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->User->save($this->data)) {
                $this->Session->setFlash(__('The User has been saved', true));
                $this->redirect(array('action'=>'index'));
            }
            else {
                $this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->User->read(null, $id);
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

    function delete($id=null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for User', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->User->del($id)) {
            $this->Session->setFlash(__('User deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

    function admin_login() {
        $this->pageTitle = 'Liiv - Administration';
        $this->layout = 'login';
    }

    function admin_index($search='all', $group=null) {
        $this->layout = 'admin';
        $this->User->recursive = 0;
        $this->Session->write('UserType', $group);
        if (isset($group) && preg_match('/^\\d+$/s', $group) && ($group != 0)) {
            $filter = array('User.group_id' => $group);
        }
        elseif ($search !== 'all') {
            $filter = array('or'=>array('User.email LIKE'=>"%$search%", 'User.username LIKE'=>"%$search%"));
        }
        else {
            $filter = array('1=1');
        }
        $this->set('users', $this->paginate(null, $filter));
        $this->set('filter', (isset($group)? "all/$group": (isset($search)? $search: null)));
        $groups = $this->User->Group->find('list');
        array_unshift($groups, 'All');
        $this->set(compact('groups'));
    }

    function admin_view($id=null) {
        $this->layout = 'admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid User.', true));
            $this->redirect(array('action'=>'index'));
        }
        $sql = "SELECT * FROM iplogs WHERE user_id='".$id."'";
        $iplog = $this->{$this->modelClass}->query($sql);
        $user = $this->User->read(null, $id);
        $this->set(compact('user', 'iplog'));
    }

    function admin_add() {
        $this->layout = 'admin';
        if (!empty($this->data)) {
            if(!isset ($this->data['User']['password'])) {
                $this->data['User']['password'] = '';
            }
            $this->User->create();
            if ($this->User->save($this->data)) {
                $this->Session->setFlash(__('The User has been saved', true));
                // adding the user into the acl tree with member permissions
                $group = $this->User->Group->field('title', array('Group.id' =>$this->data['User']['group_id']));
                $aro =& $this->Acl->Aro;
                $parent = $aro->findByAlias($group);
                $parent_id = $parent['Aro']['id'];
                $aro->create();
                $aro->save(array(
                        'model' => $this->User->name,
                        'foreign_key' => $this->User->id,
                        'parent_id' => $parent_id,
                        'alias' => $this->data['User']['email']
                ));
                // end acl tree
                $this->redirect(array('action'=>'index'));
            }else {
                pr($this->data);
                $this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
                unset($this->data['User']['password']);
            }
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
        $this->set('userType', ($this->Session->check('UserType')? $this->Session->read('UserType'): 1));
    }

    function admin_edit($id=null) {
        $this->layout = 'admin';
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid User', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->User->save($this->data)) {
                $this->Session->setFlash(__('The User has been saved', true));
                $this->redirect(array('action'=>'index'));
            }
            else {
                unset($this->data['User']['password']);
                $this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->User->read(null, $id);
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

    function admin_delete($id=null) {
        $this->layout = 'admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for User', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->User->del($id)) {
            $this->Session->setFlash(__('User deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

    function admin_logout() {
        $this->Auth->logoutRedirect = '/';
        $this->redirect($this->Auth->logout());
    }

    /**
     * Calls the "error: unauthorized" page integrated into the admin template
     */
    function admin_unauthorized() {
        $this->layout = (preg_match('/\\/admin\\//si', Controller::referer())? 'admin': 'default');
    }

    function unauthorized() {
        $this->layout = ('404');
    }

    function admin_changePassword() {
        $userInfo = $this->Auth->user();
        if (!$this->RequestHandler->isAjax() || !$userInfo)
            return;

        Configure::write('debug', 0);
        $this->autoRender = false;
        if (!empty($this->data)) {
            // Verify user authenticity
            $conditions = array(
                    'User.id'		=> $this->data['User']['id'],
                    'User.password'	=> $this->data['User']['password']
            );
            if (($this->data['User']['id'] != $userInfo['User']['id']) || !$this->User->find('first', array('conditions' => $conditions))) {
                $response = array(
                        'success' => false,
                        'message' => __('Current password is invalid', true)
                );
            }
            // Validate passwords
            elseif (empty($this->data['User']['new_pass'])) {
                $response = array(
                        'success' => false,
                        'message' => __('Please enter your New Password', true)
                );
            }
            elseif (empty($this->data['User']['confirm'])) {
                $response = array(
                        'success' => false,
                        'message' => __('Please Confirm your New Password', true)
                );
            }
            elseif ($this->data['User']['new_pass'] != $this->data['User']['confirm']) {
                $response = array(
                        'success' => false,
                        'message' => __('New password and confirmation do not match', true)
                );
            }
            else {
                // Prepare new password for saving
                $this->data['User']['password'] = $this->data['User']['new_pass'];
                $this->data = User::hashPasswords($this->data);
                unset($this->data['User']['new_pass'], $this->data['User']['confirm']);
                // Save data
                if ($this->User->save($this->data)) {
                    $response = array(
                            'success' => true,
                            'message' => __('Your password was successfully changed', true)
                    );
                }
                else {
                    $response = array(
                            'success' => false,
                            'message' => __('Your password could not be changed. Please, try again.', true)
                    );
                }
            }
            echo json_encode($response);
            exit();
        }
        else {
            $this->data = $userInfo;
            $this->render('admin_ajaxchangepassword', 'ajax');
        }
    }
}
?>
<?php
class VideosController extends AppController {

    var $name = 'Videos';
    var $helpers = array('Text','Banner');
    var $paginate = array("order"=>"Video.created DESC");

    function index() {
        $this->Video->recursive = 0;
        $this->set('videos', $this->paginate());
    }


    function featured(){
    	$video = $this->Video->find("first",array("conditions"=>array("Video.featured"=>true),"order"=>array("Video.updated DESC")));
    	$video = (!empty($video)&&isset($video['Video']))? $video : array("Video"=>array("title"=>"","url"=>"","path"=>"","description_short"=>""));
    	$this->pageTitle = "Featured Video";
    	return $video;
    }
    
    function view($url = null) {
        if (!$url) {
            $this->Session->setFlash(__('Invalid Video.', true));
            $this->redirect(array('action'=>'index'));
        }
        $video = $this->Video->findByUrl($url);
        if(!$video){
			$this->cakeError('error404');
		}
        $this->set(compact('video'));
    }

    function add() {
        if (!empty($this->data)) {
            $this->Video->create();
            if ($this->Video->save($this->data)) {
                $this->Session->setFlash(__('The Video has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Video could not be saved. Please, try again.', true));
            }
        }
        $articles = $this->Video->Article->find('list');
        $stories = $this->Video->Story->find('list');
        $audios = $this->Video->Audio->find('list');
        $authorities = $this->Video->Authority->find('list');
        $conditions = $this->Video->Condition->find('list');
        $events = $this->Video->Event->find('list');
        $organizations = $this->Video->Organization->find('list');
        $publications = $this->Video->Publication->find('list');
        $symptoms = $this->Video->Symptom->find('list');
        $therapies = $this->Video->Therapy->find('list');
        $videos = $this->Video->VideoRelated->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $organizations = $this->Video->Organization->find('list');
        $areas = $this->Video->Area->find('list');
        $this->set(compact('articles', 'stories', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'publications', 'symptoms', 'therapies', 'videos', 'organizations', 'areas'));
    }

    function admin_getVimeoThumb($id=null) {
        $this->autoRender = false;
        if(!empty($id)) {
            //Configure::write('debug', 0);
            $url = "http://vimeo.com/api/clip/$id/php";
            $thumb = "/img/unavailable.png";
            if($ret=file_get_contents($url,'FILE_TEXT')) {
                $data = unserialize($ret);
                $thumb =  (isset($data['0']['thumbnail_small']))?$data['0']['thumbnail_small']:"/img/unavailable.png";
            }
            header("Content-Type: image/".((stristr($thumb,".jpg"))?"jpeg":"png"));
            readfile($thumb);
        }
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Video', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Video->save($this->data)) {
                $this->Session->setFlash(__('The Video has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Video could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Video->read(null, $id);
        }
        $articles = $this->Video->Article->find('list');
        $stories = $this->Video->Story->find('list');
        $audios = $this->Video->Audio->find('list');
        $authorities = $this->Video->Authority->find('list');
        $conditions = $this->Video->Condition->find('list');
        $events = $this->Video->Event->find('list');
        $organizations = $this->Video->Organization->find('list');
        $publications = $this->Video->Publication->find('list');
        $symptoms = $this->Video->Symptom->find('list');
        $therapies = $this->Video->Therapy->find('list');
        $videos = $this->Video->VideoRelated->find('list',array("fields"=>array("Video.id","Video.title","Video.content")));
        $organizations = $this->Video->Organization->find('list');
        $areas = $this->Video->Area->find('list');
        $this->set(compact('articles','stories','audios','authorities','conditions','events','organizations','publications','symptoms','therapies','videos','organizations','areas'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Video', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Video->del($id)) {
            $this->Session->setFlash(__('Video deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }


    function admin_index($search=null) {
        $filter = (isset($search)) ? array("or"=>array("Video.title LIKE" =>"%$search%","Video.description_short LIKE" =>"%$search%","Video.content LIKE"=>"%$search%")) : array("1=1");
        $this->Video->recursive = 0;
        $videos = $this->paginate(null,$filter);
        $this->set(compact('videos'));
        $this->set("filter",$search);
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Video.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('video', $this->Video->read(null, $id));
    }

    function admin_add() {
        if($this->RequestHandler->isAjax()) {
            if(empty($this->data)) {
                debug($this->data);
            }
        }else {
            if (!empty($this->data)) {
                $this->Video->create();
                if ($this->Video->save($this->data)) {
                    $this->Session->setFlash(__('The Video has been saved', true));
                    $this->redirect(array('action'=>'index'));
                } else {
                    $this->Session->setFlash(__('The Video could not be saved. Please, try again.', true));
                }
            }
            $articles = $this->Video->Article->find('list');
            $stories = $this->Video->Story->find('list');
            $audios = $this->Video->Audio->find('list');
            $authorities = $this->Video->Authority->find('list');
            $conditions = $this->Video->Condition->find('list');
            $events = $this->Video->Event->find('list');
            $organizations = $this->Video->Organization->find('list');
            $publications = $this->Video->Publication->find('list');
            $therapies = $this->Video->Therapy->find('list');
            $videos = $this->Video->VideoRelated->find('list',array("fields"=>array("VideoRelated.id","VideoRelated.title","VideoRelated.content")));
            $organizations = $this->Video->Organization->find('list');
            $areas = $this->Video->Area->find('list');
            $this->set(compact('articles', 'stories', 'audios', 'authorities', 'conditions', 'events', 'organizations', 'publications', 'therapies', 'videos', 'organizations', 'areas'));
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Video', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Video->save($this->data)) {
                $this->Session->setFlash(__('The Video has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Video could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Video->read(null, $id);
        }
        $articles = $this->Video->Article->find('list');
        $stories = $this->Video->Story->find('list');
        $audios = $this->Video->Audio->find('list');
        $authorities = $this->Video->Authority->find('list');
        $conditions = $this->Video->Condition->find('list');
        $events = $this->Video->Event->find('list');
        $organizations = $this->Video->Organization->find('list');
        $publications = $this->Video->Publication->find('list');
        $therapies = $this->Video->Therapy->find('list');
        $videos = $this->Video->VideoRelated->find('list',array("fields"=>array("VideoRelated.id","VideoRelated.title","VideoRelated.content")));
        $organizations = $this->Video->Organization->find('list');
        $areas = $this->Video->Area->find('list');
        $this->set(compact('videoseries','articles','stories','audios','authorities','conditions','events','organizations','publications','therapies','videos','organizations','areas'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Video', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Video->del($id)) {
            $this->Session->setFlash(__('Video deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

}
?>
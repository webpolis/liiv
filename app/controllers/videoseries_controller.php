<?php
class VideoseriesController extends AppController {

    var $name = 'Videoseries';
    var $helpers = array('Html', 'Form');
    var $pageTitle = "Video Series";
    var $paginate = array("order"=>"Videoseries.created DESC");

    function index() {
        $this->Videoseries->recursive = 0;
        $this->set('videoseries', $this->paginate());
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Videoseries.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('videoseries', $this->Videoseries->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            $this->Videoseries->create();
            if ($this->Videoseries->save($this->data)) {
                $this->Session->setFlash(__('The Videoseries has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Videoseries could not be saved. Please, try again.', true));
            }
        }
        $users = $this->Videoseries->User->find('list');
        $this->set(compact('users'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Videoseries', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Videoseries->save($this->data)) {
                $this->Session->setFlash(__('The Videoseries has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Videoseries could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Videoseries->read(null, $id);
        }
        $users = $this->Videoseries->User->find('list');
        $this->set(compact('users'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Videoseries', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Videoseries->del($id)) {
            $this->Session->setFlash(__('Videoseries deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }


    function admin_index($search=null) {
        $filter = (isset($search)) ? array("or"=>array("Videoseries.title LIKE" =>"%$search%")) : array("1=1");
        $this->Videoseries->recursive = 0;
        $this->set('videoseries', $this->paginate(null,$filter));
        $this->set("filter",$search);
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Videoseries.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('videoseries', $this->Videoseries->read(null, $id));
    }

    function admin_remove($id=null) {
        if($this->RequestHandler->isAjax()) {
            $this->autoRender = false;
            Configure::write('debug', 0);
            $this->Videoseries->Video->id = $id;
            $this->Videoseries->Video->saveField("videoseries_id",null);
        }
    }

    private function _processVideoCollection() {
        // existing videos
        $col = (isset($this->data['Videoseries']['collection']))?$this->data['Videoseries']['collection']:null;
        $order = array();
        if(!empty($col)) {
            preg_match_all("/Video\[\]\=([^&]+)/",$col,$order);
            array_walk($order['1'],create_function('&$v,$k','$v = (string)$v;'));
        }
        if(isset($this->data['Videoseries']['Video'])) {
            $this->Videoseries->Video->updateAll(
                    array(
                    "Video.videoseries_id" => null
                    ),
                    array("Video.videoseries_id" => $this->Videoseries->id)
            );
            if(!empty($this->data['Videoseries']['Video'])) {
                $this->Videoseries->Video->updateAll(
                        array(
                        "Video.videoseries_id" => $this->Videoseries->id
                        ),
                        array("Video.id" => $this->data['Videoseries']['Video'])
                );
            }
            // update order
            if(is_array($this->data['Videoseries']['Video'])) {
                array_walk($this->data['Videoseries']['Video'], create_function('&$v,$k,&$arr','
                $arr[\'0\']->id = (int)$v;
                $arr[\'0\']->saveField("videoseries_order",array_search($v, $arr[\'1\']));
                '
                        ),array(&$this->Videoseries->Video,&$order['1']));
            }
        }
        // new videos
        if(isset($this->data['Videoseries']['NewVideos']) && !empty($this->data['Videoseries']['NewVideos'])) {
            foreach($this->data['Videoseries']['NewVideos'] as $k => $video) {
                $content = (preg_match("/^\d+$/s",$video))? "/videos/series/$video.flv":"http://www.youtube.com/watch?v=$video";
                $data = array(
                        "videoseries_id" => (!isset($this->data['Videoseries']['id'])) ? $this->Videoseries->id: $this->data['Videoseries']['id'],
                        "videoseries_order" => array_search((string)$this->data['Videoseries']['id'],$order['1']),
                        "title" => $this->data['Videoseries']['NewTitles'][$k],
                        "content" => $content
                );
                $this->Videoseries->Video->create();
                $this->Videoseries->Video->save(array("Video"=>$data));
            }
        }
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Videoseries->create();
            if ($this->Videoseries->save($this->data)) {
                $this->_processVideoCollection();
                $this->Session->setFlash(__('The Video serie has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Video serie could not be saved. Please, try again.', true));
            }
        }
        $videos = $this->Videoseries->Video->find("list");
        $this->set(compact("videos"));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Videoseries', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Videoseries->save($this->data)) {
                $this->_processVideoCollection();
                $this->Session->setFlash(__('The Video serie has been saved', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Video serie could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Videoseries->read(null, $id);
        }
        $videos = $this->Videoseries->Video->find("list",array("fields"=>array("Video.id","Video.title","Video.content")));
        $this->set(compact("videos"));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Video serie', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Videoseries->del($id)) {
            $this->Videoseries->Video->updateAll(
                    array(
                    "Video.videoseries_id" => null
                    ),
                    array(
                    "Video.videoseries_id" => $id
                    )
            );
            $this->Session->setFlash(__('Video serie deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

}
?>
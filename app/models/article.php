<?php
class Article extends AppModel {

	var $name = 'Article';

	var $validate = array(
		'content' => array(
			'rule-1' => array(
				'rule' => 'notEmpty',
				'message' => VALIDATION_NOT_EMPTY,
				'last' => true
			),
			'rule-2' => array(
				'rule' => 'url',
				'message' => VALIDATION_URL,
				'last' => true
			)
		),
		'title' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => VALIDATION_NOT_EMPTY
		),
		/*'author' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => VALIDATION_NOT_EMPTY
		)*/
	);

	var $belongsTo = array(
	    'Area' => array(
		    'className' => 'Area',
		    'foreignKey' => 'area_id',
		    'conditions' => '',
		    'fields' => '',
		    'order' => ''
	    ),
		'CreatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'createdby',
			'fields' => 'email'
		),
		'UpdatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'updatedby',
			'fields' => 'email'
		),
    );

	var $hasAndBelongsToMany = array(
		'ArticleRelated' => array(
			'className' => 'Article',
			'joinTable' => 'articles_articles',
			'foreignKey' => 'article_id',
			'associationForeignKey' => 'article_sec_id',
			'unique' => true,
			'conditions' => 'ArticleRelated.published=1',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Audio' => array(
			'className' => 'Audio',
			'joinTable' => 'articles_audios',
			'foreignKey' => 'article_id',
			'associationForeignKey' => 'audio_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Authority' => array(
			'className' => 'Authority',
			'joinTable' => 'articles_authorities',
			'foreignKey' => 'article_id',
			'associationForeignKey' => 'authority_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Condition' => array(
			'className' => 'Condition',
			'joinTable' => 'articles_conditions',
			'foreignKey' => 'article_id',
			'associationForeignKey' => 'condition_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Event' => array(
			'className' => 'Event',
			'joinTable' => 'articles_events',
			'foreignKey' => 'article_id',
			'associationForeignKey' => 'event_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Organization' => array(
			'className' => 'Organization',
			'joinTable' => 'articles_organizations',
			'foreignKey' => 'article_id',
			'associationForeignKey' => 'organization_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Publication' => array(
			'className' => 'Publication',
			'joinTable' => 'articles_publications',
			'foreignKey' => 'article_id',
			'associationForeignKey' => 'publication_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Story' => array(
			'className' => 'Story',
			'joinTable' => 'articles_stories',
			'foreignKey' => 'article_id',
			'associationForeignKey' => 'story_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Therapy' => array(
			'className' => 'Therapy',
			'joinTable' => 'articles_therapies',
			'foreignKey' => 'article_id',
			'associationForeignKey' => 'therapy_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Video' => array(
			'className' => 'Video',
			'joinTable' => 'articles_videos',
			'foreignKey' => 'article_id',
			'associationForeignKey' => 'video_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

	function beforeSave(){
		/* Add http if url doesn't have it */
		parent::beforeSave();
		if(!empty($this->data['Article']['content'])){
			if(!preg_match('/^http:\/\/.*$/i', $this->data['Article']['content'])){
				$this->data['Article']['content'] = 'http://'.$this->data['Article']['content'];
			}
		}
		return true;
	}

}
?>
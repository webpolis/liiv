<?php
class AuthorityType extends AppModel {

	var $name = 'AuthorityType';
        var $useTable = "authority_types";
        var $primaryKey = "id";
        var $belongsTo = array(
            "Authority" => array(
                "className" => "Authority",
                "foreignKey" => "type_id"
            )
        );
}
?>
<?php
class Banner extends AppModel {
    var $name = 'Banner';
    var $validate = array(
	        "width" => array(
	            "rule" => "numeric",
	            "message" => VALIDATION_NUMBER
	        ),
	        "height" => array(
	            "rule" => "numeric",
	            "message" => VALIDATION_NUMBER
	        ),
	        "link" => array(
		        "rule" => "url",
	        	"message" => VALIDATION_URL
	        )
        );
        
	var $hasAndBelongsToMany = array(
		'Position' => array(
		    'className' => 'Position'
	    )
	);
	
	var $belongsTo = array(
		'CreatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'createdby',
			'fields' => 'email'
		),
		'UpdatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'updatedby',
			'fields' => 'email'
		)
	);
}
?>
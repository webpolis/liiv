<?php
class Condition extends AppModel {
	var $name = 'Condition';

	var $actsAs = array('Searchable');

	var $validate = array(
		'title' => array(
			'rule' => 'notEmpty',
			'message' => VALIDATION_NOT_EMPTY
		),
		'path' => array(
			'rule-1' => array(
				'rule' => 'notEmpty',
				'message' => VALIDATION_NOT_EMPTY,
				'last' => true
			),
			'rule-2' => array(
				'rule' => array('maxLength', 100),
				'message' => 'This field can\'t contain more than 100 characters',
				'last' => true
			),
			'rule-3' => array(
				'rule' => '/^[a-z0-9\\-]+$/',
				'message' => 'Path can only contain lowercase letters, numbers and "-", and can\'t contain spaces',
				'last' => true
			),
			'rule-4' => array(
				'rule' => 'isUnique',
				'message' => VALIDATION_EXISTING_PATH,
				'last' => true
			)
		)
	);

	var $belongsTo = array(
		'Area' => array(
			'className' => 'Area',
			'foreignKey' => 'area_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CreatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'createdby',
			'fields' => 'email'
		),
		'UpdatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'updatedby',
			'fields' => 'email'
		),
	);

	var $hasAndBelongsToMany = array(
		'Article' => array(
			'className' => 'Article',
			'joinTable' => 'articles_conditions',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'article_id',
			'unique' => true,
			'conditions' => 'Article.published=1',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Audio' => array(
			'className' => 'Audio',
			'joinTable' => 'audios_conditions',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'audio_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Authority' => array(
			'className' => 'Authority',
			'joinTable' => 'authorities_conditions',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'authority_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Organization' => array(
			'className' => 'Organization',
			'joinTable' => 'conditions_organizations',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'organization_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Product' => array(
			'className' => 'Product',
			'joinTable' => 'conditions_products',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'product_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'PrimarySymptom' => array(
			'className' => 'Symptom',
			'joinTable' => 'conditions_symptoms',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'symptom_id',
			'unique' => true,
			'conditions' => 'ConditionsSymptom.classification=1',
			'fields' => array('ConditionsSymptom.classification', 'PrimarySymptom.id', 'PrimarySymptom.title')
		),
		'SecondarySymptom' => array(
			'className' => 'Symptom',
			'joinTable' => 'conditions_symptoms',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'symptom_id',
			'unique' => true,
			'conditions' => 'ConditionsSymptom.classification=2',
			'fields' => array('ConditionsSymptom.classification', 'SecondarySymptom.id', 'SecondarySymptom.title')
		),
		'Therapy' => array(
			'className' => 'Therapy',
			'joinTable' => 'conditions_therapies',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'therapy_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Tradition' => array(
			'className' => 'Tradition',
			'joinTable' => 'traditions_conditions',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'tradition_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Event' => array(
			'className' => 'Event',
			'joinTable' => 'events_conditions',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'event_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Publication' => array(
			'className' => 'Publication',
			'joinTable' => 'publications_conditions',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'publication_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Story' => array(
			'className' => 'Story',
			'joinTable' => 'stories_conditions',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'story_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Video' => array(
			'className' => 'Video',
			'joinTable' => 'videos_conditions',
			'foreignKey' => 'condition_id',
			'associationForeignKey' => 'video_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
}
?>
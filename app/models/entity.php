<?php
class Entity extends AppModel {

	var $name = 'Entity';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
		'Subscription' => array(
			'className' => 'Subscription',
			'foreignKey' => 'entity_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>
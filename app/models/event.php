<?php
class Event extends AppModel {
	var $name = 'Event';

	// The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Area' => array(
			'className' => 'Area',
			'foreignKey' => 'area_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasAndBelongsToMany = array(
		'Article' => array(
			'className' => 'Article',
			'joinTable' => 'articles_events',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'article_id',
			'unique' => true,
			'conditions' => 'Article.published=1',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Audio' => array(
			'className' => 'Audio',
			'joinTable' => 'audios_events',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'audio_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Authority' => array(
			'className' => 'Authority',
			'joinTable' => 'authorities_events',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'authority_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Condition' => array(
			'className' => 'Condition',
			'joinTable' => 'events_conditions',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'condition_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Organization' => array(
			'className' => 'Organization',
			'joinTable' => 'events_organizations',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'organization_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Story' => array(
			'className' => 'Story',
			'joinTable' => 'events_stories',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'story_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Therapy' => array(
			'className' => 'Therapy',
			'joinTable' => 'events_therapies',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'therapy_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Publication' => array(
			'className' => 'Publication',
			'joinTable' => 'publications_events',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'publication_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Video' => array(
			'className' => 'Video',
			'joinTable' => 'videos_events',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'video_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
}
?>
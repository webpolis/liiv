<?php
class LiivToday extends AppModel {

    var $name = "LiivToday";
    var $useTable = "liiv_today";
    var $displayField = "LiivTodayType.title";
    var $order = array("LiivToday.created DESC");
    var $validate = array(
    	'when' => array('notempty'),
    	'topic' => array(    		
	    	'rule-1' => array(
					'rule' => array('maxLength', 100),
					'message' => 'This field can\'t contain more than 100 characters',
					'last' => true)),
    	'content' => array(
    		'rule-1' => array(
				'rule' => array('maxLength', 400),
				'message' => 'This field can\'t contain more than 400 characters',
				'last' => true),
    	),
    	'link' => array(
			'rule-1' => array(
				'rule' => 'url',
				'message' => VALIDATION_URL,
    			'allowEmpty' => true,
				'last' => true
			),
	    	'rule-2' => array(
				'rule' => array('maxLength', 240),
				'message' => 'This field can\'t contain more than 400 characters',
				'last' => true),
	    	)
    );
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'LiivTodayType' => array(
            'className' => 'LiivTodayType',
            'foreignKey' => 'type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasAndBelongsToMany = array(
            'Audio' => array(
                'className' => 'Audio',
                'joinTable' => 'liiv_today_habtm',
                'foreignKey' => 'liiv_today_id',
                'associationForeignKey' => 'item_id',
                'unique' => true,
                'conditions' => 'LiivTodayHabtm.entity_id = 9' /* <-- switch this number to the corresponding entity id */,
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'deleteQuery' => '',
                'insertQuery' => ''
            )
    );
    
	function beforeSave(){
		parent::beforeSave();
		/* Add http if url doesn't have it */
		if(!empty($this->data['LiivToday']['link'])){
			if(!preg_match('/^http:\/\/.*$/i', $this->data['LiivToday']['link'])){
				$this->data['LiivToday']['link'] = 'http://'.$this->data['LiivToday']['link'];
			}
		}
		return true;
	}
}
?>

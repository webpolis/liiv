<?php
class Page extends AppModel {

	var $name = 'Page';
	var $belongsTo = array(
		'CreatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'createdby',
			'fields' => 'email'
			),
		'UpdatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'updatedby',
			'fields' => 'email'
			)
	);
}
?>
<?php
class Poll extends AppModel {

	var $name = 'Poll';
	var $validate = array(
		'question' => array('notempty')
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
		'Polloption' => array(
			'className' => 'Polloption',
			'foreignKey' => 'poll_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'Polloption.order',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>
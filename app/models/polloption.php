<?php
class Polloption extends AppModel {

	var $name = 'Polloption';
	
	var $order = array('order');
	var $validate = array(
		"answer" => array("notEmpty")
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Poll' => array(
			'className' => 'Poll',
			'foreignKey' => 'poll_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
?>
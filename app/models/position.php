<?php
class Position extends AppModel {

	var $name = 'Position';

    var $validate = array(
    	'name' => array(
				'rule' => 'notEmpty',
				'message' => VALIDATION_NOT_EMPTY
    	),
    	'identifier' => array(
			'rule-1' => array(
				'rule' => 'notEmpty',
				'message' => VALIDATION_NOT_EMPTY,
				'last' => true
			),
			'rule-2' => array(
				'rule' => array('maxLength', 45),
				'message' => 'This field can\'t contain more than 45 characters',
				'last' => true),
			'rule-3' => array(
				'rule' => '/^[a-z\-_]+$/',
				'message' => 'Identifiers can only contain lowercase letters, "-" and "_", and can\'t contain spaces.',
				'last' => true
				),
    		'rule-4' => array(
    			'rule' => 'isUnique',
    			'message' => 'This identifier is already in use, please choose another one.',
				'last' => true)
    	)
    );
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasAndBelongsToMany = array(
		'Banner' => array(
			'className' => 'Banner',
			'joinTable' => 'banners_positions',
			'foreignKey' => 'position_id',
			'associationForeignKey' => 'banner_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
?>
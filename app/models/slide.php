<?php
class Slide extends AppModel {

	var $name = 'Slide';
	var $validate = array(
		'content' => array('notempty'),
		'description' => array(
			"rule"=> array("maxLength",190),
			"message" => "The description must not be larger than 190 characters long"
			),
        "link" => array(
	        "rule" => "url",
        	"message" => "This is not a valid URL"
        	)
        );
}
?>
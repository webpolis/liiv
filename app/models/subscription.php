<?php
class Subscription extends AppModel {

	var $name = 'Subscription';
	var $validate = array(
		'type' => array('numeric'),
		'item' => array('numeric')
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Entity' => array(
			'className' => 'Entity',
			'foreignKey' => 'entity_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
?>
<?php
class Therapy extends AppModel {
	var $name = 'Therapy';

	var $validate = array(
		'title' => array(
			'rule' => 'notEmpty',
			'message' => VALIDATION_NOT_EMPTY
		),
		'introduction' => array(
			'rule' => array('maxLength', 512),
			'message' => 'This field can\'t contain more than 512 characters'
		),
		'path' => array(
			'rule-1' => array(
				'rule' => 'notEmpty',
				'message' => VALIDATION_NOT_EMPTY,
				'last' => true
			),
			'rule-2' => array(
				'rule' => array('maxLength', 100),
				'message' => 'This field can\'t contain more than 100 characters',
				'last' => true),
			'rule-3' => array(
				'rule' => '/^[a-z0-9\\-]+$/',
				'message' => 'Path can only contain lowercase letters, numbers and "-", and can\'t contain spaces.',
				'last' => true
				),
			'rule-4' => array(
				'rule' => 'isUnique',
				'message' => VALIDATION_EXISTING_PATH,
				'last' => true)
		)
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Area' => array(
			'className' => 'Area',
			'foreignKey' => 'area_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CreatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'createdby',
			'fields' => 'email'
		),
		'UpdatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'updatedby',
			'fields' => 'email'
		),
	);

	var $hasAndBelongsToMany = array(
		'Article' => array(
			'className' => 'Article',
			'joinTable' => 'articles_therapies',
			'foreignKey' => 'therapy_id',
			'associationForeignKey' => 'article_id',
			'unique' => true,
			'conditions' => 'Article.published=1',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Audio' => array(
			'className' => 'Audio',
			'joinTable' => 'audios_therapies',
			'foreignKey' => 'therapy_id',
			'associationForeignKey' => 'audio_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Authority' => array(
			'className' => 'Authority',
			'joinTable' => 'authorities_therapies',
			'foreignKey' => 'therapy_id',
			'associationForeignKey' => 'authority_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Condition' => array(
			'className' => 'Condition',
			'joinTable' => 'conditions_therapies',
			'foreignKey' => 'therapy_id',
			'associationForeignKey' => 'condition_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Event' => array(
			'className' => 'Event',
			'joinTable' => 'events_therapies',
			'foreignKey' => 'therapy_id',
			'associationForeignKey' => 'event_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Publication' => array(
			'className' => 'Publication',
			'joinTable' => 'publications_therapies',
			'foreignKey' => 'therapy_id',
			'associationForeignKey' => 'publication_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Story' => array(
			'className' => 'Story',
			'joinTable' => 'stories_therapies',
			'foreignKey' => 'therapy_id',
			'associationForeignKey' => 'story_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Organization' => array(
			'className' => 'Organization',
			'joinTable' => 'therapies_organizations',
			'foreignKey' => 'therapy_id',
			'associationForeignKey' => 'organization_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Product' => array(
			'className' => 'Product',
			'joinTable' => 'therapies_products',
			'foreignKey' => 'therapy_id',
			'associationForeignKey' => 'product_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Video' => array(
			'className' => 'Video',
			'joinTable' => 'videos_therapies',
			'foreignKey' => 'therapy_id',
			'associationForeignKey' => 'video_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
}
?>
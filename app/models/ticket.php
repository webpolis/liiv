<?php
class Ticket extends AppModel {

	var $name = 'Ticket';
	var $validate = array(
                'subject' => array(array(
				'rule' => 'notEmpty',
				'message' => VALIDATION_NOT_EMPTY
			)),
		'resume' => array(array(
				'rule' => 'notEmpty',
				'message' => VALIDATION_NOT_EMPTY
			))
	);


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasMany = array(
		'Resolution' => array(
			'className' => 'Resolution',
			'foreignKey' => 'ticket_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>
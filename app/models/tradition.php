<?php
class Tradition extends AppModel {
	var $name = 'Tradition';

	var $actsAs = array('Searchable');

	var $validate = array(
		'title' => array(
			'rule' => 'notEmpty',
			'message' => VALIDATION_NOT_EMPTY
		),
		'path' => array(
			'rule-1' => array(
				'rule' => 'notEmpty',
				'message' => VALIDATION_NOT_EMPTY,
				'last' => true
			),
			'rule-2' => array(
				'rule' => array('maxLength', 100),
				'message' => 'This field can\'t contain more than 100 characters',
				'last' => true
			),
			'rule-3' => array(
				'rule' => '/^[a-z0-9\\-]+$/',
				'message' => 'Path can only contain lowercase letters, numbers and "-", and can\'t contain spaces.',
				'last' => true
			),
			'rule-4' => array(
				'rule' => 'isUnique',
				'message' => VALIDATION_EXISTING_PATH,
				'last' => true
			)
		)
	);

	var $belongsTo = array(
		'CreatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'createdby',
			'fields' => 'email'
		),
		'UpdatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'updatedby',
			'fields' => 'email'
		)
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasAndBelongsToMany = array(
		'Article' => array(
			'className' => 'Article',
			'joinTable' => 'traditions_articles',
			'foreignKey' => 'tradition_id',
			'associationForeignKey' => 'article_id',
			'unique' => true,
			'conditions' => 'Article.published=1',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Audio' => array(
			'className' => 'Audio',
			'joinTable' => 'traditions_audios',
			'foreignKey' => 'tradition_id',
			'associationForeignKey' => 'audio_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Authority' => array(
			'className' => 'Authority',
			'joinTable' => 'traditions_authorities',
			'foreignKey' => 'tradition_id',
			'associationForeignKey' => 'authority_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Condition' => array(
			'className' => 'Condition',
			'joinTable' => 'traditions_conditions',
			'foreignKey' => 'tradition_id',
			'associationForeignKey' => 'condition_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Event' => array(
			'className' => 'Event',
			'joinTable' => 'traditions_events',
			'foreignKey' => 'tradition_id',
			'associationForeignKey' => 'event_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Organization' => array(
			'className' => 'Organization',
			'joinTable' => 'traditions_organizations',
			'foreignKey' => 'tradition_id',
			'associationForeignKey' => 'organization_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Product' => array(
			'className' => 'Product',
			'joinTable' => 'traditions_products',
			'foreignKey' => 'tradition_id',
			'associationForeignKey' => 'product_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Publication' => array(
			'className' => 'Publication',
			'joinTable' => 'traditions_publications',
			'foreignKey' => 'tradition_id',
			'associationForeignKey' => 'publication_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Story' => array(
			'className' => 'Story',
			'joinTable' => 'traditions_stories',
			'foreignKey' => 'tradition_id',
			'associationForeignKey' => 'story_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Video' => array(
			'className' => 'Video',
			'joinTable' => 'traditions_videos',
			'foreignKey' => 'tradition_id',
			'associationForeignKey' => 'video_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
}
?>
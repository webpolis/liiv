<?php
class User extends AppModel {
	var $name = 'User';
	var $validate = array(
		'username' => array('alphaNumeric' => array('rule' => '/^[\w\d]*$/','message' => 'Alphanumeric characters only'),
                                    'notEmpty' => array('rule' => 'notEmpty', 'message' => 'This field is mandatory')),
		'password' => array('notEmpty' => array('rule' => 'notEmpty', 'message' => 'This field is mandatory')),
		'email' => array('email' => array('rule' => 'email','message' => 'Please supply a valid email address.'),
                                'isUnique' => array('rule' => 'isUnique', 'message' => 'That address is already in use'))
            );
	// The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	var $hasMany = array(
		'Story' => array(
			'className' => 'Story',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Resolution' => array(
			'className' => 'Resolution',
			'foreignKey' => 'user_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ConditionCreated' => array(
			'className' => 'Condition',
			'foreignKey' => 'createdby'
		),
		'ConditionUpdated' => array(
			'className' => 'Condition',
			'foreignKey' => 'updatedby'
		)
	);
	var $displayField = 'email';

	/**
	 * @param  array $data
	 * @return array
	 */
	function hashPasswords($data) {
		if (isset($data['User']['password'])) {
			if (!empty($data['User']['password'])) {
				$data['User']['password'] = md5($data['User']['password']);
			}
			else {
				unset($data['User']['password']);
			}
		}
		return $data;
	}
}
?>
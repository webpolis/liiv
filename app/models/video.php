<?php
class Video extends AppModel {
	var $name = 'Video';

	var $actsAs = array(/*'Sluggable' => array('slug'=>'url'),*/ 'Searchable');

	var $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'The title is empty'
			),
			'maxLength' => array(
				'rule' => array('maxLength', 70),
				'message' => 'The title has more than 70 characters'
			)
		),
		'content' => array('notempty'),
		'url' => array(
/*
			'rule-1' => array(
				'rule' => 'notEmpty',
				'message' => VALIDATION_NOT_EMPTY,
				'last' => true
			),
*/
			'rule-2' => array(
				'rule' => array('maxLength', 100),
				'message' => 'This field can\'t contain more than 100 characters',
				'last' => true
			),
			'rule-3' => array(
				'rule' => '/^[a-z0-9\\-]*$/',
				'message' => 'Path can only contain lowercase letters, numbers and "-", and can\'t contain spaces',
				'last' => true
			),
			'rule-4' => array(
				'rule' => 'isUnique',
				'message' => VALIDATION_EXISTING_PATH,
				'last' => true
			)
		)
	);

	var $order = array('videoseries_order ASC');

	// The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Area' => array(
			'className' => 'Area',
			'foreignKey' => 'area_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Videoseries' => array(
			'className' => 'Videoseries',
			'foreignKey' => 'videoseries_id',
			'conditions' => '',
			'fields' => ''
		),
		'CreatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'createdby',
			'fields' => array('username', 'email')
		),
		'UpdatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'updatedby',
			'fields' => array('username', 'email')
		)
	);

	var $hasAndBelongsToMany = array(
		'Article' => array(
			'className' => 'Article',
			'joinTable' => 'articles_videos',
			'foreignKey' => 'video_id',
			'associationForeignKey' => 'article_id',
			'unique' => true,
			'conditions' => 'Article.published=1',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Story' => array(
			'className' => 'Story',
			'joinTable' => 'stories_videos',
			'foreignKey' => 'video_id',
			'associationForeignKey' => 'story_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Audio' => array(
			'className' => 'Audio',
			'joinTable' => 'videos_audios',
			'foreignKey' => 'video_id',
			'associationForeignKey' => 'audio_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Authority' => array(
			'className' => 'Authority',
			'joinTable' => 'videos_authorities',
			'foreignKey' => 'video_id',
			'associationForeignKey' => 'authority_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Condition' => array(
			'className' => 'Condition',
			'joinTable' => 'videos_conditions',
			'foreignKey' => 'video_id',
			'associationForeignKey' => 'condition_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Event' => array(
			'className' => 'Event',
			'joinTable' => 'videos_events',
			'foreignKey' => 'video_id',
			'associationForeignKey' => 'event_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Organization' => array(
			'className' => 'Organization',
			'joinTable' => 'videos_organizations',
			'foreignKey' => 'video_id',
			'associationForeignKey' => 'organization_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Publication' => array(
			'className' => 'Publication',
			'joinTable' => 'videos_publications',
			'foreignKey' => 'video_id',
			'associationForeignKey' => 'publication_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Therapy' => array(
			'className' => 'Therapy',
			'joinTable' => 'videos_therapies',
			'foreignKey' => 'video_id',
			'associationForeignKey' => 'therapy_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'VideoRelated' => array(
			'className' => 'Video',
			'joinTable' => 'videos_videos',
			'foreignKey' => 'video_id',
			'associationForeignKey' => 'video_sec_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

	function beforeSave() {
		parent::beforeSave();
		$data =& $this->data[$this->name];
		if (empty($data['url'])) {
			$data['url'] = $this->getUniqueUrl($data['title'], 'url');
		}
		return true;
	}
}
?>
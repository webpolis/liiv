<?php
class Videoseries extends AppModel {

	var $name = 'Videoseries';
        var $useTable ="videoseries";
	var $validate = array(
		'title' => array('notempty')
	);
        var $hasMany = array("Video" => array(
              "className" => "Video",
              "foreignKey" =>"videoseries_id"
        ));

}
?>
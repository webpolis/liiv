<?php 
/* SVN FILE: $Id$ */
/* ArticlesAuthoritiesController Test cases generated on: 2009-10-28 12:12:49 : 1256739169*/
App::import('Controller', 'ArticlesAuthorities');

class TestArticlesAuthorities extends ArticlesAuthoritiesController {
	var $autoRender = false;
}

class ArticlesAuthoritiesControllerTest extends CakeTestCase {
	var $ArticlesAuthorities = null;

	function startTest() {
		$this->ArticlesAuthorities = new TestArticlesAuthorities();
		$this->ArticlesAuthorities->constructClasses();
	}

	function testArticlesAuthoritiesControllerInstance() {
		$this->assertTrue(is_a($this->ArticlesAuthorities, 'ArticlesAuthoritiesController'));
	}

	function endTest() {
		unset($this->ArticlesAuthorities);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* AudiosController Test cases generated on: 2009-10-28 12:03:56 : 1256738636*/
App::import('Controller', 'Audios');

class TestAudios extends AudiosController {
	var $autoRender = false;
}

class AudiosControllerTest extends CakeTestCase {
	var $Audios = null;

	function startTest() {
		$this->Audios = new TestAudios();
		$this->Audios->constructClasses();
	}

	function testAudiosControllerInstance() {
		$this->assertTrue(is_a($this->Audios, 'AudiosController'));
	}

	function endTest() {
		unset($this->Audios);
	}
}
?>
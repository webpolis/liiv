<?php 
/* SVN FILE: $Id$ */
/* AuthoritiesController Test cases generated on: 2009-10-28 12:04:39 : 1256738679*/
App::import('Controller', 'Authorities');

class TestAuthorities extends AuthoritiesController {
	var $autoRender = false;
}

class AuthoritiesControllerTest extends CakeTestCase {
	var $Authorities = null;

	function startTest() {
		$this->Authorities = new TestAuthorities();
		$this->Authorities->constructClasses();
	}

	function testAuthoritiesControllerInstance() {
		$this->assertTrue(is_a($this->Authorities, 'AuthoritiesController'));
	}

	function endTest() {
		unset($this->Authorities);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* ConditionsController Test cases generated on: 2009-10-28 12:22:27 : 1256739747*/
App::import('Controller', 'Conditions');

class TestConditions extends ConditionsController {
	var $autoRender = false;
}

class ConditionsControllerTest extends CakeTestCase {
	var $Conditions = null;

	function startTest() {
		$this->Conditions = new TestConditions();
		$this->Conditions->constructClasses();
	}

	function testConditionsControllerInstance() {
		$this->assertTrue(is_a($this->Conditions, 'ConditionsController'));
	}

	function endTest() {
		unset($this->Conditions);
	}
}
?>
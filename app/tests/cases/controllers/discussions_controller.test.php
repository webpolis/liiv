<?php 
/* SVN FILE: $Id$ */
/* DiscussionsController Test cases generated on: 2009-10-28 14:08:53 : 1256746133*/
App::import('Controller', 'Discussions');

class TestDiscussions extends DiscussionsController {
	var $autoRender = false;
}

class DiscussionsControllerTest extends CakeTestCase {
	var $Discussions = null;

	function startTest() {
		$this->Discussions = new TestDiscussions();
		$this->Discussions->constructClasses();
	}

	function testDiscussionsControllerInstance() {
		$this->assertTrue(is_a($this->Discussions, 'DiscussionsController'));
	}

	function endTest() {
		unset($this->Discussions);
	}
}
?>
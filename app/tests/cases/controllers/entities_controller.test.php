<?php 
/* SVN FILE: $Id$ */
/* EntitiesController Test cases generated on: 2009-10-28 14:25:45 : 1256747145*/
App::import('Controller', 'Entities');

class TestEntities extends EntitiesController {
	var $autoRender = false;
}

class EntitiesControllerTest extends CakeTestCase {
	var $Entities = null;

	function startTest() {
		$this->Entities = new TestEntities();
		$this->Entities->constructClasses();
	}

	function testEntitiesControllerInstance() {
		$this->assertTrue(is_a($this->Entities, 'EntitiesController'));
	}

	function endTest() {
		unset($this->Entities);
	}
}
?>
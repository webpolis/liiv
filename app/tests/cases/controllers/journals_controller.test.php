<?php 
/* SVN FILE: $Id$ */
/* JournalsController Test cases generated on: 2009-10-28 12:06:28 : 1256738788*/
App::import('Controller', 'Journals');

class TestJournals extends JournalsController {
	var $autoRender = false;
}

class JournalsControllerTest extends CakeTestCase {
	var $Journals = null;

	function startTest() {
		$this->Journals = new TestJournals();
		$this->Journals->constructClasses();
	}

	function testJournalsControllerInstance() {
		$this->assertTrue(is_a($this->Journals, 'JournalsController'));
	}

	function endTest() {
		unset($this->Journals);
	}
}
?>
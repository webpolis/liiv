<?php 
/* SVN FILE: $Id$ */
/* OrganizationsController Test cases generated on: 2009-10-28 12:06:35 : 1256738795*/
App::import('Controller', 'Organizations');

class TestOrganizations extends OrganizationsController {
	var $autoRender = false;
}

class OrganizationsControllerTest extends CakeTestCase {
	var $Organizations = null;

	function startTest() {
		$this->Organizations = new TestOrganizations();
		$this->Organizations->constructClasses();
	}

	function testOrganizationsControllerInstance() {
		$this->assertTrue(is_a($this->Organizations, 'OrganizationsController'));
	}

	function endTest() {
		unset($this->Organizations);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* PagesController Test cases generated on: 2009-10-28 12:06:40 : 1256738800*/
App::import('Controller', 'Pages');

class TestPages extends PagesController {
	var $autoRender = false;
}

class PagesControllerTest extends CakeTestCase {
	var $Pages = null;

	function startTest() {
		$this->Pages = new TestPages();
		$this->Pages->constructClasses();
	}

	function testPagesControllerInstance() {
		$this->assertTrue(is_a($this->Pages, 'PagesController'));
	}

	function endTest() {
		unset($this->Pages);
	}
}
?>
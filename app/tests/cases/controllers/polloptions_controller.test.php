<?php 
/* SVN FILE: $Id$ */
/* PolloptionsController Test cases generated on: 2009-11-27 17:28:53 : 1259353733*/
App::import('Controller', 'Polloptions');

class TestPolloptions extends PolloptionsController {
	var $autoRender = false;
}

class PolloptionsControllerTest extends CakeTestCase {
	var $Polloptions = null;

	function startTest() {
		$this->Polloptions = new TestPolloptions();
		$this->Polloptions->constructClasses();
	}

	function testPolloptionsControllerInstance() {
		$this->assertTrue(is_a($this->Polloptions, 'PolloptionsController'));
	}

	function endTest() {
		unset($this->Polloptions);
	}
}
?>
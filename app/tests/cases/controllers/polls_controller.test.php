<?php 
/* SVN FILE: $Id$ */
/* PollsController Test cases generated on: 2009-11-27 17:28:42 : 1259353722*/
App::import('Controller', 'Polls');

class TestPolls extends PollsController {
	var $autoRender = false;
}

class PollsControllerTest extends CakeTestCase {
	var $Polls = null;

	function startTest() {
		$this->Polls = new TestPolls();
		$this->Polls->constructClasses();
	}

	function testPollsControllerInstance() {
		$this->assertTrue(is_a($this->Polls, 'PollsController'));
	}

	function endTest() {
		unset($this->Polls);
	}
}
?>
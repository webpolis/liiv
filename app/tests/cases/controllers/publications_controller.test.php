<?php 
/* SVN FILE: $Id$ */
/* PublicationsController Test cases generated on: 2009-10-28 12:06:52 : 1256738812*/
App::import('Controller', 'Publications');

class TestPublications extends PublicationsController {
	var $autoRender = false;
}

class PublicationsControllerTest extends CakeTestCase {
	var $Publications = null;

	function startTest() {
		$this->Publications = new TestPublications();
		$this->Publications->constructClasses();
	}

	function testPublicationsControllerInstance() {
		$this->assertTrue(is_a($this->Publications, 'PublicationsController'));
	}

	function endTest() {
		unset($this->Publications);
	}
}
?>
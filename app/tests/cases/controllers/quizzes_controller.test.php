<?php 
/* SVN FILE: $Id$ */
/* QuizzesController Test cases generated on: 2009-10-28 12:06:58 : 1256738818*/
App::import('Controller', 'Quizzes');

class TestQuizzes extends QuizzesController {
	var $autoRender = false;
}

class QuizzesControllerTest extends CakeTestCase {
	var $Quizzes = null;

	function startTest() {
		$this->Quizzes = new TestQuizzes();
		$this->Quizzes->constructClasses();
	}

	function testQuizzesControllerInstance() {
		$this->assertTrue(is_a($this->Quizzes, 'QuizzesController'));
	}

	function endTest() {
		unset($this->Quizzes);
	}
}
?>
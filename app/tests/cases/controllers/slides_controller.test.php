<?php 
/* SVN FILE: $Id$ */
/* SlidesController Test cases generated on: 2009-12-22 12:13:53 : 1261491233*/
App::import('Controller', 'Slides');

class TestSlides extends SlidesController {
	var $autoRender = false;
}

class SlidesControllerTest extends CakeTestCase {
	var $Slides = null;

	function startTest() {
		$this->Slides = new TestSlides();
		$this->Slides->constructClasses();
	}

	function testSlidesControllerInstance() {
		$this->assertTrue(is_a($this->Slides, 'SlidesController'));
	}

	function endTest() {
		unset($this->Slides);
	}
}
?>
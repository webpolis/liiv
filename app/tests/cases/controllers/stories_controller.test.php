<?php 
/* SVN FILE: $Id$ */
/* StoriesController Test cases generated on: 2009-10-28 12:06:59 : 1256738819*/
App::import('Controller', 'Stories');

class TestStories extends StoriesController {
	var $autoRender = false;
}

class StoriesControllerTest extends CakeTestCase {
	var $Stories = null;

	function startTest() {
		$this->Stories = new TestStories();
		$this->Stories->constructClasses();
	}

	function testStoriesControllerInstance() {
		$this->assertTrue(is_a($this->Stories, 'StoriesController'));
	}

	function endTest() {
		unset($this->Stories);
	}
}
?>
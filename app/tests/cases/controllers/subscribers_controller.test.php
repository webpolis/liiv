<?php 
/* SVN FILE: $Id$ */
/* SubscribersController Test cases generated on: 2010-02-09 11:14:37 : 1265724877*/
App::import('Controller', 'Subscribers');

class TestSubscribers extends SubscribersController {
	var $autoRender = false;
}

class SubscribersControllerTest extends CakeTestCase {
	var $Subscribers = null;

	function startTest() {
		$this->Subscribers = new TestSubscribers();
		$this->Subscribers->constructClasses();
	}

	function testSubscribersControllerInstance() {
		$this->assertTrue(is_a($this->Subscribers, 'SubscribersController'));
	}

	function endTest() {
		unset($this->Subscribers);
	}
}
?>
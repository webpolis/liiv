<?php 
/* SVN FILE: $Id$ */
/* SubscriptionsController Test cases generated on: 2009-10-28 14:18:07 : 1256746687*/
App::import('Controller', 'Subscriptions');

class TestSubscriptions extends SubscriptionsController {
	var $autoRender = false;
}

class SubscriptionsControllerTest extends CakeTestCase {
	var $Subscriptions = null;

	function startTest() {
		$this->Subscriptions = new TestSubscriptions();
		$this->Subscriptions->constructClasses();
	}

	function testSubscriptionsControllerInstance() {
		$this->assertTrue(is_a($this->Subscriptions, 'SubscriptionsController'));
	}

	function endTest() {
		unset($this->Subscriptions);
	}
}
?>
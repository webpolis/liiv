<?php 
/* SVN FILE: $Id$ */
/* SymptomsController Test cases generated on: 2009-10-28 12:07:05 : 1256738825*/
App::import('Controller', 'Symptoms');

class TestSymptoms extends SymptomsController {
	var $autoRender = false;
}

class SymptomsControllerTest extends CakeTestCase {
	var $Symptoms = null;

	function startTest() {
		$this->Symptoms = new TestSymptoms();
		$this->Symptoms->constructClasses();
	}

	function testSymptomsControllerInstance() {
		$this->assertTrue(is_a($this->Symptoms, 'SymptomsController'));
	}

	function endTest() {
		unset($this->Symptoms);
	}
}
?>
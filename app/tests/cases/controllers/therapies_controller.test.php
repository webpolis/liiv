<?php 
/* SVN FILE: $Id$ */
/* TherapiesController Test cases generated on: 2009-10-28 14:09:00 : 1256746140*/
App::import('Controller', 'Therapies');

class TestTherapies extends TherapiesController {
	var $autoRender = false;
}

class TherapiesControllerTest extends CakeTestCase {
	var $Therapies = null;

	function startTest() {
		$this->Therapies = new TestTherapies();
		$this->Therapies->constructClasses();
	}

	function testTherapiesControllerInstance() {
		$this->assertTrue(is_a($this->Therapies, 'TherapiesController'));
	}

	function endTest() {
		unset($this->Therapies);
	}
}
?>
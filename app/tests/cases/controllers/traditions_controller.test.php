<?php 
/* SVN FILE: $Id$ */
/* TraditionsController Test cases generated on: 2009-11-23 15:11:32 : 1258999892*/
App::import('Controller', 'Traditions');

class TestTraditions extends TraditionsController {
	var $autoRender = false;
}

class TraditionsControllerTest extends CakeTestCase {
	var $Traditions = null;

	function startTest() {
		$this->Traditions = new TestTraditions();
		$this->Traditions->constructClasses();
	}

	function testTraditionsControllerInstance() {
		$this->assertTrue(is_a($this->Traditions, 'TraditionsController'));
	}

	function endTest() {
		unset($this->Traditions);
	}
}
?>
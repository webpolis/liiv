<?php 
/* SVN FILE: $Id$ */
/* TypesController Test cases generated on: 2009-10-28 12:10:12 : 1256739012*/
App::import('Controller', 'Types');

class TestTypes extends TypesController {
	var $autoRender = false;
}

class TypesControllerTest extends CakeTestCase {
	var $Types = null;

	function startTest() {
		$this->Types = new TestTypes();
		$this->Types->constructClasses();
	}

	function testTypesControllerInstance() {
		$this->assertTrue(is_a($this->Types, 'TypesController'));
	}

	function endTest() {
		unset($this->Types);
	}
}
?>
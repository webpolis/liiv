<?php 
/* SVN FILE: $Id$ */
/* VideosController Test cases generated on: 2009-10-28 12:13:20 : 1256739200*/
App::import('Controller', 'Videos');

class TestVideos extends VideosController {
	var $autoRender = false;
}

class VideosControllerTest extends CakeTestCase {
	var $Videos = null;

	function startTest() {
		$this->Videos = new TestVideos();
		$this->Videos->constructClasses();
	}

	function testVideosControllerInstance() {
		$this->assertTrue(is_a($this->Videos, 'VideosController'));
	}

	function endTest() {
		unset($this->Videos);
	}
}
?>
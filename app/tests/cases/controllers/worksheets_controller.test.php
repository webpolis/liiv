<?php 
/* SVN FILE: $Id$ */
/* WorksheetsController Test cases generated on: 2009-10-28 12:13:25 : 1256739205*/
App::import('Controller', 'Worksheets');

class TestWorksheets extends WorksheetsController {
	var $autoRender = false;
}

class WorksheetsControllerTest extends CakeTestCase {
	var $Worksheets = null;

	function startTest() {
		$this->Worksheets = new TestWorksheets();
		$this->Worksheets->constructClasses();
	}

	function testWorksheetsControllerInstance() {
		$this->assertTrue(is_a($this->Worksheets, 'WorksheetsController'));
	}

	function endTest() {
		unset($this->Worksheets);
	}
}
?>
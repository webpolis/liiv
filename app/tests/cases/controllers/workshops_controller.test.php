<?php 
/* SVN FILE: $Id$ */
/* WorkshopsController Test cases generated on: 2009-10-28 12:13:27 : 1256739207*/
App::import('Controller', 'Workshops');

class TestWorkshops extends WorkshopsController {
	var $autoRender = false;
}

class WorkshopsControllerTest extends CakeTestCase {
	var $Workshops = null;

	function startTest() {
		$this->Workshops = new TestWorkshops();
		$this->Workshops->constructClasses();
	}

	function testWorkshopsControllerInstance() {
		$this->assertTrue(is_a($this->Workshops, 'WorkshopsController'));
	}

	function endTest() {
		unset($this->Workshops);
	}
}
?>
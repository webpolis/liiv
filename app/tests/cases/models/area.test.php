<?php 
/* SVN FILE: $Id$ */
/* Area Test cases generated on: 2009-10-28 12:14:10 : 1256739250*/
App::import('Model', 'Area');

class AreaTestCase extends CakeTestCase {
	var $Area = null;
	var $fixtures = array('app.area');

	function startTest() {
		$this->Area =& ClassRegistry::init('Area');
	}

	function testAreaInstance() {
		$this->assertTrue(is_a($this->Area, 'Area'));
	}

	function testAreaFind() {
		$this->Area->recursive = -1;
		$results = $this->Area->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Area' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
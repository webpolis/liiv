<?php 
/* SVN FILE: $Id$ */
/* Article Test cases generated on: 2009-10-28 12:02:03 : 1256738523*/
App::import('Model', 'Article');

class ArticleTestCase extends CakeTestCase {
	var $Article = null;
	var $fixtures = array('app.article');

	function startTest() {
		$this->Article =& ClassRegistry::init('Article');
	}

	function testArticleInstance() {
		$this->assertTrue(is_a($this->Article, 'Article'));
	}

	function testArticleFind() {
		$this->Article->recursive = -1;
		$results = $this->Article->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Article' => array(
			'title'  => 'Lorem ipsum dolor sit amet',
			'content'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created'  => '2009-10-28 12:02:03',
			'updated'  => '2009-10-28 12:02:03',
			'publication'  => 'Lorem ipsum dolor sit amet',
			'author'  => 'Lorem ipsum dolor sit amet',
			'organization_id'  => 1,
			'description'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'area_id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
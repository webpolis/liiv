<?php 
/* SVN FILE: $Id$ */
/* ArticlesAuthority Test cases generated on: 2009-10-28 12:12:49 : 1256739169*/
App::import('Model', 'ArticlesAuthority');

class ArticlesAuthorityTestCase extends CakeTestCase {
	var $ArticlesAuthority = null;
	var $fixtures = array('app.articles_authority');

	function startTest() {
		$this->ArticlesAuthority =& ClassRegistry::init('ArticlesAuthority');
	}

	function testArticlesAuthorityInstance() {
		$this->assertTrue(is_a($this->ArticlesAuthority, 'ArticlesAuthority'));
	}

	function testArticlesAuthorityFind() {
		$this->ArticlesAuthority->recursive = -1;
		$results = $this->ArticlesAuthority->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('ArticlesAuthority' => array(
			'id'  => 1,
			'article_id'  => 1,
			'authority_id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Audio Test cases generated on: 2009-10-28 12:03:56 : 1256738636*/
App::import('Model', 'Audio');

class AudioTestCase extends CakeTestCase {
	var $Audio = null;
	var $fixtures = array('app.audio');

	function startTest() {
		$this->Audio =& ClassRegistry::init('Audio');
	}

	function testAudioInstance() {
		$this->assertTrue(is_a($this->Audio, 'Audio'));
	}

	function testAudioFind() {
		$this->Audio->recursive = -1;
		$results = $this->Audio->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Audio' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'author'  => 'Lorem ipsum dolor sit amet',
			'organization_id'  => 1,
			'description_short'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'content'  => 'Lorem ipsum dolor sit amet',
			'area_id'  => 1,
			'created'  => '2009-10-28 12:03:56',
			'updated'  => '2009-10-28 12:03:56'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
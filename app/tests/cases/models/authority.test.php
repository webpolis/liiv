<?php 
/* SVN FILE: $Id$ */
/* Authority Test cases generated on: 2009-10-28 12:04:39 : 1256738679*/
App::import('Model', 'Authority');

class AuthorityTestCase extends CakeTestCase {
	var $Authority = null;
	var $fixtures = array('app.authority');

	function startTest() {
		$this->Authority =& ClassRegistry::init('Authority');
	}

	function testAuthorityInstance() {
		$this->assertTrue(is_a($this->Authority, 'Authority'));
	}

	function testAuthorityFind() {
		$this->Authority->recursive = -1;
		$results = $this->Authority->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Authority' => array(
			'id'  => 1,
			'name'  => 'Lorem ipsum dolor sit amet',
			'title'  => 'Lorem ipsum dolor sit amet',
			'bio'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'image'  => 'Lorem ipsum dolor sit amet',
			'url'  => 'Lorem ipsum dolor sit amet',
			'area_id'  => 1,
			'created'  => '2009-10-28 12:04:39',
			'updated'  => '2009-10-28 12:04:39'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
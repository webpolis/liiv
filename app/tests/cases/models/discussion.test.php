<?php 
/* SVN FILE: $Id$ */
/* Discussion Test cases generated on: 2009-10-28 12:06:06 : 1256738766*/
App::import('Model', 'Discussion');

class DiscussionTestCase extends CakeTestCase {
	var $Discussion = null;
	var $fixtures = array('app.discussion');

	function startTest() {
		$this->Discussion =& ClassRegistry::init('Discussion');
	}

	function testDiscussionInstance() {
		$this->assertTrue(is_a($this->Discussion, 'Discussion'));
	}

	function testDiscussionFind() {
		$this->Discussion->recursive = -1;
		$results = $this->Discussion->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Discussion' => array(
			'id'  => 1,
			'condition_id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'content'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'updated'  => '2009-10-28 12:06:06',
			'created'  => '2009-10-28 12:06:06'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
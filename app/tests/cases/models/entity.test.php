<?php 
/* SVN FILE: $Id$ */
/* Entity Test cases generated on: 2009-10-28 12:06:15 : 1256738775*/
App::import('Model', 'Entity');

class EntityTestCase extends CakeTestCase {
	var $Entity = null;
	var $fixtures = array('app.entity');

	function startTest() {
		$this->Entity =& ClassRegistry::init('Entity');
	}

	function testEntityInstance() {
		$this->assertTrue(is_a($this->Entity, 'Entity'));
	}

	function testEntityFind() {
		$this->Entity->recursive = -1;
		$results = $this->Entity->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Entity' => array(
			'id'  => 1,
			'name'  => 'Lorem ipsum dolor sit amet'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Event Test cases generated on: 2009-10-28 12:06:20 : 1256738780*/
App::import('Model', 'Event');

class EventTestCase extends CakeTestCase {
	var $Event = null;
	var $fixtures = array('app.event');

	function startTest() {
		$this->Event =& ClassRegistry::init('Event');
	}

	function testEventInstance() {
		$this->assertTrue(is_a($this->Event, 'Event'));
	}

	function testEventFind() {
		$this->Event->recursive = -1;
		$results = $this->Event->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Event' => array(
			'id'  => 1,
			'name'  => 'Lorem ipsum dolor sit amet',
			'description_short'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'description_long'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'event_date'  => '2009-10-28 12:06:20',
			'link'  => 'Lorem ipsum dolor sit amet',
			'event_blob'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'tags'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'location'  => 'Lorem ipsum dolor sit amet',
			'teacher'  => 'Lorem ipsum dolor sit amet',
			'subject'  => 'Lorem ipsum dolor sit amet',
			'source'  => 'Lorem ipsum dolor sit amet',
			'area_id'  => 1,
			'created'  => '2009-10-28 12:06:20',
			'updated'  => '2009-10-28 12:06:20'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Journal Test cases generated on: 2009-10-28 12:06:28 : 1256738788*/
App::import('Model', 'Journal');

class JournalTestCase extends CakeTestCase {
	var $Journal = null;
	var $fixtures = array('app.journal');

	function startTest() {
		$this->Journal =& ClassRegistry::init('Journal');
	}

	function testJournalInstance() {
		$this->assertTrue(is_a($this->Journal, 'Journal'));
	}

	function testJournalFind() {
		$this->Journal->recursive = -1;
		$results = $this->Journal->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Journal' => array(
			'id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Organization Test cases generated on: 2009-10-28 12:06:35 : 1256738795*/
App::import('Model', 'Organization');

class OrganizationTestCase extends CakeTestCase {
	var $Organization = null;
	var $fixtures = array('app.organization');

	function startTest() {
		$this->Organization =& ClassRegistry::init('Organization');
	}

	function testOrganizationInstance() {
		$this->assertTrue(is_a($this->Organization, 'Organization'));
	}

	function testOrganizationFind() {
		$this->Organization->recursive = -1;
		$results = $this->Organization->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Organization' => array(
			'id'  => 1,
			'name'  => 'Lorem ipsum dolor sit amet',
			'website'  => 'Lorem ipsum dolor sit amet'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Poll Test cases generated on: 2009-10-28 12:06:45 : 1256738805*/
App::import('Model', 'Poll');

class PollTestCase extends CakeTestCase {
	var $Poll = null;
	var $fixtures = array('app.poll');

	function startTest() {
		$this->Poll =& ClassRegistry::init('Poll');
	}

	function testPollInstance() {
		$this->assertTrue(is_a($this->Poll, 'Poll'));
	}

	function testPollFind() {
		$this->Poll->recursive = -1;
		$results = $this->Poll->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Poll' => array(
			'id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Polloption Test cases generated on: 2009-11-27 17:28:53 : 1259353733*/
App::import('Model', 'Polloption');

class PolloptionTestCase extends CakeTestCase {
	var $Polloption = null;
	var $fixtures = array('app.polloption');

	function startTest() {
		$this->Polloption =& ClassRegistry::init('Polloption');
	}

	function testPolloptionInstance() {
		$this->assertTrue(is_a($this->Polloption, 'Polloption'));
	}

	function testPolloptionFind() {
		$this->Polloption->recursive = -1;
		$results = $this->Polloption->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Polloption' => array(
			'id'  => 1,
			'poll_id'  => 1,
			'content'  => 'Lorem ipsum dolor sit amet',
			'message'  => 'Lorem ipsum dolor sit amet',
			'order'  => 1,
			'hits'  => 1,
			'published'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
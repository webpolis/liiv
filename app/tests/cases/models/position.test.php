<?php 
/* SVN FILE: $Id$ */
/* Position Test cases generated on: 2009-12-30 14:17:40 : 1262193460*/
App::import('Model', 'Position');

class PositionTestCase extends CakeTestCase {
	var $Position = null;
	var $fixtures = array('app.position');

	function startTest() {
		$this->Position =& ClassRegistry::init('Position');
	}

	function testPositionInstance() {
		$this->assertTrue(is_a($this->Position, 'Position'));
	}

	function testPositionFind() {
		$this->Position->recursive = -1;
		$results = $this->Position->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Position' => array(
			'id'  => 1,
			'name'  => 'Lorem ipsum dolor sit amet',
			'identifier'  => 'Lorem ipsum dolor sit amet',
			'created'  => '2009-12-30 14:17:39',
			'updated'  => '2009-12-30 14:17:39'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
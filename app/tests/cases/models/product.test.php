<?php 
/* SVN FILE: $Id$ */
/* Product Test cases generated on: 2009-10-28 12:06:47 : 1256738807*/
App::import('Model', 'Product');

class ProductTestCase extends CakeTestCase {
	var $Product = null;
	var $fixtures = array('app.product');

	function startTest() {
		$this->Product =& ClassRegistry::init('Product');
	}

	function testProductInstance() {
		$this->assertTrue(is_a($this->Product, 'Product'));
	}

	function testProductFind() {
		$this->Product->recursive = -1;
		$results = $this->Product->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Product' => array(
			'id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
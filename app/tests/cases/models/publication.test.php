<?php 
/* SVN FILE: $Id$ */
/* Publication Test cases generated on: 2009-10-28 12:06:52 : 1256738812*/
App::import('Model', 'Publication');

class PublicationTestCase extends CakeTestCase {
	var $Publication = null;
	var $fixtures = array('app.publication');

	function startTest() {
		$this->Publication =& ClassRegistry::init('Publication');
	}

	function testPublicationInstance() {
		$this->assertTrue(is_a($this->Publication, 'Publication'));
	}

	function testPublicationFind() {
		$this->Publication->recursive = -1;
		$results = $this->Publication->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Publication' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'author'  => 'Lorem ipsum dolor sit amet',
			'organization_id'  => 1,
			'description_short'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'content'  => 'Lorem ipsum dolor sit amet',
			'area_id'  => 1,
			'created'  => '2009-10-28 12:06:52',
			'updated'  => '2009-10-28 12:06:52'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
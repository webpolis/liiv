<?php 
/* SVN FILE: $Id$ */
/* Quiz Test cases generated on: 2009-10-28 12:06:58 : 1256738818*/
App::import('Model', 'Quiz');

class QuizTestCase extends CakeTestCase {
	var $Quiz = null;
	var $fixtures = array('app.quiz');

	function startTest() {
		$this->Quiz =& ClassRegistry::init('Quiz');
	}

	function testQuizInstance() {
		$this->assertTrue(is_a($this->Quiz, 'Quiz'));
	}

	function testQuizFind() {
		$this->Quiz->recursive = -1;
		$results = $this->Quiz->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Quiz' => array(
			'id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
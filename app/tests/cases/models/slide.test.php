<?php 
/* SVN FILE: $Id$ */
/* Slide Test cases generated on: 2009-12-22 12:13:29 : 1261491209*/
App::import('Model', 'Slide');

class SlideTestCase extends CakeTestCase {
	var $Slide = null;
	var $fixtures = array('app.slide');

	function startTest() {
		$this->Slide =& ClassRegistry::init('Slide');
	}

	function testSlideInstance() {
		$this->assertTrue(is_a($this->Slide, 'Slide'));
	}

	function testSlideFind() {
		$this->Slide->recursive = -1;
		$results = $this->Slide->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Slide' => array(
			'id'  => 1,
			'content'  => 'Lorem ipsum dolor sit amet',
			'title'  => 'Lorem ipsum dolor sit amet',
			'description'  => 'Lorem ipsum dolor sit amet',
			'link'  => 'Lorem ipsum dolor sit amet',
			'created'  => '2009-12-22 12:13:29',
			'updated'  => '2009-12-22 12:13:29'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
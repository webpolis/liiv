<?php 
/* SVN FILE: $Id$ */
/* Story Test cases generated on: 2009-10-28 12:06:59 : 1256738819*/
App::import('Model', 'Story');

class StoryTestCase extends CakeTestCase {
	var $Story = null;
	var $fixtures = array('app.story');

	function startTest() {
		$this->Story =& ClassRegistry::init('Story');
	}

	function testStoryInstance() {
		$this->assertTrue(is_a($this->Story, 'Story'));
	}

	function testStoryFind() {
		$this->Story->recursive = -1;
		$results = $this->Story->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Story' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'content'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created'  => '2009-10-28 12:06:59',
			'updated'  => '2009-10-28 12:06:59',
			'video'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'photo'  => 'Lorem ipsum dolor sit amet',
			'area_id'  => 1,
			'user_id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
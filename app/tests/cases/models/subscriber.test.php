<?php 
/* SVN FILE: $Id$ */
/* Subscriber Test cases generated on: 2010-02-09 11:14:37 : 1265724877*/
App::import('Model', 'Subscriber');

class SubscriberTestCase extends CakeTestCase {
	var $Subscriber = null;
	var $fixtures = array('app.subscriber');

	function startTest() {
		$this->Subscriber =& ClassRegistry::init('Subscriber');
	}

	function testSubscriberInstance() {
		$this->assertTrue(is_a($this->Subscriber, 'Subscriber'));
	}

	function testSubscriberFind() {
		$this->Subscriber->recursive = -1;
		$results = $this->Subscriber->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Subscriber' => array(
			'id'  => 1,
			'email'  => 'Lorem ipsum dolor sit amet',
			'first_name'  => 'Lorem ipsum dolor sit amet',
			'last_name'  => 'Lorem ipsum dolor sit amet'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
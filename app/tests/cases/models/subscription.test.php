<?php 
/* SVN FILE: $Id$ */
/* Subscription Test cases generated on: 2009-10-28 14:18:07 : 1256746687*/
App::import('Model', 'Subscription');

class SubscriptionTestCase extends CakeTestCase {
	var $Subscription = null;
	var $fixtures = array('app.subscription');

	function startTest() {
		$this->Subscription =& ClassRegistry::init('Subscription');
	}

	function testSubscriptionInstance() {
		$this->assertTrue(is_a($this->Subscription, 'Subscription'));
	}

	function testSubscriptionFind() {
		$this->Subscription->recursive = -1;
		$results = $this->Subscription->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Subscription' => array(
			'id'  => 1,
			'type'  => 1,
			'entity_id'  => 1,
			'item'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
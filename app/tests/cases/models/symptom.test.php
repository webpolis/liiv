<?php 
/* SVN FILE: $Id$ */
/* Symptom Test cases generated on: 2009-10-28 12:07:05 : 1256738825*/
App::import('Model', 'Symptom');

class SymptomTestCase extends CakeTestCase {
	var $Symptom = null;
	var $fixtures = array('app.symptom');

	function startTest() {
		$this->Symptom =& ClassRegistry::init('Symptom');
	}

	function testSymptomInstance() {
		$this->assertTrue(is_a($this->Symptom, 'Symptom'));
	}

	function testSymptomFind() {
		$this->Symptom->recursive = -1;
		$results = $this->Symptom->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Symptom' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'definition'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'area_id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
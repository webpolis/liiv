<?php 
/* SVN FILE: $Id$ */
/* Therapy Test cases generated on: 2009-10-28 12:07:08 : 1256738828*/
App::import('Model', 'Therapy');

class TherapyTestCase extends CakeTestCase {
	var $Therapy = null;
	var $fixtures = array('app.therapy');

	function startTest() {
		$this->Therapy =& ClassRegistry::init('Therapy');
	}

	function testTherapyInstance() {
		$this->assertTrue(is_a($this->Therapy, 'Therapy'));
	}

	function testTherapyFind() {
		$this->Therapy->recursive = -1;
		$results = $this->Therapy->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Therapy' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'definition'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'area_id'  => 1,
			'beliefs'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'discussion_id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
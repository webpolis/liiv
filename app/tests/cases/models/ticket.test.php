<?php 
/* SVN FILE: $Id$ */
/* Ticket Test cases generated on: 2009-11-05 17:22:41 : 1257448961*/
App::import('Model', 'Ticket');

class TicketTestCase extends CakeTestCase {
	var $Ticket = null;
	var $fixtures = array('app.ticket', 'app.user', 'app.resolution');

	function startTest() {
		$this->Ticket =& ClassRegistry::init('Ticket');
	}

	function testTicketInstance() {
		$this->assertTrue(is_a($this->Ticket, 'Ticket'));
	}

	function testTicketFind() {
		$this->Ticket->recursive = -1;
		$results = $this->Ticket->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Ticket' => array(
			'id'  => 1,
			'user_id'  => 1,
			'subject'  => 'Lorem ipsum dolor sit amet',
			'resume'  => 'Lorem ipsum dolor sit amet',
			'created'  => '2009-11-05 17:22:41',
			'updated'  => '2009-11-05 17:22:41'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
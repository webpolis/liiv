<?php 
/* SVN FILE: $Id$ */
/* Type Test cases generated on: 2009-10-28 12:10:12 : 1256739012*/
App::import('Model', 'Type');

class TypeTestCase extends CakeTestCase {
	var $Type = null;
	var $fixtures = array('app.type');

	function startTest() {
		$this->Type =& ClassRegistry::init('Type');
	}

	function testTypeInstance() {
		$this->assertTrue(is_a($this->Type, 'Type'));
	}

	function testTypeFind() {
		$this->Type->recursive = -1;
		$results = $this->Type->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Type' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
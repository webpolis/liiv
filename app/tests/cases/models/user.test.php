<?php 
/* SVN FILE: $Id$ */
/* User Test cases generated on: 2009-10-28 12:13:00 : 1256739180*/
App::import('Model', 'User');

class UserTestCase extends CakeTestCase {
	var $User = null;
	var $fixtures = array('app.user');

	function startTest() {
		$this->User =& ClassRegistry::init('User');
	}

	function testUserInstance() {
		$this->assertTrue(is_a($this->User, 'User'));
	}

	function testUserFind() {
		$this->User->recursive = -1;
		$results = $this->User->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('User' => array(
			'id'  => 1,
			'username'  => 'Lorem ipsum dolor sit amet',
			'password'  => 'Lorem ipsum dolor sit amet',
			'email'  => 'Lorem ipsum dolor sit amet',
			'picture'  => 'Lorem ipsum dolor sit amet',
			'link'  => 'Lorem ipsum dolor sit amet',
			'group_id'  => 1,
			'created'  => '2009-10-28 12:12:51',
			'updated'  => '2009-10-28 12:12:51'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Video Test cases generated on: 2009-10-28 12:13:20 : 1256739200*/
App::import('Model', 'Video');

class VideoTestCase extends CakeTestCase {
	var $Video = null;
	var $fixtures = array('app.video');

	function startTest() {
		$this->Video =& ClassRegistry::init('Video');
	}

	function testVideoInstance() {
		$this->assertTrue(is_a($this->Video, 'Video'));
	}

	function testVideoFind() {
		$this->Video->recursive = -1;
		$results = $this->Video->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Video' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'author'  => 'Lorem ipsum dolor sit amet',
			'organization_id'  => 1,
			'description_short'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'content'  => 'Lorem ipsum dolor sit amet',
			'area_id'  => 1,
			'created'  => '2009-10-28 12:13:20',
			'updated'  => '2009-10-28 12:13:20'
		));
		$this->assertEqual($results, $expected);
	}
}
?>
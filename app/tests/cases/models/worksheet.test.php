<?php 
/* SVN FILE: $Id$ */
/* Worksheet Test cases generated on: 2009-10-28 12:13:25 : 1256739205*/
App::import('Model', 'Worksheet');

class WorksheetTestCase extends CakeTestCase {
	var $Worksheet = null;
	var $fixtures = array('app.worksheet');

	function startTest() {
		$this->Worksheet =& ClassRegistry::init('Worksheet');
	}

	function testWorksheetInstance() {
		$this->assertTrue(is_a($this->Worksheet, 'Worksheet'));
	}

	function testWorksheetFind() {
		$this->Worksheet->recursive = -1;
		$results = $this->Worksheet->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Worksheet' => array(
			'id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Workshop Test cases generated on: 2009-10-28 12:13:27 : 1256739207*/
App::import('Model', 'Workshop');

class WorkshopTestCase extends CakeTestCase {
	var $Workshop = null;
	var $fixtures = array('app.workshop');

	function startTest() {
		$this->Workshop =& ClassRegistry::init('Workshop');
	}

	function testWorkshopInstance() {
		$this->assertTrue(is_a($this->Workshop, 'Workshop'));
	}

	function testWorkshopFind() {
		$this->Workshop->recursive = -1;
		$results = $this->Workshop->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Workshop' => array(
			'id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
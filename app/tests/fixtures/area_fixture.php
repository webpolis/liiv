<?php 
/* SVN FILE: $Id$ */
/* Area Fixture generated on: 2009-10-28 12:14:10 : 1256739250*/

class AreaFixture extends CakeTestFixture {
	var $name = 'Area';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'title' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'title'  => 'Lorem ipsum dolor sit amet'
	));
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Article Fixture generated on: 2009-10-28 12:02:03 : 1256738523*/

class ArticleFixture extends CakeTestFixture {
	var $name = 'Article';
	var $fields = array(
		'title' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'content' => array('type'=>'text', 'null' => true, 'default' => NULL),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'updated' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'publication' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'author' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'organization_id' => array('type'=>'integer', 'null' => true, 'default' => NULL),
		'description' => array('type'=>'text', 'null' => true, 'default' => NULL),
		'area_id' => array('type'=>'integer', 'null' => true, 'default' => NULL),
		'indexes' => array()
	);
	var $records = array(array(
		'title'  => 'Lorem ipsum dolor sit amet',
		'content'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'created'  => '2009-10-28 12:02:03',
		'updated'  => '2009-10-28 12:02:03',
		'publication'  => 'Lorem ipsum dolor sit amet',
		'author'  => 'Lorem ipsum dolor sit amet',
		'organization_id'  => 1,
		'description'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'area_id'  => 1
	));
}
?>
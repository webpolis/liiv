<?php 
/* SVN FILE: $Id$ */
/* ArticlesAuthority Fixture generated on: 2009-10-28 12:12:49 : 1256739169*/

class ArticlesAuthorityFixture extends CakeTestFixture {
	var $name = 'ArticlesAuthority';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'article_id' => array('type'=>'integer', 'null' => true, 'default' => NULL),
		'authority_id' => array('type'=>'integer', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'article_id'  => 1,
		'authority_id'  => 1
	));
}
?>
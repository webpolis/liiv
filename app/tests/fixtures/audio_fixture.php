<?php 
/* SVN FILE: $Id$ */
/* Audio Fixture generated on: 2009-10-28 12:03:56 : 1256738636*/

class AudioFixture extends CakeTestFixture {
	var $name = 'Audio';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'title' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'author' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'organization_id' => array('type'=>'integer', 'null' => true, 'default' => NULL),
		'description_short' => array('type'=>'text', 'null' => true, 'default' => NULL),
		'content' => array('type'=>'string', 'null' => true, 'default' => NULL),
		'area_id' => array('type'=>'integer', 'null' => true, 'default' => NULL),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'updated' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'title'  => 'Lorem ipsum dolor sit amet',
		'author'  => 'Lorem ipsum dolor sit amet',
		'organization_id'  => 1,
		'description_short'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'content'  => 'Lorem ipsum dolor sit amet',
		'area_id'  => 1,
		'created'  => '2009-10-28 12:03:56',
		'updated'  => '2009-10-28 12:03:56'
	));
}
?>
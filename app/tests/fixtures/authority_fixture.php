<?php 
/* SVN FILE: $Id$ */
/* Authority Fixture generated on: 2009-10-28 12:04:39 : 1256738679*/

class AuthorityFixture extends CakeTestFixture {
	var $name = 'Authority';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'name' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'title' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'bio' => array('type'=>'text', 'null' => true, 'default' => NULL),
		'image' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'url' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'area_id' => array('type'=>'integer', 'null' => true, 'default' => NULL),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'updated' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'name'  => 'Lorem ipsum dolor sit amet',
		'title'  => 'Lorem ipsum dolor sit amet',
		'bio'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'image'  => 'Lorem ipsum dolor sit amet',
		'url'  => 'Lorem ipsum dolor sit amet',
		'area_id'  => 1,
		'created'  => '2009-10-28 12:04:39',
		'updated'  => '2009-10-28 12:04:39'
	));
}
?>
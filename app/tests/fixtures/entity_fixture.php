<?php 
/* SVN FILE: $Id$ */
/* Entity Fixture generated on: 2009-10-28 12:06:15 : 1256738775*/

class EntityFixture extends CakeTestFixture {
	var $name = 'Entity';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
		'name' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 45),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'name'  => 'Lorem ipsum dolor sit amet'
	));
}
?>
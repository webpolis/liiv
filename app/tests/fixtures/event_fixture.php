<?php 
/* SVN FILE: $Id$ */
/* Event Fixture generated on: 2009-10-28 12:06:20 : 1256738780*/

class EventFixture extends CakeTestFixture {
	var $name = 'Event';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'name' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'description_short' => array('type'=>'text', 'null' => true, 'default' => NULL),
		'description_long' => array('type'=>'text', 'null' => true, 'default' => NULL),
		'event_date' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'link' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'event_blob' => array('type'=>'text', 'null' => true, 'default' => NULL),
		'tags' => array('type'=>'text', 'null' => true, 'default' => NULL),
		'location' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'teacher' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'subject' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'source' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 64),
		'area_id' => array('type'=>'integer', 'null' => true, 'default' => NULL),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'updated' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'name'  => 'Lorem ipsum dolor sit amet',
		'description_short'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'description_long'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'event_date'  => '2009-10-28 12:06:20',
		'link'  => 'Lorem ipsum dolor sit amet',
		'event_blob'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'tags'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'location'  => 'Lorem ipsum dolor sit amet',
		'teacher'  => 'Lorem ipsum dolor sit amet',
		'subject'  => 'Lorem ipsum dolor sit amet',
		'source'  => 'Lorem ipsum dolor sit amet',
		'area_id'  => 1,
		'created'  => '2009-10-28 12:06:20',
		'updated'  => '2009-10-28 12:06:20'
	));
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Group Fixture generated on: 2009-10-28 12:12:03 : 1256739123*/

class GroupFixture extends CakeTestFixture {
	var $name = 'Group';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'title' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 40),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'title'  => 'Lorem ipsum dolor sit amet'
	));
}
?>
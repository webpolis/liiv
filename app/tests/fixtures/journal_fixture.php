<?php 
/* SVN FILE: $Id$ */
/* Journal Fixture generated on: 2009-10-28 12:06:28 : 1256738788*/

class JournalFixture extends CakeTestFixture {
	var $name = 'Journal';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1
	));
}
?>
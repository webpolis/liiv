<?php 
/* SVN FILE: $Id$ */
/* Organization Fixture generated on: 2009-10-28 12:06:35 : 1256738795*/

class OrganizationFixture extends CakeTestFixture {
	var $name = 'Organization';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'name' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'website' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'name'  => 'Lorem ipsum dolor sit amet',
		'website'  => 'Lorem ipsum dolor sit amet'
	));
}
?>
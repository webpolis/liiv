<?php 
/* SVN FILE: $Id$ */
/* Poll Fixture generated on: 2009-10-28 12:06:45 : 1256738805*/

class PollFixture extends CakeTestFixture {
	var $name = 'Poll';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1
	));
}
?>
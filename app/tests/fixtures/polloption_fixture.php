<?php 
/* SVN FILE: $Id$ */
/* Polloption Fixture generated on: 2009-11-27 17:28:53 : 1259353733*/

class PolloptionFixture extends CakeTestFixture {
	var $name = 'Polloption';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
		'poll_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
		'content' => array('type'=>'string', 'null' => true, 'default' => NULL),
		'message' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 512),
		'order' => array('type'=>'integer', 'null' => true, 'default' => NULL),
		'hits' => array('type'=>'integer', 'null' => true, 'default' => NULL, 'length' => 10),
		'published' => array('type'=>'boolean', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'poll_id'  => 1,
		'content'  => 'Lorem ipsum dolor sit amet',
		'message'  => 'Lorem ipsum dolor sit amet',
		'order'  => 1,
		'hits'  => 1,
		'published'  => 1
	));
}
?>
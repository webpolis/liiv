<?php 
/* SVN FILE: $Id$ */
/* Position Fixture generated on: 2009-12-30 14:17:39 : 1262193459*/

class PositionFixture extends CakeTestFixture {
	var $name = 'Position';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
		'name' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 80),
		'identifier' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 45),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'updated' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'name'  => 'Lorem ipsum dolor sit amet',
		'identifier'  => 'Lorem ipsum dolor sit amet',
		'created'  => '2009-12-30 14:17:39',
		'updated'  => '2009-12-30 14:17:39'
	));
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Product Fixture generated on: 2009-10-28 12:06:47 : 1256738807*/

class ProductFixture extends CakeTestFixture {
	var $name = 'Product';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1
	));
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Quiz Fixture generated on: 2009-10-28 12:06:58 : 1256738818*/

class QuizFixture extends CakeTestFixture {
	var $name = 'Quiz';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1
	));
}
?>
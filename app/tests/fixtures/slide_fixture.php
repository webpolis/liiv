<?php 
/* SVN FILE: $Id$ */
/* Slide Fixture generated on: 2009-12-22 12:13:29 : 1261491209*/

class SlideFixture extends CakeTestFixture {
	var $name = 'Slide';
	var $table = 'slides';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
		'content' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 128),
		'title' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 45),
		'description' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 512),
		'link' => array('type'=>'string', 'null' => false, 'default' => NULL),
		'created' => array('type'=>'datetime', 'null' => false, 'default' => NULL),
		'updated' => array('type'=>'datetime', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'content'  => 'Lorem ipsum dolor sit amet',
		'title'  => 'Lorem ipsum dolor sit amet',
		'description'  => 'Lorem ipsum dolor sit amet',
		'link'  => 'Lorem ipsum dolor sit amet',
		'created'  => '2009-12-22 12:13:29',
		'updated'  => '2009-12-22 12:13:29'
	));
}
?>
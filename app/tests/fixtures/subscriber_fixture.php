<?php 
/* SVN FILE: $Id$ */
/* Subscriber Fixture generated on: 2010-02-09 11:14:37 : 1265724877*/

class SubscriberFixture extends CakeTestFixture {
	var $name = 'Subscriber';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
		'email' => array('type'=>'string', 'null' => false, 'default' => NULL),
		'first_name' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 100),
		'last_name' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 100),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'email'  => 'Lorem ipsum dolor sit amet',
		'first_name'  => 'Lorem ipsum dolor sit amet',
		'last_name'  => 'Lorem ipsum dolor sit amet'
	));
}
?>
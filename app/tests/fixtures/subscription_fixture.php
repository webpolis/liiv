<?php 
/* SVN FILE: $Id$ */
/* Subscription Fixture generated on: 2009-10-28 14:18:05 : 1256746685*/

class SubscriptionFixture extends CakeTestFixture {
	var $name = 'Subscription';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
		'type' => array('type'=>'integer', 'null' => false, 'default' => '0'),
		'entity_id' => array('type'=>'integer', 'null' => true, 'default' => NULL),
		'item' => array('type'=>'integer', 'null' => false, 'default' => '0'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'type'  => 1,
		'entity_id'  => 1,
		'item'  => 1
	));
}
?>
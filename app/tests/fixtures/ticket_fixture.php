<?php 
/* SVN FILE: $Id$ */
/* Ticket Fixture generated on: 2009-11-05 17:22:41 : 1257448961*/

class TicketFixture extends CakeTestFixture {
	var $name = 'Ticket';
	var $table = 'tickets';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
		'user_id' => array('type'=>'integer', 'null' => false, 'length' => 10),
		'subject' => array('type'=>'string', 'null' => false, 'length' => 45),
		'resume' => array('type'=>'string', 'null' => false, 'length' => 1024),
		'created' => array('type'=>'datetime', 'null' => false),
		'updated' => array('type'=>'datetime', 'null' => false),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'user_id'  => 1,
		'subject'  => 'Lorem ipsum dolor sit amet',
		'resume'  => 'Lorem ipsum dolor sit amet',
		'created'  => '2009-11-05 17:22:41',
		'updated'  => '2009-11-05 17:22:41'
	));
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Type Fixture generated on: 2009-10-28 12:10:12 : 1256739012*/

class TypeFixture extends CakeTestFixture {
	var $name = 'Type';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'title' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 40),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'title'  => 'Lorem ipsum dolor sit amet'
	));
}
?>
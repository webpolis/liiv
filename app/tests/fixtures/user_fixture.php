<?php 
/* SVN FILE: $Id$ */
/* User Fixture generated on: 2009-10-28 12:12:51 : 1256739171*/

class UserFixture extends CakeTestFixture {
	var $name = 'User';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'username' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 50),
		'password' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 40),
		'email' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128, 'key' => 'index'),
		'picture' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 128),
		'link' => array('type'=>'string', 'null' => true, 'default' => NULL),
		'group_id' => array('type'=>'integer', 'null' => true, 'default' => NULL, 'length' => 10),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'updated' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'UNIQUE' => array('column' => 'email', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'username'  => 'Lorem ipsum dolor sit amet',
		'password'  => 'Lorem ipsum dolor sit amet',
		'email'  => 'Lorem ipsum dolor sit amet',
		'picture'  => 'Lorem ipsum dolor sit amet',
		'link'  => 'Lorem ipsum dolor sit amet',
		'group_id'  => 1,
		'created'  => '2009-10-28 12:12:51',
		'updated'  => '2009-10-28 12:12:51'
	));
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Worksheet Fixture generated on: 2009-10-28 12:13:25 : 1256739205*/

class WorksheetFixture extends CakeTestFixture {
	var $name = 'Worksheet';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1
	));
}
?>
<?php 
/* SVN FILE: $Id$ */
/* Workshop Fixture generated on: 2009-10-28 12:13:27 : 1256739207*/

class WorkshopFixture extends CakeTestFixture {
	var $name = 'Workshop';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1
	));
}
?>
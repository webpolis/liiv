<?
class Auth {
	
	function Auth($user,$enabled) {
		global $_SERVER;
		
		/*if($this->localIP()) 
			return false;*/
		if($enabled) {
			$msg=$_SERVER['SERVER_NAME'].$_SERVER['REDIRECT_URL'];
			if (!isset($_SERVER['PHP_AUTH_USER'])) {
				return $this->Authenticate($msg);
			} 
			else {
				if(count($user)>1) {
					$login = false;
						for($x=0;$x<count($user);$x++) {
							if( ($_SERVER['PHP_AUTH_USER'] == $user[$x]['username']) && ($_SERVER['PHP_AUTH_PW'] == $user[$x]['userpass']) )
								$login = true;
						}	
						if(!$login)
							return $this->Authenticate($msg);
						else
							return true;
					} else {
						if( ($_SERVER['PHP_AUTH_USER'] == $user[0]['username']) && ($_SERVER['PHP_AUTH_PW'] == $user[0]['userpass']) )
							return true;
						else {
							return $this->Authenticate($msg);
						} 
					}
			}			
		}
	}

	function Authenticate($msg) {
		header('WWW-Authenticate: Basic realm='.$msg.'');
		header('HTTP/1.0 401 Unauthorized');
		echo "You must enter a valid login ID and password to access this resource\n";
		exit;
	}
	
	function localIP() {
		global $REMOTE_ADDR;
		list($a,$b,$c,$d)=explode('.',$REMOTE_ADDR);
		if($a == '172')
			if($b == '16')
				if($c == '0')
					return true;
				else
					return false;
			else
				return false;
		else
			return false;
	}
}
?>
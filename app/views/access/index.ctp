<?php
// load aco parents
$acoList = "<span id=\"selectAco\"><select class=\"AroAcoList\" name=\"data[Access][Parent]\" id=\"aco\">";
$acoList .= "<option selected value=\"0\">No parent</option>";
foreach ($acos as $aco) {
    if ($aco['Aco']['parent_id'] < 1) {
        $acoList .= "<option value=\"" . $aco['Aco']['id'] . "\">" . up($aco['Aco']['alias']) .
            "</option>";
    }
}
$acoList .= "</select></span>";
// load aro parents
$aroList = "<span id=\"selectAro\"><select class=\"AroAcoList\" name=\"data[Access][Parent]\" id=\"aro\">";
$aroList .= "<option selected value=\"0\">No parent</option>";
foreach ($aros as $aro) {
    if ($aro['Aro']['parent_id'] < 1) {
        $aroList .= "<option value=\"" . $aro['Aro']['id'] . "\">" . up($aro['Aro']['alias']) .
            "</option>";
    }
}
$aroList .= "</select></span>";
echo $javascript->codeBlock("
var currentUrl = location.href;
var aro = '" . $aroList . "';
var aco = '" . $acoList . "';

j(function() {
    j('#AccessType').bind('change',
    function() {
        var chosen = eval(j('#AccessType').attr('value'));
        j('#selectBox').html(chosen);
    });
	j('.toggleList').bind('click', function()
	    {
	        var id= j(this).attr('id');
	        j('p.' + id).toggleClass('editorOn');
	    }
	);
});
function setPermission(){
    var acoid = j('input#AcoPermission').attr('value');
    var aroid = j('input#AroPermission').attr('value');
    var type = j('#PermissionType').attr('value');
    location.href = escape('/access/setPermission/'+aroid+'/'+acoid+'/'+type);
}
");

?>
<div id="accessControl">
	The access controller handles the permissions in a pyramidal way: if you belong to "Administrators", there is no need
	 to specify that you want also the same access as the "Publishers" because you're already on the top and you will have access 
	 to whole system features, unless you deny/allow access to a specific resource for your user, group or anyone. 
    <fieldset>
        <legend>New</legend>
        <?php
        echo $form->create("Access",array("url"=>"/access/add"));
        echo $form->input("Name", array("style" => "width:200px", "div" => false, "after" =>
        "&nbsp;"));
        echo $form->input("Type", array("div" => false, "options" => array("aro" =>
        "User", "aco" => "Resource")));
        echo "&nbsp;";
        echo "Parent&nbsp;<span id=\"selectBox\">" . $aroList . "</span>";
        echo $form->submit('Save', array("div" => false, 'class' => 'btn', 'id' =>
        'submitButton'));
        echo $form->end();
        ?>
    </fieldset>
    <fieldset id="permissionPanel"><legend>Permission</legend>
        <?php
        echo $form->input("Aco.Permission", array("label" => "Resources", "size" => 3,
        "div" => false, "after" => "&nbsp;", "style" => "font-size:11px"));
        echo $form->input("Aro.Permission", array("label" => "Users", "size" => 3, "div" => false,
        "after" => "&nbsp;", "style" => "font-size:11px"));
        echo $form->input("Permission.Type", array("div" => false, "multiple" => false,
        "options" => array("Allow" => "Allow", "Deny" => "Deny")));
        echo $form->submit('Set', array("div" => false, 'class' => 'btn', 'id' =>
        'setPermission', "onclick" => "javascript:void(setPermission());"));
        echo "<span id=\"finish\"></span>";
        ?>
    </fieldset>
    <br />
    <div id="wrapper">
        <div id="left" style="background:#dcdcdc;padding:5px;width:48%;">
            <h1>Resources</h1>
            <?php
            foreach ($acos as $aco) {
                if ($aco['Aco']['parent_id'] < 1) {
                    $parent = "parent" . $aco['Aco']['id'];
                    __("<h2>");
                    echo $html->para("parent", $html->link($html->image("icons/delete.gif",
                        array("border" => 0)), "javascript:if(confirm('Are you sure?')){location.href=currentUrl + '/delete/" .
                        $aco['Aco']['id'] . "/aco';}", array("escape" => false)) . $html->
                        link($html->image("icons/set.gif", array("border" => 0)),
                        "javascript:void(j('#AcoPermission').attr('value', '" . $aco['Aco']['id'] .
                        "'));", array("escape" => false, "class" => "permissionSetter")) .
                        "&nbsp;<span id=\"aco$parent\" class=\"toggleList\">" . up(preg_replace
                        ("/(.*)::(.*)/", "$1", $aco['Aco']['alias'])) . "</span>");
                    __("</h2>");
                }
                else {
                    echo $html->para("aco editorOff aco" . $parent, $html->link($html->
                        image("icons/delete.gif", array("border" => 0)),
                        "javascript:if(confirm('Are you sure?')){location.href=currentUrl + '/delete/" .
                        $aco['Aco']['id'] . "/aco';}", array("escape" => false)) . $html->
                        link($html->image("icons/set.gif", array("border" => 0)),
                        "javascript:void(j('#AcoPermission').attr('value', '" . $aco['Aco']['id'] .
                        "'));", array("escape" => false, "class" => "permissionSetter")) .
                        "&nbsp;" . preg_replace("/(.*)::(.*)/", "$1", $aco['Aco']['alias']));
                }
            }
            ?>
        </div>
        <div id="right" style="background:#f4f4f4;padding:5px;width:48%;">
            <h1>Users</h1>
            <?php
            foreach ($aros as $aro) {
                if ($aro['Aro']['parent_id'] < 1) {
                    $parent = "parent" . $aro['Aro']['id'];
                    __("<h2>");
                    $aroAlias = up(preg_replace("/(.*)::(.*)/", "$1", $aro['Aro']['alias']));
                    $deleteLink = $html->link($html->image("icons/delete.gif",array("border" => 0)), "javascript:if(confirm('Are you sure?')){location.href=currentUrl + '/delete/" .
                        $aro['Aro']['id'] . "/aro';}", array("escape" => false));
                    echo $html->para("parent", ((low($aroAlias)=="anonymous")?null:$deleteLink) . $html->
                        link($html->image("icons/set.gif", array("border" => 0)),
                        "javascript:void(j('#AroPermission').attr('value', '" . $aro['Aro']['id'] .
                        "'));", array("escape" => false, "class" => "permissionSetter")) .
                        "&nbsp;<span id=\"aro$parent\" class=\"toggleList\">" . $aroAlias . "</span>");
                    __("</h2>");
                }
                else {
                	$aroAlias = low($aro['Aro']['alias']);
                	$deleteLink = $html->link($html->image("icons/delete.gif", array("border" => 0)),"javascript:if(confirm('Are you sure?')){location.href=currentUrl + '/delete/" .
                        $aro['Aro']['id'] . "/aro';}", array("escape" => false));
                    echo $html->para("aro  editorOff aro" . $parent, ((low($aroAlias)=="anonymous")?null:$deleteLink) . $html->
                        link($html->image("icons/set.gif", array("border" => 0)),
                        "javascript:void(j('#AroPermission').attr('value', '" . $aro['Aro']['id'] .
                        "'));", array("escape" => false, "class" => "permissionSetter")) .
                        "&nbsp;" . $aroAlias);
                }
            }
            ?>
        </div>
    </div>
</div>
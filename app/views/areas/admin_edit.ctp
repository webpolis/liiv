<div class="areas form">
<?php echo $form->create('Area');?>
	<fieldset>
 		<legend><?php __('Edit Area');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('title');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Area.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Area.id'))); ?></li>
		<li><?php echo $html->link(__('List Areas', true), array('action' => 'index'));?></li>
	</ul>
</div>

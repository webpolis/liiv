<?php
echo
	$javascript->codeBlock('var tmp = {primary:[], secondary:[]}', array('inline'=>false)),
	$wyswig->attach('ArticleDescription', 'Medium');
?>
<div class="articles form">
	<?php echo $form->create('Article'); ?>
	<fieldset>
		<legend><?php __('Edit Article'); ?></legend>
		<div class="left">
			<?php
			echo
				$form->input('id'),
				$form->input('title', array('label'=>'<b>Title</b>')),
				$form->input('author', array('label'=>'<b>Author</b>', 'readonly'=>'readonly', 'style'=>'background:#CCC')),
				$form->input('area_id', array('label'=>'<b>Content area</b>')),
				$form->input('description', array('readonly'=>'readonly', 'style'=>'background:#CCC')),
				$form->input('content', array('label'=>'Link to article')),
				$form->input('Article', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Audio', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Authority', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Organization', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Product', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Symptom', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Therapy', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Event', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Publication', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Story', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Video', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('published', array('label'=>'Published'));
			?>
		</div>
		<div class="right">
			<?php
			echo
				$this->element('auto_complete', array('box'=>'ArticleRelated', 'url'=>'/admin/articles', 'referrer'=>'Article', 'cache'=>true)),
				$html->div('related', false, array('id'=>'selectedRelated'));
			?>
		</div>
	</fieldset>
	<?php echo $form->end('Submit'); ?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('Article.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Article.id'))); ?></li>
		<li><?php echo $html->link(__('List Articles', true), array('action'=>'index')); ?></li>
		<li><?php echo $html->link(__('New Article', true), array('controller'=>'articles', 'action'=>'add')); ?></li>
	</ul>
</div>

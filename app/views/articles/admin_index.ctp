<?php $paginator->options(array('url'=>$filter)); ?>
<div class="articles index">
	<h2><?php __('Articles'); ?><span class="listing-actions"><?php echo $html->link(__('New Article', true), array('action' => 'add')); ?></span></h2>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $paginator->sort('title'); ?></th>
			<th><?php echo $paginator->sort('content'); ?></th>
			<th><?php echo $paginator->sort('author'); ?></th>
			<th><?php echo $paginator->sort('description'); ?></th>
			<th><?php echo $paginator->sort('area_id'); ?></th>
			<th><?php echo $paginator->sort('published'); ?></th>
			<th><?php echo $paginator->sort('created'); ?></th>
			<th><?php echo $paginator->sort('updated'); ?></th>
			<th class="actions"><?php __('Actions'); ?></th>
		</tr>
<?php
$i = 0;
foreach ($articles as $article):
?>
		<tr<?php echo(($i++ % 2) == 0? ' class="altrow"': ''); ?>>
			<td><?php echo $article['Article']['title']; ?></td>
			<td><?php echo $html->link($article['Article']['content'], $article['Article']['content'], array('target' => '_blank')); ?></td>
			<td><?php echo $article['Article']['author']; ?></td>
			<td><?php echo $text->truncate(strip_tags($article['Article']['description']), ADMIN_TEXT_TRIM, '...', true); ?></td>
			<td><?php echo $html->link(strip_tags($article['Area']['title']), array('controller' => 'areas', 'action' => 'view', $article['Area']['id'])); ?></td>
			<td><?php echo($article['Article']['published']? 'Yes': 'No'); ?></td>
			<td><?php echo $article['Article']['created']; ?></td>
			<td><?php echo $article['Article']['updated']; ?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('action' => 'view', $article['Article']['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('action' => 'edit', $article['Article']['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('action' => 'delete', $article['Article']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $article['Article']['id'])); ?>
			</td>
		</tr>
<?php endforeach; ?>
	</table>
</div>
<p><?php
echo $paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled')); ?>
	| <?php echo $paginator->numbers(); ?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled')); ?>
</div>

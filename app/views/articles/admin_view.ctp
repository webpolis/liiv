<div class="articles view">
	<h2><?php __('Article'); ?></h2>
	<div class="actions actions-inline listing-actions">
		<ul>
			<li><?php echo $html->link(__('Edit Article', true), array('action' => 'edit', $article['Article']['id'])); ?></li>
			<li><?php echo $html->link(__('Delete Article', true), array('action' => 'delete', $article['Article']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $article['Article']['id'])); ?></li>
			<li><?php echo $html->link(__('List Articles', true), array('action' => 'index')); ?></li>
			<li><?php echo $html->link(__('New Article', true), array('action' => 'add')); ?></li>
		</ul>
	</div>
<?php
$i = 0;
$class = ' class="altrow"';
?>
	<dl>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo $article['Article']['id']; ?>&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo $article['Article']['title']; ?>&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Author'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo $article['Article']['author']; ?>&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo $article['Article']['description']; ?>&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Content'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo $article['Article']['content']; ?>&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo $html->link($article['Area']['title'], array('controller' => 'areas', 'action' => 'view', $article['Area']['id'])); ?>&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Published'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo($article['Article']['published']? 'Yes': 'No'); ?>&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo $article['CreatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($article['Article']['created'])); ?>&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo $article['UpdatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($article['Article']['updated'])); ?>&nbsp;</dd>
	</dl>
</div>
<?php
echo
	related_articles_table($article['ArticleRelated'], $html, $text),
	related_audios_table($article['Audio'], $html, $text),
	related_authorities_table($article['Authority'], $html, $text),
	related_conditions_table($article['Condition'], $html, $text),
	related_events_table($article['Event'], $html, $text),
	related_organizations_table($article['Organization'], $html, $text),
	related_publications_table($article['Publication'], $html, $text),
	related_stories_table($article['Story'], $html, $text),
	related_therapies_table($article['Therapy'], $html, $text),
	related_videos_table($article['Video'], $html, $text);
?>
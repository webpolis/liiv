<?php
$iframeSrc = (!empty($article['Article']['content'])? $article['Article']['content']: '');
$height = 1200;
$iframe = $html->tag('iframe', '', array(
	'src'	 => $iframeSrc,
	'id'	 => 'articleIframe',
	'width'	 => '100%',
	'height' => $height,
	'style'	 => 'margin:0;border:0 none;overflow:auto',
//	'frameborder' => '0',
//	'scrolling' => 'yes',
));
echo $this->element('home_white_box', array('width'=>980, 'height'=>$height, 'content'=>$iframe));
?>
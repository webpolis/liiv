<?php
echo $javascript->link(array("swfu/swfupload","swfu/handlers","swfu/fileprogress","swfu/default"),false);
echo $javascript->codeBlock("
j(function(){
    initUploaderPro(true);
});",array("inline"=>false));
?>
<div class="audios form">
    <?php echo $form->create('Audio');?>
    <fieldset>
        <legend><?php __('Add Audio');?></legend>
        <div class="left">
            <?php
            echo $form->input('title');
            echo $form->input('author');
            echo $form->input('description_short');
            echo $form->input('content',array("style"=>"width:60%;float:left;display:inline;clear:none;margin-right:5px"));
            echo '<span style="display:inline;float:left;clear:none" id="upBtnHolder"></span>';
            echo $this->element("mp3_player",array("song"=>$this->data['Audio']['content']));
            echo $form->input('area_id');
            echo $form->input('Article',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Audio',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Authority',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Organization',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Product',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Symptom',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Therapy',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Event',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Publication',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Story',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Video',array("class"=>"hidden","label"=>false,"div"=>false));
            ?>
        </div>
        <div class="right">
            <?php
            echo $html->div(false,false,array("id" =>"upBtnCancel"));
            ?>
            <span id="upBtnHolder"></span>
            <div id="upProgress"></div>
            <div id="upPercent"></div>
            <br />
            <?php
            echo $this->element("auto_complete",array("box"=>"AudioRelated","url"=>"/admin/audios","referrer"=>"Audio","cache"=>true));
            echo $html->div("related",false,array("id"=>"selectedRelated"));
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('List Audios', true), array('action' => 'index'));?></li>
        <li><?php echo $html->link(__('New Audio', true), array('controller' => 'audios', 'action' => 'add')); ?> </li>
    </ul>
</div>

<?php $paginator->options(array("url"=>$filter)); ?>
<div class="audios index">
<h2><?php __('Audios');?><span class="listing-actions"><?php echo $html->link(__('New Audio', true), array('action' => 'add')); ?></span></h2>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('author');?></th>
	<th><?php echo $paginator->sort('description_short');?></th>
	<th><?php echo $paginator->sort('content');?></th>
	<th><?php echo $paginator->sort('area_id');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('updated');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($audios as $audio):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $audio['Audio']['id']; ?>
		</td>
		<td>
			<?php echo $audio['Audio']['title']; ?>
		</td>
		<td>
			<?php echo $audio['Audio']['author']; ?>
		</td>
		<td>
			<?php echo $audio['Audio']['description_short']; ?>
		</td>
		<td>
			<?php echo $audio['Audio']['content']; ?>
		</td>
		<td>
			<?php echo $html->link($audio['Area']['title'], array('controller' => 'areas', 'action' => 'view', $audio['Area']['id'])); ?>
		</td>
		<td>
			<?php echo $audio['Audio']['created']; ?>
		</td>
		<td>
			<?php echo $audio['Audio']['updated']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $audio['Audio']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $audio['Audio']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $audio['Audio']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $audio['Audio']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
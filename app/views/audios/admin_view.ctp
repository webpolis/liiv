<div class="audios view">
<h2><?php  __('Audio');?></h2>
<div class="actions actions-inline listing-actions">
	<ul>
		<li><?php echo $html->link(__('Edit Audio', true), array('action' => 'edit', $audio['Audio']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Audio', true), array('action' => 'delete', $audio['Audio']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $audio['Audio']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Audios', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Audio', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $audio['Audio']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $audio['Audio']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Author'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $audio['Audio']['author']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description Short'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $audio['Audio']['description_short']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Content'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $audio['Audio']['content']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($audio['Area']['title'], array('controller' => 'areas', 'action' => 'view', $audio['Area']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $audio['Audio']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $audio['Audio']['updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php echo related_articles_table($audio['Article'], $html, $text);?>

<?php echo related_audios_table($audio['AudioRelated'], $html, $text);?>

<?php echo related_authorities_table($audio['Authority'], $html, $text);?>

<?php echo related_conditions_table($audio['Condition'], $html, $text);?>

<?php echo related_events_table($audio['Event'], $html, $text); ?>

<?php echo related_organizations_table($audio['Organization'], $html, $text);?>

<?php echo related_publications_table($audio['Publication'], $html, $text); ?>

<?php echo related_stories_table($audio['Story'], $html, $text); ?>

<?php echo related_symptoms_table($audio['Symptom'], $html, $text);?>

<?php echo related_therapies_table($audio['Therapy'], $html, $text); ?>

<?php echo related_videos_table($audio['Video'], $html, $text); ?>
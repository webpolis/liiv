<?php
echo $wyswig->attach(array('AuthorityBio', 'AuthorityOverview'),'Medium');
?>
<div class="authorities form">
    <?php echo $form->create('Authority');?>
    <fieldset>
        <legend><?php __('Edit Authority');?></legend>
        <div class="left">
            <?php
            echo $form->input("id");
            echo $form->input('name');
            echo $form->input('title');
            echo $form->input('practice_area');
            echo $form->input('bio',array("rows"=>2));
            echo $form->input('overview');
            echo $form->input('interview',array("style"=>"width:50%;display:inline;clear:left;float:left;margin-right:5px"));
            echo $this->element("video_uploader",array("field"=>"AuthorityInterview","folder"=>"authority"));
            echo $form->input('image',array("style"=>"width:50%;clear:left;display:inline;float:left;margin-right:5px"));
            echo $this->element("image_uploader",array("folder"=>"authority"));
            echo $form->input('url');
            $selecta = $form->input("type_id",array("div"=>false));
            echo $html->div(false,$selecta,array("style"=>"display:inline;clear:none;float:left;margin-right:5px"));
            $selectb = $form->input('area_id',array("div"=>false));
            echo $html->div(false,$selectb,array("style"=>"display:inline;clear:none;float:left"));
            echo $form->input('Article',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Audio',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Authority',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Organization',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Product',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Symptom',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Therapy',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Event',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Publication',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Story',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Video',array("class"=>"hidden","label"=>false,"div"=>false));
            ?>
        </div>
        <div class="right">
            <?php
            echo $this->element("auto_complete",array("box"=>"AuthorityRelated","url"=>"/admin/authorities","referrer"=>"Authority","cache"=>true,"additional"=>array("workshops"=>"Workshop")));
            echo $html->div("related",false,array("id"=>"selectedRelated"));
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Authority.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Authority.id'))); ?></li>
        <li><?php echo $html->link(__('List Authorities', true), array('action' => 'index'));?></li>
    </ul>
</div>

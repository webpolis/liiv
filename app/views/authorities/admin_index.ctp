<?php $paginator->options(array("url"=>$filter)); ?>
<div class="authorities index">
<h2><?php __('Authorities');?><span class="listing-actions"><?php echo $html->link(__('New Authority', true), array('action' => 'add')); ?></span></h2>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('bio');?></th>
	<th><?php echo $paginator->sort('area_id');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('updated');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($authorities as $authority):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $authority['Authority']['name']; ?>
		</td>
		<td>
			<?php echo $text->truncate(strip_tags($authority['Authority']['bio']), ADMIN_TEXT_TRIM, '...', true); ?>
		</td>
		<td>
			<?php echo $html->link($authority['Area']['title'], array('controller' => 'areas', 'action' => 'view', $authority['Area']['id'])); ?>
		</td>
		<td>
			<?php echo $authority['Authority']['created']; ?>
		</td>
		<td>
			<?php echo $authority['Authority']['updated']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $authority['Authority']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $authority['Authority']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $authority['Authority']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $authority['Authority']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
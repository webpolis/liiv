<div class="authorities view">
<h2><?php  __('Authority');?></h2>
<div class="actions actions-inline listing-actions">
	<ul>
		<li><?php echo $html->link(__('Edit Authority', true), array('action' => 'edit', $authority['Authority']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Authority', true), array('action' => 'delete', $authority['Authority']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $authority['Authority']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Authorities', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Authority', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $authority['Authority']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $authority['Authority']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Bio'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $authority['Authority']['bio']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Image'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $authority['Authority']['image']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Url'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $authority['Authority']['url']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($authority['Area']['title'], array('controller' => 'areas', 'action' => 'view', $authority['Area']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $authority['CreatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($authority['Authority']['created'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $authority['UpdatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($authority['Authority']['updated'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php echo related_articles_table($authority['Article'], $html, $text);?>

<?php echo related_audios_table($authority['Audio'], $html, $text);?>

<?php echo related_authorities_table($authority['AuthorityRelated'], $html, $text);?>

<?php echo related_conditions_table($authority['Condition'], $html, $text);?>

<?php echo related_events_table($authority['Event'], $html, $text); ?>

<?php echo related_organizations_table($authority['Organization'], $html, $text);?>

<?php echo related_publications_table($authority['Publication'], $html, $text); ?>

<?php echo related_stories_table($authority['Story'], $html, $text); ?>

<?php echo related_therapies_table($authority['Therapy'], $html, $text); ?>

<?php echo related_videos_table($authority['Video'], $html, $text); ?>
<?php
$w = (!empty($this->data['Banner']['width'])) ? $this->data['Banner']['width']: 980;
$h = (!empty($this->data['Banner']['height'])) ? $this->data['Banner']['height']: 30;
echo $javascript->codeBlock("
var banner = {w: $w, h: $h};
j(function(){
    j('#BannerWidth').bind('keyup',function(){
        var v = j('#BannerWidth').attr('value');
        if(v.match(/^\d+$/g)){
            window.banner.w = v;
        }else if(v!=''){
            showMessage('Is not a valid number');
        }
    });
    j('#BannerHeight').bind('keyup',function(){
        var v = j('#BannerHeight').attr('value');
        if(v.match(/^\d+$/g)){
            window.banner.w = v;
        }else if(v!=''){
            showMessage('Is not a valid number');
        }
    });

});
");
?>
<div class="banners form">
    <?php echo $form->create('Banner');?>
    <fieldset>
        <legend><?php __('Edit Banner');?></legend>
        <div class="left">
            <?php
            echo $form->input("id");
            echo $form->input('title');
            echo $form->input('link');
            echo $form->input('width',array("style"=>"width:32px;","div"=>false));
            echo $form->input('height',array("style"=>"width:32px;","div"=>false));
            echo $form->input('content',array("style"=>"width:70%;clear:left;display:inline;float:left;margin-right:5px"));
            echo $this->element("image_uploader",array("folder"=>"banners","allowOthers"=>true));
			echo $form->input('Position', array('multiple' => 'checkbox'));
            ?>
        </div>
        <div class="right">
        </div>
        <div style="display:block;clear:both" id="bannerPreview">
            <?php
            if(preg_match("/\.(jpg|png|gif|bmp)/si",$this->data['Banner']['content'])){
                echo $html->image($this->data['Banner']['content']);
            }
            if(preg_match("/\.(swf)/si",$this->data['Banner']['content'])){
                echo $javascript->codeBlock("
                    j(function(){
                        swfobject.embedSWF('".$this->data['Banner']['content']."','bannerPreview','".$this->data['Banner']['width']."','".$this->data['Banner']['height']."','9');
                    });
                    ",array("inline"=>false));
            }
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('List Banners', true), array('action' => 'index'));?></li>
    </ul>
</div>

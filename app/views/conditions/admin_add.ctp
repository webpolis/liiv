<?php
echo $javascript->codeBlock("
var tmp = {primary:[],secondary:[]}

function subscript(){
	j('#ConditionSymptomsList').replaceSelection('<sup>'+j('#ConditionSymptomsList').getSelection().text+'</sup>',true);
}
",array("inline"=>false));
echo $wyswig->attach(
        array(
                array(
                        'id' => 'ConditionIntroduction',
                        'layout' => 'Basic'),
                array(
                        'id' => 'ConditionDefinitionShort',
                        'layout' => 'Basic'),
                array(
                        'id' => 'ConditionDefinitionLong',
                        'layout' => 'Medium'),
                array(
                        'id' => 'ConditionHolisticApproach',
                        'layout' => 'Medium'),
                array(
                        'id' => 'ConditionStandardApproach',
                        'layout' => 'Medium'),
                 array(
                        'id' => 'ConditionReferences',
                        'layout' => 'Medium')
                ));
?>
<div class="conditions form">
    <?php echo $form->create('Condition');?>
    <fieldset>
        <legend><?php __('Add Condition');?></legend>
        <div class="left">
            <?php
            echo $form->input('title');
            echo $form->input('path', array('label' => 'Path <br /><span class="sublabel">
            This field represents the url from where this condition will be available<br />
            Paths can only include letters, numbers and "-". I.e.: "sleep-dissorders"<br />
            This condition would be accessed at http://www.liiv.com/condition/sleep-dissorders</span>'));
            echo $form->input('published');
            echo $form->input('area_id', array('label' => 'Content Area'));
            echo $form->input('introduction',array("rows"=>2));
            echo $form->input('definition_short',array("rows"=>2, 'label' => 'Short definition'));
            echo $form->input('definition_long',array("rows"=>4, 'label' => 'Long definition'));
            //echo $form->input('symptoms_text',array("rows"=>2,'label' => 'Symptoms Description'));
			echo $form->input('symptoms_list',array("rows"=>2,'label' => 'Symptoms: bulleted list<br /><span class="subLabel">Write one item per line to format them in bullets</span>'.
			'&nbsp;'.$form->button("SUPERSCRIPT",array("onclick"=>"javascript:void(subscript());")), 'style' => 'overflow: scroll;'));
           
            echo $form->input('standard_approach',array("rows"=>2, "label" => "Standard Approach"));            
            echo $form->input('holistic_approach',array("rows"=>2, "label" => "Holistic Approach"));
			echo $form->input('references',array("rows"=>2));
            echo $form->input('Article',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Audio',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Authority',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Organization',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Product',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Therapy',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Event',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Publication',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Story',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Video',array("class"=>"hidden","label"=>false,"div"=>false));
            $symp = $form->input("PrimarySymptom",array("options"=>array(),"multiple"=>true,"div"=>false));
            $symp .= $form->input("SecondarySymptom",array("options"=>array(),"multiple"=>true,"div"=>false));
            echo $html->div(false,$symp,array("style"=>"display:none"));
            ?>
        </div>
        <div class="right">
            <?php
            echo $this->element("auto_complete",array("box"=>"ConditionRelated","referrer"=>"Condition","url"=>"/admin/conditions","cache"=>true));
            echo $html->div("related",$html->div(false,false,array("id"=>"selectedSymptoms")),array("id"=>"selectedRelated"));
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('List Conditions', true), array('action' => 'index'));?></li>
    </ul>
</div>

<?php
$primarySymptoms = (isset($this->data['PrimarySymptom']))?json_encode($this->data['PrimarySymptom']):"[]";
$secondarySymptoms = (isset($this->data['PrimarySymptom']))?json_encode($this->data['SecondarySymptom']):"[]";
echo $javascript->codeBlock("
var tmp = {primary: ".$primarySymptoms.", secondary: ".$secondarySymptoms."};
var conditionsTherapies = ".((!empty($conditionsTherapies))?json_encode($conditionsTherapies):"[]")."
j(function(){
    getSymptoms();
    getTherapies();
});

function subscript(){
	j('#ConditionSymptomsList').replaceSelection('<sup>'+j('#ConditionSymptomsList').getSelection().text+'</sup>',true);
}
",array("inline"=>false));
echo $javascript->link(array("jquery.selector"),false);
echo $javascript->codeBlock("var tmp = {primary:[],secondary:[]}",array("inline"=>false));
echo $wyswig->attach(
        array(
                array(
                        'id' => 'ConditionIntroduction',
                        'layout' => 'Basic'),
                array(
                        'id' => 'ConditionDefinitionShort',
                        'layout' => 'Basic'),
                array(
                        'id' => 'ConditionDefinitionLong',
                        'layout' => 'Medium'),
                array(
                        'id' => 'ConditionHolisticApproach',
                        'layout' => 'Medium'),
                array(
                        'id' => 'ConditionStandardApproach',
                        'layout' => 'Medium'),
                 array(
                        'id' => 'ConditionReferences',
                        'layout' => 'Medium')
                ));
?>
<div class="conditions form"><?php echo $form->create('Condition');?>
<fieldset><legend><?php __('Edit Condition');?></legend>
<div class="left"><?php
echo $form->input('id');
echo $form->input('title');
echo $form->input('path', array('label' => 'Path <br /><span class="sublabel">
            This field represents the url from where this condition will be available<br />
            Paths can only include letters, numbers and "-". I.e.: "sleep-dissorders"<br />
            This condition would be accessed at http://www.liiv.com/condition/sleep-dissorders</span>', 'disabled' => 'disabled'));
echo $form->input('published');
echo $form->input('area_id', array('label' => 'Content Area'));
echo $form->input('introduction',array("rows"=>2));
echo $form->input('definition_short',array("rows"=>2, 'label' => 'Short definition'));
echo $form->input('definition_long',array("rows"=>4, 'label' => 'Long definition'));
//echo $form->input('symptoms_text',array("rows"=>2,'label' => 'Symptoms Description'));
echo $form->input('symptoms_list',array("rows"=>2,'label' => 'Symptoms: bulleted list<br /><span class="subLabel">Write one item per line to format them in bullets</span>'.
'&nbsp;'.$form->button("SUPERSCRIPT",array("onclick"=>"javascript:void(subscript());")), 'style' => 'overflow: scroll;'));
echo $form->input('standard_approach',array("rows"=>2, "label" => "Standard Approach"));
echo $form->input('holistic_approach',array("rows"=>2, "label" => "Holistic Approach"));
echo $form->input('references',array("rows"=>2));
echo $form->input('Article',array("class"=>"hidden","label"=>false,"div"=>false));
echo $form->input('Audio',array("class"=>"hidden","label"=>false,"div"=>false));
echo $form->input('Authority',array("class"=>"hidden","label"=>false,"div"=>false));
echo $form->input('Organization',array("class"=>"hidden","label"=>false,"div"=>false));
echo $form->input('Product',array("class"=>"hidden","label"=>false,"div"=>false));
echo $form->input('Therapy',array("class"=>"hidden","label"=>false,"div"=>false));
echo $form->input('Event',array("class"=>"hidden","label"=>false,"div"=>false));
echo $form->input('Publication',array("class"=>"hidden","label"=>false,"div"=>false));
echo $form->input('Story',array("class"=>"hidden","label"=>false,"div"=>false));
echo $form->input('Video',array("class"=>"hidden","label"=>false,"div"=>false));
echo $form->input("Therapy.Pro",array("style"=>"display:none","label"=>false,"options"=>array(),"multiple"=>true,"div"=>false));
echo $form->input("Therapy.Con",array("style"=>"display:none","label"=>false,"options"=>array(),"multiple"=>true,"div"=>false));
$symp = $form->input("PrimarySymptom",array("options"=>array(),"multiple"=>true,"div"=>false));
$symp .= $form->input("SecondarySymptom",array("options"=>array(),"multiple"=>true,"div"=>false));
echo $html->div(false,$symp,array("style"=>"display:none"));
?></div>
<div class="right"><?php
echo $this->element("auto_complete",array("box"=>"ConditionRelated","url"=>"/admin/conditions","referrer"=>"Condition","cache"=>true));
echo $html->div("related",$html->div(false,false,array("id"=>"selectedSymptoms")),array("id"=>"selectedRelated"));
?></div>
</fieldset>
<?php echo $form->end('Submit');?></div>
<div class="actions">
<ul>
	<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Condition.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Condition.id'))); ?></li>
	<li><?php echo $html->link(__('List Conditions', true), array('action' => 'index'));?></li>
	<li><?php echo $html->link(__('New Condition', true), array('controller' => 'conditions', 'action' => 'add')); ?>
	</li>
</ul>
</div>

<?php $paginator->options(array("url"=>$filter)); ?>
<div class="conditions index">
<h2><?php __('Conditions');?><span class="listing-actions"><?php echo $html->link(__('New Condition', true), array('action' => 'add')); ?></span></h2>

<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('Short definition','definition_short');?></th>
	<th><?php echo $paginator->sort('Long definition','definition_long');?></th>
	<th><?php echo $paginator->sort('area_id');?></th>
	<th><?php echo $paginator->sort('published');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($conditions as $condition):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $condition['Condition']['title']; ?>
		</td>
		<td>
			<?php echo $text->truncate(strip_tags($condition['Condition']['definition_short']), ADMIN_TEXT_TRIM, '...', true);?>
		</td>
		<td>
			<?php echo $text->truncate(strip_tags($condition['Condition']['definition_long']), ADMIN_TEXT_TRIM, '...', true);?>
		</td>
		<td>
			<?php echo $html->link($condition['Area']['title'], array('controller' => 'areas', 'action' => 'view', $condition['Area']['id'])); ?>
		</td>
		<td style="text-align:center;">
			<?php echo $condition['Condition']['published'] ? 'yes': 'no';?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $condition['Condition']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $condition['Condition']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $condition['Condition']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $condition['Condition']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>

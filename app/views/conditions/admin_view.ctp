<div class="conditions view">
<h2><?php  __('Condition');?></h2>
<div class="actions actions-inline listing-actions">
	<ul>
		<li><?php echo $html->link(__('Edit Condition', true), array('action' => 'edit', $condition['Condition']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Condition', true), array('action' => 'delete', $condition['Condition']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $condition['Condition']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Conditions', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Condition', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $condition['Condition']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Path'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($html->url('/conditions/'.$condition['Condition']['path'], true),'/conditions/'.$condition['Condition']['path'], array('target' => '_blank')); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Introduction'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $condition['Condition']['introduction']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Short Definition'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $condition['Condition']['definition_short']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Long Definition'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $condition['Condition']['definition_long']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Symptoms Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $condition['Condition']['symptoms_text']; ?>
			&nbsp;
		</dd>	
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Symptoms: left bulleted list'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo textToList($condition['Condition']['symptoms_list_left'], null, 12); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Symptoms: right bulleted list'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo textToList($condition['Condition']['symptoms_list_right'], null, 12); ?>
			&nbsp;
		</dd>	
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Holistic Approach'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $condition['Condition']['holistic_approach']; ?>
			&nbsp;
		</dd>
	</dl>
<?php echo related_therapies_table($condition['Therapy'], $html, $text); ?>
	<dl>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Standard Approach'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $condition['Condition']['standard_approach']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Content Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $condition['Area']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $condition['CreatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($condition['Condition']['created'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $condition['UpdatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($condition['Condition']['updated'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php echo related_articles_table($condition['Article'], $html, $text);?>

<?php echo related_audios_table($condition['Audio'], $html, $text);?>

<?php echo related_authorities_table($condition['Authority'], $html, $text);?>

<?php echo related_organizations_table($condition['Organization'], $html, $text);?>

<?php echo related_products_table($condition['Product'], $html, $text);?>

<?php echo related_events_table($condition['Event'], $html, $text); ?>

<?php echo related_publications_table($condition['Publication'], $html, $text); ?>

<?php echo related_stories_table($condition['Story'], $html, $text); ?>

<?php echo related_videos_table($condition['Video'], $html, $text); ?>
<?php
/**
 * @param array  $val
 * @param string $key
 * @param string $output
 */
function text_list(&$val, $key, &$output) {
	if (isset($val['title']))
		$output .= $val['title']."\n";
}

/*echo $javascript->codeBlock('
window.poll = '.(isset($polls)? json_encode($polls): '[]').';
j(function() {
	generatePoll();
	j("#deflong-readmore").click(function() {
		j("#deflong-remaining").fadeIn(1000);
		j(this).hide();
		return false;
	});
});
', array('inline' => false));*/

$symptoms = '';
array_walk($condition['PrimarySymptom'], 'text_list', &$symptoms);
array_walk($condition['SecondarySymptom'], 'text_list', &$symptoms);

$therapies = '';
array_walk($condition['Therapy'], 'text_list', &$therapies);

/* @var $html HtmlHelper */
?>
<div class="conditions view">
	<div class="twoColsTop"></div>
	<div class="twoColsMiddle">
		<div class="twoColsLeft">
			<h2>Condition: <?php __($condition['Condition']['title']) ?></h2>
			<?php echo $html->div('introduction', (!empty($condition['Condition']['introduction'])? $condition['Condition']['introduction']: '&nbsp;')) ?>
			<h2>Short Definition</h2>
			<?php echo $html->div('definition_short', (!empty($condition['Condition']['definition_short'])? $condition['Condition']['definition_short']: '&nbsp;')) ?>
			<?php if(!empty($condition['Condition']['definition_long'])):?>
			<h2>Long Definition</h2>
			<?php
			echo $html->div('definition_long', $condition['Condition']['definition_long']);
			endif;
/*if (!empty($condition['Condition']['definition_long'])) {
	$definition_long = $condition['Condition']['definition_long'];
	$p = '</p>';
	$def_long_1st_p = substr($definition_long, 0, strpos($definition_long, $p) + strlen($p));
	$def_long_remaining = substr($definition_long, strlen($def_long_1st_p));
	$more = '<p><a href="#" id="deflong-readmore">Read more...</a></p>
<div id="deflong-remaining" style="display:none">'.$def_long_remaining.'</div>';
	echo $html->div('definition_long', $def_long_1st_p.$more);
	unset($definition_long, $p, $def_long_1st_p, $def_long_remaining, $more);
}
else {
	echo $html->div('definition_long', '&nbsp;');
}*/
?>
			<h2>Symptoms</h2>
			<?php 
			$width = "44%";
			if(empty($condition['Condition']['symptoms_list_left']) && empty($condition['Condition']['symptoms_list_right'])){
				$width = "97%";
			}	else if(empty($condition['Condition']['symptoms_list_left']) || empty($condition['Condition']['symptoms_list_right'])){
				$width = "70%";
			}
			?>
			<?php //$out = $html->div('condition_symptoms_text', (!empty($condition['Condition']['symptoms_text'])? $condition['Condition']['symptoms_text']: '&nbsp;'), array('style'=> 'width:'.$width.';')) ?>
			<?php $out = twoColsTextToList($condition['Condition']['symptoms_list'], 'listStyle') ?>
			<?php echo $html->div('condition_symptoms', $out);?>
			<?php echo $html->div('clearer', false, array('style'=>'height:5px')) ?>
			<?php if (!empty ($condition['Condition']['standard_approach'])):?><h2>Standard Approach</h2>
			<?php echo $html->div('standard_approach', $condition['Condition']['standard_approach']) ?>
			<?php endif;?>
			<?php if(!empty($condition['Condition']['holistic_approach']) || (count($condition['Therapy']) > 0)):?>
			<h2>Holistic Approach</h2>
			<?php echo $html->div('holistic_approach', $condition['Condition']['holistic_approach']) ?>
			<?php echo $html->div('clearer', false, array('style'=>'height:5px')) ?>
			<?php endif;?>
			<?php if (!empty ($condition['Condition']['references'])):?><h2>References</h2>
			<?php echo $html->div('references', $condition['Condition']['references']); ?>
			<?php endif;?>
			<?php echo $this->element('share_this'); ?>
			<?php echo $this->element('frontend_relations'); ?>
		</div>
		<div class="twoColsRight">
			<?php echo $this->element('front_right_column') ?>
		</div>
		<div class="clearer" style="margin:0"></div>
	</div>
	<div class="twoColsBottom"></div>
	<div class="clearer" style="margin:0"></div>
</div>
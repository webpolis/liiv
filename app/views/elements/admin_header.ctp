<script type="text/javascript">window.changeUserPassword.action = "<?php echo $html->url(array('admin'=>true, 'controller'=>'users', 'action'=>'changePassword')) ?>";</script>
<?php
$image	 = $html->image('/img/ajax-loader-big-transp.gif', array('alt'=>'Loading...', 'width'=>32, 'height'=>32));
$loading = $html->para('', $image, array('id'=>'UsrChngPsswrdLoading', 'style'=>'text-align:center;display:none'));
$content = $html->div('', '', array('id'=>'UsrChngPsswrdContent', 'style'=>'display:none'));
echo
	$html->div('', '', array('id'=>'ajaxProcess')),
	$html->div('', $loading.$content, array('id'=>'UserChangePasswordDialog')),
	__('Welcome, ', true), $userInfo['User']['username'], '. ',
	$html->link($html->image('icons/Log-Out-icon.png', array('class'=>'logoutIcon')).'&nbsp;'.__('Logout', true), array('admin'=>true, 'controller'=>'users', 'action'=>'logout'), array('escape'=>false)),
	'<br />',
	$html->link(__('Change password', true), 'javascript:changeUserPassword()', array('escape'=>false));
unset($image, $loading, $content);
?>
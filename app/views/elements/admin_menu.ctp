<?php
/* Get current controller and action */
$currentController = $this->params['controller'];
$currentAction = $this->params['action'];

/*
 * Array defining menu hierarchy.
 * First level array items define main menus. These may or may not have a controller. 
 * Menu item structure is:
 * 
 * Array
 * 
 * 	title: name to display on the menu
 * 	admin: uses the admin routing
 * 	controller: controller to link to (optional, if not present item displays as plain text)
 * 	action: action to link to (optional, default is index)
 * 	
 * 	optionally, one level (only one) of subitems can be defined by adding another array 
 * 	with the same structure as another parameter
 * 
 */
 
$menuStructure = array(
	array(
		'title' => 'Conditions',
		'controller' => 'conditions'),
	array(
		'title' => 'Therapies',
		'controller' => 'therapies'),
	array(
		'title' => 'Traditions',
		'controller' => 'traditions'),
	array(
		'title' => 'General content', array(
			array(		
				'title' => 'Articles',
				'controller' => 'articles',
			),
			array(		
				'title' => 'Videos',
				'controller' => 'videos'),
                        array(
                            "title" => "Video Series",
                            "controller" => "videoseries",
                        	"disabled"=>false
                        ),
			array(		
				'title' => 'Authorities',
				'controller' => 'authorities',
				"disabled"=>false
			),
			array(		
				'title' => 'Events',
				'controller' => 'events',
				"disabled"=>false
			)
			/*array(		
				'title' => 'Audios',
				'controller' => 'audios'),*/
			/*array(		
				'title' => 'Stories',
				'controller' => 'stories'),
			/*array(		
				'title' => 'Publications',
				'controller' => 'publications'),*/

			/*array(
				'title' => 'Polls',
				'controller' => 'polls'),
			/*array(		
				'title' => 'Events',
				'controller' => 'events'),*/
			/*array(		
				'title' => 'Organizations',
				'controller' => 'organizations'),*/
			/*array(
				'title' => 'Areas',
				'controller' => 'areas'),*/
			/*array(
				'title' => 'Banner positions',
				'controller' => 'positions'
			),
			array(
			'title' => 'Banners',
			'controller' => 'banners'
			),*/
		)),
	array(
		'title' => 'Page Narratives',
		'controller' => 'pages'	
	),
array(
		'title' => 'Pictures', array(
			array(		
				'title' => 'Ads',
				'controller' => 'banners'),
			array(		
				'title' => 'Homepage Slides',
				'controller' => 'slides'
			)
		)),
	array(
		'title' => 'Polls',
		'controller' => 'polls',
		'admin' => true	
	),
	array(
		'title' => 'Liiv Today',
		array(
                    array(
                        "title"=>"All",
                        "controller"=>"liiv_todays"
                    ),
                    array(
                        "title"=>"Wellness Tip of the Day",
                        "controller"=>"liiv_todays",
                        "action"=>"index/1"
                    ),
                    array(
                        "title"=>"Intention for the Day",
                        "controller"=>"liiv_todays",
                        "action"=>"index/2"
                    ),
                    array(
                        "title"=>"Book of the Day",
                        "controller"=>"liiv_todays",
                        "action"=>"index/5"
                    ),
                    array(
                        "title"=>"The Daily Wild Card",
                        "controller"=>"liiv_todays",
                        "action"=>"index/6"
                    ),
                    array(
                        "title"=>"Daily Laugh",
                        "controller"=>"liiv_todays",
                        "action"=>"index/7"
                    )
                )
            ),
	array(
		'title' => 'User Management',
		'controller' => 'users', array(
			array(
				'title' => 'List users',
				'controller' => 'users'),
			array(
				'title' => 'Add user',
				'controller' => 'users',
				'action' => 'add'
			),
			array(
				'title' => 'List groups',
				'controller' => 'groups'),
			array(
				'title' => 'Add group',
				'controller' => 'groups',
				'action' => 'add'
			),
                        array(
							'title' => 'Access Control',
							'controller' => 'access',
                        	'admin' => false
                        )
		))
);

$menuMarkup = '<ul id="nav" class="sf-menu sf-vertical">';

foreach ($menuStructure as $menuItem) {
	if(isset($menuItem['controller'])){
		/* Default action: index */
		$action = isset($menuItem['action']) ? $menuItem['action'] : 'index';
		/* If it's set on the admin routing, the action changes to admin_<action> */
		$adminCode = true;
		$adminString = 'admin_';
		if(isset($menuItem['admin'])){
			if ($menuItem['admin'] == false){
				$adminString = '';
				$adminCode = false;	
			}
		}
		/* Check if it's selected */
		if($currentController == $menuItem['controller'] && $currentAction == ($adminString.$action)){
			$menuMarkup.= '<li class="active">'.$html->link(__($menuItem['title'], true), array("admin" => $adminCode,"controller" => $menuItem['controller'], "action" => $action));
		} else {
			$menuMarkup.= '<li>'.$html->link(__($menuItem['title'], true),array("admin" => $adminCode, "controller" => $menuItem['controller'], "action" => $action));
		}
	} else {
		/* It's only a subcategory, no action & controller */
		$menuMarkup.= '<li><a href="#" class="submenu-title">'.__($menuItem['title'],true).'</a>';
	}
	$subMenuMarkup = '';
	foreach ($menuItem as $menuSubItem) {
		if(is_array($menuSubItem)){ /* Check if it's array because it also cycles the item's options */
			foreach ($menuSubItem as $item){
				$action = isset($item['action']) ? $item['action'] : 'index';
				$adminString = 'admin_';
				$adminCode = true;
				if(isset($item['admin'])){
					if ($item['admin'] == false){
						$adminString = '';
						$adminCode = false;	
					}
				}
				if(isset($item['controller'])&&$currentController == $item['controller'] && $currentAction == ($adminString.$action)){
					$subMenuMarkup.= '<li class="active">'.$html->link(__($item['title'], true),array("admin"=>$adminCode,"controller" => $item['controller'],"action" => $action)).'</li>';
				} else {
					$disabled = (isset($item['disabled'])&&$item['disabled'])? true:false;
					if(!$disabled){
						$subMenuMarkup.= '<li>'.$html->link(__($item['title'], true),array("admin"=>$adminCode,"controller" => $item['controller'],"action" => $action)).'</li>';
					}else{
						$subMenuMarkup.= '<li>'.$html->link(__($item['title'], true),'javascript:void(alert("The item is temporarily disabled"));',array("class"=>"disabled")).'</li>';
					}
				}
			}
		}
	}
	if (strlen($subMenuMarkup) > 0){
		$menuMarkup.= '<ul>'.$subMenuMarkup.'</ul>';
	}
	$menuMarkup.= '</li>';
}

$menuMarkup.='</ul>';

echo $menuMarkup;

?>
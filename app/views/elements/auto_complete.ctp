<?php
echo $javascript->codeBlock("
j(function(){
    attachAutocompleteRelated('$box','$url','$referrer');
    getRelated();
    window.autoComplete.filter = '".((!isset($filter))?"all":$filter)."';
    j('#autoCompleteFilter').change(function(e){
        window.autoComplete.filter = e.currentTarget.value;
        window.autoComplete.filterOpen = true;
        j('#$box').trigger('keyup');
    });
});
",array("inline"=>false));
echo $javascript->link(array("jquery.scrollTo"),false);
$f = $form->input("related",array("type" => "text","label"=>((isset($title))?$title:"Related Content")."<br /><span class=\"sublabel\">(start typing to search for items)</span>","style"=>"width:70%;display:inline;clear:left;margin-right:4px","div"=>false));
$opts = array("all"=>"Filter by","articles"=>"Articles",
    "authorities"=>"Authorities","conditions" =>"Conditions","organizations"=>"Organizations","publications"=>"Publications","therapies"=>"Therapies","traditions"=>"Traditions","videos"=>"Videos");
if(isset($additional)){
    array_push($opts,$additional);
}
$f .= (isset($dropdown) &&!$dropdown)? null:$form->select("Filter",$opts,"all",array("div"=>false,"label"=>false,"style"=>"display:inline;clear:none;","id"=>"autoCompleteFilter"),false);
echo $html->div("autoCompleteBox",$f);
?>
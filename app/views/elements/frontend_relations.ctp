<?php
/**
 * @param  array  $arr
 * @param  string $controller
 * @param  HtmlHelper $html
 * @return string
 */
function arrayToText($arr=array(), $controller, &$html) {
	$ret = '';
	foreach ($arr as $v) {
		$text = (isset($v['title'])? $v['title']: (isset($v['name'])? $v['name']: ''));
		if ($text) {
			$id = ($controller=='videos'? $v['url']: $v['id']);
			$ret .= $html->link($text, array('controller' => $controller, 'action' => 'view', $id), array('class'=>'none'))."\n";
		}
	}
	return $ret;
}

echo
	'<h2>Related information:</h2>',
	$html->div('clearer', false, array('style'=>'height:12px'));

$isView = (isset($this->params['action']) && ($this->params['action'] == 'view'));
if ($modelName !== 'Page' && $isView) {
	$model = strtolower($modelName);
	$data = $$model;
	if (!empty($data)) {
		$allowed = array('video', 'article', 'publication', 'authority', 'organization', 'event', 'story');
		$n = 0;
		foreach (array_keys($data) as $assocModel) {
			if (in_array(strtolower($assocModel), $allowed)) {
				if ($assocModel == $modelName) {
					$selfRelated = $assocModel.'Related';
					$data[$assocModel] = (isset($data[$selfRelated])? $data[$selfRelated]: array());
				}
				$data[$assocModel] = array_slice($data[$assocModel], 0, 4, true);
				$controllerName = ucwords(Inflector::pluralize($assocModel));
				$list = textToList(arrayToText($data[$assocModel], strtolower($controllerName), $html), null, 4);
				# For now, don't show the "More" link, as there's no page to link to
//				$more = (!empty($list)? '<br />'.$html->link('More&nbsp;'.$html->image('icons/arrow-right-b.png', array('border'=>0)), 'javascript:void(0);', array('escape'=>false, 'class'=>'moreLink')): '');
				$more = '';
				$list = (!empty($list)? $list: __('Coming soon...', true));
				echo $html->div('related', '<h2>'.($controllerName=='Stories'? ' User Stories': $controllerName).'</h2>'.$list.$more);
				if ((++$n % 4) == 0)
					echo $html->div('clearer', false, array('style'=>'height:12px'));
			}
		}//END foreach
	}
}
?>
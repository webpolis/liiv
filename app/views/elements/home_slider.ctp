<?php
$gallery = $html->div('gallery', '', array('id'=>'homeSlider', 'style'=>'width:100%;height:310px'));
$pager = $html->div('pager', '', array('id'=>'homePager', 'style'=>'text-align:center;height:14px;margin:3px 4px 0'));
$description = $html->div('slideDescription', '', array('id'=>'homeDescription', 'style'=>'padding:2px 10px'));
echo
	$html->div(false, $gallery.$pager.$description, array('style'=>'width:310px;height:407px;margin:8px 0 0 10px;position:relative')),
	$javascript->codeBlock('
function setActiveSlide(ix) {
	var active = {},
		n = 0,
		x, o, desc;
	if (typeof(elements) != "undefined" && elements.length > 0) {
		for (x in elements) {
			o = (typeof(elements[x].Slide) == "object"? elements[x].Slide: false);
			if (o && (ix == (n++))) {
				active = o;
				break;
			}
		}
	}
	j(".pagerItem").each(function(i, e) {
		j(e).removeClass("activeSlide");
	});
	j("#slide-"+ix).toggleClass("activeSlide");
	desc = (o.description.length > 140? o.description.substr(0, 140)+"...": o.description);
	j("#homeDescription").html(desc+\'<div class="clickHere"><a class="slideLink" href="\'+o.link+\'">\'+o.link_title+\'</a> &nbsp;</div>\');
}

j(function() {
	var x, o, link;
	if (typeof(elements) != "undefined" && elements.length > 0) {
		for (x in elements) {
			o = (typeof(elements[x].Slide) == "object"? elements[x].Slide: false);
			if (o) {
				link = (o.link != ""? o.link: "#");
				j("#homeSlider").append(\'<img src="\'+o.content+\'" border="0" onclick="javascript:location.href=\\\'\'+link+\'\\\'"/>\');
			}
		}
	}
	j("#homeSlider").cycle({
		pager: "#homePager",
		pagerAnchorBuilder: function(index, DOM) {
			return \'<div class="pagerItem" onclick="javascript:void(j(\\\'#homeSlider\\\').cycle(\\\'pause\\\'));void(setActiveSlide(\\\'\'+index+\'\\\'))" id="slide-\'+index+\'"></div>\';
		},
		before: function(cur, next, opt, ff) {
			var active = (opt.currSlide + 1) % opt.slideCount;
			setActiveSlide(active);
		}
	});
	j("#slide-0").click();
	j("#homeSlider").cycle("resume");
});
', array('inline'=>false));
?>
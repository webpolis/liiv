<?php
$container = !empty($container) ? $container : "box";
$class = (isset($class))?$class : null;
$w = !empty($width) ? $width.((strstr($width,"%"))?null:"px") : "323px";

$wc = (isset($paddingLeft)) ? ((int)$width-(int)$paddingLeft)."px" : $width.'px';
$wc = (isset($paddingRight)) ? (intval($wc)-(int)$paddingRight)."px" : $wc;

$removePadding = 0;
if(isset($noPadding)) $removePadding = 5;
$h = !empty($height) ? ($height-(19-$removePadding)).("px") : "auto";
$corners = !empty($corners) ? $corners : array('top-left', 'top-right', 'bottom-left', 'bottom-right');
$topCornersWidth = 0;
$bottomCornersWidth = 0;

$topLeft = '';
$topRight = '';
$bottomLeft = '';
$bottomRight = '';

if(in_array('top-left', $corners)){
    $topLeft = $html->div("top-left", false);
    $topCornersWidth += 7;
}
if(in_array('top-right', $corners)){
    $topRight = $html->div("top-right", false);
    $topCornersWidth += 7;
}
$middleTop = $html->div("middle", false, array("style" => "width:".($w-$topCornersWidth)."px"));
$topHeader = $html->div("box-header",$topLeft.$middleTop.$topRight);

if(in_array('bottom-left', $corners)){
    $bottomLeft = $html->div("bottom-left", false);
    $bottomCornersWidth += 7;
}
if(in_array('bottom-right', $corners)){
    $bottomRight = $html->div("bottom-right", false);
    $bottomCornersWidth += 7;
}
$middleBottom = $html->div("middle", false, array("style" => "width:".($w-$bottomCornersWidth)."px"));
$bottomHeader = $html->div("box-header",$bottomLeft.$middleBottom.$bottomRight);

$content = !empty($content) ? $content : '&nbsp;';
$body = $html->div("box-content $class",$content,array("style"=>"position: relative;width:$wc;height:$h;".(isset($paddingRight)?"padding-right:".$paddingRight."px;":'').(isset($paddingLeft)?"padding-left:".$paddingLeft."px;":'').(isset($noPadding)? "padding-top: 0;":'')));
echo $html->div($container,$topHeader.$body.$bottomHeader,array("style"=>((isset($style))?$style:"")));
?>
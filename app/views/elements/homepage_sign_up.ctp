<?php
$javascript->codeBlock("
j(document).ready(function(){
	initializeSignUpForm();
});
", array('inline' => false));
$out= '<span class="boxTitleSprite signUp"></span>';
$out.= '<span style="font-size: 11px;">Sign up to receive <i>updates and surveys<br />to</i> the Liiv Community.</span><br />';
$out.= $form->create('Subscriber',array('action' => 'add'));
$out.= $form->input('first_name', array('label'=>false, 'title' => 'first name'));
$out.= $form->input('last_name', array('label'=>false, 'title' => 'last name'));
$out.= $html->div(null,
	$form->input('email', array('label'=>false, 'title' => 'email')).
	$html->tag('span', '', array('id' => 'SubscriberSubmit')).
	$html->image('ajax-loader-orange-white.gif', array('class' => 'ajaxLoader')));
$out.= $form->end();
echo $this->element("home_white_box",
		array("width"=>320,"paddingLeft"=>15,"height"=>155,"content"=>$out));?>
<?php
echo $javascript->codeBlock("
j(function(){
    initLiivTodayAccordion();
});
",array("inline"=>false));
?>
<div id="liivTodayAccordion">
    <h3 style="border-top:1px solid #B3F5FF;padding-top:3px"><a id="accordion-1" class="1" href="#">Wellness Tip of the Day</a></h3>
    <div id="liivTodayContent-1">
        <div class="left"></div>
        <div class="right"></div>
    </div>
    <h3><a class="2" href="#">Intention of the Day</a></h3>
    <div id="liivTodayContent-2">
        <div class="left"></div>
        <div class="right"></div>
    </div>
    <h3><a class="5" href="#">Book of the Day</a></h3>
    <div id="liivTodayContent-5">
        <div class="left"></div>
        <div class="right"></div>
    </div>
    <h3><a class="6" href="#">The Daily Wild Card</a></h3>
    <div id="liivTodayContent-6">
        <div class="left"></div>
        <div class="right"></div>
    </div>
</div>
<?php
echo $javascript->link(array("swfu/swfupload","swfu/fileprogress","swfu/imageUpload","swfu/handlers","dateFormat"),false);
echo $wyswig->attach(
	'data[LiivToday][content]'
);
echo $javascript->codeBlock("
window.reserved = ".$reserved.";
j(function(){
    j('#LiivTodayWhen').datepicker({
        dateFormat:'yy-mm-dd',
        changeMonth:true,
        changeYear:true,
        yearRange:'2000:2099',
        beforeShowDay:function(date){
            var when = date.format('yyyy-mm-dd');
            var available = true;
            var tooltip = '';
            if(typeof(reserved)!='undefined' && reserved.length > 0){
                for(x in reserved){
                    var e = reserved[x];
                    if(typeof(e.LiivToday)!='undefined'){
                        if(e.LiivToday.when == when){
                            available = false;
                            tooltip = e.LiivTodayType.title+': '+e.LiivToday.topic;
                        }
                    }
                }
            }
            return [available,'',tooltip];
        }
    });
});
",array("inline"=>false));
?>
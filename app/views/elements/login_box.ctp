<?php 
if(isset($userInfo)){
	$out = __('Welcome, ',true).$userInfo['User']['username'].'. '.$html->link(__('Go to my profile', true),array('controller' => 'users', 'action' => 'edit'));
	$out.= '<br />'.$html->link(__('Logout', true),array('controller' => 'users', 'action' => 'logout',"admin"=>false));
	echo $html->div('loggedBox',$out);
} else {
	echo '<div id="loginBox"></div>';	
}
?>

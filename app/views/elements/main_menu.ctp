<?php
/* Get current controller and action */
$currentController = $this->params['controller'];
$currentAction = $this->params['action'];
$pass = isset($this->params['pass'][0])? $this->params['pass'][0] : '';

function comingSoon (){
	$out = '<span class="comingSoon">'.
			'New Content Coming Soon<br />'.
			'</span>';
	return $out;
}
?>
<div id="mainMenu" class="clearfloat">
	<div class="leftEnd"></div>
	<div class="container">
	<ul>
		<li>
			<?php if ($currentController == 'pages' && $currentAction == 'display' && $pass == 'home'):?>
				<a id="menuHome" class="active">Home</a>
			<?php else: ?>
				<a id="menuHome" href="/">Home</a>
			<?php endif;?>
		</li>
		<li class="hasSubitems">
			<?php if ($currentController == 'pages' && $currentAction == 'display' && $pass == 'health'):?>
				<a id="menuYourhealth" class="active">Your health</a>
			<?php else: ?>
				<a id="menuYourhealth" href="/health">Your health</a>
			<?php endif;?>
			<div>
				<ul>
					<li><a href="/health">Health Overview</a></li>
					<li><a href="/conditions">Conditions</a></li>
					<li><a href="/therapies">Healing Therapies</a></li>
					<li><a href="/traditions">Spiritual Traditions</a></li>
					<li><a href="/stress">Stress Overview</a></li>
				</ul>
				<div class="endShadow"></div>
			</div>
		</li>
		<li>
			<?php if ($currentController == 'pages' && $currentAction == 'display' && $pass == 'liivtoday'):?>
				<a id="menuLiivtoday" class="active">Liivtoday</a>
			<?php else: ?>
				<a id="menuLiivtoday" href="/">Liivtoday</a>
			<?php endif;?>
			<?php echo comingSoon();?>	
		</li>
		<?php /* REMOVE cSoon class once this menu is used*/?>
		<li>
			<?php if ($currentController == 'pages' && $currentAction == 'display' && $pass == 'directory'):?>
				<a id="menuDirectory" class="active">Directory</a>
			<?php else: ?>
				<a id="menuDirectory" href="/" class="cSoon">Directory</a>
			<?php endif;?>	
			<?php echo comingSoon();?>	
		</li>
		<li>
			<?php if ($currentController == 'pages' && $currentAction == 'display' && $pass == 'events'):?>
				<a id="menuEvents" class="active">Events</a>
			<?php else: ?>
				<a id="menuEvents" href="/" class="cSoon">Events</a>
			<?php endif;?>	
			<?php echo comingSoon();?>	
		</li>
		<li>
			<?php  if ($currentController == 'pages' && $currentAction == 'display' && $pass == 'store'):?>
				<a id="menuProducts" class="active">Products</a>
			<?php else: ?>
				<a id="menuProducts" href="/store">Products</a>
			<?php endif;?>
		</li>
	</ul>
	</div>





	<div class="rightEnd"></div>
</div>

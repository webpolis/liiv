<?php
$loc = (isset($this->params['controller']))? "search ".$this->params['controller']."...":"search...";
$search = $form->input("SearchBox",array("title"=>"Searches in current section","value"=>$loc,"div"=>false,"label"=>false));
$search .= $form->button("Go",array("id"=>"SearchBtn","style"=>"width:30px;display: inline; clear: none; float:right"));
echo $html->div(false,$search,array("id"=>"searchBar"));
echo $javascript->codeBlock("
j(function(){
    var init = '$loc';
    j('#SearchBtn').click(makeGlobalSearch);
    j('#SearchBox').click(function(){
        var val = j('#SearchBox').attr('value');
        if(val==init){
            j('#SearchBox').attr('value','');
        }
    }).blur(function(){
        var val = j('#SearchBox').attr('value');
        if(val==''){
            j('#SearchBox').attr('value',init);
        }
    }).bind('keypress',function(e,c){
        var k = (typeof(e.keyCode)!='undefined')?e.keyCode : e.which;
        if(k==13){
            makeGlobalSearch();
        }
    });
});
",false);
?>
<?php
$title = urlencode('Liiv.com');
$url = urlencode(Router::url('',true));
$message = urlencode('Currently reading ');

$out  = $html->div('clearer', false, array('style'=>'height:5px'));
$out .= $html->tag('span', __('Share this on:', true), array('class'=>'customTitle1'));
$out .= $html->div('clearer', false, array('style'=>'height:2px'));
/* Share this JS code */
$out .= $html->div(null, '<script type="text/javascript" src="http://w.sharethis.com/button/sharethis.js#publisher=6b371232-b9d7-4750-8438-f4885e6aaa33&amp;type=website&amp;post_services=email%2Cfacebook%2Ctwitter%2Cmyspace%2Cdigg%2Csms%2Cdelicious%2Cstumbleupon%2Creddit%2Cgoogle_bmarks%2Clinkedin%2Cyahoo_bmarks%2Cfriendfeed%2Cpropeller%2Cwordpress%2Cblogger&amp;linkfg=%233a3a3a"></script>');
/* Facebook */
$out .= $html->link('Facebook', 'http://www.facebook.com/sharer.php?u='.$url.'&t='.$title, array('class' => 'facebook', 'target' => '_blank'));
/* Twitter */
$out .= $html->link('Twitter', 'http://twitter.com/home?status='.$message.$url, array('class' => 'twitter', 'target' => '_blank'));
/* StumbleUpon */
$out .= $html->link('StumbleUpon', 'http://www.stumbleupon.com/submit?url='.$url.'&title='.$title, array('class' => 'stumbleUpon', 'target' => '_blank'));
/* Digg */
$out .= $html->link('Digg', 'http://digg.com/submit?phase=2&url='.$url.'&title='.$title, array('class' => 'digg', 'target' => '_blank'));
$out .= $html->div('clearer', false, array('style'=>'height:20px'));
echo $html->div('shareThis', $out);
?>
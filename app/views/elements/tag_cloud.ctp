<?php
/* Obtain desired controller/model to build tag cloud */
$c = (isset($this->params['controller']))?$this->params['controller']:"conditions";
$tagCloudModel = (isset($tagCloudModel)) ? ucwords($tagCloudModel) : ucwords(Inflector::singularize($c));
$tagCloudController = Inflector::pluralize($tagCloudModel);
$tagNavTitle = (isset($tagNavTitle)&&!empty($tagNavTitle)) ? $tagNavTitle : $tagCloudController." Navigation";
/* Obtain array with visits and target information */
$tagCloud = $this->requestAction(array("controller"=>$tagCloudController,"action"=>"tag_cloud"),array("return","cache"=>"+1 hour"));

$max = 0; //Max value will be defined here
$tags = null;

// To find if item is selected
$route = (isset($this->params['url']['url']))? Router::parse($this->params['url']['url']) : null; 
$currentPath = (isset($route['pass']['0'])) ? $route['pass']['0'] : null;

// Get the maximum value from all the items in the tag cloud. This will be the one that gets full size.
function getMax (&$v, &$k, &$arr){
	if(isset($v['0']) && !empty($v[$arr['2']]['id']) && isset($v['0']['counter'])){
		if((int)$v['0']['counter'] > (int)$arr['1'])
			$arr['1'] = $v['0']['counter'];
	}
	if(empty($v[$arr['2']]['id'])) unset($arr['0'][$k]);
}

if(!empty($tagCloud)){
	array_walk($tagCloud,'getMax',array(&$tagCloud,&$max,&$tagCloudModel)); //Calls getMax
	foreach($tagCloud as $tc){ //Generate tag cloud
		// Calculate what is the percentage of the max value that this item has
		$per = ($tc['0']['counter']) / $max;
		
		$pt = (PT_MAX - PT_MIN) * $per + PT_MIN;
		$add = 'font-weight:normal;';
		if($per > 0.66){
			$add = 'font-weight:bold;';
		} else if ($per > 0.33){
			$add = 'font-weight:bolder;';
		}
		$tags .= '<span class="tag '.((($tc[$tagCloudModel]['path']==$currentPath) && (strtolower($this->params['controller']) == strtolower($tagCloudController)))?"currentTag":null).'" style="font-size:'.$pt.'pt;"><a style="'.$add.'" href="/'.strtolower($tagCloudController).'/'.$tc[$tagCloudModel]['path'].'">'.$tc[$tagCloudModel]['title'].'</a></span>';
	}
}

$out = $html->div("tagCloud","<h3>".ucwords($tagNavTitle)."</h3><div id=\"tags\">$tags</div>",array("id"=>$tagCloudModel."Tags"));
echo $out;
?>
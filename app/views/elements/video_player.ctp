<?php
echo $javascript->link(array('swfu/swfupload', 'swfu/handlers', 'swfu/fileprogress', 'swfu/default'), false);
echo $javascript->codeBlock('
j(function() {
    initUploaderPro(false, "VideoContent", "", "LowRes");
    initUploaderPro(false, "VideoVideoHighRes", "", "HiRes");
    initUploaderPdf(\'VideoPdf\',"Pdf");
    j("#videoPreview").dialog({
        width: default_video.w+50,
        height: default_video.h+50,
        title: "Video Preview",
        autoOpen: false,
        position: "top"
    });
    j("#previewBtnLowRes").click(function() {
        checkPreview("#VideoContent");
        getVideoPreview("#VideoContent");
    });
    j("#previewBtnHiRes").click(function() {
        checkPreview("#VideoVideoHighRes");
        getVideoPreview("#VideoVideoHighRes");
    });
});', array('inline'=>false));
?>
<script type="text/javascript" src="/player/swfobject.js"></script>

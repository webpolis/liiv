<div style="position:relative;display:inline;clear:none;position:relative;width:100%">
    <?php
    $folder = (isset($folder))?$folder:null;
    $field = (isset($field))?$field:null;
    $ran = "c".sha1(rand(10000000, 1000000000000));
    echo $javascript->link(array("swfu/swfupload","swfu/fileprogress","swfu/default","swfu/handlers"),false);
    echo $javascript->codeBlock("
j(function(){
    initUploaderPro(false,'$field','$folder','$ran');
});
    ",array("inline"=>false));
    echo $html->div(false,false,array("id"=>"upBtnHolder".$ran,"style"=>"display:inline;clear:none;float:left"));
    echo $html->div("percent",false,array("id"=>"upPercent".$ran,"style"=>"top:-8px;right:-50px"));
    ?>
    <div style="display:none" id="upProgress<?php echo $ran;?>"></div>
    <div style="display:none" id="upBtnCancel<?php echo $ran;?>"></div>
</div>
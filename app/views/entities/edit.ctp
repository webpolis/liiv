<div class="entities form">
<?php echo $form->create('Entity');?>
	<fieldset>
 		<legend><?php __('Edit Entity');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('name');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Entity.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Entity.id'))); ?></li>
		<li><?php echo $html->link(__('List Entities', true), array('action' => 'index'));?></li>
		<li><?php echo $html->link(__('List Subscriptions', true), array('controller' => 'subscriptions', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Subscription', true), array('controller' => 'subscriptions', 'action' => 'add')); ?> </li>
	</ul>
</div>

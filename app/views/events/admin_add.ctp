<?php
echo $javascript->codeBlock("
j(function(){
    j('#EventEventDate').datepicker({
        dateFormat:'yy-mm-dd'
    });
});
",array("inline"=>false));
echo $wyswig->attach(array(array('id' => 'EventDescriptionShort', 'layout' => 'Basic'), 'EventDescriptionLong', 'EventEventBlob'),'Medium');
?>
<div class="events form">
    <?php echo $form->create('Event');?>
    <fieldset>
        <legend><?php __('Add Event');?></legend>
        <div class="left">
            <?php
            echo $form->input('name');
            echo $form->input('description_short',array("rows"=>2));
            echo $form->input('description_long');
            echo $form->input('event_date',array("type"=>"text","style"=>"width:30%"));
            echo $form->input('link');
            echo $form->input('event_blob');
            echo $form->input('tags');
            echo $form->input('location');
            echo $form->input('teacher');
            echo $form->input('subject');
            echo $form->input('source');
            echo $form->input('area_id');
            echo $form->input('Article',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Audio',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Authority',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Organization',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Product',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Symptom',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Therapy',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Publication',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Story',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Video',array("class"=>"hidden","label"=>false,"div"=>false));
            ?>
        </div>
        <div class="right">
            <?php
            echo $this->element("auto_complete",array("box"=>"EventRelated","url"=>"/admin/events","referrer"=>"Event","cache"=>true,"additional"=>array("workshops"=>"Workshop")));
            echo $html->div("related",false,array("id"=>"selectedRelated"));
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('List Events', true), array('action' => 'index'));?></li>
    </ul>
</div>

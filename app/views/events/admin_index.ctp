<?php $paginator->options(array("url"=>$filter)); ?>
<div class="events index">
<h2><?php __('Events');?> <span class="listing-actions"><?php echo $html->link(__('New Event', true), array('action' => 'add')); ?></span></h2>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('description_short');?></th>
	<th><?php echo $paginator->sort('event_date');?></th>
	<th><?php echo $paginator->sort('event_blob');?></th>
	<th><?php echo $paginator->sort('tags');?></th>
	<th><?php echo $paginator->sort('location');?></th>
	<th><?php echo $paginator->sort('teacher');?></th>
	<th><?php echo $paginator->sort('subject');?></th>
	<th><?php echo $paginator->sort('source');?></th>
	<th><?php echo $paginator->sort('area_id');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('updated');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($events as $event):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $event['Event']['id']; ?>
		</td>
		<td>
			<?php echo $event['Event']['name']; ?>
		</td>
		<td>
			<?php echo $text->truncate($event['Event']['description_short'], ADMIN_TEXT_TRIM, '...', true); ?>
		</td>
		<td>
			<?php echo $event['Event']['event_date']; ?>
		</td>
		<td>
			<?php echo $text->truncate($event['Event']['event_blob'], ADMIN_TEXT_TRIM, '...', true); ?>
		</td>
		<td>
			<?php echo $event['Event']['tags']; ?>
		</td>
		<td>
			<?php echo $event['Event']['location']; ?>
		</td>
		<td>
			<?php echo $event['Event']['teacher']; ?>
		</td>
		<td>
			<?php echo $event['Event']['subject']; ?>
		</td>
		<td>
			<?php echo $event['Event']['source']; ?>
		</td>
		<td>
			<?php echo $html->link($event['Area']['title'], array('controller' => 'areas', 'action' => 'view', $event['Area']['id'])); ?>
		</td>
		<td>
			<?php echo $event['Event']['created']; ?>
		</td>
		<td>
			<?php echo $event['Event']['updated']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $event['Event']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $event['Event']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $event['Event']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $event['Event']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>

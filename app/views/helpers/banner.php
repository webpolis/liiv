<?php
class BannerHelper extends Helper {
	var $helpers = array('Html');
	private $positionModel;
	function display ($position){
		App::import('Model','Position');
		$this->positionModel = new Position();
		$p = $this->positionModel->find("first",array('conditions' => array("Position.identifier"=>$position)));
		/* Fetch random banner */
		$num = rand(0, count($p['Banner']) -1 );
		if(isset($p['Banner'][$num]))
			return $this->Html->link($this->Html->image($p['Banner'][$num]['content'], false),$p['Banner'][$num]['link'], array('title' => $p['Banner'][$num]['title'], 'target' => '_blank'), null, false);
	}
}
<?php
class WyswigHelper extends Helper {
	/* USAGE: 
	 * editor1 is the id of the textarea to replace
	 * $wyswig->attach('editor1');
	 * or
	 * $wyswig->attach(array('editor1','editor2');
	 * or 
	 * $wyswig->attach(
	 *	array(
	 *		'id' => 'ArticleDescription',
	 *		'layout' => "[Basic|Medium|Full]"
	 *	));
         *
         * 
	 */

    var $debug = false;
    var $helpers = array('Html', 'Javascript');
    function attach($fields, $layoutDefault = 'Basic') {
        $this->Javascript->link(array("ckeditor/ckeditor","ckfinder/ckfinder"),false);
        //Set default layout to basic:
        $ReturnCode = 'j(function(){';
        if(is_array($fields)) {
            if(isset($fields['id'])) {
                $id = $fields['id'];
                $layout = $fields['layout'];
                $ReturnCode .= "window.$id = CKEDITOR.replace( '$id', {toolbar: '$layout'});\n";
                $ReturnCode .= "CKFinder.SetupCKEditor($id,'/js/ckfinder/');";
            }else {
                foreach ($fields as $field) {
                    $id = '';
                    $layout = $layoutDefault;
                    if(is_array($field)) {
                        $id = $field['id'];
                        $layout = $field['layout'];
                    }else {
                        $id = $field;
                    }
                    $ReturnCode .= "window.$id = CKEDITOR.replace( '$id', {toolbar: '$layout'});\n";
                    $ReturnCode .= "CKFinder.SetupCKEditor($id,'/js/ckfinder/');";
                }
            }
        }else {
            $id = '';
            $layout = $layoutDefault;
            $id = $fields;
            $ReturnCode .= "CKEDITOR.replace( '$id', {toolbar: '$layout'});\n";
        }
        $ReturnCode .= '});';
        $this->Javascript->codeBlock($ReturnCode,array("inline"=>false));
    }
}

?>
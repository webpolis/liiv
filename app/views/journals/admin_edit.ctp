<div class="journals form">
<?php echo $form->create('Journal');?>
	<fieldset>
 		<legend><?php __('Edit Journal');?></legend>
	<?php
		echo $form->input('id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Journal.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Journal.id'))); ?></li>
		<li><?php echo $html->link(__('List Journals', true), array('action' => 'index'));?></li>
	</ul>
</div>

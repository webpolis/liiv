<div class="journals view">
<h2><?php  __('Journal');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $journal['Journal']['id']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Journal', true), array('action' => 'edit', $journal['Journal']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Journal', true), array('action' => 'delete', $journal['Journal']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $journal['Journal']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Journals', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Journal', true), array('action' => 'add')); ?> </li>
	</ul>
</div>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $html->charset(); ?>
	<title><?php echo $title_for_layout; ?></title>
	<?php
		echo $html->meta('icon', '/favicon_bak.ico');
		echo $html->css(array('liiv/jquery.ui', 'menu/superfish', 'menu/superfish-vertical', 'admin'));
		echo $javascript->link(array('jquery', 'jquery.ui', 'jquery.validate.min','jquery.selector', 'jquery.form.min', 'menu/jquery.bgiframe.min', 'menu/hoverIntent', 'menu/superfish', 'menu/supersubs', 'admin'));
		echo $javascript->codeBlock('var default_video = {w: '.VIDEO_WIDTH.', h: '.VIDEO_HEIGHT.'};');
		if (isset($additionalJs))
			echo $javascript->codeBlock($additionalJs, false);
		echo $scripts_for_layout;
	?>
</head>
<body>
	<?php
	echo
		$html->div('', '', array('id' => 'alertWindow')),
		$html->div('', '', array('id' => 'successMessage', 'style' => 'display:none')),
		$html->div('', '', array('id' => 'confirmWindow',  'style' => 'display:none'));
	?>
	<div id="container">
		<div id="header"></div>
		<div id="content">
			<div id="left_menu">
				<div id="logo"><?php echo $html->image('liiv.png', array('alt' => 'Logo', 'width' => 150)); ?></div>
				<?php
					echo
						$this->element('admin_menu'),
						$this->element('search_bar');
				?>
			</div>
			<div id="admin_header"><?php echo $this->element('admin_header'); ?></div>
			<div id="right_body">
				<?php
					$session->flash();
					echo $content_for_layout;
				?>
				<div class="clear-float"></div>
			</div>
		</div>
		<div id="footer"></div>
	</div>
	<?php echo $cakeDebug; ?>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $html->charset(); ?>
	<title><?php echo $title_for_layout; ?></title>
	<?php
	if (!isset($javascript)) {
		App::import('Helper', array('Javascript'));
		$javascript =& new JavascriptHelper();
	}
	echo
		$html->meta('icon'),
		$javascript->link(array('jquery', 'jquery.ui', 'dateFormat', 'jquery.cycle.min','jquery.form.min','jquery.validate.min', 'jquery.labelify', 'default'), false);
		$html->css(array('liiv/jquery.ui', 'style'), null, null, false);
		echo $asset->scripts_for_layout('css');
	?>
</head>
<body>
<?php

?>
	<div id="container">
		<?php
		if (in_array($this->params['controller'], array('conditions', 'therapies', 'traditions'))) {
			echo $html->div('headerBanner', $banner->display('header'));
		}
		?>
		<div id="header">
			<?php if($_SERVER['REQUEST_URI'] == '/'):?>
			<span id="logo"></span>
			<?php else:?>
			<a id="logo" href="/"></a>
			<?php endif;?>
			<?php echo $this->element('main_menu'); ?>
			<div id="topRightBox">
				<?php //echo $this->element('login_box'); ?>
				<!--<div id="searchBox"></div>-->
				<div id="shareLinks">
					<div id="findOn"></div>
					<script type="text/javascript" src="http://w.sharethis.com/button/sharethis.js#publisher=6b371232-b9d7-4750-8438-f4885e6aaa33&amp;type=website&amp;post_services=email%2Cfacebook%2Ctwitter%2Cmyspace%2Cdigg%2Csms%2Cdelicious%2Cstumbleupon%2Creddit%2Cgoogle_bmarks%2Clinkedin%2Cyahoo_bmarks%2Cfriendfeed%2Cpropeller%2Cwordpress%2Cblogger&amp;linkfg=%233a3a3a"></script>
					<a href="http://www.facebook.com/liivwell" class="facebook" target="_blank"></a><a href="http://www.facebook.com/liivwell" class="def" target="_blank">facebook</a>
					<a href="http://www.twitter.com/liivtoday" class="twitter" target="_blank"></a><a href="http://www.twitter.com/liivtoday" class="def" target="_blank">twitter</a>
				</div>
			</div>
		</div>
		<div id="content">
			<?php
			$session->flash();
			echo $content_for_layout;
			?>
		</div>
		<div id="footer"><?php echo $this->element('frontend_footer'); ?></div>
	</div>
	<?php
	if ($cakeDebug) {
		echo '<div class="cakeDebug">', $cakeDebug, '</div>';
	}
	if(!Configure::read('debug')){ //Fix for Asset helper. Was returning all scripts again (not splitting)
		echo $asset->scripts_for_layout(array('js','codeblock'));
	}
	// Google analytics, only on live.
	if($_SERVER['HTTP_HOST'] == 'www.liiv.com'):
	?>
	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
	try {
	var pageTracker = _gat._getTracker("UA-3102352-41");
	pageTracker._trackPageview();
	} catch(err) {}</script>
	<?php endif;?>
</body>
</html>

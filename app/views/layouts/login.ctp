<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        echo $html->meta('icon');
		echo $html->css('general');
        echo $html->css('admin');
        //echo $html->css('ui-lightness/jquery.ui');
		//echo $html->css('menu');
        //echo $javascript->link(array("jquery","jquery.ui","admin"));
        echo $scripts_for_layout;
        ?>
        <style type="text/css">
            input[type="submit"] {
                width: 20px;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <div id="header">
            </div>
            <div id="content">
                <?php $session->flash(); ?>
                <?php echo $content_for_layout; ?>
            </div>
            <div id="footer">
            </div>
        </div>
        <?php echo $cakeDebug; ?>
    </body>
</html>

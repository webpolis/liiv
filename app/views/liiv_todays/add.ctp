<div class="liivTodays form">
<?php echo $form->create('LiivToday');?>
	<fieldset>
 		<legend><?php __('Add LiivToday');?></legend>
	<?php
		echo $form->input('when');
		echo $form->input('user_id');
		echo $form->input('content');
		echo $form->input('type_id');
		echo $form->input('item_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List LiivTodays', true), array('action' => 'index'));?></li>
	</ul>
</div>

<?php
echo $this->element("liiv_today_admin", array("reserved"=>$used));
?>
<div class="liivTodays form">
    <?php echo $form->create('LiivToday');?>
    <fieldset>
        <legend><?php __('Add to Liiv Today');?></legend>
        <div class="left">
            <?php
            echo $form->input('when', array("type"=>"text","style"=>"width:80px","label"=>"Date"));
            echo $form->input('type_id',array("div"=>false,"selected"=>$selectedType));
            echo $form->input('user_id',array("type"=>"hidden","value" =>$userInfo['User']['id']));
            echo $form->input('topic',array("label" =>"Keywords / Topic"));
            echo $form->input('link');
            echo $form->input('image',array("style"=>"width:50%;display:inline;clear:left;float:left;margin-right:5px"));
            echo $this->element("image_uploader",array("folder"=>"liiv_today/$selectedType"));
            echo $form->input('content');
            ?>
        </div>
        <div class="right">
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('List Liiv Today', true), array('action' => 'index', $selectedType));?></li>
    </ul>
</div>
<div style="display:none" id="upProgress"></div>
<div style="display:none" id="upBtnCancel"></div>

<?php
$paginator->options(array("url"=>$filter));
echo $javascript->codeBlock("
j(function(){
    j('#Filter').change(function(){
        location.href = '/admin/liiv_todays/index/'+j('#Filter').attr('value');
    });
});
",array("inline"=>false));
?>
<div class="liivTodays index">
    <h2>
        <?php
        __('Liiv Today');
        echo $form->select("Filter",array(
        "all"=>"All",
        1=>"Wellness Tip of the Day",
        2=>"Intention for the Day",
        4=>"Story of the Day",
        5=>"Book of the Day",
        6=>"The Daily Wild Card",
        7=>"Daily Laugh"
        ),$filter,array("div"=>false,"label"=>false,"style"=>"display:inline;clear:none;margin-left:10px"),false);
        ?>
    </h2>
<div class="actions actions-inline listing-actions">
    <ul>
        <li><?php echo $html->link(__('New Liiv Today', true), ($filter==4)?array("controller"=>"stories","action"=>"add"):array('action' => 'add')); ?></li>
    </ul>
</div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $paginator->sort('when');?></th>
            <th><?php echo $paginator->sort('user_id');?></th>
            <th><?php echo $paginator->sort('content');?></th>
            <th><?php echo $paginator->sort('Type','LiivTodayType.title');?></th>
            <th class="actions"><?php __('Actions');?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($liivTodays as $liivToday):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
        <tr<?php echo $class;?>>
            <td>
                    <?php echo $liivToday[($filter!=="4"&&!isset($liivToday['Story']))?'LiivToday':"Story"]['when']; ?>
            </td>
            <td>
                    <?php echo $liivToday['User']['email']; ?>
            </td>
            <td>
                    <?php echo $text->truncate(strip_tags($liivToday[($filter!=="4"&&!isset($liivToday['Story']))?'LiivToday':"Story"]['content']),ADMIN_TEXT_TRIM,"...",true); ?>
            </td>
            <td>
                    <?php echo ($filter!=="4"&&!isset($liivToday['Story']))?$liivToday['LiivTodayType']['title']:"Story of the Day"; ?>
            </td>
            <td class="actions">
                    <?php echo $html->link(__('Edit', true), array("controller"=>(($filter!=="4"&&!isset($liivToday['Story']))?"liiv_todays":"stories"),'action' => 'edit', $liivToday[($filter!=="4"&&!isset($liivToday['Story']))?'LiivToday':"Story"]['id'])); ?>
                    <?php echo $html->link(__('Delete', true), array("controller"=>(($filter!=="4"&&!isset($liivToday['Story']))?"liiv_todays":"stories"),'action' => 'delete', $liivToday[($filter!=="4"&&!isset($liivToday['Story']))?'LiivToday':"Story"]['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $liivToday[($filter!=="4"&&!isset($liivToday['Story']))?'LiivToday':"Story"]['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>
<p>
    <?php
    echo $paginator->counter(array(
    'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
    ));
    ?></p>
<div class="paging">
    <?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
    | 	<?php echo $paginator->numbers();?>
    <?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>

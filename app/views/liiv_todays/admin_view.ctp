<div class="liivTodays view">
<h2><?php  __('LiivToday');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $liivToday['LiivToday']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('When'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $liivToday['LiivToday']['when']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $liivToday['LiivToday']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $liivToday['LiivToday']['updated']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $liivToday['LiivToday']['user_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Content'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $liivToday['LiivToday']['content']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Type Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $liivToday['LiivToday']['type_id']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit LiivToday', true), array('action' => 'edit', $liivToday['LiivToday']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete LiivToday', true), array('action' => 'delete', $liivToday['LiivToday']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $liivToday['LiivToday']['id'])); ?> </li>
		<li><?php echo $html->link(__('List LiivTodays', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New LiivToday', true), array('action' => 'add')); ?> </li>
	</ul>
</div>

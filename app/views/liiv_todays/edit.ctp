<div class="liivTodays form">
<?php echo $form->create('LiivToday');?>
	<fieldset>
 		<legend><?php __('Edit LiivToday');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('when');
		echo $form->input('user_id');
		echo $form->input('content');
		echo $form->input('type_id');
		echo $form->input('item_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('LiivToday.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('LiivToday.id'))); ?></li>
		<li><?php echo $html->link(__('List LiivTodays', true), array('action' => 'index'));?></li>
	</ul>
</div>

<div class="organizations form">
    <?php echo $form->create('Organization');?>
    <fieldset>
        <legend><?php __('Edit Organization');?></legend>
        <div class="left">
            <?php
            echo $form->input('id');
            echo $form->input('name');
            echo $form->input('website');
            echo $form->input('Article',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Audio',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Authority',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Organization',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Product',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Symptom',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Therapy',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Event',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Publication',array("class"=>"hidden","label"=>false,"div"=>false,"multiple"=>true));
            echo $form->input('Story',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Video',array("class"=>"hidden","label"=>false,"div"=>false));
            ?>
        </div>
        <div class="right">
            <?php
            echo $this->element("auto_complete",array("box"=>"OrganizationRelated","url"=>"/admin/organizations","referrer"=>"Organization","cache"=>true));
            echo $html->div("related",false,array("id"=>"selectedRelated"));
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Organization.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Organization.id'))); ?></li>
        <li><?php echo $html->link(__('List Organizations', true), array('action' => 'index'));?></li>
    </ul>
</div>

<div class="organizations view">
<h2><?php  __('Organization');?></h2>
<div class="actions actions-inline listing-actions">
	<ul>
		<li><?php echo $html->link(__('Edit Organization', true), array('action' => 'edit', $organization['Organization']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Organization', true), array('action' => 'delete', $organization['Organization']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $organization['Organization']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Organizations', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Organization', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $organization['Organization']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $organization['Organization']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Website'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $organization['Organization']['website']; ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php echo related_articles_table($organization['Article'], $html, $text);?>

<?php echo related_audios_table($organization['Audio'], $html, $text);?>

<?php echo related_publications_table($organization['Publication'], $html, $text); ?>

<?php echo related_videos_table($organization['Video'], $html, $text); ?>

<?php echo related_authorities_table($organization['Authority'], $html, $text);?>

<?php echo related_conditions_table($organization['Condition'], $html, $text);?>

<?php echo related_events_table($organization['Event'], $html, $text); ?>

<?php echo related_stories_table($organization['Story'], $html, $text); ?>

<?php echo related_therapies_table($organization['Therapy'], $html, $text); ?>
<div class="organizations view">
<h2><?php  __('Organization');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $organization['Organization']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $organization['Organization']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Website'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $organization['Organization']['website']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Organization', true), array('action' => 'edit', $organization['Organization']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Organization', true), array('action' => 'delete', $organization['Organization']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $organization['Organization']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Organizations', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Organization', true), array('action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Articles', true), array('controller' => 'articles', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Article', true), array('controller' => 'articles', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Audios', true), array('controller' => 'audios', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Audio', true), array('controller' => 'audios', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Publications', true), array('controller' => 'publications', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Publication', true), array('controller' => 'publications', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Videos', true), array('controller' => 'videos', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Video', true), array('controller' => 'videos', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Authorities', true), array('controller' => 'authorities', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Authority', true), array('controller' => 'authorities', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Conditions', true), array('controller' => 'conditions', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Condition', true), array('controller' => 'conditions', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Events', true), array('controller' => 'events', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Event', true), array('controller' => 'events', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Stories', true), array('controller' => 'stories', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Story', true), array('controller' => 'stories', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Therapies', true), array('controller' => 'therapies', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Therapy', true), array('controller' => 'therapies', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Articles');?></h3>
	<?php if (!empty($organization['Article'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Title'); ?></th>
		<th><?php __('Content'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th><?php __('Publication'); ?></th>
		<th><?php __('Author'); ?></th>
		<th><?php __('Organization Id'); ?></th>
		<th><?php __('Description'); ?></th>
		<th><?php __('Area Id'); ?></th>
		<th><?php __('Id'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($organization['Article'] as $article):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $article['title'];?></td>
			<td><?php echo $article['content'];?></td>
			<td><?php echo $article['created'];?></td>
			<td><?php echo $article['updated'];?></td>
			<td><?php echo $article['publication'];?></td>
			<td><?php echo $article['author'];?></td>
			<td><?php echo $article['organization_id'];?></td>
			<td><?php echo $article['description'];?></td>
			<td><?php echo $article['area_id'];?></td>
			<td><?php echo $article['id'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'articles', 'action' => 'view', $article['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'articles', 'action' => 'edit', $article['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'articles', 'action' => 'delete', $article['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $article['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Article', true), array('controller' => 'articles', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Audios');?></h3>
	<?php if (!empty($organization['Audio'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Title'); ?></th>
		<th><?php __('Author'); ?></th>
		<th><?php __('Organization Id'); ?></th>
		<th><?php __('Description Short'); ?></th>
		<th><?php __('Content'); ?></th>
		<th><?php __('Area Id'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($organization['Audio'] as $audio):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $audio['id'];?></td>
			<td><?php echo $audio['title'];?></td>
			<td><?php echo $audio['author'];?></td>
			<td><?php echo $audio['organization_id'];?></td>
			<td><?php echo $audio['description_short'];?></td>
			<td><?php echo $audio['content'];?></td>
			<td><?php echo $audio['area_id'];?></td>
			<td><?php echo $audio['created'];?></td>
			<td><?php echo $audio['updated'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'audios', 'action' => 'view', $audio['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'audios', 'action' => 'edit', $audio['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'audios', 'action' => 'delete', $audio['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $audio['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Audio', true), array('controller' => 'audios', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Publications');?></h3>
	<?php if (!empty($organization['Publication'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Title'); ?></th>
		<th><?php __('Author'); ?></th>
		<th><?php __('Organization Id'); ?></th>
		<th><?php __('Description Short'); ?></th>
		<th><?php __('Content'); ?></th>
		<th><?php __('Area Id'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($organization['Publication'] as $publication):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $publication['id'];?></td>
			<td><?php echo $publication['title'];?></td>
			<td><?php echo $publication['author'];?></td>
			<td><?php echo $publication['organization_id'];?></td>
			<td><?php echo $publication['description_short'];?></td>
			<td><?php echo $publication['content'];?></td>
			<td><?php echo $publication['area_id'];?></td>
			<td><?php echo $publication['created'];?></td>
			<td><?php echo $publication['updated'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'publications', 'action' => 'view', $publication['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'publications', 'action' => 'edit', $publication['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'publications', 'action' => 'delete', $publication['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $publication['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Publication', true), array('controller' => 'publications', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Videos');?></h3>
	<?php if (!empty($organization['Video'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Title'); ?></th>
		<th><?php __('Author'); ?></th>
		<th><?php __('Organization Id'); ?></th>
		<th><?php __('Description Short'); ?></th>
		<th><?php __('Content'); ?></th>
		<th><?php __('Area Id'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($organization['Video'] as $video):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $video['id'];?></td>
			<td><?php echo $video['title'];?></td>
			<td><?php echo $video['author'];?></td>
			<td><?php echo $video['organization_id'];?></td>
			<td><?php echo $video['description_short'];?></td>
			<td><?php echo $video['content'];?></td>
			<td><?php echo $video['area_id'];?></td>
			<td><?php echo $video['created'];?></td>
			<td><?php echo $video['updated'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'videos', 'action' => 'view', $video['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'videos', 'action' => 'edit', $video['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'videos', 'action' => 'delete', $video['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $video['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Video', true), array('controller' => 'videos', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Authorities');?></h3>
	<?php if (!empty($organization['Authority'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Title'); ?></th>
		<th><?php __('Bio'); ?></th>
		<th><?php __('Image'); ?></th>
		<th><?php __('Url'); ?></th>
		<th><?php __('Area Id'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($organization['Authority'] as $authority):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $authority['id'];?></td>
			<td><?php echo $authority['name'];?></td>
			<td><?php echo $authority['title'];?></td>
			<td><?php echo $authority['bio'];?></td>
			<td><?php echo $authority['image'];?></td>
			<td><?php echo $authority['url'];?></td>
			<td><?php echo $authority['area_id'];?></td>
			<td><?php echo $authority['created'];?></td>
			<td><?php echo $authority['updated'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'authorities', 'action' => 'view', $authority['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'authorities', 'action' => 'edit', $authority['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'authorities', 'action' => 'delete', $authority['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $authority['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Authority', true), array('controller' => 'authorities', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Conditions');?></h3>
	<?php if (!empty($organization['Condition'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Title'); ?></th>
		<th><?php __('Definition Short'); ?></th>
		<th><?php __('Definition Long'); ?></th>
		<th><?php __('Introduction'); ?></th>
		<th><?php __('Holistic'); ?></th>
		<th><?php __('Preventative'); ?></th>
		<th><?php __('Beliefs'); ?></th>
		<th><?php __('Area Id'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($organization['Condition'] as $condition):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $condition['id'];?></td>
			<td><?php echo $condition['title'];?></td>
			<td><?php echo $condition['definition_short'];?></td>
			<td><?php echo $condition['definition_long'];?></td>
			<td><?php echo $condition['introduction'];?></td>
			<td><?php echo $condition['holistic'];?></td>
			<td><?php echo $condition['preventative'];?></td>
			<td><?php echo $condition['beliefs'];?></td>
			<td><?php echo $condition['area_id'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'conditions', 'action' => 'view', $condition['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'conditions', 'action' => 'edit', $condition['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'conditions', 'action' => 'delete', $condition['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $condition['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Condition', true), array('controller' => 'conditions', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Events');?></h3>
	<?php if (!empty($organization['Event'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Description Short'); ?></th>
		<th><?php __('Description Long'); ?></th>
		<th><?php __('Event Date'); ?></th>
		<th><?php __('Link'); ?></th>
		<th><?php __('Event Blob'); ?></th>
		<th><?php __('Tags'); ?></th>
		<th><?php __('Location'); ?></th>
		<th><?php __('Teacher'); ?></th>
		<th><?php __('Subject'); ?></th>
		<th><?php __('Source'); ?></th>
		<th><?php __('Area Id'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($organization['Event'] as $event):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $event['id'];?></td>
			<td><?php echo $event['name'];?></td>
			<td><?php echo $event['description_short'];?></td>
			<td><?php echo $event['description_long'];?></td>
			<td><?php echo $event['event_date'];?></td>
			<td><?php echo $event['link'];?></td>
			<td><?php echo $event['event_blob'];?></td>
			<td><?php echo $event['tags'];?></td>
			<td><?php echo $event['location'];?></td>
			<td><?php echo $event['teacher'];?></td>
			<td><?php echo $event['subject'];?></td>
			<td><?php echo $event['source'];?></td>
			<td><?php echo $event['area_id'];?></td>
			<td><?php echo $event['created'];?></td>
			<td><?php echo $event['updated'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'events', 'action' => 'view', $event['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'events', 'action' => 'edit', $event['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'events', 'action' => 'delete', $event['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $event['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Event', true), array('controller' => 'events', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Stories');?></h3>
	<?php if (!empty($organization['Story'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Title'); ?></th>
		<th><?php __('Content'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th><?php __('Video'); ?></th>
		<th><?php __('Photo'); ?></th>
		<th><?php __('Area Id'); ?></th>
		<th><?php __('User Id'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($organization['Story'] as $story):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $story['id'];?></td>
			<td><?php echo $story['title'];?></td>
			<td><?php echo $story['content'];?></td>
			<td><?php echo $story['created'];?></td>
			<td><?php echo $story['updated'];?></td>
			<td><?php echo $story['media'];?></td>
			<td><?php echo $story['photo'];?></td>
			<td><?php echo $story['area_id'];?></td>
			<td><?php echo $story['user_id'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'stories', 'action' => 'view', $story['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'stories', 'action' => 'edit', $story['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'stories', 'action' => 'delete', $story['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $story['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Story', true), array('controller' => 'stories', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Therapies');?></h3>
	<?php if (!empty($organization['Therapy'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Title'); ?></th>
		<th><?php __('Definition'); ?></th>
		<th><?php __('Area Id'); ?></th>
		<th><?php __('Beliefs'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($organization['Therapy'] as $therapy):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $therapy['id'];?></td>
			<td><?php echo $therapy['title'];?></td>
			<td><?php echo $therapy['definition'];?></td>
			<td><?php echo $therapy['area_id'];?></td>
			<td><?php echo $therapy['beliefs'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'therapies', 'action' => 'view', $therapy['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'therapies', 'action' => 'edit', $therapy['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'therapies', 'action' => 'delete', $therapy['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $therapy['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Therapy', true), array('controller' => 'therapies', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>

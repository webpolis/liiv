<div style="padding-left:20px;padding-top:60px;">
    <p><?php echo $html->link("Add New Condition", "/admin/conditions/add");?></p>
    <p><?php echo $html->link("Add New Therapy", "/admin/therapies/add");?></p>
    <p><?php echo $html->link("Add New Tradition", "/admin/traditions/add");?></p>
    <p><?php echo $html->link("Add New Video", "/admin/videos/add");?></p>
    <p><?php echo $html->link("Add New Authority", "/admin/authorities/add");?></p>
</div>

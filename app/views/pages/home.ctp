<?php
echo $javascript->codeBlock("
var elements = ".((isset($elements))?json_encode($elements):"[]").";
var banners ={displayed:[],containers:[]};
j(function(){
	displayBanner();
});
",array("inline"=>false));
?>
<?php echo $banner->display('homepage-top');?>
<div id="middleContent">
	<div id="slider"><?php echo $this->element("home_slider");?></div>
	<map name="map">
	<area shape="rect" coords="65,91,150,111" href="/conditions" />
	<area shape="rect" coords="167,103,301,124" href="/therapies" />
	<area shape="rect" coords="177,212,297,230" href="/stress" />
	<area shape="rect" coords="31,231,172,251" href="/traditions" />
	</map>
	<?php echo $html->image('body.jpg', array('style' => 'float: left; border: 0;', 'usemap' => '#map'))?>
	<div id="liivToday"><?php echo $this->element("liiv_today_accordion");?>
	</div>
</div>
<div class="clearer" style="margin: 0px"></div>
<?php echo $banner->display('homepage-middle');?>
<div class="clearer"></div>
<div class="leftColumn">
	<?php echo $this->element('homepage_sign_up');?>	
	<div class="clearer" style="height: 5px"></div>
	<?php
	$featuredStoryCreated = (isset($story)&&isset($story['Story'])&&!empty($story['Story']['created']))? date("F j, Y",strtotime($story['Story']['created'])): "";
	$featuredStoryPath = (isset($story)&&isset($story['Story'])&&!empty($story['Story']['path']))? "/stories/".$story['Story']['path']: "";
	$featuredStoryCreatedBy = (isset($story)&&isset($story['CreatedBy']))? ' by '.$story['CreatedBy']['username']: "";
	$featuredStoryContent = (isset($story)&&isset($story['Story'])&&!empty($story['Story']['content']))? $story['Story']['content']: 
	"There is not a featured story today, please check out some of our other stories above.";
	$featuredStory = '<span class="boxTitleSprite stories"></span>';
	$featuredStory .= '<span class="date">'.$featuredStoryCreated.'</span>';
	$featuredStory .= $featuredStoryCreatedBy;
	$featuredStory .= $html->div(false,$text->truncate($featuredStoryContent,ADMIN_TEXT_TRIM,"..."),array("id"=>"storyContentTruncated"));
	$featuredStory .= (!empty($featuredStoryPath))?$html->link("More&nbsp;".$html->image("icons/arrow-right-b.png",array("border"=>0)),$featuredStoryPath,array("escape"=>false)):null;
	echo $this->element("home_white_box",array("width"=>320,"paddingLeft"=>15,"paddingRight"=>15,"height"=>155,"content"=>$html->div("featuredIntro", $featuredStory)));
	?>
	<div class="clearer" style="height: 5px"></div>
	<?php
	$featuredVideoUrl = (isset($video)&&isset($video['Video'])&&!empty($video['Video']['url']))? '/video/'.$video['Video']['url']: "";
	$featuredVideoTitle = (isset($video)&&isset($video['Video'])&&!empty($video['Video']['title']))? $video['Video']['title']: "";
	$featuredVideoShort = (isset($video)&&isset($video['Video'])&&!empty($video['Video']['description_short']))? $video['Video']['description_short']: 
	"There is not a featured video today, please check out some of our other videos above.";
	$featuredVideo = '<span class="boxTitleSprite videos"></span>';
	$featuredVideo .= '<span class="date">'.$featuredVideoTitle.'</span>';
	$featuredVideo .= $html->div(false,$text->truncate($featuredVideoShort,ADMIN_TEXT_TRIM,"..."),array("id"=>"storyContentTruncated"));
	$featuredVideo .= (!empty($featuredVideoUrl))? $html->link("More&nbsp;".$html->image("icons/arrow-right-b.png",array("border"=>0)),$featuredVideoUrl,array("escape"=>false)):null;
	echo $this->element("home_white_box",array("width"=>320,"paddingLeft"=>15,"height"=>155,"content"=>$html->div("featuredIntro", $featuredVideo)));
	?>
</div>
<div class="middleColumn">
	<?php echo $this->element("home_white_box",array("width"=>320,"paddingLeft"=>10,"height"=>485,"content"=>$html->image("directory.jpg")));?>
</div>
<div class="rightColumn">
	<?php 
	if($ad = $banner->display('homepage-box'))
		echo $this->element("home_white_box",array("width"=>320,"paddingLeft"=>5,"height"=>295,"content"=>$html->div('center-align',$ad))),
		$html->div('clearer','',array('style' => 'height: 5px;'));
	?>
	<?php echo $this->element('store_widget');?>
</div>

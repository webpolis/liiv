<?php
$rightContent  = '<div style="padding:10px;">';
$rightContent .= '<h1 class="static-content-title">'.$titleq.'</h1>';
$rightContent .= '<div class="static-content-body">'.$content.'</div>';
$rightContent .= '</div>';
?>
<div class="view page spaced">
<?php 
	echo $this->element('home_white_box', array('width' => 980, 'content' => $html->div(null,$rightContent, array('style' => 'padding: 0 5px 0 10px;'))));
?>
</div>
<div class="clearer" style="margin:0px"></div>
<?php
$rightContent  = '<div style="padding:10px;">';
$rightContent .= '<h2 class="static-content-title">'.$titleq.'</h2>';
$rightContent .= '<div class="static-content-body">'.$content.'</div>';
$rightContent .= '</div>';
echo $javascript->codeBlock("
window.tagCloud = ".((isset($tagCloud))?$tagCloud:"[]").";
window.poll = ".((isset($polls))?json_encode($polls):"[]").";
j(function(){
	generateTagCloud('condition');
	generatePoll();
});
",array("inline"=>false));
?>
<div class="twoColsTop"></div>
<div class="twoColsMiddle">
    <div class="twoColsLeft">
        <?php echo $rightContent ?>
    </div>
    <div class="twoColsRight">
        <?php echo $this->element("front_right_column",array("tagCloudModel"=>"Condition")); ?>
    </div>
    <div class="clearer" style="margin:0px"></div>
</div>
<div class="twoColsBottom"></div>
<div class="clearer" style="margin:0px"></div>
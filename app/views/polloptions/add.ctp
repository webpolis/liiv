<div class="polloptions form">
<?php echo $form->create('Polloption');?>
	<fieldset>
 		<legend><?php __('Add Polloption');?></legend>
	<?php
		echo $form->input('poll_id');
		echo $form->input('content');
		echo $form->input('message');
		echo $form->input('order');
		echo $form->input('hits');
		echo $form->input('published');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Polloptions', true), array('action' => 'index'));?></li>
	</ul>
</div>

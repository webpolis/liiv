<script type="text/javascript">
j(function(){
	j('#PolloptionEditForm').validate();
});
</script>
<?php
echo $form->create('Polloption', array('id' => 'PolloptionAjaxEditForm', 'url' => 'javascript: window.pollOptions.addOption();'));
echo $form->input('answer',array('id' =>'PolloptionAjaxAnswer',  'type' => 'text','label' => 'Answer', 'div' => 'false', 'class' => 'required PolloptionAnswer', 'maxlength' => '255'));
echo $form->input('message',array('id'=>'PolloptionAjaxMessage','type' => 'text','label' => 'Message', 'div' => 'false', 'class' => 'required PolloptionMessage', 'maxlength' => '512'));
echo $form->input('id', array('type' => 'hidden', 'id' => 'PolloptionAjaxId'));
echo $form->input('poll_id', array('type' => 'hidden', 'id' => 'PolloptionAjaxPollId'));
echo $form->end();
?>

<div class="polloptions index">
<h2><?php __('Polloptions');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('poll_id');?></th>
	<th><?php echo $paginator->sort('answer');?></th>
	<th><?php echo $paginator->sort('message');?></th>
	<th><?php echo $paginator->sort('order');?></th>
	<th><?php echo $paginator->sort('hits');?></th>
	<th><?php echo $paginator->sort('published');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($polloptions as $polloption):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $polloption['Polloption']['id']; ?>
		</td>
		<td>
			<?php echo $polloption['Polloption']['poll_id']; ?>
		</td>
		<td>
			<?php echo $polloption['Polloption']['answer']; ?>
		</td>
		<td>
			<?php echo $polloption['Polloption']['message']; ?>
		</td>
		<td>
			<?php echo $polloption['Polloption']['order']; ?>
		</td>
		<td>
			<?php echo $polloption['Polloption']['hits']; ?>
		</td>
		<td>
			<?php echo $polloption['Polloption']['published']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $polloption['Polloption']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $polloption['Polloption']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $polloption['Polloption']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $polloption['Polloption']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Polloption', true), array('action' => 'add')); ?></li>
	</ul>
</div>

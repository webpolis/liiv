<div class="polloptions form">
<?php echo $form->create('Polloption');?>
	<fieldset>
 		<legend><?php __('Edit Polloption');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('poll_id');
		echo $form->input('content');
		echo $form->input('message');
		echo $form->input('order');
		echo $form->input('hits');
		echo $form->input('published');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Polloption.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Polloption.id'))); ?></li>
		<li><?php echo $html->link(__('List Polloptions', true), array('action' => 'index'));?></li>
	</ul>
</div>

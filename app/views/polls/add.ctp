<div class="polls form">
<?php echo $form->create('Poll');?>
	<fieldset>
 		<legend><?php __('Add Poll');?></legend>
	<?php
		echo $form->input('question');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Polls', true), array('action' => 'index'));?></li>
	</ul>
</div>

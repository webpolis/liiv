<?php
//debug($this->data['Polloption']);

$pollOptions = array();
foreach ($this->data['Polloption'] as $option) {
	$pollOptions[] = $option;
}
$javascript->codeBlock('
j(function(){
	j("#pollSave").click(pollSave);
	window.pollOptions = PollOptionsFactory('.json_encode($pollOptions).', \''.
	$html->url(array('controller'=>'polloptions', 'action'=>'add')).
	'\', \''.$html->url(array('controller'=>'polloptions', 'action'=>'delete')).'\');
	window.pollOptions.pollSaveUrl = "'.$html->url(array('controller'=>'polls', 'action'=>'edit')).'";
	window.pollOptions.saveOrderUrl = "'.$html->url(array('controller'=>'polloptions', 'action'=>'saveorder')).'";
});
', array('inline'=>false));
unset($pollOptions, $option);
?>
<div class="polls form">
<div id="pollOptionEditDialog"></div>
<fieldset>
		<legend><?php __('Edit Poll'); ?></legend>
		<div class="left">
		<?php
			echo
				$form->create('Poll', array('id'=>'PollEditForm', 'url'=>'javascript:pollSave()')),
				$form->input('id'),
				$html->tag('h3', 'Question'),
				$form->input('question', array('label'=>false, 'maxlength'=>'512')),
				'<div style="margin-top:10px">',
					$form->input('published', array('label'=>false, 'style'=>'display:inline', 'div'=>false)),
					$html->tag('span', 'Published', array('style'=>'display:inline;margin-left:4px')),
				'</div>',
				$form->input('Polloption', array('label'=>false, 'div'=>false, 'multiple'=>'true', 'style'=>'display:none')),
				$form->end(),
				$html->tag('h3', 'Answers', array('style'=>'margin-bottom:8px')),
				$html->tag('ul', '<li class="temptext"><p class="sublabel">No answers added yet.</p></li>', array('id'=>'selectedOptions', 'class'=>'pollOptions')),
				$html->tag('input', '', array('type'=>'button', 'value'=>'Save poll', 'id'=>'pollSave')),
				$html->image('ajax-loader2.gif', array('id'=>'ajaxLoading', 'style'=>'margin:auto 0 -5px 10px;display:none'));
		?>
		</div>
		<div class="right">
		<?php
			echo
				$form->create('Polloption', array('id'=>'PolloptionAddForm', 'url'=>'javascript:window.pollOptions.addOption()')),
				$html->tag('h3', 'Add answers'),
				'<span class="sublabel">Fill in the required information and click add</span>',
				$form->input('answer', array('type'=>'text', 'label'=>'Answer', 'div'=>'false', 'class'=>'required', 'maxlength'=>'255')),
				$form->input('message', array('type'=>'text', 'label'=>'Message', 'div'=>'false', 'class'=>'required', 'maxlength'=>'512')),
				$form->input('poll_id', array('type'=>'hidden', 'value'=>$this->data['Poll']['id'])),
				$form->end('Add');
		?>
	</div>
</fieldset>
</div>
<br />
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('Poll.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Poll.id'))); ?></li>
		<li><?php echo $html->link(__('List Polls', true), array('action'=>'index')); ?></li>
	</ul>
</div>

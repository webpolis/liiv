<?php $paginator->options(array("url"=>$filter)); ?>
<div class="polls index">
<h2><?php __('Polls');?></h2>
<div class="actions actions-inline listing-actions">
	<ul>
		<li><?php echo $html->link(__('New Poll', true), array('action' => 'add')); ?></li>
	</ul>
</div>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('question');?></th>
	<th><?php echo $paginator->sort('published');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($polls as $poll):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $poll['Poll']['question']; ?>
		</td>
		<td>
			<?php if($poll['Poll']['published']) {echo 'yes';} else {echo 'no';};?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $poll['Poll']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $poll['Poll']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $poll['Poll']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $poll['Poll']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>

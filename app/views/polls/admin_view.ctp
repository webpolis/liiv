<div class="polls view">
<h2><?php  __('Poll');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $poll['Poll']['id']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Poll', true), array('action' => 'edit', $poll['Poll']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Poll', true), array('action' => 'delete', $poll['Poll']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $poll['Poll']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Polls', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Poll', true), array('action' => 'add')); ?> </li>
	</ul>
</div>

<div class="polls form">
<?php echo $form->create('Poll');?>
	<fieldset>
 		<legend><?php __('Edit Poll');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('question');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Poll.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Poll.id'))); ?></li>
		<li><?php echo $html->link(__('List Polls', true), array('action' => 'index'));?></li>
	</ul>
</div>

<div class="positions form">
<?php echo $form->create('Position');?>
	<fieldset>
 		<legend><?php __('Add Position');?></legend>
	<?php
		echo $form->input('name');
		echo $form->input('identifier');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Positions', true), array('action' => 'index'));?></li>
	</ul>
</div>

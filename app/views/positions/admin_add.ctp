<div class="positions form">
<?php echo $form->create('Position');?>
	<fieldset>
 		<legend><?php __('Add Position');?></legend>
 		<div class="left">
		<?php
			echo $form->input('name');
			echo $form->input('identifier', array('label' => 'Identifier<br /><span class="sublabel">This field will identify the position in PHP code</span>'));
		?>
		</div>
		<div class="right"></div>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Positions', true), array('action' => 'index'));?></li>
	</ul>
</div>

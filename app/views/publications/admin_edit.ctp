<?php
echo $wyswig->attach(array(
        'PublicationDescriptionShort',
        array('id'=>'PublicationContent', 'layout' => 'Medium')
    )
);
?>
<div class="publications form">
    <?php echo $form->create('Publication');?>
    <fieldset>
        <legend><?php __('Add Publication');?></legend>
        <div class="left">
            <?php
            echo $form->input("id");
            echo $form->input('title');
            echo $form->input('author', array('between' => '<span class="sublabel">This field will be ignored if an Authority is selected</span>'));
            echo $form->input('authority_id', array('empty' => '(none)'));
            echo $form->input('organization_id', array('empty' => '(none)'));
            echo $form->input('description_short');
            echo $form->input('link');
            echo $form->input('content');
            echo $form->input('area_id');
            echo $form->input('Article',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Audio',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Condition',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Event',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Publication',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Story',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Therapy',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Video',array("class"=>"hidden","label"=>false,"div"=>false));
            ?>
        </div>
        <div class="right">
            <?php
            echo $this->element("auto_complete",array("box"=>"PublicationRelated","url"=>"/admin/publications","referrer"=>"Publication","cache"=>true));
            echo $html->div("related",false,array("id"=>"selectedRelated"));
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('List Publications', true), array('action' => 'index'));?></li>
        <li><?php echo $html->link(__('New Publication', true), array('controller' => 'publications', 'action' => 'add')); ?> </li>
    </ul>
</div>

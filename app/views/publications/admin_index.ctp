<?php $paginator->options(array("url"=>$filter)); ?>
<div class="publications index">
<h2><?php __('Publications');?><span class="listing-actions"><?php echo $html->link(__('New Publication', true), array('action' => 'add')); ?></span></h2>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('authority_id');?></th>
	<th><?php echo $paginator->sort('organization_id');?></th>
	<th><?php echo $paginator->sort('description_short');?></th>
	<th><?php echo $paginator->sort('content');?></th>
	<th><?php echo $paginator->sort('area_id');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('updated');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($publications as $publication):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $publication['Publication']['id']; ?>
		</td>
		<td>
			<?php echo $publication['Publication']['title']; ?>
		</td>
		<td>
			<?php echo $html->link($publication['Authority']['name'], array('controller' => 'authorities', 'action' => 'view', $publication['Authority']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($publication['Organization']['name'], array('controller' => 'organizations', 'action' => 'view', $publication['Organization']['id'])); ?>
		</td>
		<td>
			<?php echo $text->truncate($publication['Publication']['description_short'], ADMIN_TEXT_TRIM, '...', true); ?>
		</td>
		<td>
			<?php echo $text->truncate($publication['Publication']['content'], ADMIN_TEXT_TRIM, '...', true); ?>
		</td>
		<td>
			<?php echo $html->link($publication['Area']['title'], array('controller' => 'areas', 'action' => 'view', $publication['Area']['id'])); ?>
		</td>
		<td>
			<?php echo $publication['Publication']['created']; ?>
		</td>
		<td>
			<?php echo $publication['Publication']['updated']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $publication['Publication']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $publication['Publication']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $publication['Publication']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $publication['Publication']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
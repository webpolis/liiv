<div class="publications view">
<h2><?php  __('Publication');?></h2>
<div class="actions actions-inline listing-actions">
    <ul>
	<li><?php echo $html->link(__('Edit Publication', true), array('action' => 'edit', $publication['Publication']['id'])); ?> </li>
        <li><?php echo $html->link(__('Delete Publication', true), array('action' => 'delete', $publication['Publication']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $publication['Publication']['id'])); ?> </li>
	<li><?php echo $html->link(__('List Publications', true), array('action' => 'index')); ?> </li>
	<li><?php echo $html->link(__('New Publication', true), array('action' => 'add')); ?> </li>
    </ul>
</div>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $publication['Publication']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $publication['Publication']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Author'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $publication['Publication']['author']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description Short'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $publication['Publication']['description_short']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Content'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $publication['Publication']['content']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($publication['Area']['title'], array('controller' => 'areas', 'action' => 'view', $publication['Area']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $publication['Publication']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $publication['Publication']['updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php echo related_articles_table($publication['Article'], $html, $text);?>

<?php echo related_audios_table($publication['Audio'], $html, $text);?>

<?php echo related_authorities_table($publication['Authority'], $html, $text);?>

<?php echo related_conditions_table($publication['Condition'], $html, $text);?>

<?php echo related_events_table($publication['Event'], $html, $text); ?>

<?php echo related_organizations_table($publication['Organization'], $html, $text);?>

<?php echo related_publications_table($publication['PublicationRelated'], $html, $text); ?>

<?php echo related_stories_table($publication['Story'], $html, $text); ?>

<?php echo related_therapies_table($publication['Therapy'], $html, $text); ?>

<?php echo related_videos_table($publication['Video'], $html, $text); ?>
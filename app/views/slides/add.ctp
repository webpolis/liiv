<div class="slides form">
<?php echo $form->create('Slide');?>
	<fieldset>
 		<legend><?php __('Add Slide');?></legend>
	<?php
		echo $form->input('content');
		echo $form->input('title');
		echo $form->input('description');
		echo $form->input('link');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Slides', true), array('action' => 'index'));?></li>
	</ul>
</div>

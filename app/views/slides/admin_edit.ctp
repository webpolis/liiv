<?php 
echo $wyswig->attach(array("SlideDescription"));
?>
<div class="slides form"><?php echo $form->create('Slide');?>
<fieldset><legend><?php __('Edit Slide');?></legend>
<div class="left">
 <?php
 	echo $form->input("id");
	echo $form->input('title');
	echo $form->input('description',array("type"=>"textarea","style"=>"height:80px","label"=>"Description&nbsp;<span class=\"sublabel\">Due to space restrictions, only the first 140 characters will be displayed</span>"));
	echo $form->input('content',array("style"=>"width:50%;display:inline;clear:left;float:left;margin-right:5px"));
	echo $this->element("image_uploader",array("folder"=>"slider"));
	echo $form->input('link');
	echo $form->input('link_title',array("label"=>"Link title&nbsp;<span class=\"sublabel\">This field will be displayed as a blue link below the slide's description</span>", 'maxlength' => 45));
?>
</div>
<div class="right">
<?php
	echo $html->image($this->data['Slide']['content']);
?>
</div>
</fieldset>
<?php echo $form->end('Submit');?></div>
<div class="actions">
<ul>
	<li><?php echo $html->link(__('List Slides', true), array('action' => 'index'));?></li>
</ul>
</div>
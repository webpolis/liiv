<?php $paginator->options(array("url"=>$filter)); ?>
<div class="slides index">
<h2><?php __('Slides');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('content');?></th>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('description');?></th>
	<th><?php echo $paginator->sort('link');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('updated');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($slides as $slide):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $slide['Slide']['id']; ?>
		</td>
		<td>
			<?php echo $slide['Slide']['content']; ?>
		</td>
		<td>
			<?php echo $slide['Slide']['title']; ?>
		</td>
		<td>
			<?php echo $slide['Slide']['description']; ?>
		</td>
		<td>
			<?php echo $slide['Slide']['link']; ?>
		</td>
		<td>
			<?php echo $slide['Slide']['created']; ?>
		</td>
		<td>
			<?php echo $slide['Slide']['updated']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $slide['Slide']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $slide['Slide']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $slide['Slide']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $slide['Slide']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Slide', true), array('action' => 'add')); ?></li>
	</ul>
</div>

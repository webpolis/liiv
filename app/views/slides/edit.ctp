<div class="slides form">
<?php echo $form->create('Slide');?>
	<fieldset>
 		<legend><?php __('Edit Slide');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('content');
		echo $form->input('title');
		echo $form->input('description');
		echo $form->input('link');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Slide.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Slide.id'))); ?></li>
		<li><?php echo $html->link(__('List Slides', true), array('action' => 'index'));?></li>
	</ul>
</div>

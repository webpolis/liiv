<?php
echo $javascript->link(array("dateFormat"),false);
echo $javascript->codeBlock("
window.isLiivToday = ".((($this->data['Story']['when']!=null&&$this->data['Story']['when']!="0000-00-00")||$isLiivToday)?"true":"false").";
window.reserved = ".$used.";
function enableDateField(){
    if(window.isLiivToday){
        j('#dateField').show('slide');
        j('#isLiivToday').attr('checked',true);
    }else{
        j('#dateField').hide('slide');
        j('#isLiivToday').attr('checked',false);
        j('#StoryWhen').attr('value','');
    }
    window.isLiivToday = !window.isLiivToday;
}

j(function(){
    enableDateField();
    j('#isLiivToday').click(function(){
        window.isLiivToday = j(this).attr('checked');
        enableDateField();
    });
    j('#StoryWhen').datepicker({
        dateFormat:'yy-mm-dd',
        changeMonth:true,
        changeYear:true,
        yearRange:'2000:2099',
        beforeShowDay:function(date){
            var when = date.format('yyyy-mm-dd');
            var available = true;
            var tooltip = '';
            if(typeof(reserved)!='undefined' && reserved.length > 0){
                for(x in reserved){
                    var e = reserved[x];
                    if(typeof(e.Story)!='undefined'){
                        if(e.Story.when == when){
                            available = false;
                            tooltip = 'Story: '+e.Story.title;
                        }
                    }
                }
            }
            return [available,'',tooltip];
        }
    });
});
",array("inline"=>false));
echo $wyswig->attach(array("StoryContent"));
?>
<div class="stories form">
    <?php echo $form->create('Story');?>
    <fieldset>
        <legend><?php __('Add Story');?></legend>
        <div class="left">
            <input type="checkbox" id="isLiivToday" />&nbsp;<b>Liiv Today</b>
            <?php echo $form->input("featured",array("div"=>false,"label"=>"Featured Story","style"=>"display:inline;clear:none;margin-left:5px"));?>
            <div id="dateField">
                <?php echo $form->input("when",array("div"=>false,"type"=>"text","style"=>"width:30%","label"=>"Date<br /><span class='sublabel'>(choose a date if you want to show this on \"Liiv Today\")</span>"));?>
            </div>
            <?php
            echo $form->input('title');
            echo $form->input('content');
            echo $form->input('photo',array("style"=>"width:50%;display:inline;clear:left;float:left;margin-right:5px"));
            echo $this->element("image_uploader",array("folder"=>"story"));
            echo $form->input('media',array("style"=>"width:50%;display:inline;clear:left;float:left;margin-right:5px","label"=>"Video"));
            echo $this->element("video_uploader",array("field"=>"StoryMedia","folder"=>"story"));
            echo $form->input('area_id',array("style"=>"display:inline;clear:none;float:left;margin-right:5px","div"=>false));
            echo $form->input('user_id',array("style"=>"display:inline;clear:none;float:left;margin-right:5px","div"=>false));
            echo $form->input('Article',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Audio',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Authority',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Organization',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Product',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Therapy',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Event',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Publication',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Story',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Video',array("class"=>"hidden","label"=>false,"div"=>false));
            ?>
        </div>
        <div class="right">
            <?php
            echo $this->element("auto_complete",array("box"=>"StoryRelated","url"=>"/admin/stories","referrer"=>"Story","cache"=>true));
            echo $html->div("related",false,array("id"=>"selectedRelated"));
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Story.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Story.id'))); ?></li>
        <li><?php echo $html->link(__('List Stories', true), array('action' => 'index'));?></li>
        <li><?php echo $html->link(__('New Story', true), array('controller' => 'stories', 'action' => 'add')); ?> </li>
        <li><?php echo $html->link(__('Stories of the Day', true), array("admin"=>true,'controller' => 'liiv_todays', 'action' => 'index',4)); ?> </li>
    </ul>
</div>

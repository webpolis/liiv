<?php $paginator->options(array("url"=>$filter));?>
<div class="stories index">
<h2><?php __('Stories');?><span class="listing-actions"><?php echo $html->link(__('New Story', true), array('action' => 'add')); ?></span></h2>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('content');?></th>
	<th><?php echo $paginator->sort('user_id');?></th>
	<th><?php echo $paginator->sort('path');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('updated');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($stories as $story):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $story['Story']['title']; ?>
		</td>
		<td>
			<?php echo $text->truncate(strip_tags($story['Story']['content']),ADMIN_TEXT_TRIM,"..."); ?>
		</td>
		<td>
			<?php echo $html->link($story['User']['email'], array('controller' => 'users', 'action' => 'view', $story['User']['id'])); ?>
		</td>
		<td>
			<?php echo $story['Story']['path']; ?>
		</td>
		<td>
			<?php echo $story['Story']['created']; ?>
		</td>
		<td>
			<?php echo $story['Story']['updated']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $story['Story']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $story['Story']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $story['Story']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $story['Story']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="stories view">
<h2><?php  __('Story');?></h2>
<div class="actions actions-inline listing-actions">
	<ul>
		<li><?php echo $html->link(__('Edit Story', true), array('action' => 'edit', $story['Story']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Story', true), array('action' => 'delete', $story['Story']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $story['Story']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Stories', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Story', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $story['Story']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $story['Story']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Content'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $story['Story']['content']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $story['Story']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $story['Story']['updated']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Photo'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $story['Story']['photo']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Video'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $story['Story']['media']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($story['Area']['title'], array('controller' => 'areas', 'action' => 'view', $story['Area']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($story['User']['email'], array('controller' => 'users', 'action' => 'view', $story['User']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php echo related_articles_table($story['Article'], $html, $text);?>

<?php echo related_audios_table($story['Audio'], $html, $text);?>

<?php echo related_authorities_table($story['Authority'], $html, $text);?>

<?php echo related_events_table($story['Event'], $html, $text); ?>

<?php echo related_publications_table($story['Publication'], $html, $text); ?>

<?php echo related_conditions_table($story['Condition'], $html, $text);?>

<?php echo related_organizations_table($story['Organization'], $html, $text);?>

<?php echo related_products_table($story['Product'], $html, $text);?>

<?php echo related_stories_table($story['StoryRelated'], $html, $text); ?>

<?php echo related_therapies_table($story['Therapy'], $html, $text); ?>

<?php echo related_videos_table($story['Video'], $html, $text); ?>
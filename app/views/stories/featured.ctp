<?php
$rightContent = '<h1>'.__("Featured Story",true)." : ".$story['Story']['title'].'</h1>';
$rightContent .= $html->div("clearer",false,array("style"=>"height:5px"));
$rightContent .= $story['Story']['content'];
?>
<div class="view page spaced featured"><?php 
echo $this->element('home_white_box', array('width' => 980, "height"=>"auto",'content' => $html->div(false,$rightContent, array('style' => 'padding: 0 5px 0 10px;'))));
?></div>
<div class="clearer" style="margin: 0px"></div>

<div class="stories view">
<div class="twoColsTop"></div>
<div class="twoColsMiddle">
    <div class="twoColsLeft">
	    <div style="padding-top: 10px;">
		    <h1 class="static-content-title">
		  		<?php echo $story['Story']['title']?>
		  	</h1>
		    <div class="static-content-body">
		    	<?php echo $story['Story']['content']?>
		    </div>
	    </div>
	</div>
    <div class="twoColsRight">
		<?php echo $this->element("front_right_column",array("tagCloudModel"=>"Conditions")); ?>
    </div>
    <div class="clearer" style="margin:0px"></div>
</div>
<div class="twoColsBottom"></div>
<div class="clearer" style="margin:0px"></div>
</div>
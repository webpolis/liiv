<div class="subscriptions form">
<?php echo $form->create('Subscription');?>
	<fieldset>
 		<legend><?php __('Edit Subscription');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('type');
		echo $form->input('entity_id');
		echo $form->input('item');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Subscription.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Subscription.id'))); ?></li>
		<li><?php echo $html->link(__('List Subscriptions', true), array('action' => 'index'));?></li>
	</ul>
</div>

<div class="symptoms form">
    <?php echo $form->create('Symptom');?>
    <fieldset>
        <legend><?php __('Add Symptom');?></legend>
        <div class="left">
            <?php
            echo $form->input('title');
            echo $form->input('definition');
            echo $form->input('area_id');
            echo $form->input('Condition');
            ?>
        </div>
        <div class="right">
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('List Symptoms', true), array('action' => 'index'));?></li>
    </ul>
</div>

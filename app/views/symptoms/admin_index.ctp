<?php $paginator->options(array("url"=>$filter)); ?>
<div class="symptoms index">
<h2><?php __('Symptoms');?><span class="listing-actions"><?php echo $html->link(__('New Symptom', true), array('action' => 'add')); ?></span></h2>

<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('definition');?></th>
	<th><?php echo $paginator->sort('area_id');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($symptoms as $symptom):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $symptom['Symptom']['id']; ?>
		</td>
		<td>
			<?php echo $symptom['Symptom']['title']; ?>
		</td>
		<td>
			<?php echo $text->truncate($symptom['Symptom']['definition'], ADMIN_TEXT_TRIM, '...', true); ?>
		</td>
		<td>
			<?php echo $html->link($symptom['Area']['title'], array('controller' => 'areas', 'action' => 'view', $symptom['Area']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $symptom['Symptom']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $symptom['Symptom']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $symptom['Symptom']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $symptom['Symptom']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>


<div class="symptoms view">
<h2><?php  __('Symptom');?></h2>
<div class="actions actions-inline listing-actions">
	<ul>
		<li><?php echo $html->link(__('Edit Symptom', true), array('action' => 'edit', $symptom['Symptom']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Symptom', true), array('action' => 'delete', $symptom['Symptom']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $symptom['Symptom']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Symptoms', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Symptom', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $symptom['Symptom']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $symptom['Symptom']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Definition'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $symptom['Symptom']['definition']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($symptom['Area']['title'], array('controller' => 'areas', 'action' => 'view', $symptom['Area']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php
 /*
 *  This functions reside in /config/related_listing.php
 *  They were moved to avoid code replication and ease modification
 */
?>


<?php 
$wyswig->attach(
	array(
		array(
		"id"=> 'TherapyShortDefinition',
		"layout" => "Medium" ),
		array(
			'id' => 'TherapyLongDefinition',
			'layout' => 'Medium' ), 
		array(
			'id' => 'TherapyIntroduction',
			'layout' => 'Basic' ),
		array(
			'id' => 'TherapyReferences',
			'layout' => 'Medium')
	)
);
?>
<div class="therapies form">
    <?php echo $form->create('Therapy');?>
    <fieldset>
        <legend><?php __('Edit Therapy');?></legend>
        <div class="left">
            <?php
            echo $form->input("id");
            echo $form->input('title');
            echo $form->input('path', array('label' => 'Path <br /><span class="sublabel">
            This field represents the url from where this condition will be available<br />
            Paths can only include letters, numbers and "-". I.e.: "sleep-dissorders"<br />
            This condition would be accessed at http://www.liiv.com/condition/sleep-dissorders</span>', 'disabled' => 'disabled'));
            echo $form->input('published');            
            echo $form->input('area_id');
            echo $form->input("introduction",array("rows"=>3));
            echo $form->input('short_definition',array("rows"=>3));
            echo $form->input('long_definition',array("rows"=>3));
            echo $form->input('benefits_list',array("rows"=>3,"label"=>"List of Benefits<br /><span class=\"sublabel\">Put each item on a separate line</span>"));
            echo $form->input('conditions_list',array("rows"=>3, 'label' => 'Conditions that may Benefit from this Therapy<br /><span class="sublabel">Put each item on a separate line</span>'));             
            echo $form->input('references',array("rows"=>3));
            echo $form->input('Article',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Audio',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Authority',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Organization',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Product',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Symptom',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Therapy',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Event',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Publication',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Story',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Video',array("class"=>"hidden","label"=>false,"div"=>false));
            ?>
        </div>
        <div class="right">
            <?php
            echo $this->element("auto_complete",array("box"=>"TherapyRelated","referrer"=>"Therapy","url"=>"/admin/therapies","cache"=>true));
            echo $html->div("related",false,array("id"=>"selectedRelated"));
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('List Therapies', true), array('action' => 'index'));?></li>
    </ul>
</div>

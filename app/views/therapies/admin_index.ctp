<?php $paginator->options(array("url"=>$filter)); ?>
<div class="therapies index">
<h2><?php __('Therapies');?><span class="listing-actions"><?php echo $html->link(__('New Therapy', true), array('action' => 'add')); ?></span></h2>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('short_definition');?></th>
	<th><?php echo $paginator->sort('long_definition');?></th>
	<th><?php echo $paginator->sort('area_id');?></th>
	<th><?php echo $paginator->sort('published');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($therapies as $therapy):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $therapy['Therapy']['title']; ?>
		</td>
		<td>
			<?php echo $text->truncate(strip_tags($therapy['Therapy']['short_definition']), ADMIN_TEXT_TRIM, '...', true);?>
		</td>
		<td>
			<?php echo $text->truncate(strip_tags($therapy['Therapy']['long_definition']), ADMIN_TEXT_TRIM, '...', true);?>
		</td>
		<td>
			<?php echo $html->link($therapy['Area']['title'], array('controller' => 'areas', 'action' => 'view', $therapy['Area']['id'])); ?>
		</td>
		<td style="text-align:center;">
			<?php echo $therapy['Therapy']['published'] ? 'yes': 'no';?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $therapy['Therapy']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $therapy['Therapy']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $therapy['Therapy']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $therapy['Therapy']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="therapies view">
<h2><?php  __('Therapy');?></h2>
<div class="actions actions-inline listing-actions">
	<ul>
		<li><?php echo $html->link(__('Edit Therapy', true), array('action' => 'edit', $therapy['Therapy']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Therapy', true), array('action' => 'delete', $therapy['Therapy']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $therapy['Therapy']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Therapies', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Therapy', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $therapy['Therapy']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Path'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($html->url('/therapies/'.$therapy['Therapy']['path'], true),'/therapies/'.$therapy['Therapy']['path'], array('target' => '_blank')); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Short Definition'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $therapy['Therapy']['short_definition']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Long Definition'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $therapy['Therapy']['long_definition']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Benefits Text'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $therapy['Therapy']['benefits_text']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('List of Benefits'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo textToList($therapy['Therapy']['benefits_list'], null, 12); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('References'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $therapy['Therapy']['references']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($therapy['Area']['title'], array('controller' => 'areas', 'action' => 'view', $therapy['Area']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $therapy['CreatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($therapy['Therapy']['created'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $therapy['UpdatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($therapy['Therapy']['updated'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php echo related_articles_table($therapy['Article'], $html, $text);?>

<?php echo related_audios_table($therapy['Audio'], $html, $text);?>

<?php echo related_authorities_table($therapy['Authority'], $html, $text);?>

<?php echo related_conditions_table($therapy['Condition'], $html, $text);?>

<?php echo related_events_table($therapy['Event'], $html, $text); ?>

<?php echo related_publications_table($therapy['Publication'], $html, $text); ?>

<?php echo related_stories_table($therapy['Story'], $html, $text); ?>

<?php echo related_organizations_table($therapy['Organization'], $html, $text);?>

<?php echo related_products_table($therapy['Product'], $html, $text);?>

<?php echo related_videos_table($therapy['Video'], $html, $text); ?>
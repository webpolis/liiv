<?php
echo $javascript->codeBlock("
window.tagCloud = ".((isset($tagCloud))?$tagCloud:"[]").";
window.poll = ".((isset($polls))?json_encode($polls):"[]").";
j(function(){
	generateTagCloud('therapy');
	generatePoll();
	if(j('#view_middle').height()<j('.right').height()){
		j('#view_middle').css({
			height: j('.right').height()+'px'
		});
	}
});
",array("inline"=>false));?>
<div class="conditions index">
	<div class="twoColsTop"></div>
	<div class="twoColsMiddle">
	    <div class="twoColsLeft">
			<h2><?php __('Therapies');?></h2>
			<?php echo $therapiesHomepage['Page']['content']?>
		</div>
	    <div class="twoColsRight">
			<?php echo $this->element("front_right_column"); ?>
	    </div>
	    <div class="clearer" style="margin:0px"></div>
	</div>
	<div class="twoColsBottom"></div>
</div>
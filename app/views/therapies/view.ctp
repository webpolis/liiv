<?php
echo $javascript->codeBlock("
window.poll = ".((isset($polls))?json_encode($polls):"[]").";
j(function(){
	generatePoll();
});
",array("inline"=>false));
?>
<div class="therapies view">
<div class="twoColsTop"></div>
<div class="twoColsMiddle">
    <div class="twoColsLeft">
			<h2>Therapy: <?php __($therapy['Therapy']['title']);?></h2>
			<?php echo $html->div("introduction",(!empty($therapy['Therapy']['introduction']))?$therapy['Therapy']['introduction']:"&nbsp;");?>
			<h2>Short Definition</h2>
			<?php echo $html->div("short_definition",(!empty($therapy['Therapy']['short_definition']))?$therapy['Therapy']['short_definition']:"&nbsp;");?>
			<h2>Long Definition</h2>
			<?php echo $html->div("long_definition",(!empty($therapy['Therapy']['long_definition']))?$therapy['Therapy']['long_definition']:"&nbsp;");?>
			<?php if(!empty($therapy['Therapy']['benefits_list'])):?>
			<h2>Benefits</h2>
			<?php echo $html->div("benefits_list",twoColsTextToList($therapy['Therapy']['benefits_list'], 'listStyle'));?>
			<div class="clearfloat"></div>
			<?php endif;?>
			<?php if (!empty($therapy['Therapy']['conditions_list'])):?>
			<h2>Conditions that may Benefit from this Therapy</h2>
			<?php echo $html->div('conditions_list', twoColsTextToList($therapy['Therapy']['conditions_list'],'listStyle'));?>
			<div class="clearfloat"></div>
			<?php endif;?>
			<?php echo $html->div('clearer', false, array('style'=>'height:5px')) ?>
			<?php if (!empty($therapy['Therapy']['references'])):?><h2>References</h2>
			<?php echo $html->div('references', $therapy['Therapy']['references']); ?>
			<?php endif;?>
			<?php echo $this->element("share_this");?>
			<?php echo $this->element("frontend_relations");?>
		</div>
    <div class="twoColsRight">
		<?php echo $this->element("front_right_column"); ?>
    </div>
    <div class="clearer" style="margin:0px"></div>
</div>
<div class="twoColsBottom"></div>
<div class="clearer" style="margin:0px"></div>
</div>

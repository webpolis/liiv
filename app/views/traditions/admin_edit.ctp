<?php
$wyswig->attach(array(
		'TraditionIntroduction',
        array('id'=>'TraditionDefinitionShort','layout' => 'Medium'),
        array('id' => 'TraditionDefinitionLong','layout' => 'Medium'),
        array('id' => 'TraditionBeliefs','layout' => 'Medium')
        )
    );
?>
<div class="traditions form">
    <?php echo $form->create('Tradition');?>
    <fieldset>
        <legend><?php __('Edit Tradition');?></legend>
        <div class="left">
            <?php
            echo $form->input("id");
            echo $form->input('title');
            echo $form->input('path', array('label' => 'Path <br /><span class="sublabel">
            This field represents the url from where this condition will be available<br />
            Paths can only include letters, numbers and "-". I.e.: "sleep-dissorders"<br />
            This tradition would be accessed at http://www.liiv.com/traditions/sleep-dissorders</span>', 'disabled' => 'disabled'));
            echo $form->input('published');
            echo $form->input('introduction',array("rows"=>2));
            echo $form->input('definition_short',array("rows"=>2, 'label' => __('Short Definition', true)));
            echo $form->input('definition_long', array('label' => __('Long Definition', true)));
            echo $form->input('beliefs',array("rows"=>3, 'label' => __('Foundational Beliefs', true)));
            echo $form->input('Article',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Audio',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Authority',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Organization',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Product',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Symptom',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Therapy',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Event',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Publication',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Story',array("class"=>"hidden","label"=>false,"div"=>false));
            echo $form->input('Video',array("class"=>"hidden","label"=>false,"div"=>false));
            ?>
        </div>
        <div class="right">
            <?php
            echo $this->element("auto_complete",array("box"=>"TraditionRelated","url"=>"/admin/traditions","referrer"=>"Tradition","cache"=>true));
            echo $html->div("related",false,array("id"=>"selectedRelated"));
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('List Traditions', true), array('action' => 'index'));?></li>
    </ul>
</div>

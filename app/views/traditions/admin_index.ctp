<div class="traditions index">
<h2><?php __('Traditions');?></h2>
<div class="actions actions-inline listing-actions">
	<ul>
		<li><?php echo $html->link(__('New Tradition', true), array('action' => 'add')); ?></li>
	</ul>
</div>

<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('definition_short');?></th>
	<th><?php echo $paginator->sort('definition_long');?></th>
	<th><?php echo $paginator->sort('published');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($traditions as $tradition):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $tradition['Tradition']['title']; ?>
		</td>
		<td>
			<?php echo $text->truncate(strip_tags($tradition['Tradition']['definition_short']), ADMIN_TEXT_TRIM, '...', true); ?>
		</td>
		<td>
			<?php echo $text->truncate(strip_tags($tradition['Tradition']['definition_long']), ADMIN_TEXT_TRIM, '...', true); ?>
		</td>
		<td style="text-align:center;">
			<?php echo $tradition['Tradition']['published'] ? 'yes': 'no';?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $tradition['Tradition']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $tradition['Tradition']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $tradition['Tradition']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $tradition['Tradition']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>


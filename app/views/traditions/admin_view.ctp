<div class="traditions view">
<h2><?php  __('Tradition');?></h2>
<div class="actions actions-inline listing-actions">
	<ul>
		<li><?php echo $html->link(__('Edit Tradition', true), array('action' => 'edit', $tradition['Tradition']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Tradition', true), array('action' => 'delete', $tradition['Tradition']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $tradition['Tradition']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Traditions', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Tradition', true), array('action' => 'add')); ?> </li>
	</ul>
</div>

	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tradition['Tradition']['title']; ?>
			&nbsp;
		</dd>
		<?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Path'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($html->url('/traditions/'.$tradition['Tradition']['path'], true),'/traditions/'.$tradition['Tradition']['path'], array('target' => '_blank')); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Short Definition'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tradition['Tradition']['definition_short']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Long Definition'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tradition['Tradition']['definition_long']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foundational Beliefs'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tradition['Tradition']['beliefs']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tradition['CreatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($tradition['Tradition']['created'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tradition['UpdatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($tradition['Tradition']['updated'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="traditions form">
<?php echo $form->create('Tradition');?>
	<fieldset>
 		<legend><?php __('Edit Tradition');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('definition_short');
		echo $form->input('definition_long');
		echo $form->input('beliefs');
		echo $form->input('learn_more');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Tradition.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Tradition.id'))); ?></li>
		<li><?php echo $html->link(__('List Traditions', true), array('action' => 'index'));?></li>
	</ul>
</div>

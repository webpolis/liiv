<?php
/*echo $javascript->codeBlock("
window.poll = ".((isset($polls))?json_encode($polls):"[]").";
j(function(){
	generatePoll();
});
",array("inline"=>false));*/
?>
<div class="traditions view">
<div class="twoColsTop"></div>
<div class="twoColsMiddle">
    <div class="twoColsLeft">
			<h2>Spiritual Traditions: <?php __($tradition['Tradition']['title']);?></h2>
			<?php echo $html->div("introduction",$tradition['Tradition']['introduction']);?>
			<?php if (!empty($tradition['Tradition']['definition_short'])):?>
				<h2>Short Definition</h2>
				<?php echo $html->div("definition_short",$tradition['Tradition']['definition_short']);
			endif;?>
			<?php if (!empty($tradition['Tradition']['definition_long'])):?>
				<h2>Long Definition</h2>
				<?php echo $html->div("definition_long",$tradition['Tradition']['definition_long']);
			endif;?>
			<?php if (!empty($tradition['Tradition']['beliefs'])):?>
				<h2>Foundational Beliefs</h2>
				<?php echo $html->div("beliefs",$tradition['Tradition']['beliefs']);?>
				<?php echo $html->div("clearer",false,array("style"=>"height:5px"));
			endif;?>
			<?php echo $this->element("share_this");?>
			<?php echo $this->element("frontend_relations");?>
		</div>
    <div class="twoColsRight">
		<?php echo $this->element("front_right_column",array("tagNavTitle"=>"Spiritual Traditions Navigation")); ?>
    </div>
    <div class="clearer" style="margin:0px"></div>
</div>
<div class="twoColsBottom"></div>
<div class="clearer" style="margin:0px"></div>
</div>
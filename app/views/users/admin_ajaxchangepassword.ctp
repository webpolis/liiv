<?php
/* @var $form FormHelper */
echo
	$form->create('User', array(
		'id'	=> 'UserAjaxChangePasswordForm',
		'url'	=> '#',
	)),
	$form->hidden('id'),
	$form->input('password', array(
		'label'	=> 'Current password',
		'type'	=> 'password',
		'id'	=> 'UserAjaxPassword',
		'value'	=> '',
		'class'	=> 'required UserAjaxPassword',
		'div'	=> false,
	)),
	$form->input('new_pass', array(
		'label'	=> 'New password',
		'type'	=> 'password',
		'id'	=> 'UserAjaxNewPass',
		'value'	=> '',
		'class'	=> 'required UserAjaxNewPass',
		'div'	=> false,
	)),
	$form->input('confirm', array(
		'label'	=> 'Confirm new password',
		'type'	=> 'password',
		'value'	=> '',
		'class'	=> 'required',
		'div'	=> false,
	)),
	$form->end();
?>
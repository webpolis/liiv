<?php
$paginator->options(array("url"=>$filter));
echo $javascript->codeBlock("
j(function(){
    j('#Filter').change(function(){
        location.href = '/admin/users/index/all/'+j('#Filter').attr('value');
    });
});
",array("inline"=>false));
?>
<div class="users index">
    <h2><?php
        __('Users');
        echo $form->select("Filter",$groups,$filter,array("div"=>false,"label"=>false,"style"=>"display:inline;clear:none;margin-left:10px"),false);
        ?>
        <span class="listing-actions"><?php echo $html->link(__('Add User', true), array('action' => 'add')); ?></span></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $paginator->sort('id');?></th>
            <th><?php echo $paginator->sort('username');?></th>
            <th><?php echo $paginator->sort('email');?></th>
            <th><?php echo $paginator->sort('picture');?></th>
            <th><?php echo $paginator->sort('link');?></th>
            <th><?php echo $paginator->sort('group_id');?></th>
            <th><?php echo $paginator->sort('created');?></th>
            <th><?php echo $paginator->sort('updated');?></th>
            <th class="actions"><?php __('Actions');?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($users as $user):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
        <tr<?php echo $class;?>>
            <td>
                    <?php echo $user['User']['id']; ?>
            </td>
            <td>
                    <?php echo $user['User']['username']; ?>
            </td>
            <td>
                    <?php echo $user['User']['email']; ?>
            </td>
            <td>
                    <?php echo $user['User']['picture']; ?>
            </td>
            <td>
                    <?php echo $user['User']['link']; ?>
            </td>
            <td>
                    <?php echo $html->link($user['Group']['title'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>
            </td>
            <td>
                    <?php echo $user['User']['created']; ?>
            </td>
            <td>
                    <?php echo $user['User']['updated']; ?>
            </td>
            <td class="actions">
                    <?php echo $html->link(__('View', true), array('action' => 'view', $user['User']['id'])); ?>
                    <?php echo $html->link(__('Edit', true), array('action' => 'edit', $user['User']['id'])); ?>
                    <?php echo $html->link(__('Delete', true), array('action' => 'delete', $user['User']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $user['User']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>
<p>
    <?php
    echo $paginator->counter(array(
    'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
    ));
    ?></p>
<div class="paging">
    <?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
    | 	<?php echo $paginator->numbers();?>
    <?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>

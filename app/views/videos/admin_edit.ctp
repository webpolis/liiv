<?php
echo $this->element('video_player');
?>
<div class="videosadd form">
	<?php echo $form->create('Video'); ?>
	<fieldset>
		<legend><?php __('Edit Video'); ?></legend>
		<div class="left">
			<?php
			echo
				$form->input('id'),
				$form->input('title'),
				$form->input('published'),
				$form->input('featured'),
				$form->input('url', array('label'=>'Path', 'disabled'=>true)),
				$form->input('author'),
				$form->input('description_short'),
				$form->input('content', array('style'=>'float:left;width:83%;display:inline;clear:left', 'div'=>false, 'label'=>'YouTube/Vimeo URL or path to video (updated once uploaded)')),
					$html->tag('span', '', array('id' =>'upBtnHolderLowRes')),
					$html->div('', '', array('id' =>'upBtnCancelLowRes')),
					$html->div('', '', array('id' =>'upProgressLowRes')),
					$html->div('', '', array('id' =>'upPercentLowRes')),
					$form->button('Preview', array('id'=>'previewBtnLowRes', 'style'=>'display:block;clear:both')),
				$form->input('video_high_res', array('readonly'=>true, 'style'=>'float:left;width:83%;display:inline;clear:left', 'div'=>false, 'label'=>'Click "Upload" for Hi-res video (updated once uploaded)')),
					$html->tag('span', '', array('id' =>'upBtnHolderHiRes')),
					$html->div('', '', array('id' =>'upBtnCancelHiRes')),
					$html->div('', '', array('id' =>'upProgressHiRes')),
					$html->div('', '', array('id' =>'upPercentHiRes')),
					$form->button('Preview', array('id'=>'previewBtnHiRes', 'style'=>'display:block;clear:both')),
				$form->input('pdf', array('style'=>'float:left;width:83%;display:inline;clear:left', 'div'=>false, 'label'=>'Click "Upload" to attach a PDF document to this video')),
					$html->tag('span', '', array('id' =>'upBtnHolderPdf')),
					$html->div('', '', array('id' =>'upBtnCancelPdf')),
					$html->div('', '', array('id' =>'upProgressPdf')),
					$html->div('', '', array('id' =>'upPercentPdf')),
				$form->input("pdf_title"),
				$form->input('area_id'),
				$form->input('Article', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Condition', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Audio', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Authority', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Organization', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Product', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Symptom', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Therapy', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Event', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Publication', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Story', array('class'=>'hidden', 'label'=>false, 'div'=>false)),
				$form->input('Video', array('class'=>'hidden', 'label'=>false, 'div'=>false));
			?>
		</div>
		<div class="right" style="padding:0 10px;position:relative">
			<?php
			echo
				$this->element('auto_complete', array('box'=>'VideoRelated', 'url'=>'/admin/videos', 'referrer'=>'Video', 'cache'=>true)),
				$html->div('related', false, array('id'=>'selectedRelated'));
			?>
		</div>
	</fieldset>
	<?php echo $form->end('Submit'); ?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Videos', true), array('controller' => 'videos', 'action' => 'index')); ?></li>
		<li><?php echo $html->link(__('New Video', true), array('controller' => 'videos', 'action' => 'add')); ?></li>
	</ul>
</div>
<div id="videoPreview" style="display:none"></div>

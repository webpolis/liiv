<?php
/* @var $paginator PaginatorHelper */
$paginator->options(array('url'=>$filter));
?>
<div class="videosindex index">
	<h2><?php __('Videos'); ?><span class="listing-actions"><?php echo $html->link(__('New Video', true), array('action' => 'add')); ?></span></h2>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $paginator->sort('title'); ?></th>
			<th>Thumbnail</th>
			<th><?php echo $paginator->sort('Path', 'url'); ?></th>
			<th><?php echo $paginator->sort('author'); ?></th>
			<th><?php echo $paginator->sort('area_id'); ?></th>
			<th><?php echo $paginator->sort('created'); ?></th>
			<th class="actions"><?php __('Actions'); ?></th>
		</tr>
<?php $i = 0; foreach ($videos as $video): ?>
		<tr<?php if (($i++ % 2) == 0) echo ' class="altrow"'; ?>>
			<td><?php echo $video['Video']['title']; ?></td>
			<td style="position:relative" align="center"><?php
				$re_tube = '/youtube\\.com\\/(?:watch\\?v\\=|v\\/)([\\w\\-]+)/i';
				$re_vimeo = '/vimeo\\.com\\/(\\d+)/i';
				$re_custom = '/^\\/videos\\/([^.]+)\\.(?:flv|mp4|m4v|mov|avi|mpe?g)$/i';
				$path = $video['Video']['content'];
				$match = null;
				if (preg_match($re_tube, $path, $match)) {
					$thumb = 'http://img.youtube.com/vi/'.$match[1].'/default.jpg';
				}
				elseif (preg_match($re_vimeo, $path, $match)) {
					$thumb = '/admin/videos/getVimeoThumb/'.$match[1];
				}
				elseif (preg_match($re_custom, $path, $match)) {
					$thumb = '/videos/'.$match[1].'.jpg';
					if (!file_exists(WWW_ROOT.substr($thumb, 1))) {
						$thumb = '/img/unavailable.png';
					}
				}
				else {
					$thumb = '/img/unavailable.png';
				}
				echo $html->image($thumb, array('border'=>0, 'class'=>'thumbnail', 'style'=>'position:static'));
			?></td>
			<td><?php echo $video['Video']['url']; ?></td>
			<td><?php echo $video['Video']['author']; ?></td>
			<td><?php echo $html->link($video['Area']['title'], array('controller' => 'areas', 'action' => 'view', $video['Area']['id'])); ?></td>
			<td><?php echo $video['Video']['created']; ?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('action' => 'view', $video['Video']['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('action' => 'edit', $video['Video']['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('action' => 'delete', $video['Video']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $video['Video']['id'])); ?>
			</td>
		</tr>
<?php endforeach; ?>
	</table>
</div>
<p><?php echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true))); ?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled')); ?>
	| <?php echo $paginator->numbers(); ?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled')); ?>
</div>
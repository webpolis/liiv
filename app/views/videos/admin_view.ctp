<div class="admin_videos view">
    <h2><?php  __('Video');?></h2>
<div class="actions actions-inline listing-actions">
    <ul>
        <li><?php echo $html->link(__('Edit Video', true), array('action' => 'edit', $video['Video']['id'])); ?> </li>
        <li><?php echo $html->link(__('Delete Video', true), array('action' => 'delete', $video['Video']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $video['Video']['id'])); ?> </li>
        <li><?php echo $html->link(__('List Videos', true), array('action' => 'index')); ?> </li>
        <li><?php echo $html->link(__('New Video', true), array('action' => 'add')); ?> </li>
    </ul>
</div>
    <dl><?php $i = 0; $class = ' class="altrow"';?>
        <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class;?>>
            <?php echo $video['Video']['id']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class;?>>
            <?php echo $video['Video']['title']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Author'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class;?>>
            <?php echo $video['Video']['author']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description Short'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class;?>>
            <?php echo strip_tags($video['Video']['description_short']); ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Content'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class;?>>
            <?php echo $video['Video']['content']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Area'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class;?>>
            <?php echo $html->link($video['Area']['title'], array('controller' => 'areas', 'action' => 'view', $video['Area']['id'])); ?>
            &nbsp;
        </dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $video['CreatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($video['Video']['created'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $video['UpdatedBy']['email'], ' on ', date('m-d-y g:i a',strtotime($video['Video']['updated'])); ?>
			&nbsp;
		</dd>
    </dl>
</div>

<?php echo related_articles_table($video['Article'], $html, $text);?>
<?php echo related_stories_table($video['Story'], $html, $text); ?>
<?php echo related_audios_table($video['Audio'], $html, $text);?>
<?php echo related_authorities_table($video['Authority'], $html, $text);?>
<?php echo related_conditions_table($video['Condition'], $html, $text);?>
<?php echo related_events_table($video['Event'], $html, $text); ?>
<?php echo related_organizations_table($video['Organization'], $html, $text);?>
<?php echo related_publications_table($video['Publication'], $html, $text); ?>
<?php echo related_therapies_table($video['Therapy'], $html, $text); ?>
<?php echo related_videos_table($video['VideoRelated'], $html, $text); ?>
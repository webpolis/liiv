<?php
$rightContent = '<h1>'.__("Featured Video",true)." : ".$video['Video']['title'].'</h1>';
$rightContent .= $html->div("clearer",false,array("style"=>"height:5px"));
$rightContent .= $video['Video']['description_short'];
?>
<div class="view page spaced featured"><?php 
echo $this->element('home_white_box', array('width' => 980, "height"=>"auto",'content' => $html->div(false,$rightContent, array('style' => 'padding: 0 5px 0 10px;'))));
?></div>
<div class="clearer" style="margin: 0px"></div>

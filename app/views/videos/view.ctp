<?php
// for fn in *.flv;  do ffmpeg -i $fn -vframes 1 -ss 5 -an -s 618x384 $fn.jpg; done;
$preloadImage = null;
$fileId = (isset($video['Video']['content'])) ? preg_replace("/\/videos\/(\d+)\.(.*)$/si","$1",$video['Video']['content']) : null;
if(file_exists(dirname(dirname(dirname(__FILE__)))."/webroot/videos/".$fileId.".flv.jpg")){
	$preloadImage = $fileId.".flv.jpg";
}

$javascript->link(array('flowplayer.min.js', 'flowplayer.embed.min.js', 'player.js'), false);
echo $javascript->codeBlock('
window.tagCloud = '.(isset($tagCloud)? $tagCloud: '[]').';
window.poll = '.(isset($polls)? json_encode($polls): '[]').';
j(function() {
	generateTagCloud("condition");
	generatePoll();
	var view_middle = j("#view_middle"),
		right_height = j(".right").height();
	if (view_middle.height() < right_height) {
		view_middle.css({height:right_height+"px"});
	}
	previewVideo(\''.$preloadImage.'\');
});
', array('inline'=>false));
?>
<div class="twoColsTop"></div>
<div class="twoColsMiddle">
	<div class="twoColsLeft">
		<h2><?php  __('Now playing:'); ?></h2>
		<p class="videoInfo">
			<?php echo $video['Video']['title']; ?><br />
			<?php if (!empty($video['Videoseries']['title'])) echo $video['Videoseries']['title'], '<br />'; ?>
			<?php echo $video['Video']['author']; if (!empty($video['Organization']['name'])) echo ', ', $video['Organization']['name'] ?><br />
			00:00:00 | <a href="#desc">View description</a>
			<?php if(isset($video['Video']['pdf'])&&!empty($video['Video']['pdf'])){?>
			| Download companion workshop PDF: <a href="<?php echo $video['Video']['pdf'];?>">
			<?php echo (isset($video['Video']['pdf_title'])&&!empty($video['Video']['pdf_title']))? $video['Video']['pdf_title'] :$video['Video']['pdf']?>
			</a>
			<?php }?>
		</p>
		<div id="player-container" style="position:relative">
<?php
echo $html->div(false,
$html->link($html->image("play_large.png",array("border"=>0,"style"=>"margin-left:268px;margin-top:150px;z-index:10001")),"javascript:void(playVideo());",array("escape"=>false)),
array("style"=>"display:none;position:absolute;background:#000;width:618px;height:384px;border:1px solid #000;z-index:10000",
"id"=>"preloadContainer"));
$rel = array();
if (!empty($video['Video']['video_high_res'])) {
	$rel[] = 'hiRes:"'.$video['Video']['video_high_res'].'"';
}
if (!empty($video['Video']['captions_file'])) {
	$rel[] = 'captions:"'.$video['Video']['captions_file'].'"';
}
?>
			<a href="<?php echo $video['Video']['content']; ?>" id="player-video"<?php if (!empty($rel)) echo ' rel="', htmlspecialchars(implode(',', $rel)), '"'; ?>></a>
			<div id="player-controls">
				<a href="#" id="player-embed" title="Embed this video in your site">Embed</a>
				<a href="#" id="player-hiRes" title="Watch this video in Hi-res">Watch Hi-res</a>
				<a href="#" id="player-lowRes" title="Watch this video in Low-res">Watch Low-res</a>
				<a href="#" id="player-cc" title="Show captions">CC</a>
				<div id="player-embed-dialog"><textarea readonly></textarea></div>
			</div>
		</div>
		<a name="desc" id="desc"></a>
		<div class="video-description-short"><?php echo $video['Video']['description_short']; ?></div>
		<?php echo $this->element('share_this') ?>
		<div class="view"><?php echo $this->element('frontend_relations') ?></div>
	</div>
	<div class="twoColsRight">
		<?php echo $this->element('front_right_column', array('tagCloudModel' => 'Condition')); ?>
	</div>
	<div class="clearer" style="margin:0"></div>
</div>
<div class="twoColsBottom"></div>
<div class="clearer" style="margin:0"></div>

<div class="videoseries form">
<?php echo $form->create('Videoseries');?>
	<fieldset>
 		<legend><?php __('Add Videoseries');?></legend>
	<?php
		echo $form->input('title');
		echo $form->input('user_id');
		echo $form->input('createdby');
		echo $form->input('updatedby');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Videoseries', true), array('action' => 'index'));?></li>
	</ul>
</div>

<?php
echo $javascript->link(array("/player/swfobject","swfu/default"),false);
echo $javascript->codeBlock("
j(function(){
    initVideoSeries();
    j('#videoPreview').dialog({
        width:default_video.w+50,
        height:default_video.h+50,
        title:'Video Preview',
        autoOpen:false,
        position:'top'
    });
});
",array("inline"=>false));
echo $html->div(false,false,array("id"=>"videoPreview","style"=>"display:none"));
?>
<div class="videoseries form">
    <?php echo $form->create('Videoseries');?>
    <fieldset>
        <legend><?php __('Edit Video Serie');?></legend>
        <div class="left">
            <?php
            echo $form->input("id");
            echo $form->input('title',array("label"=>"Title of the Serie"));
            echo $form->input('author');
            echo '<fieldset class="sub">';
            echo $html->para(false,"You can add into this serie a new Video loaded from an external source, uploading a new one or by choosing an existing item.");
            $custom = $form->input('media',array("style"=>"width:50%;display:inline;clear:left;float:left;margin-right:5px","label"=>"Custom Video"));
            $custom .= $this->element("video_uploader",array("field"=>"VideoseriesMedia","folder"=>"series"));
            $external = $form->input("ExternalVideo",array("div"=>false,"style"=>"width:50%;display:inline;clear:left;float:left;margin-right:5px","label"=>"External URL"));
            $external .= $form->button("Add",array("id"=>"btnAdd","style"=>"margin:0!important;display:inline;clear:right;float:left"));
            $autocomplete = $this->element("auto_complete",array("dropdown"=>false,"title"=>"Add Existing Video","box"=>"VideoseriesRelated","url"=>"/admin/videoseries","referrer"=>"Videoseries","cache"=>true,"filter"=>"videos"));
            echo $form->input("SourceTitle",array("label"=>"Video Title"));
            echo $form->radio("Source",array("custom"=>"Upload","external"=>"External Source"),array("checked"=>"external","legend"=>false,"label"=>false,"separator"=>"&nbsp;","id"=>"Source"));
            echo $html->div(false,$custom,array("id"=>"customVideo","style"=>"display:none"));
            echo $html->div(false,$external,array("id"=>"externalVideo","style"=>"display:none"));
            echo $html->div(false,$autocomplete,array("style"=>"display:block;clear:both"));
            echo $form->input("collection",array("type"=>"hidden"));
            echo $form->input("NewVideos",array("label"=>false,"div"=>false,"class"=>"hidden","type"=>"select","multiple"=>true));
            echo $form->input("NewTitles",array("label"=>false,"div"=>false,"class"=>"hidden","type"=>"select","multiple"=>true));
            echo $form->input("Video",array("label"=>false,"div"=>false,"class"=>"hidden","multiple"=>true));
            echo '</fieldset>';
            ?>
        </div>
        <div class="right">
            <?php
            echo $html->div("related","<ul id=\"existing\" class=\"videoSeries\" style=\"list-style-type:none\"></ul>",array("id"=>"selectedRelated"));
            ?>
        </div>
    </fieldset>
    <?php echo $form->end('Submit');?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $html->link(__('List Video Series', true), array('action' => 'index'));?></li>
    </ul>
</div>

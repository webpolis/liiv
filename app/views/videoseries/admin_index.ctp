<?php $paginator->options(array("url"=>$filter)); ?>
<div class="videoseries index">
<h2><?php __('Video Series');?></h2>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('updated');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($videoseries as $videoseries):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $videoseries['Videoseries']['id']; ?>
		</td>
		<td>
			<?php echo $videoseries['Videoseries']['title']; ?>
		</td>
		<td>
			<?php echo $videoseries['Videoseries']['created']; ?>
		</td>
		<td>
			<?php echo $videoseries['Videoseries']['updated']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $videoseries['Videoseries']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $videoseries['Videoseries']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $videoseries['Videoseries']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $videoseries['Videoseries']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Serie', true), array('action' => 'add')); ?></li>
	</ul>
</div>

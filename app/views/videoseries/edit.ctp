<div class="videoseries form">
<?php echo $form->create('Videoseries');?>
	<fieldset>
 		<legend><?php __('Edit Videoseries');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('title');
		echo $form->input('user_id');
		echo $form->input('createdby');
		echo $form->input('updatedby');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Videoseries.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Videoseries.id'))); ?></li>
		<li><?php echo $html->link(__('List Videoseries', true), array('action' => 'index'));?></li>
	</ul>
</div>

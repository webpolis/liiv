<div class="videoseries view">
<h2><?php  __('Videoseries');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $videoseries['Videoseries']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $videoseries['Videoseries']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $videoseries['Videoseries']['user_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $videoseries['Videoseries']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $videoseries['Videoseries']['updated']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Createdby'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $videoseries['Videoseries']['createdby']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updatedby'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $videoseries['Videoseries']['updatedby']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Videoseries', true), array('action' => 'edit', $videoseries['Videoseries']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Videoseries', true), array('action' => 'delete', $videoseries['Videoseries']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $videoseries['Videoseries']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Videoseries', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Videoseries', true), array('action' => 'add')); ?> </li>
	</ul>
</div>

<div class="workshops form">
<?php echo $form->create('Workshop');?>
	<fieldset>
 		<legend><?php __('Add Workshop');?></legend>
	<?php
		echo $form->input('title');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Workshops', true), array('action' => 'index'));?></li>
	</ul>
</div>

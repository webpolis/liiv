<?php
$root = dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR;
require_once($root.'config'.DIRECTORY_SEPARATOR.'bootstrap.php');

// Unlimited execution time
set_time_limit(0);

/**
 * @return string
 */
function filenameHash() {
	return sha1(rand(0, 10000000) * microtime());
}

/**
 * @param  string $folder
 * @param  string $ext
 * @param  string $hash
 * @return string
 */
function assureFilenameUniqueness($folder, $ext, &$hash) {
	$filename = $hash.'.'.$ext;
	while (file_exists($folder.$filename)) {
		$hash = filenameHash();
		$filename = $hash.'.'.$ext;
	}
	return $filename;
}

$isWin = (bool)preg_match('/WIN/i', PHP_OS);
if (isset($_FILES) && isset($_FILES['Filedata'])) {
	$data = $_FILES['Filedata'];
	$hash = (isset($_POST['newname'])? strtolower(trim($_POST['newname'])): filenameHash());
	$ext = strtolower(substr(strrchr($data['name'], '.'), 1));
	if (!isset($_POST['image'])) {
		if ($data['error'] == 0) {
			$isAudio = (preg_match('/\\.(?:mp3|wav|ogg)$/i', $ext)) ? true : false;
			$isPdf = (preg_match('/\\.(?:pdf)$/i', $ext)) ? true : false;
			$videofolder = $root.'/webroot/'.($isAudio? 'audios/': 'videos/');
			if (isset($_POST['path'])) {
				$videofolder .= strtolower(trim($_POST['path'])).'/';
			}
			if ($isWin) {
				$videofolder = str_replace('/', '\\', $videofolder);
			}
			if (!is_dir($videofolder)) {
				mkdir($videofolder, 0775);
			}
			$filename = assureFilenameUniqueness($videofolder, $ext, $hash);
			if (move_uploaded_file($data['tmp_name'], $videofolder.$filename)) {
				$newname = $hash.($isAudio? '.mp3': '.'.$ext);
				if ($ext !== 'flv') {
					// File needs to be converted
/*
# Juan@2010-02-10
# Video files are now uploaded in an already supported format
					if ($isAudio) {
						$cmd = 'ffmpeg -y -i '.$videofolder.$filename.' -s '.VIDEO_WIDTH.'x'.VIDEO_HEIGHT.' -r 25 -ar 22050 -ab 56 -b 400 -f flv '.$videofolder.$newname;
					}
					else {
						$cmd = 'lame'.($ext=='ogg'? ' --ogginput': ($ext=='mp3'? ' --mp3input': '')).' -h -b 128 -m j --tl \'Liiv.com\' '.$videofolder.$filename.' '.$videofolder.$newname;
					}
					if ($isWin) {
						$cmd = 'C:\\ffmpeg\\'.str_replace('/', '\\', $cmd);
					}
					$out = system($cmd);
*/
					if ($isAudio) {
						$cmd = 'ffmpeg -y -i '.$videofolder.$filename.' -s '.VIDEO_WIDTH.'x'.VIDEO_HEIGHT.' -r 25 -ar 22050 -ab 56 -b 400 -f flv '.$videofolder.$newname;
						if ($isWin) {
							$cmd = 'C:\\ffmpeg\\'.str_replace('/', '\\', $cmd);
						}
						$out = system($cmd);
					}
				}
				if (!$isAudio) {
					// Generate thumbnail for video
					$thumb = 'ffmpeg -i '.$videofolder.$filename.' -vcodec mjpeg -ss 5 -vframes 1 -an -f rawvideo -s 60x60 '.$videofolder.$hash.'.jpg';
					$thumb2 = 'ffmpeg -i '.$videofolder.$filename.' -vcodec mjpeg -ss 5 -vframes 1 -an -f rawvideo -s 618x384 '.$videofolder.$hash.'_preload.jpg';
					if ($isWin) {
						$thumb = 'C:\\ffmpeg\\'.str_replace('/', '\\', $thumb);
					}
					else {
						$thumb = '/usr/local/bin/'.$thumb;
					}
					$out = system($thumb);
					$out = system($thumb2);
				}
				elseif ($ext != 'mp3') {
					unlink($videofolder.$filename);
				}
				echo $newname;
			}
		}
		else {
			echo 'Error while processing upload.';
		}
	}
	else {
		$imgfolder = (!isset($_POST['path']))? $root.'webroot/img/wysiwyg/': $root.'webroot/img/'.($_POST['path']!=''? strtolower(trim($_POST['path'])): '').'/';
		$imgfolder = (isset($_POST['path']) && $_POST['path']=='banners')? $root.'webroot/banners/': $imgfolder;
		if (!is_dir($imgfolder)) {
			mkdir($imgfolder, 0775, true);
		}
		$filename = assureFilenameUniqueness($imgfolder, $ext, $hash);
		if (move_uploaded_file($data['tmp_name'], $imgfolder.$filename)) {
			$f = ($isWin? str_replace('/', '\\', $imgfolder.$filename): $imgfolder.$filename);
			$app = ($isWin? 'C:\\apps\\imagemagick\\': '');
			if (stripos($imgfolder, 'liiv_today') !== false) {
				// Resize
				$cmd = $app."/usr/local/bin/convert -resize 100x100! -quality 75 $f $f";
				$out = system($cmd);
			}
			if (stripos($imgfolder, 'banners') !== false) {
				// Resize
				$cmd = $app."convert $f $f"; //-scale 980x !
				$out = system($cmd);
			}
			if (stripos($imgfolder, 'slider') !== false) {
				// Resize
				$cmd = $app."convert -scale 310x310! -quality 75 $f $f";
				$out = system($cmd);
			}
			echo $filename;
		}
	}
}
?>
﻿var j = jQuery.noConflict();

j(function() {
	var alertWin = j("#alertWindow");
	alertWin.dialog({
		resizable: false,
		modal :true,
		autoOpen: false,
		title: "Information",
		buttons: {
			"Ok": function() {
				j(this).dialog("close");
			}
		}
	});

	window.alert = function(t, fn) {
		var cbk = (typeof(fn) != "undefined"? fn.cbk: false);
		t = t+""; // Ensure t is a string
		if (cbk) {
			alertWin
				.html(t)
				.dialog("option", "width", (fn.width? fn.width: 400))
				.dialog("option", "height", (fn.height? fn.height: 180))
				.dialog("option", "buttons", {"Ok": cbk})
				.dialog("open");
		}
		else {
			alertWin.html(t).dialog("open");
		}
	};

	window.showMessage = function(message) {
		var msg = j("#successMessage");
		msg.html(message).fadeIn(1000);
		setTimeout(function() {
			msg.fadeOut(1000);
		}, 3000);
	};
	window.showError = function(message) {
	};

	// Fix to show parent of submenu selected items as selected too
	j("li.active").parent("ul").parent("li").addClass("active");
});

/**
 * set autoComplete object
 */
function attachAutocompleteRelated(id, from, referrer) {
	window.autoComplete = {
		filter:'all',
		limit:10,
		offset:0,
		ajax:false,
		position:0,
		current:{
			model:'',
			id:0,
			referrer:''
		},
		results:0,
		filterOpen: false
	};
	j('body').bind('click',function(ev) {
		if (j('#autocomplete').is('div')) {
			if (!window.autoComplete.filterOpen) j('#autocomplete').fadeOut();
			window.autoComplete.position = 0;
		}
	});
	j('#'+id).bind('keyup',function(e) {
		j('body').bind('keypress',function(e,o) {
			if (e.keyCode == 13) {
				return false;
			}
		});
		var charCode = (typeof(e.keyCode)!='undefined')?e.keyCode : e.which;
		var cur = '';
		var prev = '';
		var next ='';
		var max = 0;
		switch (charCode) {
			case 37: // left
				break;
			case 39: // right
				break;
			case 13:
				if (typeof(window.autoComplete.current.id)!=0) {
					addRelated(window.autoComplete.current.model,window.autoComplete.current.id,window.autoComplete.current.referrer);
					j('#autocomplete').empty().fadeOut();
				}
				break;
			case 38: // up
				max = (window.autoComplete.results<window.autoComplete.limit)?window.autoComplete.results-1:window.autoComplete.limit;
				if ((window.autoComplete.position--)<=0) window.autoComplete.position = max;
				cur = '.position-'+window.autoComplete.position;
				j('div.result').each(function(i,e) {
					j(e).removeClass('resultHighlighted');
				});
				j(cur).toggleClass('resultHighlighted');
				window.autoComplete.current.model = (typeof(j(cur).attr('id'))!='undefined')?j(cur).attr('id').replace(/div\-selected\-([^\-]+)\-\d+/gi,'$1'):'';
				window.autoComplete.current.id = (typeof(j(cur).attr('id'))!='undefined')?parseInt(j(cur).attr('id').replace(/div\-selected\-[^\-]+\-(\d+)/gi,'$1')):'';
				j('#autocomplete').scrollTo(cur, 'fast', {
					over: {
						top: -2
					}
				});
				break;
			case 40: // down
				max = (window.autoComplete.results < window.autoComplete.limit)?window.autoComplete.results-1:window.autoComplete.limit;
				cur = '.position-'+window.autoComplete.position;
				if ((window.autoComplete.position++)>=max) window.autoComplete.position = 0;
				j('div.result').each(function(i,e) {
					j(e).removeClass('resultHighlighted');
				});
				j(cur).toggleClass('resultHighlighted');
				window.autoComplete.current.model = (typeof(j(cur).attr('id'))!='undefined')?j(cur).attr('id').replace(/div\-selected\-([^\-]+)\-\d+/gi,'$1'):'';
				window.autoComplete.current.id = (typeof(j(cur).attr('id'))!='undefined')?parseInt(j(cur).attr('id').replace(/div\-selected\-[^\-]+\-(\d+)/gi,'$1')):'';
				j('#autocomplete').scrollTo(cur, 'fast', {
					over: {
						top: -2
					}
				});
				break;
			default:
				var v = j('#'+id).attr('value');
				var div = '<div class="autocomplete" id="autocomplete"></div>';
				if (!j('#autocomplete').is('div')) {
					j('label[for='+referrer+'Related]').after(div);
				}
				if (v.length>1) {
					if (typeof(window.autoComplete) != 'undefined' && window.autoComplete.ajax) {
						if (typeof(window.autoComplete.ajax.abort())!='undefined') {
							window.autoComplete.ajax.abort();
						}
					}
					j('#autocomplete').css({
						top:'58px',
						left:'0px',
						width:'95%'
					}).html('<img src="/img/ajax-loader.gif" border="0" />&nbsp;Searching items...').fadeIn();
					window.autoComplete.ajax = j.ajax({
						url:from+'/relate/search/'+escape(v)+'/'+autoComplete.filter+'/'+autoComplete.limit+'/'+autoComplete.offset,
						cache:false,
						type:'GET',
						dataType:'json',
						success:function(d) {
							var ret = getRelatedResults(d,referrer);
							j('#autocomplete').html(ret);
							window.autoComplete.ajax = false;
							window.autoComplete.filterOpen = false;
						}
					});
				}
				else {
					j('#autocomplete').empty().fadeOut();
				}
				break;
		}
	});
}

function getYouTubeId(t) {
	var re_tube = /.*youtube\.com\/(watch\?v\=|v\/)([a-z0-9_\-]+).*/gi;
	return t.replace(re_tube,'$2');
}

function getVimeoId(t) {
	var re_vimeo = /.*vimeo\.com\/(\d+).*/gi;
	return t.replace(re_vimeo,'$1');
}

function getRelatedResults(result,referrer) {
	var re_tube = /.*youtube\.com\/(watch\?v\=|v\/)([a-z0-9_\-]+).*/gi;
	var re_vimeo = /(.*vimeo\.com\/)(\d+).*/gi;
	var re_custom = /^(\/videos\/)([^\.]+)\.flv$/gi;
	var out = 'Your search does not match any item.';
	var results = '';
	var n = 0;
	if (typeof(result)!='undefined') {
		for (type in result) {
			if (result[type] && result[type].length >0) {
				for (x in result[type]) {
					var e = result[type][x];
					var model = '';
					var videoid = '';
					var thumb = '';
					for (property in e) {
						model = property;
						eval('e = e.'+property);
					}
					model = model.replace(/related/gi,'').replace(/[a-z]+Symptom/gi,'Symptom'); // fix
																								// for
																								// "Primary"
																								// &
																								// "Secondary"
																								// Symptoms,
																								// treath
																								// all
																								// the
																								// same
					if (model == "Video") {
						if (e.content.match(re_tube)) {
							videoid = getYouTubeId(e.content);
							thumb = '<img src="http://img.youtube.com/vi/'+videoid+'/default.jpg" class="thumbnail" />';
						}
						if (e.content.match(re_custom)) {
							videoid = e.content.replace(re_custom,'$2');
							thumb = '<img src="/videos/'+videoid.replace(/^https?:.*$/gi,'undef')+'.jpg" class="thumbnail" />';
						}
						if (e.content.match(re_vimeo)) {
							videoid = e.content.replace(re_vimeo,'$2');
							thumb = '<img src="/admin/videos/getVimeoThumb/'+videoid+'" class="thumbnail" />';
						}
					}
					var title = (typeof(e.title)!='undefined')?e.title:e.name;
					results += '<div id="div-selected-'+model+'-'+e.id+'" class="result '+type+' '+((model=="Symptom")?"symptom":'')+' position-'+(n++)+'" onclick="javascript:void(addRelated(\''+model+'\','+e.id+',\''+referrer+'\'));"><a href="javascript:void(0)"><span class="model">'+model+'</span> '+thumb+title+'</a></div>';
				}
			}
		}
	}
	window.autoComplete.results = n;
	window.autoComplete.limit = (window.autoComplete.results>window.autoComplete.limit)?window.autoComplete.results:window.autoComplete.limit;
	window.autoComplete.current.referrer = referrer;
	return (n>0)?results:out;
}

function addRelated(model,id,referrer) {
	var newid = 'selected-'+model+'-'+id;
	var habtm = (j('#'+model+model).is('select'))?true : false;
	j('#'+((!habtm)?referrer+model:model+model)+' option').each(function(i,e) {
		var val = j(e).attr('value');
		if (val == id) {
			j(e).attr('selected',true);
		}
	});
	var thumb = (j('#div-'+newid).is('div') && j('#div-'+newid).html().match(/thumbnail/gi)) ?
	j('#div-'+newid).html().replace(/.*(\<img[^\>]+\>).*/gi,'$1'):'';
	var added = (j('#div-'+newid).is('div'))?j('#div-'+newid).html().replace(/<img[^>]+>/gi,'')+thumb:'';
	if (!j('#added-'+newid).is('div')) {
		var therapy = (model=='Therapy'&& location.pathname.match(/conditions/gi))? '&nbsp;<a href="javascript:void(editProCon('+id+'));"><img title="Edit Pro/Con" alt="Edit Pro/Con" src="/img/icons/Computer_File_052.gif" border="0" /></a>': '';
		var newhtml = '<div style="position:relative;'+((model=="Symptom")?'height:35px;line-height:35px':((model=="Video")?'line-height:64px;height:64px':''))+'" class="result '+((model=="Symptom")?"Symptom":'')+'" id="added-'+newid+'">'+
		'<span class="delete" onclick="javascript:void(removeRelated(\'added-'+newid+'\',\''+model+'\'));">[X]</span>&nbsp;'+added+therapy+'</div>';
		if (model=="Symptom") {
			j('#selectedSymptoms').append(newhtml);
		}
		else {
			if (referrer=="Videoseries") {
				addVideoSerieItem(added,id);
			}
			else {
				j('#selectedRelated').append(newhtml);
			}
		}
		if (model=="Therapy" && location.pathname.match(/conditions/gi)) {
			editProCon(id);
		}
		if (model=="Symptom") {
			var selex = '<select onchange="javascript:void(processSymptoms(this,\''+id+'\'));" style="position:absolute;right:2px;top:7px" class="SymptomClassification" name="symptomtype"><option value="1">Primary</option><option value="2">Secondary</option></select>';
			j('#added-'+newid).append(selex);
			var o = {
				id:id,
				title:added.replace(/^.*\<span[^\>]+\>[^\<]+\<\/[^\>]+\>\s+([^\<]+).*$/gi,'$1'),
				ConditionsSymptom:{
					classification:1
				}
			};
			var isOnPrimary = ixOf(window.tmp.primary,o.id);
			var isOnSecondary = ixOf(window.tmp.secondary,o.id);
			if (isOnPrimary== -1 && isOnSecondary == -1) { // if it hasn't been
															// added before
				window.tmp.primary.push(o);
			}
			getSymptoms();
		}
	}
}

function getTherapies() {
	if (conditionsTherapies.length >0) {
		var s = function(a,b) {
			var ta = 0;
			var tb=0;
			if (typeof(a.ConditionsTherapy.therapy_id)!='undefined'&&typeof(b.ConditionsTherapy.therapy_id)!='undefined') {
				ta = a.ConditionsTherapy.therapy_id;
				tb = b.ConditionsTherapy.therapy_id;
			}
			return (ta<tb) ? -1 : (ta==tb)?0:1;
		};
		conditionsTherapies.sort(s);
		j('select#TherapyPro').empty();
		j('select#TherapyCon').empty();
		for (x in conditionsTherapies) {
			var o = (typeof(conditionsTherapies[x])== 'object') ? conditionsTherapies[x].ConditionsTherapy: false;
			if (o) {
				var pt = '<option id="Therapy_'+o.therapy_id+'_Pro" selected="true" value="'+o.pro.replace(/"/g,'\"')+'">'+o.therapy_id+'</option>';
				var ct = '<option id="Therapy_'+o.therapy_id+'_Con" selected="true" value="'+o.con.replace(/"/g,'\"')+'">'+o.therapy_id+'</option>';
				j('select#TherapyPro').append(pt);
				j('select#TherapyCon').append(ct);
			}
		}
	}
}

function editProCon(id) {
	j('body').unbind('keypress');
	var protext='';
	var context='';
	var hasPro = false;
	var hasCon = false;
	var o = {
		ConditionsTherapy:{
			therapy_id:id,
			pro:'',
			con:''
		}
	};
	if (j('option#Therapy_'+id+'_Pro').is('option')) {
		protext = j('option#Therapy_'+id+'_Pro').attr('value');
		hasPro = true;
	}
	if (j('option#Therapy_'+id+'_Con').is('option')) {
		context = j('option#Therapy_'+id+'_Con').attr('value');
		hasCon = true;
	}
	var pro = '<label>Pro</label><textarea name="ProText" style="height:160px" id="ProText">'+protext+'</textarea><br />';
	var con = '<label>Con</label><textarea name="ConText" style="height:160px" id="ConText">'+context+'</textarea>';
	alert(pro+con,{
		width:500,
		height:510,
		cbk: function() {
			try{
				if (j('option#Therapy_'+id+'_Pro').is('option')) {
					protext = j('#ProText').attr('value').replace(/"/g,'\"');
					j('option#Therapy_'+id+'_Pro').attr('value',protext);
				}
				else {
					protext = j('#ProText').attr('value').replace(/"/g,'\"');
					var newopt = '<option selected="true" id="Therapy_'+id+'_Pro" value="'+protext+'">'+id+'</option>';
					j('select#TherapyPro').append(newopt);
				}
				if (j('option#Therapy_'+id+'_Con').is('option')) {
					context = j('#ConText').attr('value').replace(/"/g,'\"');
					j('option#Therapy_'+id+'_Con').attr('value',context);
				}
				else {
					context = j('#ConText').attr('value').replace(/"/g,'\"');
					var newopt1 = '<option selected="true" id="Therapy_'+id+'_Con" value="'+context+'">'+id+'</option>';
					j('select#TherapyCon').append(newopt1);
				}
				if (!hasPro && !hasCon) {
					o.ConditionsTherapy.pro = protext;
					o.ConditionsTherapy.con = context;
					conditionsTherapies.push(o);
				}
				else {
					for (x in conditionsTherapies) {
						if (typeof(conditionsTherapies[x])=='object') {
							var o = conditionsTherapies[x].ConditionsTherapy;
							if (o.therapy_id==id) {
								o.pro = protext;
								o.con = context;
							}
						}
					}
				}
			}catch(e) {}
			getTherapies();
			j(this).dialog('close');
		}
	});
}

function removeRelated(id,model) {
	if (confirm("Are you sure you want to dismiss this relation?")) {
		j('#'+id).remove();
		j('#'+model+model+' option').each(function(i,e) {
			var val = j(e).attr('value');
			if (val == parseInt(id.replace(/[^\d]+/g,''))) {
				j(e).attr('selected',false);
			}
		});
		if (model=="Symptom") {
			var isOnPrimary = ixOf(window.tmp.primary,id.replace(/[^\d]+/,''));
			var isOnSecondary = ixOf(window.tmp.secondary,id.replace(/[^\d]+/,''));
			if (isOnPrimary!= -1) {
				window.tmp.primary.splice(isOnPrimary,1);
			}
			if (isOnSecondary!= -1) {
				window.tmp.secondary.splice(isOnSecondary,1);
			}
		}
	}
}

function getRelated() {
	var newhtml = '';
	var re_tube = /.*youtube\.com\/(watch\?v\=|v\/)([a-z0-9_\-]+).*/gi;
	var re_custom = /^(\/videos\/)([^\.]+)\.flv$/gi;
	var re_vimeo = /(.*vimeo\.com\/)(\d+).*/gi;
	j('select.hidden').each(function(i,e) {
		var val = j(e).attr('name');
		var model = (val.match(/^.*\[([^\]]+)\]\[\]$/gi))? val.replace(/^.*\[([^\]]+)\]\[\]$/gi,'$1'):
		(val.match(/^.*\[[^\]]+\]\[([^\]]+)\]$/gi))? val.replace(/^.*\[[^\]]+\]\[([^\]]+)\]$/gi,'$1'):'Unknown';
		j(e).find('option').each(function(x,o) {
			if (j(o).attr('selected')) {
				var id = j(o).attr('value');
				var therapy = (model=='Therapy' && location.pathname.match(/conditions/gi))? '&nbsp;<a href="javascript:void(editProCon('+id+'));"><img title="Edit Pro/Con" alt="Edit Pro/Con" src="/img/icons/Computer_File_052.gif" border="0" /></a>': '';
				var added = '<span class="model">'+model+'</span>&nbsp;'+j(o).text()+therapy;
				var newid = 'selected-'+model+'-'+id;
				var thumb = '';
				if (j(o).parent().is('optgroup')) {
					var t = j(o).parent().attr('label');
					var isTube = (t.match(re_tube))? true : false;
					var isVimeo = (t.match(re_vimeo))? true : false;
					var videoid = j(o).parent().attr('label').replace((!isTube)?((t.match(re_vimeo))?re_vimeo:re_custom):re_tube,'$2');
					videoid = (typeof(videoid)!='undefined') ? videoid : '';
					thumb = '<img src="'+((!isTube)?((!isVimeo)?'/videos/'+videoid.replace(/^https?:.*$/gi,'undef')+'.jpg':
						'/admin/videos/getVimeoThumb/'+videoid
						):'http://img.youtube.com/vi/'+videoid+'/default.jpg')+'" class="thumbnail" />';
				}
				if (!location.href.match(/admin\/videoseries/gi)) {
					newhtml += '<div class="result '+((model=="Video")?'videos':'')+'" id="added-'+newid+'">'+
					'<span class="delete" onclick="javascript:void(removeRelated(\'added-'+newid+'\',\''+model+'\'));">[X]</span>&nbsp;'+added+thumb+'</div>';
				}
				else {
					thumb = '<img src="'+((!isTube)?
						((isVimeo)?'/admin/videos/getVimeoThumb/'+videoid:'/videos/'+videoid.replace(/^https?:.*$/gi,'undef')+'.jpg')
						:'http://img.youtube.com/vi/'+videoid+'/default.jpg')+'" class="thumbnail" />';
					var html = '<a href=\"javascript:void(0)\">'+added+'</a>'+thumb;
					addVideoSerieItem(html,id,false);
				}
			}
		});
	});
	if (!location.href.match(/admin\/videoseries/gi)) {
		j('#selectedRelated').append(newhtml);
	}
}

function ixOf(arr,id) {
	var ret = -1;
	for (x in arr) {
		if (typeof(arr[x])!='undefined' && typeof(arr[x].id)!='undefined') {
			if (parseInt(arr[x].id)==parseInt(id)) {
				ret = x;
				return ret;
			}
		}
	}
	return ret;
}

function processSymptoms(e,id) {
	var t = j(e).attr('value');
	var isOnPrimary = ixOf(window.tmp.primary,id.replace(/[^\d]+/,''));
	var isOnSecondary = ixOf(window.tmp.secondary,id.replace(/[^\d]+/,''));
	var o = {};
	if (t==1) {
		if (isOnSecondary != -1) {
			o = window.tmp.secondary.splice(isOnSecondary,1);
		}
		if (isOnPrimary == -1) {
			o[0].ConditionsSymptom.classification = '1';
			window.tmp.primary.push(o[0]);
		}
	}
	if (t==2) {
		if (isOnPrimary != -1) {
			o = window.tmp.primary.splice(isOnPrimary,1);
		}
		if (isOnSecondary == -1) {
			o[0].ConditionsSymptom.classification = '2';
			window.tmp.secondary.push(o[0]);
		}
	}
	setTimeout(getSymptoms,500);
}

function getSymptoms() {
	var newhtml = '';
	j('select#PrimarySymptomPrimarySymptom').empty();
	j('select#SecondarySymptomSecondarySymptom').empty();
	j('#selectedSymptoms').empty();
	if (typeof(window.tmp.primary)!='undefined') {
		for (x in window.tmp.primary) {
			var e = window.tmp.primary[x];
			if (typeof(e)=='object') {
				var newid = 'selected-Symptom-'+e.id;
				var type = parseInt(e.ConditionsSymptom.classification);
				var selex = '<select onchange="javascript:void(processSymptoms(this,\''+e.id+'\'));" style="position:absolute;right:2px;top:7px" class="SymptomClassification" name="symptomtype"><option value="1" '+((type==1)?'selected="true"':'')+'>Primary</option><option value="2" '+((type==2)?'selected="true"':'')+'>Secondary</option></select>';
				newhtml += '<div style="position:relative;height:35px;line-height:35px" class="result Symptom" id="added-'+newid+'">'+
				'<span class="delete" onclick="javascript:void(removeRelated(\'added-'+newid+'\',\'Symptom\'));">[X]</span>&nbsp;<span class="model">Symptom</span>&nbsp;'+e.title;
				newhtml += selex +'</div>';
				var opt = '<option selected="true" id="opt-'+e.id+'" value="'+e.id+'">'+e.title+'</option>';
				j('select#PrimarySymptomPrimarySymptom').append(opt);
			}
		}
	}
	if (typeof(window.tmp.secondary)!='undefined') {
		for (x in window.tmp.secondary) {
			e = window.tmp.secondary[x];
			if (typeof(e)=='object') {
				newid = 'selected-Symptom-'+e.id;
				type = parseInt(e.ConditionsSymptom.classification);
				selex = '<select onchange="javascript:void(processSymptoms(this,\''+e.id+'\'));" style="position:absolute;right:2px;top:7px" class="SymptomClassification" name="symptomtype"><option value="1" '+((type==1)?'selected="true"':'')+'>Primary</option><option value="2" '+((type==2)?'selected="true"':'')+'>Secondary</option></select>';
				newhtml += '<div style="position:relative;height:35px;line-height:35px" class="result Symptom" id="added-'+newid+'">'+
				'<span class="delete" onclick="javascript:void(removeRelated(\'added-'+newid+'\',\'Symptom\'));">[X]</span>&nbsp;<span class="model">Symptom</span>&nbsp;'+e.title;
				newhtml += selex +'</div>';
				opt = '<option selected="true" id="opt-'+e.id+'" value="'+e.id+'">'+e.title+'</option>';
				j('select#SecondarySymptomSecondarySymptom').append(opt);
			}
		}
	}
	j('#selectedRelated #selectedSymptoms').append(newhtml);
}

function PollOptionsFactory(savedoptions, polladdurl, polldeleteurl) {
	var obj = {
		options: '',
		postUrl: '/admin/polloptions/add',
		deleteUrl: '/admin/polloptions/delete',
		saveOrderUrl: '/admin/polloptions/saveorder',
		pollSaveUrl: '/admin/polls/edit',
		polloptionEditUrl: '/admin/polloptions/edit',
		pollListUrl: '/admin/polls',
		noOptionsText: '',
		saveOptions: '',
		addOption: function() {
			/* Create a new poll option */
			var item = {
				answer: j('#PolloptionAnswer').val(),
				message: j('#PolloptionMessage').val(),
				poll_id: j('#PolloptionPollId').val()
			};
			j('#PolloptionAddForm').ajaxSubmit({
				url: obj.postUrl,
				dataType: 'json',
				success: function(response) {
					item.id = response.id;
					obj.addToList(item);
					obj.addToSelect(item);
					j(obj.options).push(item);
					j('#PolloptionAddForm').clearForm();
				}
			});
		},
		deleteOption: function(itemId) {
			var answer = confirm("Are you shure you want to delete this item?");
			if (answer) {
				j.ajax({
					url: obj.deleteUrl,
					type: 'POST',
					dataType: 'json',
					data: {
						'data[Polloption][poll_id]': itemId
					},
					success: function (data) {
						if (data.code == 'ok') {
							obj.removeFromList(itemId);
						}
					}
				});
			}
		},
		removeFromList: function(itemId) {
			j('#id_'+itemId).fadeOut(1000, function() {
				j('#id_'+itemId).remove();
				if (j("#selectedOptions li.pollOptionItem").length == 0) {
					j("#selectedOptions").append(obj.noOptionsText.clone());
				}
			});
		},
		addToList: function(item) {
			/* Insert into the items ordered list */
			if (j("#selectedOptions li.temptext").length) {
				obj.noOptionsText = j("#selectedOptions li.temptext").clone();
				j("#selectedOptions li.temptext").remove();
			}
			var insertItem = j("<li>").attr({
				'class' : 'pollOptionItem',
				'id' : 'id_'+item.id,
				style: 'display: none;'
			}).html(
				'<img src="/img/icons/draggable.gif" class="dragger" />'+
				'<span class="pollOptionItemAnswer"><b>Answer:</b> '+item.answer+'</span>'+
				'<span class="pollOptionItemMessage"><b>Message:</b> '+item.message+'</span>&nbsp;&nbsp;&nbsp;'+
				'<a href="javascript: pollOptions.deleteOption('+item.id+')" class="pollOptionDelete"><img title="Delete" alt="Delete" src="/img/icons/Computer_File_106.gif" /></a>'+
				'<a href="javascript: pollOptions.editOption('+item.id+')" class="pollOptionEdit"><img title="Edit" alt="Edit" src="/img/icons/edit.gif" /></a>');
			j("ul#selectedOptions").append(insertItem);
			insertItem.fadeIn(1000);
		},
		addToSelect: function(item) {
			var insertItem = j("<option>").attr({
				value: item.id,
				selected: 'selected'
			}).html(item.id);
			j("select#PollPolloption").append(insertItem);
			j("select#PollPolloption").append(insertItem);
		},
		saveOrder: function() {
			if (j("#selectedOptions li.pollOptionItem").length) {
				j.ajax ({
					url: obj.saveOrderUrl,
					data: {
						'data[order]': j("#selectedOptions").sortable('serialize')
					},
					type: 'post'
				});
			}
			return true;
		},
		editOption: function(itemId) {
			j('#pollOptionEditDialog').dialog({
				resizable: false,
				modal:true,
				autoOpen:false,
				title:'Edit poll answer',
				buttons:{
					'Save': function() {
						var answr = j('#PolloptionAjaxEditForm #PolloptionAjaxAnswer').val();
						var mesg = j('#PolloptionAjaxEditForm #PolloptionAjaxMessage').val();
						var poptId = j('#PolloptionAjaxEditForm #PolloptionAjaxId').val();
						var pollId = j('#PolloptionAjaxEditForm #PolloptionAjaxPollId').val();
						j('#PolloptionAjaxEditForm').ajaxSubmit({
							url: obj.polloptionEditUrl,
							dataType: 'json',
							success: function(response) {
								j('#id_'+poptId+' span.pollOptionItemAnswer').html('<b>Answer:</b> '+answr);
								j('#id_'+poptId+' span.pollOptionItemMessage').html('<b>Message:</b> '+mesg);
								j('#pollOptionEditDialog').dialog('close');
							}
						});
						j('#pollOptionEditDialog').html('<p style="text-align: center;"><img alt="Loading..." src="/img/ajax-loader-big-transp.gif" /></p>');
					},
					'Cancel': function() {
						j('#pollOptionEditDialog').dialog('close');
					}
				}
			});
			j('#pollOptionEditDialog').html('<p style="text-align: center;"><img alt="Loading..." src="/img/ajax-loader-big-transp.gif" /></p>').dialog('open');
			j('#pollOptionEditDialog').load(obj.polloptionEditUrl +'/'+itemId);
		},
		construct: function() {
			j("ul#selectedOptions").sortable({
				cursor: 'crosshair',
				handle: 'img.dragger'
			});
			if (polladdurl) this.postUrl = polladdurl; // To make sure we use
														// the correct URL
			if (polldeleteurl) this.deleteUrl = polldeleteurl;
			if (savedoptions) {
				this.options = savedoptions;
				j(this.options).each(function(index) {
					obj.addToList(this);
				});
			}
			j('#PolloptionAddForm').validate();
			j('#PollAddForm').validate();
			j('#PollEditForm').validate();
			j('#add').click(function() {
				obj.addOption();
			});
		}
	};
	obj.construct();
	return obj;
}

function disableButton(button, ajaxitem) {
	if (button) {
		j("#"+ajaxitem).toggle();
		j("#"+button).attr("disabled", "true");
		j("#"+button).addClass("disabled");
	}
}

function enableButton(button, ajaxitem) {
	if (button) {
		j("#"+ajaxitem).toggle();
		j("#"+button).removeAttr("disabled");
		j("#"+button).removeClass("disabled");
	}
}

function pollSave() {
	if (j("#PollEditForm").length) {
		var pollForm = "#PollEditForm";
	}
	else {
		var pollForm = "#PollAddForm";
	}
	disableButton('pollSave', 'ajaxLoading');
	j(pollForm).ajaxSubmit( {
		url : pollOptions.pollSaveUrl,
		dataType : 'json',
		success : function(response) {
			if (response.code != 'ok') {
				alert('Error saving poll, please try again.');
				enableButton('pollSave', 'ajaxLoading');
			}
			else if (pollOptions.saveOrder()) {
				enableButton('pollSave', 'ajaxLoading');
				if (pollForm == '#PollAddForm') {
					window.location.href = pollOptions.pollListUrl;
				}
				else {
					showMessage('Poll saved successfully.');
				}
			}
			else {
				alert('Error saving, please try again');
				enableButton('pollSave', 'ajaxLoading');
			}
		}
	});
}

function previewVideoSerieItem(id,type) {
	var val = '';
	switch (type) {
		case 'vimeo':
			val = 'http://www.vimeo.com/'+id;
			break;
		case 'youtube':
			val = 'http://www.youtube.com/watch?v='+id;
			break;
		case 'local':
			val = '/videos/'+id+'.flv';
			break;
	}
	var i = '<input value="'+val+'" id="content-'+id+'" style="display:none"/>';
	if (!j('#content-'+id).is('input')) {
		j('body').append(i);
	}
	checkPreview('#content-'+id);
	getVideoPreview('#content-'+id);
}

function addVideoSerieItem(html,id,isnew) {
	/** regex just for thumbnails * */
	var re_tube = /^.*youtube\.com\/(vi)\/([a-z0-9_\-]+)\/.*$/gi;
	var re_custom = /^.*(\/img\/videos\/)([^\.]+)\.jpg.*$/gi;
	var re_vimeo = /^.*(getVimeoThumb\/)(\d+)[^\"]?\"?.*$/gi;
	var videoid = '';
	isnew = (typeof(isnew)!='undefined') ? isnew : false;
	var t = 'Video_'+id;// (isnew) ? 'New_'+id : 'Video_'+id;
	if (!j('#selectedRelated ul#existing').is('ul')) {
		j('#selectedRelated').append('<ul id="existing" style="list-style-type:none"></ul>');
	}
	var isVimeo = (html.match(re_vimeo))?true:false;
	var isTube = (html.match(re_tube))?true:false;
	if (isVimeo) {
		videoid = html.replace(re_vimeo,'$2');
	}
	if (isTube) {
		videoid = html.replace(re_tube,'$2');
	}
	if (html.match(re_custom)) {
		videoid = html.replace(re_custom,'$2');
	}
	html = '<a href="javascript:void(removeFromSerie(\''+id+'\'))">'+
	'<img alt="Remove item" title="Remove item" border="0" src="/img/icons/Computer_File_106.gif" /></a>&nbsp;'+
	'<a href="javascript:void(previewVideoSerieItem(\''+videoid+'\',\''+
	((isVimeo)?'vimeo':((isTube)?'youtube':'local'))
	+'\'));"><img src="/img/icons/Computer_File_115.gif" border="0" /></a>&nbsp;'
	+html;
	var newli = '<li class="ui-state-default" id="'+t+'" style="height:66px;line-height:66px;padding-left:2px">'+html+'</li>';
	if (!j('li#Video_'+id).is('li')) {
		j('ul#existing').append(newli);
	}
	else {
		alert('Video was already loaded');
		return;
	}
	j('#VideoseriesSourceTitle').attr('value','');
	j('#VideoseriesExternalVideo').attr('value','');
	j('#VideoseriesCollection').attr('value',j('ul#existing').sortable('serialize'));
	if (isnew) {
		var title = j('li#'+t).text().replace(/^Video\s+(.*)$/gi,'$1');
		j('#VideoseriesNewVideos').append('<option selected="true" value="'+id+'">'+id+'</option>');
		j('#VideoseriesNewTitles').append('<option selected="true" value="'+title+'">'+title+'</option>');
	}
	else {
		j('#VideoseriesExistingVideos').append('<option selected="true" value="'+id+'">'+id+'</option>');
	}
}

function ajaxStart() {
	j('#ajaxProcess').html('<img src="/img/ajax-loader-big-transp.gif" alt="" border="0" /> Please wait...');
}
function ajaxStop() {
	j('#ajaxProcess').empty();
}

function removeFromSerie(id) {
	if (confirm('¿Do you want to remove this item from the collection?')) {
		ajaxStart();
		j.ajax({
			url: '/admin/videoseries/remove/'+id,
			cache: false,
			success: function() {
				j('#Video_'+id).fadeOut().remove();
				ajaxStop();
			}
		});
	}
}

function initVideoSeries() {
	var re_vimeo = /(.*vimeo\.com\/)(\d+).*/gi;
	j('#btnAdd').click(function() {
		if (j('#VideoseriesSourceTitle').attr('value')=='') {
			alert('Put a title for the video first.');
			j('#VideoseriesSourceTitle').focus();
			return false;
		}
		else {
			var val = j('#VideoseriesExternalVideo').attr('value');
			if (val.match(/([A-Za-z][A-Za-z0-9+.-]{1,120}:[A-Za-z0-9/](([A-Za-z0-9$_.+!*,;/?:@&~=-])|%[A-Fa-f0-9]{2}) {1,333}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*,;/?:@&~=%-]{0,1000}))?)/gi)) {
				var id = (val.match(re_vimeo))?getVimeoId(val):getYouTubeId(val);
				var html = '<a href=\"javascript:void(0)\"><span class=\"model\">Video</span> '+j('#VideoseriesSourceTitle').attr('value')+'</a>'
				+'<img src="'+((val.match(re_vimeo))?'/admin/videos/getVimeoThumb/'+id:
					'http://img.youtube.com/vi/'+id+'/default.jpg')+'" class="thumbnail" />';
				addVideoSerieItem(html,id,true);
			}
			else {
				alert('<b>'+val+'</b><br />This is not a valid URL.');
			}
		}
	});
	j('ul#existing').sortable({
		zIndex:10,
		revert:true,
		update:function(ev,ui) {
			j('#VideoseriesCollection').attr('value',j('ul#existing').sortable('serialize'));
		}
	});
	j('#VideoseriesCollection').attr('value',j('ul#existing').sortable('serialize'));
	j('#SourceCustom,#SourceExternal').click(function() {
		if (j(this).attr('checked')) {
			var id = (j(this).attr('id') == 'SourceExternal')?'externalVideo':'customVideo';
			j('#'+id).fadeIn();
			if (id=='externalVideo') j('#customVideo').hide(); else j('#externalVideo').hide();
		}
	});
}

function makeGlobalSearch() {
	if (j('#SearchBox').is('input')) {
		var val = j('#SearchBox').attr('value');
		if (!val.match(/^(search\.{3}|search in\s+.*)$/gi)) {
			if (location.pathname.match(/^\/admin\/[^\/]+.*$/gi)) {
				var c = location.pathname.replace(/^\/admin\/([^\/]+).*$/gi,'$1');
				location.href = '/admin/'+c+'/index/'+escape(val);
			}
		}
		else {
			alert('Put a valid search string');
		}
	}
}

function replaceWYSWIG(area, options) {
	var opts = [
		['html'], ['bold', 'italic', 'underline'],
		['link', 'unlink'],
	];
	if (options)
		opts.push(eval(options));
	j(function() {
		j('#'+area).htmlarea({
			css: '/css/wyswig.css',
			toolbar: opts
		});
		initUploader();
	});
}

function changeUserPassword() {
	function toggleDialogContents(loading) {
		if (loading) {
			$loading.show();
			$content.hide();
		}
		else {
			$loading.hide();
			$content.show();
		}
	}

	var $dialog = j("#UserChangePasswordDialog"),
		$loading = j("#UsrChngPsswrdLoading"),
		$content = j("#UsrChngPsswrdContent"),
		$action = arguments.callee.action,
		$form;
	$content.load($action, null, function() {
		toggleDialogContents(false);
		$dialog.dialog("option", "position", "center");
		$form = j("#UserAjaxChangePasswordForm");
		$form.validate({
			rules: {
				"data[User][password]":	{required: true},
				"data[User][new_pass]":	{required: true},
				"data[User][confirm]":	{required: true, equalTo: "#UserAjaxNewPass"}
			},
			submitHandler: function(form) {
				toggleDialogContents(true);
				$form.ajaxSubmit({
					url: $action,
					dataType: "json",
					success: function(response) {
						if (response.success) {
							$dialog.dialog("close");
							window.alert(response.message);
						}
						else {
							$dialog.dialog("disable");
							window.alert(response.message, {cbk: function() {
								j(this).dialog("close");
								toggleDialogContents(false);
								$dialog.dialog("enable");
							}});
						}
					}
				});
			}
		});
		j("#UserAjaxPassword").focus();
	});
	$dialog
		.dialog({
			autoOpen: false,
// bgiframe: true,
			draggable: false,
			modal: true,
			resizable: false,
			title: "Change password",
			buttons: {
				"Save": function() {
					$form.submit();
				},
				"Cancel": function() {
					$dialog.dialog("close");
				}
			}
		})
		.dialog("open");
	toggleDialogContents(true);
}

/**
 * Function : dump() Arguments: The data - array,hash(associative array),object
 * The level - OPTIONAL Returns : The textual representation of the array. This
 * function was inspired by the print_r function of PHP. This will accept some
 * data as the argument and return a text that will be a more readable version
 * of the array/hash/object that is given.
 */
function dump(arr,level) {
	var dumped_text = "";
	if (!level) level = 0;

	// The padding given at the beginning of the line.
	var level_padding = "";
	for (var j=0;j<level+1;j++) level_padding += "    ";

	if (typeof(arr) == 'object') { // Array/Hashes/Objects
		for (var item in arr) {
			var value = arr[item];

			if (typeof(value) == 'object') { // If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			}
			else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	}
	else { // Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}

function getSelection(id) {
	  if (document.getSelection) {
	    var str = document.getSelection();
	    if (window.RegExp) {
	      var regstr = unescape("%20%20%20%20%20");
	      var regexp = new RegExp(regstr, "g");
	      str = str.replace(regexp, "");
	    }
	  } else if (document.selection && document.selection.createRange) {
	    var range = document.selection.createRange();
	    var str = range.text;
	  } else {
	    alert("Your browser does not let JavaScript access your selected text.<br>Put ** around your text, example: **my subscript text**");
	  }
	  alert(str);
	}

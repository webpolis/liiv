﻿/*
Copyright (c) 2003-2009, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
    config.resize_enabled = false;
    config.toolbar_Basic= [['Source'],['Bold','Italic','Underline'],['Link','Unlink'],['SelectAll','PasteText','PasteFromWord','RemoveFormat']];
    config.toolbar_Medium= [['Source'],['Bold','Italic','Underline'],['Link','Unlink'],
                                     ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
                                     ['SelectAll','PasteText','PasteFromWord','RemoveFormat'],
                                     '/',
                                     ['Format','FontSize','Subscript','Superscript'],
                                     ['NumberedList','BulletedList'],
                                     ['TextColor','BGColor'],
                                     ['Image'],
                                     ['Maximize']
                                 ];
    config.language = 'en';
    config.pasteFromWordIgnoreFontFace=true;
    config.pasteFromWordRemoveStyle=true;
//Full configuration, for buttons reference

//config.toolbar_Full =
//[
//    ['Source','-','Save','NewPage','Preview','-','Templates'],
//    ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
//    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
//    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
//    '/',
//    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
//    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
//    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
//    ['Link','Unlink','Anchor'],
//    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
//    '/',
//    ['Styles','Format','Font','FontSize'],
//    ['TextColor','BGColor'],
//    ['Maximize', 'ShowBlocks','-','About']
//];
};

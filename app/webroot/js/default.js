var j = jQuery.noConflict();
window.LiivToday = {
	current : {
		item : {
			id : 0,
			content : '',
			when : '0000-00-00',
			type_id : 1
		},
		req : false
	}
};

function initLiivTodayAccordion() {
	j('#liivTodayAccordion').accordion( {
		clearStyle : true,
		collapsible : true,
		active : 1
	}).find('a').each(function(i, a) {
		j(a).click(function() {
			var liivTodayType = j(a).attr('class');
			if (typeof (liivTodayType) != 'undefined') {
				getLiivTodayItem(liivTodayType);
			}
		});
	});
	j('#accordion-1').click();
}

function getLiivTodayItem(type, o) {
	o = (typeof (o) != 'undefined') ? o : false;
	type = (typeof (type) != 'undefined' && type != 0) ? parseInt(type) : 0;
	if (typeof (elements) != 'undefined' && elements.length > 0 && !o) {
		for (x in elements) {
			if (typeof (elements[x]) == 'object'
					&& typeof (elements[x].LiivToday) != 'undefined') {
				if ((parseInt(elements[x].LiivToday.type_id) == type && type != 0)) {
					window.LiivToday.current.item = elements[x].LiivToday;
					window.LiivToday.current.item.prev_num = parseInt(elements[x][0].prev_num);
					window.LiivToday.current.item.next_num = parseInt(elements[x][0].next_num);
					break;
				}
			}
		}
	}
	var d = new Date(window.LiivToday.current.item.when.replace(/\-/g, '/'));
	var prev = ((window.LiivToday.current.item.prev_num > 0) ? '<img onclick="javascript:void(getLiivTodayNeighbors(\'prev\'));" src="/img/icons/arrow-left.png" border="0" />'
			: '');
	var next = ((window.LiivToday.current.item.next_num > 0) ? '<img class="LiivTodayHasNext" onclick="javascript:void(getLiivTodayNeighbors(\'next\'));" src="/img/icons/arrow-right.png" border="0" />'
			: '');
	var pager = '<div class="date">' + prev + d.format('mmmm d, yyyy') + next
			+ '<br /></div>';
	/* Add link if exists */
	var content = window.LiivToday.current.item.content;
	var image = null;
	if(typeof (window.LiivToday.current.item.image) != 'undefined' && window.LiivToday.current.item.image) 
		image = '<img src="' +  window.LiivToday.current.item.image + '" border="0" />';
	
	if(link = window.LiivToday.current.item.link){
		link = '<a href="'+link+'" target="_blank" title="'+window.LiivToday.current.item.topic+'">';
		content = link+content+'</a>';
		if(image != null) image = link+image+'</a>';
	}
	/* Fill with text */
	j('#liivTodayContent-' + window.LiivToday.current.item.type_id + ' .left')
			.html(pager + content);
	/* Fill image */
	j('#liivTodayContent-' + window.LiivToday.current.item.type_id + ' .right')
			.html(image);
}

function getLiivTodayNeighbors(direction) {
	if (window.LiivToday.current.item.id != 0) {
		if (typeof (window.LiivToday.current.req) != 'undefined'
				&& window.LiivToday.current.req) {
			if (typeof (window.LiivToday.current.req.abort()) != 'undefined') {
				window.LiivToday.current.req.abort();
			}
		}
		j(
				'#liivTodayContent-' + window.LiivToday.current.item.type_id + ' .right')
				.html(
						'<img align="center" border="0" src="/img/ajax-loader-orange-white.gif" />');
		j(
				'#liivTodayContent-' + window.LiivToday.current.item.type_id + ' .left')
				.html('Loading...');
		window.LiivToday.current.req = j
				.ajax( {
					type : 'GET',
					cache : false,
					dataType : 'json',
					url : '/liiv_todays/view/' + window.LiivToday.current.item.id + '/neighbors',
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						if (typeof (textStatus) != 'undefined') {
							j(
									'#liivTodayContent-' + window.LiivToday.current.item.type_id + ' .right')
									.html(
											'<img border="0" src="/img/icons/alert.png" />');
							j(
									'#liivTodayContent-' + window.LiivToday.current.item.type_id + ' .left')
									.text(textStatus);
						}
					},
					success : function(o) {
						if (typeof (o) == 'object') {
							if (o.prev && direction.match(/prev/gi)) {
								window.LiivToday.current.item = o.prev.LiivToday;
								window.LiivToday.current.item.prev_num = parseInt(o.prev[0].prev_num);
								window.LiivToday.current.item.next_num = parseInt(o.prev[0].next_num);
								getLiivTodayItem(0, true);
								return;
							}
							if (o.next && direction.match(/next/gi)) {
								window.LiivToday.current.item = o.next.LiivToday;
								window.LiivToday.current.item.prev_num = parseInt(o.next[0].prev_num);
								window.LiivToday.current.item.next_num = parseInt(o.next[0].next_num);
								getLiivTodayItem(0, true);
								return;
							}
							getLiivTodayItem(0, true);
						}
					}
				});
	}
}

function displayBanner() {
	var c = 0;
	if (typeof (elements) != 'undefined' && elements.length > 0) {
		for (x in elements) {
			var o = (typeof (elements[x].Banner) != 'undefined') ? elements[x]
					: false;
			if (o) {
				var id = parseInt(o.Banner.id);
				if (jQuery.inArray(id, banners.displayed) == -1 && c < 2) {
					c++;
					banners.displayed.push(id);
					/**
					 * search an available container
					 */
					var container = 'banner-top';
					if (jQuery.inArray(container, banners.containers) == -1) {
						banners.containers.push(container);
					} else {
						container = 'banner-bottom';
					}
					if (jQuery.inArray(container, banners.containers) == -1) {
						banners.containers.push(container);
					}
					if (o.Banner.content.match(/^.*\.swf$/gi)) {
						swfobject.embedSWF(o.Banner.content, container,
								o.Banner.width, o.Banner.height, '9');
					} else {
						j('#' + container).html(
								'<a href="' + o.Banner.link + '"><img src="'
										+ o.Banner.content + '" width="'
										+ o.Banner.width + '" height="'
										+ o.Banner.height
										+ '" border="0" /></a>');
					}
				}
			}
		}
	}
}

function generateTagCloud(model) {
	model = (typeof (model) != 'undefined') ? model : false;
	if (model != '') {
		if (typeof (window.tagCloud) != 'undefined'
				&& window.tagCloud.length > 0) {
			var s = function(a, b) {
				if (typeof (a[0]) != 'undefined'
						&& typeof (b[0]) != 'undefined') {
					return (a[0].counter > b[0].counter) ? -1
							: ((a[0].counter == b[0].counter) ? 0 : 1);
				}
			};
			window.tagCloud.sort(s);
			var max = parseInt(window.tagCloud[0][0].counter);
			var total = 0;
			for (x in window.tagCloud) {
				var o = (typeof (window.tagCloud[x][0]) != 'undefined') ? window.tagCloud[x][0]
						: false;
				if (o) {
					total += parseInt(o.counter);
				}
			}
			for (x in window.tagCloud) {
				var o = (typeof (window.tagCloud[x][0]) != 'undefined') ? window.tagCloud[x]
						: false;
				if (o) {
					var item = (typeof (o.Condition) != 'undefined') ? o.Condition
							: ((typeof (o.Therapy) != 'undefined') ? o.Therapy
									: ((typeof (o.Tradition) != 'undefined') ? o.Tradition
											: false));
					if (typeof (item) == 'object'
							&& typeof (item.title) == 'string') {
						var per = Math
								.floor(100 * (parseInt(o[0].counter) / total));
						var pmax = 35;
						var pmin = 8;
						var pt = ((pmax - pmin) / 100) * per + pmin;
						add = (per > 45) ? 'color:#1FA7E5;font-weight:bold;'
								: ((per >= 25) ? 'font-weight:bolder;' : '');
						var tag = '<span class="tag" style="font-size:'
								+ pt
								+ 'pt;"><a style="'
								+ add
								+ '" href="/'
								+ ((model == "condition") ? "conditions"
										: ((model == "therapy") ? "therapies"
												: 'conditions')) + '/'
								+ item.path + '">' + item.title + '</a></span>';
						var id = ((model == 'condition') ? 'Condition'
								: ((model == 'tradition') ? 'Tradition'
										: 'Therapy')) + 'Tags #tags';
						j('#' + id).append(tag);
					}
				}
			}
		}
	}
}

function generatePoll() {
	var out = '';
	var o = {
		id : 0
	};
	if (typeof (poll) != 'undefined') {
		o = (typeof (poll.Poll) != 'undefined') ? poll.Poll : false;
		if (o) {
			out += (typeof (o.question) != 'undefined') ? o.question : out;
			var opts = (typeof (poll.Polloption) != 'undefined' && poll.Polloption.length > 0) ? poll.Polloption
					: false;
			out += '<div id="answers">';
			if (opts) {
				for (x in opts) {
					out += '<input type="radio" name="answer" value="'
							+ opts[x].id + '">' + opts[x].answer
							+ '</input><br />';
				}
			}
			out += '<br /><div style="margin:0 auto;width:180px"><input type="button" id="voteBtn" value="VOTE" />';
			out += ' or <a href="javascript:void(showPollResults(' + o.id + '));">View Results</a></div>';
			out += '</div>';
		}
	}
	j('#currentPoll').html(out);
	j('#voteBtn').click(function() {
	});
}

function initializeSignUpForm(){
	j(":text").labelify();
	var validationRules = {
		rules: {
			'data[Subscriber][first_name]': {required: true, noinlinelabel: true},
			'data[Subscriber][last_name]': {required: true, noinlinelabel: true},
			'data[Subscriber][email]': {required: true, noinlinelabel: true, email: true}
		},
		errorPlacement: function(error, element) {
		//Don't place errors anywere
		}
	};
	
	j.validator.addMethod('noinlinelabel',function(value, element){
		if(j(element).hasClass('labeled')) return false;
		return true;
	});
	
	j('#SubscriberSubmit').click(function(){
		j('#SubscriberAddForm').ajaxSubmit({
			dataType: 'json',
			beforeSubmit: function(formData, jqForm, options){
				if(j('#SubscriberAddForm').validate(validationRules).form()){
					j('#SubscriberSubmit').hide();
					j('#SubscriberAddForm .ajaxLoader').show();
					return true;
				} else {
					msg = 'Please fill in all fields';
				    if(!j('#SubscriberAddForm').validate(validationRules).element('#SubscriberEmail')){
				    	msg = 'You must enter a valid email';
				    }
				    for (var i=0; i < formData.length; i++) { 
				        if (!formData[i].value) { 
				            msg = 'Please fill in all fields';
				        } 
				    }
				    createModalDialog('Error', msg);
				    return false;
				}
			},
			success: function(response){
				j('#SubscriberAddForm .ajaxLoader').hide();
				j('#SubscriberSubmit').show();
				if(response.code == 'ok'){
					j('#SubscriberAddForm').resetForm();
					createModalDialog('Success', response.message);
				} else {
					createModalDialog('Error', response.message);
				}
			}
		});
	});	
}

function createModalDialog(title, content){
	var $dialog = j('<div></div>')
	.html(content)
	.dialog({
		autoOpen: false,
		'title': title,
		buttons: {
			'Close': function(){
				j(this).dialog('close');
			}
		},
		modal: true
	});
	$dialog.dialog('open');
}
var j = jQuery.noConflict();

function previewVideo(image) {
	var bg = (typeof(image)!='undefined')? 'url(\'/videos/'+image+'\') no-repeat top left' : '#000';
	j('#preloadContainer').css( {
		background:bg/*,
		top : j('#player-video').offset().top + 'px',
		left : j('#player-video').offset().left + 'px'*/
	}).show();
}

function playVideo() {
	j('#preloadContainer').fadeOut();
	$f(0).play();
}

j(document)
		.ready(
				function() {
					function noLink() {
						return false
					}
					function toggleResolution($player, $btnHide, $btnShow,
							urlClip, urlCaptions) {
						$btnHide.hide();
						$player.pause().play(urlClip);
						if (urlCaptions)
							$player.getPlugin("captions").loadCaptions(0,
									urlCaptions);
						$btnShow.show();
						return false
					}
					function parseRel(rel) {
						var a0, a1, j, obj = {};
						a0 = rel.split(",");
						for (j = 0; j < a0.length; ++j) {
							a1 = a0[j].split(":");
							obj[a1[0]] = eval(a1[1])
						}
						return obj
					}
					var $video = j("#player-video"), urlLowRes = $video
							.attr("href"), reTube = /youtube\.com\/(?:watch\?v=|v\/)([\w\-]+)/i, reVimeo = /vimeo\.com\/(\d+)/i, params = {
						quality : "high",
						allowfullscreen : true,
						allowscriptaccess : "always",
						wmode : "transparent"
					}, vidId;
					if (vidId = urlLowRes.match(reTube)) {
						params.src = "http://www.youtube.com/v/" + vidId[1];
						$video.click(noLink).flashembed(params);
					} else if (vidId = urlLowRes.match(reVimeo)) {
						params.src = "http://vimeo.com/moogaloop.swf?clip_id="
								+ vidId[1]
								+ "&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=&fullscreen=1";
						$video.click(noLink).flashembed(params);
					} else {
						params.src = "/player/flowplayer.swf";
						params.version = [ 9, 115 ]; // For MP4 and H.264
						// support
						var extras = parseRel($video.attr("rel")), urlHiRes = (extras.hiRes ? extras.hiRes
								: ""), urlCaptions = (extras.captions ? extras.captions
								: ""), ccing = false, $embed = j("#player-embed"), $hiRes = j("#player-hiRes"), $lowRes = j("#player-lowRes"), $captions = j("#player-cc"), $dialog = j(
								"#player-embed-dialog").dialog( {
							autoOpen : false,
							draggable : false,
							resizable : false,
							modal : true,
							title : $embed.text(),
							width : "auto",
							buttons : {
								"Close" : function() {
									j(this).dialog("close")
								}
							}
						}), $player = $video
								.flowplayer(
										params,
										{
											showOnLoadBegin : true,
											clip : {
												url : urlLowRes,
												captionUrl : urlCaptions,
												autoBuffering : true,
												autoPlay : false,
												provider : "pseudo"
											},
											canvas : {
												backgroundColor : "#352009"
											},
											plugins : {
												controls : {
													bufferColor : "#4BBDEC",
													backgroundGradient : "none",
													progressGradient : "medium",
													timeBgColor : "#555555",
													buttonOverColor : "#4BBDEC",
													timeColor : "#01DAFF",
													sliderGradient : "none",
													bufferGradient : "none",
													durationColor : "#FFFFFF",
													tooltipTextColor : "#FFFFFF",
													borderRadius : "0px",
													tooltipColor : "#5F747C",
													sliderColor : "#86D0EF",
													buttonColor : "#514E40",
													backgroundColor : "#352009",
													progressColor : "#368EB2",
													volumeSliderColor : "#514E40",
													volumeSliderGradient : "none",
													height : 24,
													opacity : 1.0
												},
												rtmp : {
													url : "/player/flowplayer.rtmp.swf"
												},
												pseudo : {
													url : "/player/flowplayer.pseudostreaming.swf"
												},
												captions : {
													url : "/player/flowplayer.captions.swf",
													captionTarget : "content",
													button : null
												},
												content : {
													url : "/player/flowplayer.content.swf",
													bottom : 30,
													backgroundColor : "transparent",
													backgroundGradient : "none",
													border : 0,
													display : "none"
												}
											},
											onload : function() {
												this.setVolume(100)
											}
										}).flowplayer(0);
						j("#player-controls").show();
						j("textarea", $dialog).html(
								$player.embed().getEmbedCode());
						$embed.click(function() {
							$dialog.dialog("open");
							return false
						});
						if (urlHiRes) {
							$hiRes.click(function() {
								return toggleResolution($player, j(this),
										$lowRes, urlHiRes, urlCaptions)
							});
							$lowRes.click(function() {
								return toggleResolution($player, j(this),
										$hiRes, urlLowRes, urlCaptions)
							});
						} else
							$hiRes.hide();
						if (urlCaptions)
							$captions.click(function() {
								var cp = $player.getPlugin("content");
								if (ccing) {
									cp.hide();
									$captions.attr("title", "Show captions")
								} else {
									cp.show();
									$captions.attr("title", "Hide captions")
								}
								ccing = !ccing;
								return false
							});
						else
							$captions.hide();
					}
				});
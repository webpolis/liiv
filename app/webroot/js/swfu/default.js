var swfu;

function initUploaderPro(isAudio, field, folder, ran) {
	var ranf = (typeof(ran) != 'undefined' && ran != ''? ran: ''),
		useFolder = (typeof(folder) != 'undefined' && folder != ''),
		re_error = /error/gi,
		settings = {
			prevent_swf_caching: true,
			flash_url: '/js/swfu/swfupload.swf',
			upload_url: '/file_upload/file_upload.php',
			post_params: {
				newname: Math.ceil(Math.random() * Math.random() * 1000000000),
				path: (useFolder? escape(folder): '')
			},
			file_size_limit: '250 MB',
			file_types: (isAudio? '*.mp3;*.ogg;*.wav;': '*.flv;*.f4v;*.mp4;*.m4v;*.mov;*.avi;*.mpg;*.mpeg;'),
			file_types_description: (isAudio? 'Audio': 'Video')+' Files',
			file_upload_limit: 1,
			file_queue_limit: 10000,
			custom_settings: {
				progressTarget: 'upProgress'+ranf,
				cancelButtonId: 'upBtnCancel'+ranf
			},
			debug: false,
			// Button settings
			button_image_url: '/img/icons/btnUpload.png',
			button_width: 61,
			button_height: 22,
			button_placeholder_id: 'upBtnHolder'+ranf,
			button_cursor: 'hand',
			button_window_mode: SWFUpload.WINDOW_MODE.OPAQUE,
			// The event handler functions are defined in handlers.js
			file_queued_handler: fileQueued,
			file_queue_error_handler: fileQueueError,
			file_dialog_complete_handler: fileDialogComplete,
			upload_start_handler: uploadStart,
			upload_progress_handler: uploadProgress,
			upload_error_handler: uploadError,
			upload_success_handler: function(file, serverData) {
				try {
					var progress = new FileProgress(file, this.customSettings.progressTarget),
						params   = (ranf != ''? window[ranf]: swfu).settings.post_params,
						fieldVal = (useFolder? folder+'/': '')+serverData,
						fieldId  = (typeof(field)!='undefined'&&field!=''? field: (isAudio?'Audio':'Video')+'Content'),
						status;

					progress.setComplete();
					progress.toggleCancel(false);
					if (!re_error.test(serverData)) {
						j('#'+fieldId).attr('value', '/'+(isAudio?'audios':'videos')+'/'+fieldVal);
						status = 'Complete.';
					}
					else if (isAudio) {
						status = 'The audio conversion has failed. Please try with another audio format.';
					}
					else {
						status = 'The extraction of the thumbnail has failed.';
					}
					progress.setStatus(status);

					getVideoSerieItem(fieldVal, ranf);
				}
				catch (e) {
					this.debug(e);
				}
			},
			upload_complete_handler: uploadComplete,
			queue_complete_handler: queueComplete	// Queue plugin event
		};
	if (ranf != '') {
		window[ranf] = new SWFUpload(settings);
		window[ranf].hash = ranf;
	}
	else {
		swfu = new SWFUpload(settings);
	}
}

function initUploaderPdf(field, ran) {
	var ranf = (typeof(ran) != 'undefined' && ran != ''? ran: ''),
		useFolder = (typeof(folder) != 'undefined' && folder != ''),
		re_error = /error/gi,
		settings = {
			prevent_swf_caching: true,
			flash_url: '/js/swfu/swfupload.swf',
			upload_url: '/file_upload/file_upload.php',
			post_params: {
				newname: Math.ceil(Math.random() * Math.random() * 1000000000),
				path: (useFolder? escape(folder): '')
			},
			file_size_limit: '250 MB',
			file_types: '*.pdf',
			file_types_description: 'PDF Document',
			file_upload_limit: 1,
			file_queue_limit: 10000,
			custom_settings: {
				progressTarget: 'upProgress'+ranf,
				cancelButtonId: 'upBtnCancel'+ranf
			},
			debug: false,
			// Button settings
			button_image_url: '/img/icons/btnUpload.png',
			button_width: 61,
			button_height: 22,
			button_placeholder_id: 'upBtnHolder'+ranf,
			button_cursor: 'hand',
			button_window_mode: SWFUpload.WINDOW_MODE.OPAQUE,
			// The event handler functions are defined in handlers.js
			file_queued_handler: fileQueued,
			file_queue_error_handler: fileQueueError,
			file_dialog_complete_handler: fileDialogComplete,
			upload_start_handler: uploadStart,
			upload_progress_handler: uploadProgress,
			upload_error_handler: uploadError,
			upload_success_handler: function(file, serverData) {
				try {
					var progress = new FileProgress(file, this.customSettings.progressTarget),
						params   = (ranf != ''? window[ranf]: swfu).settings.post_params,
						fieldVal = (useFolder? folder+'/': '')+serverData,
						fieldId  = (typeof(field)!='undefined'&&field!=''? field: (isAudio?'Audio':'Video')+'Content'),
						status;

					progress.setComplete();
					progress.toggleCancel(false);
					if (!re_error.test(serverData)) {
						j('#'+fieldId).attr('value', '/videos/'+fieldVal);
						status = 'Complete.';
					}
					progress.setStatus(status);
				}
				catch (e) {
					this.debug(e);
				}
			},
			upload_complete_handler: uploadComplete,
			queue_complete_handler: queueComplete	// Queue plugin event
		};
	if (ranf != '') {
		window[ranf] = new SWFUpload(settings);
		window[ranf].hash = ranf;
	}
	else {
		swfu = new SWFUpload(settings);
	}
}


function getVideoSerieItem(id, ranf) {
	if (j('#existing').is('ul')) {
		id = id.replace(/[^\d]+/, '');
		var title = (ranf != ''? window[ranf]: swfu).customSettings.videoTitle,
			html = '<a href="javascript:void(0)"><span class="model">Video</span> '+title+'</a><img src="/videos/series/'+id+'.jpg" class="thumbnail" />';
		addVideoSerieItem(html, id, true);
	}
}

function getYouTubeVideo(id) {
	var out  = '<object width="'+default_video.w+'" height="'+default_video.h+'">';
		out += '<param name="movie" value="http://www.youtube.com/v/'+id+'" />';
		out += '<param name="allowFullScreen" value="true" />';
		out += '<param name="allowscriptaccess" value="always" />';
		out += '<param name="wmode" value="opaque" />';
		out += '<embed src="http://www.youtube.com/v/'+id+'" type="application/x-shockwave-flash" width="'+default_video.w+'" height="'+default_video.h+'" allowscriptaccess="always" allowfullscreen="true" wmode="opaque"></embed>';
		out += '</object>';
	return out;
}

function getVimeoVideo(id) {
	var out  = '<object width="'+default_video.w+'" height="'+default_video.h+'">';
		out += '<param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id='+id+'&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1" />';
		out += '<param name="allowfullscreen" value="true" />';
		out += '<param name="allowscriptaccess" value="always" />';
		out += '<param name="wmode" value="window" />';
		out += '<embed src="http://vimeo.com/moogaloop.swf?clip_id='+id+'&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1" type="application/x-shockwave-flash" width="'+default_video.w+'" height="'+default_video.h+'" allowfullscreen="true" allowscriptaccess="always" wmode="opaque"></embed>';
		out += '</object>';
	return out;
}

function checkPreview(fieldId) {
	var field = (typeof(fieldId) != 'undefined'? fieldId: '#VideoContent'),
		vid = j(field).attr('value').replace(/'/g, "\'");
	if (vid.match(/^\/videos.*\.flv$/gi)) {
		j('body').append('<div id="mediaspace" style="display:none"></div>');
		var so = new SWFObject('/player/player.swf', 'mpl', default_video.w, default_video.h, '9');
		so.addParam('allowfullscreen', 'true');
		so.addParam('allowscriptaccess', 'always');
		so.addParam('wmode', 'window');
		so.addVariable('file', vid);
		so.addVariable('backcolor', '8AD490');
		so.addVariable('frontcolor', 'FFFFFF');
		so.addVariable('lightcolor', 'CCFFFF');
		so.addVariable('screencolor', '666666');
		so.write('mediaspace');
	}
}

function getVideoPreview(fieldId) {
	var field = (typeof(fieldId) != 'undefined'? fieldId: '#VideoContent'),
		t = j(field).attr('value'),
		re_tube = /.*youtube\.com\/(?:watch\?v\=|v\/)([\w\-]+).*/i;
		re_vimeo = /.*vimeo\.com\/(\d+).*/i,
		out = '', id = '';

	if (!j('#videoPreview').is('div')) {
		j('body').append('<div id="videoPreview" style="display:none"></div>');
	}

	if (id = t.match(re_tube)) {
		out = getYouTubeVideo(id[1]);
	}
	else if (id = t.match(re_vimeo)) {
		out = getVimeoVideo(id[1]);
	}
	else if (j('#mediaspace').is('div')) {
		out = j('#mediaspace').html();
	}
	else {
		out = 'In case you load a video from an external source, it will be displayed here for debug.';
	}

	j('#videoPreview').html(out).dialog('open');
}

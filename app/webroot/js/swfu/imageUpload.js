var swfu, seed;

function getHash() {
	return Math.ceil(Math.random() * Math.random() * 1000000000);
}

function initUploader(newpath, upbtn, ran, allowOthers) {
	allowOthers = (typeof(allowOthers) != 'undefined'? allowOthers: false);
	ran = (typeof(ran) != 'undefined'? ran: '');
	window.seed = getHash();
	if (j('.ToolBar').is('div')) {
		j('.ToolBar ul: last-child').append('<li><div id="upBtnHolder" style="margin:0;padding:0"></div></li>');
	}
	var settings = {
		prevent_swf_caching: true,
		flash_url:  "/js/swfu/swfupload.swf",
		upload_url: "/file_upload/file_upload.php",
		post_params: {
			"newname": window.seed,
			"image": true,
			"path": (typeof(newpath) != 'undefined'? newpath: '')
		},
		custom_settings: {
			progressTarget: "upProgress"+ran,
			cancelButtonId: "upBtnCancel"+ran
		},
		file_size_limit: "3 MB",
		file_types: "*.jpg;*.bmp;*.gif;*.png;"+(allowOthers? '*.swf;': ''),
		file_types_description: (allowOthers? "SWF or Image banners": "Image Files"),
		file_upload_limit: 100,
		file_queue_limit: 1,
		// Button settings
		button_image_url: (upbtn? "/img/icons/btnUpload.png": "/img/icons/objects_013.png"),
		button_width: (upbtn? 61: 16),
		button_height: (upbtn? 22: 16),
		button_placeholder_id: "upBtnHolder"+ran,
		button_cursor: 'hand',
		debug: false,
		file_queued_handler: fileQueued,
		file_queue_error_handler: fileQueueError,
		file_dialog_complete_handler: fileDialogComplete,
		upload_start_handler: uploadStart,
		upload_progress_handler: uploadProgress,
		upload_error_handler: uploadError,
		upload_success_handler: function(file, data) {
			if (j('#LiivTodayContent').is('textarea') && (typeof(newpath) == 'undefined')) {
				if (data.match(/^[a-z0-9]+\.[a-z0-9]+$/gi)) {
					j('#LiivTodayContent').htmlarea('image', '/img/wysiwyg/'+data);
				}
				else {
					alert('An error ocurred. Please contact the administrator.\n'+data);
				}
			}
			if (j('#LiivTodayImage').is('input') && (typeof(newpath) != 'undefined')) {
				if (data.match(/^[a-z0-9]+\.[a-z0-9]+$/gi)) {
					j('#LiivTodayImage').attr('value', '/img/'+newpath+'/'+data);
				}
				else {
					alert('An error ocurred. Please contact the administrator.\n'+data);
				}
			}
			if (j('#PageContent').is('textarea')) {
				j('#PageContent').htmlarea('image', '/img/wysiwyg/'+data);
			}
			if (j('#AuthorityImage').is('input')) {
				j('#AuthorityImage').attr('value', '/img/'+newpath+'/'+data);
			}
			if (j('#StoryPhoto').is('input')) {
				j('#StoryPhoto').attr('value', '/img/'+newpath+'/'+data);
			}
			if (j('#SlideContent').is('input')) {
				j('#SlideContent').attr('value', '/img/'+newpath+'/'+data);
				j('.right').html('<img src="/img/'+newpath+'/'+data+'" border="0" />');
			}
			if (j('#BannerContent').is('input')) {
				j('#BannerContent').attr('value', '/'+newpath+'/'+data);
				if (!data.match(/^.*\.swf$/gi)) {
					j('#bannerPreview').html('<img src="/banners/'+data+'" border="0" />');
				}
				else {
					swfobject.embedSWF('/banners/'+data, 'bannerPreview', '980', '65', '9');
				}
			}
			window.seed = getHash();

			var uploader = (typeof(ran) != 'undefined' && ran != ''? window[ran]: swfu);
			uploader.removePostParam('newname');
			uploader.addPostParam('newname', window.seed);
		},
		upload_complete_handler: uploadComplete,
		queue_complete_handler: queueComplete
	};
	if ((typeof(ran) != 'undefined') && (ran != '')) {
		window[ran] = new SWFUpload(settings);
		window[ran].hash = ran;
	}
	else {
		swfu = new SWFUpload(settings);
	}
}
#!/bin/sh
for f in *.flv; do ffmpeg -i $f -vcodec mjpeg -vframes 1 -an -f rawvideo -ss 5 -y -s 618x384 $f.jpg; done
for f in *.mp4; do ffmpeg -i $f -vcodec mjpeg -vframes 1 -an -f rawvideo -ss 5 -y -s 618x384 $f.jpg; done
ls *.mp4.jpg | awk '{print("mv "$1" "$1)}' | sed -e 's/mp4/flv/2' | sh
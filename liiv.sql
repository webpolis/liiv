# MySQL Navigator Xport
# Database: liiv
# liiv@db.codigoaustral.com

# CREATE DATABASE liiv;
# USE liiv;

#
# Table structure for table 'acos'
#

# DROP TABLE IF EXISTS acos;
CREATE TABLE `acos` (
  `id` int(10) NOT NULL auto_increment,
  `parent_id` int(10) default NULL,
  `model` varchar(255) default NULL,
  `foreign_key` int(10) default NULL,
  `alias` varchar(255) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=217 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'acos'
#

INSERT INTO acos VALUES (1,0,'Access',NULL,'Access',1,10);
INSERT INTO acos VALUES (2,1,'Access',NULL,'index',2,3);
INSERT INTO acos VALUES (3,1,'Access',NULL,'add',4,5);
INSERT INTO acos VALUES (4,1,'Access',NULL,'delete',6,7);
INSERT INTO acos VALUES (5,0,'Users',NULL,'Users',11,40);
INSERT INTO acos VALUES (6,5,'Users',NULL,'index',12,13);
INSERT INTO acos VALUES (7,0,'Pages',NULL,'Pages',41,58);
INSERT INTO acos VALUES (8,7,'Pages',NULL,'display',42,43);
INSERT INTO acos VALUES (10,1,'Access',NULL,'setPermission',8,9);
INSERT INTO acos VALUES (11,0,'Conditions',NULL,'Conditions',59,78);
INSERT INTO acos VALUES (12,11,'Conditions',NULL,'index',60,61);
INSERT INTO acos VALUES (13,11,'Conditions',NULL,'admin_index',62,63);
INSERT INTO acos VALUES (14,0,'Symptoms',NULL,'Symptoms',79,92);
INSERT INTO acos VALUES (15,14,'Symptoms',NULL,'admin_index',80,81);
INSERT INTO acos VALUES (16,5,'Users',NULL,'admin_index',14,15);
INSERT INTO acos VALUES (17,5,'Users',NULL,'login',16,17);
INSERT INTO acos VALUES (18,5,'Users',NULL,'logout',18,19);
INSERT INTO acos VALUES (19,5,'Users',NULL,'admin_add',20,21);
INSERT INTO acos VALUES (20,0,'Therapies',NULL,'Therapies',93,112);
INSERT INTO acos VALUES (21,20,'Therapies',NULL,'admin_index',94,95);
INSERT INTO acos VALUES (22,11,'Conditions',NULL,'admin_add',64,65);
INSERT INTO acos VALUES (23,7,'Pages',NULL,'admin_index',44,45);
INSERT INTO acos VALUES (24,0,'Articles',NULL,'Articles',113,128);
INSERT INTO acos VALUES (25,0,'Videos',NULL,'Videos',129,150);
INSERT INTO acos VALUES (26,0,'Audios',NULL,'Audios',151,164);
INSERT INTO acos VALUES (27,24,'Articles',NULL,'admin_index',114,115);
INSERT INTO acos VALUES (28,0,'Stories',NULL,'Stories',165,184);
INSERT INTO acos VALUES (29,28,'Stories',NULL,'admin_index',166,167);
INSERT INTO acos VALUES (30,14,'Symptoms',NULL,'admin_add',82,83);
INSERT INTO acos VALUES (31,14,'Symptoms',NULL,'admin_delete',84,85);
INSERT INTO acos VALUES (32,14,'Symptoms',NULL,'admin_edit',86,87);
INSERT INTO acos VALUES (33,25,'Videos',NULL,'admin_index',130,131);
INSERT INTO acos VALUES (34,20,'Therapies',NULL,'admin_add',96,97);
INSERT INTO acos VALUES (36,20,'Therapies',NULL,'admin_edit',98,99);
INSERT INTO acos VALUES (37,20,'Therapies',NULL,'admin_delete',100,101);
INSERT INTO acos VALUES (38,20,'Therapies',NULL,'admin_view',102,103);
INSERT INTO acos VALUES (39,24,'Articles',NULL,'admin_add',116,117);
INSERT INTO acos VALUES (40,24,'Articles',NULL,'admin_edit',118,119);
INSERT INTO acos VALUES (41,24,'Articles',NULL,'admin_delete',120,121);
INSERT INTO acos VALUES (42,25,'Videos',NULL,'admin_add',132,133);
INSERT INTO acos VALUES (44,25,'Videos',NULL,'admin_delete',134,135);
INSERT INTO acos VALUES (45,25,'Videos',NULL,'admin_edit',136,137);
INSERT INTO acos VALUES (46,26,'Audios',NULL,'admin_index',152,153);
INSERT INTO acos VALUES (47,26,'Audios',NULL,'admin_delete',154,155);
INSERT INTO acos VALUES (48,26,'Audios',NULL,'admin_edit',156,157);
INSERT INTO acos VALUES (49,26,'Audios',NULL,'admin_add',158,159);
INSERT INTO acos VALUES (50,24,'Articles',NULL,'admin_view',122,123);
INSERT INTO acos VALUES (51,0,'publications',NULL,'publications',185,198);
INSERT INTO acos VALUES (52,51,'publications',NULL,'admin_index',186,187);
INSERT INTO acos VALUES (53,51,'publications',NULL,'admin_add',188,189);
INSERT INTO acos VALUES (54,51,'publications',NULL,'admin_edit',190,191);
INSERT INTO acos VALUES (55,51,'publications',NULL,'admin_delete',192,193);
INSERT INTO acos VALUES (56,0,'authorities',NULL,'authorities',199,212);
INSERT INTO acos VALUES (57,56,'authorities',NULL,'admin_add',200,201);
INSERT INTO acos VALUES (58,56,'authorities',NULL,'admin_delete',202,203);
INSERT INTO acos VALUES (59,56,'authorities',NULL,'admin_edit',204,205);
INSERT INTO acos VALUES (60,56,'authorities',NULL,'admin_index',206,207);
INSERT INTO acos VALUES (61,0,'events',NULL,'events',213,226);
INSERT INTO acos VALUES (62,61,'events',NULL,'admin_index',214,215);
INSERT INTO acos VALUES (63,61,'events',NULL,'admin_add',216,217);
INSERT INTO acos VALUES (64,61,'events',NULL,'admin_delete',218,219);
INSERT INTO acos VALUES (65,61,'events',NULL,'admin_edit',220,221);
INSERT INTO acos VALUES (66,61,'events',NULL,'admin_view',222,223);
INSERT INTO acos VALUES (67,56,'authorities',NULL,'admin_view',208,209);
INSERT INTO acos VALUES (68,51,'publications',NULL,'admin_view',194,195);
INSERT INTO acos VALUES (69,26,'Audios',NULL,'admin_view',160,161);
INSERT INTO acos VALUES (70,28,'Stories',NULL,'admin_add',168,169);
INSERT INTO acos VALUES (71,28,'Stories',NULL,'admin_delete',170,171);
INSERT INTO acos VALUES (72,28,'Stories',NULL,'admin_edit',172,173);
INSERT INTO acos VALUES (73,28,'Stories',NULL,'admin_view',174,175);
INSERT INTO acos VALUES (74,25,'Videos',NULL,'admin_view',138,139);
INSERT INTO acos VALUES (75,14,'Symptoms',NULL,'admin_view',88,89);
INSERT INTO acos VALUES (76,11,'Conditions',NULL,'admin_delete',66,67);
INSERT INTO acos VALUES (77,5,'Users',NULL,'admin_edit',22,23);
INSERT INTO acos VALUES (78,5,'Users',NULL,'admin_view',24,25);
INSERT INTO acos VALUES (79,5,'Users',NULL,'admin_delete',26,27);
INSERT INTO acos VALUES (80,11,'Conditions',NULL,'admin_edit',68,69);
INSERT INTO acos VALUES (81,11,'Conditions',NULL,'admin_view',70,71);
INSERT INTO acos VALUES (82,0,'organizations',NULL,'organizations',227,240);
INSERT INTO acos VALUES (83,82,'organizations',NULL,'admin_add',228,229);
INSERT INTO acos VALUES (84,82,'organizations',NULL,'admin_edit',230,231);
INSERT INTO acos VALUES (85,82,'organizations',NULL,'admin_delete',232,233);
INSERT INTO acos VALUES (86,82,'organizations',NULL,'admin_index',234,235);
INSERT INTO acos VALUES (87,82,'organizations',NULL,'admin_view',236,237);
INSERT INTO acos VALUES (97,89,'Groups',NULL,'admin_add',246,247);
INSERT INTO acos VALUES (89,0,'Groups',NULL,'Groups',241,250);
INSERT INTO acos VALUES (90,89,'Groups',NULL,'admin_index',242,243);
INSERT INTO acos VALUES (91,89,'Groups',NULL,'admin_view',244,245);
INSERT INTO acos VALUES (92,0,'Areas',NULL,'Areas',251,262);
INSERT INTO acos VALUES (93,92,'Areas',NULL,'admin_index',252,253);
INSERT INTO acos VALUES (94,92,'Areas',NULL,'admin_view',254,255);
INSERT INTO acos VALUES (95,92,'Areas',NULL,'admin_edit',256,257);
INSERT INTO acos VALUES (96,92,'Areas',NULL,'admin_add',258,259);
INSERT INTO acos VALUES (98,89,'Groups',NULL,'admin_edit',248,249);
INSERT INTO acos VALUES (99,0,'Tickets',NULL,'Tickets',263,272);
INSERT INTO acos VALUES (100,99,'Tickets',NULL,'admin_index',264,265);
INSERT INTO acos VALUES (101,99,'Tickets',NULL,'admin_add',266,267);
INSERT INTO acos VALUES (102,99,'Tickets',NULL,'admin_edit',268,269);
INSERT INTO acos VALUES (103,99,'Tickets',NULL,'admin_view',270,271);
INSERT INTO acos VALUES (104,5,'Users',NULL,'admin_login',28,29);
INSERT INTO acos VALUES (105,5,'Users',NULL,'admin_logout',30,31);
INSERT INTO acos VALUES (107,92,'Areas',NULL,'admin_delete',260,261);
INSERT INTO acos VALUES (109,0,'LiivTodays',NULL,'LiivTodays',273,288);
INSERT INTO acos VALUES (110,109,'LiivTodays',NULL,'index',274,275);
INSERT INTO acos VALUES (111,109,'LiivTodays',NULL,'admin_index',276,277);
INSERT INTO acos VALUES (205,172,'banners',NULL,'admin_view',378,379);
INSERT INTO acos VALUES (112,109,'LiivTodays',NULL,'admin_add',278,279);
INSERT INTO acos VALUES (113,109,'LiivTodays',NULL,'admin_edit',280,281);
INSERT INTO acos VALUES (114,109,'LiivTodays',NULL,'admin_delete',282,283);
INSERT INTO acos VALUES (115,109,'LiivTodays',NULL,'admin_view',284,285);
INSERT INTO acos VALUES (116,7,'Pages',NULL,'admin_add',46,47);
INSERT INTO acos VALUES (117,7,'Pages',NULL,'admin_edit',48,49);
INSERT INTO acos VALUES (118,7,'Pages',NULL,'admin_delete',50,51);
INSERT INTO acos VALUES (119,7,'Pages',NULL,'admin_view',52,53);
INSERT INTO acos VALUES (120,11,'Conditions',NULL,'admin_relate',72,73);
INSERT INTO acos VALUES (122,24,'Articles',NULL,'admin_relate',124,125);
INSERT INTO acos VALUES (123,26,'Audios',NULL,'admin_relate',162,163);
INSERT INTO acos VALUES (124,25,'Videos',NULL,'admin_relate',140,141);
INSERT INTO acos VALUES (125,56,'authorities',NULL,'admin_relate',210,211);
INSERT INTO acos VALUES (126,0,'Workshops',NULL,'Workshops',289,300);
INSERT INTO acos VALUES (127,126,'Workshops',NULL,'admin_index',290,291);
INSERT INTO acos VALUES (128,126,'Workshops',NULL,'admin_add',292,293);
INSERT INTO acos VALUES (129,126,'Workshops',NULL,'admin_edit',294,295);
INSERT INTO acos VALUES (130,126,'Workshops',NULL,'admin_view',296,297);
INSERT INTO acos VALUES (131,126,'Workshops',NULL,'admin_relate',298,299);
INSERT INTO acos VALUES (132,61,'events',NULL,'admin_relate',224,225);
INSERT INTO acos VALUES (133,0,'traditions',NULL,'traditions',301,320);
INSERT INTO acos VALUES (134,133,'traditions',NULL,'admin_index',302,303);
INSERT INTO acos VALUES (135,133,'traditions',NULL,'admin_add',304,305);
INSERT INTO acos VALUES (136,133,'traditions',NULL,'admin_edit',306,307);
INSERT INTO acos VALUES (137,133,'traditions',NULL,'admin_view',308,309);
INSERT INTO acos VALUES (138,133,'traditions',NULL,'admin_relate',310,311);
INSERT INTO acos VALUES (192,25,'Videos',NULL,'index',144,145);
INSERT INTO acos VALUES (140,20,'Therapies',NULL,'admin_relate',104,105);
INSERT INTO acos VALUES (141,5,'Users',NULL,'unauthorized',32,33);
INSERT INTO acos VALUES (142,5,'Users',NULL,'admin_unauthorized',34,35);
INSERT INTO acos VALUES (143,28,'Stories',NULL,'admin_relate',176,177);
INSERT INTO acos VALUES (144,51,'publications',NULL,'admin_relate',196,197);
INSERT INTO acos VALUES (145,0,'polls',NULL,'polls',321,336);
INSERT INTO acos VALUES (146,145,'polls',NULL,'index',322,323);
INSERT INTO acos VALUES (147,145,'polls',NULL,'admin_index',324,325);
INSERT INTO acos VALUES (148,145,'polls',NULL,'admin_add',326,327);
INSERT INTO acos VALUES (149,145,'polls',NULL,'admin_edit',328,329);
INSERT INTO acos VALUES (150,145,'polls',NULL,'admin_relate',330,331);
INSERT INTO acos VALUES (151,145,'polls',NULL,'admin_view',332,333);
INSERT INTO acos VALUES (152,0,'polloptions',NULL,'polloptions',337,350);
INSERT INTO acos VALUES (153,152,'polloptions',NULL,'admin_add',338,339);
INSERT INTO acos VALUES (154,152,'polloptions',NULL,'admin_edit',340,341);
INSERT INTO acos VALUES (155,152,'polloptions',NULL,'admin_index',342,343);
INSERT INTO acos VALUES (156,152,'polloptions',NULL,'admin_view',344,345);
INSERT INTO acos VALUES (157,152,'polloptions',NULL,'admin_delete',346,347);
INSERT INTO acos VALUES (158,82,'organizations',NULL,'admin_relate',238,239);
INSERT INTO acos VALUES (159,152,'polloptions',NULL,'admin_saveorder',348,349);
INSERT INTO acos VALUES (160,0,'videoseries',NULL,'videoseries',351,366);
INSERT INTO acos VALUES (161,160,'videoseries',NULL,'admin_index',352,353);
INSERT INTO acos VALUES (162,160,'videoseries',NULL,'admin_add',354,355);
INSERT INTO acos VALUES (163,160,'videoseries',NULL,'admin_edit',356,357);
INSERT INTO acos VALUES (164,160,'videoseries',NULL,'admin_delete',358,359);
INSERT INTO acos VALUES (165,160,'videoseries',NULL,'admin_relate',360,361);
INSERT INTO acos VALUES (166,145,'polls',NULL,'admin_delete',334,335);
INSERT INTO acos VALUES (167,160,'videoseries',NULL,'admin_remove',362,363);
INSERT INTO acos VALUES (168,7,'Pages',NULL,'admin_welcome',54,55);
INSERT INTO acos VALUES (169,160,'videoseries',NULL,'admin_view',364,365);
INSERT INTO acos VALUES (170,25,'Videos',NULL,'admin_getVimeoThumb',142,143);
INSERT INTO acos VALUES (171,109,'LiivTodays',NULL,'view',286,287);
INSERT INTO acos VALUES (172,0,'banners',NULL,'banners',367,380);
INSERT INTO acos VALUES (173,172,'banners',NULL,'admin_index',368,369);
INSERT INTO acos VALUES (174,172,'banners',NULL,'admin_add',370,371);
INSERT INTO acos VALUES (175,172,'banners',NULL,'admin_edit',372,373);
INSERT INTO acos VALUES (176,172,'banners',NULL,'admin_delete',374,375);
INSERT INTO acos VALUES (177,172,'banners',NULL,'view',376,377);
INSERT INTO acos VALUES (178,0,'Slides',NULL,'Slides',381,394);
INSERT INTO acos VALUES (179,178,'Slides',NULL,'admin_index',382,383);
INSERT INTO acos VALUES (180,178,'Slides',NULL,'admin_add',384,385);
INSERT INTO acos VALUES (181,178,'Slides',NULL,'admin_edit',386,387);
INSERT INTO acos VALUES (182,178,'Slides',NULL,'admin_delete',388,389);
INSERT INTO acos VALUES (183,178,'Slides',NULL,'index',390,391);
INSERT INTO acos VALUES (184,178,'Slides',NULL,'view',392,393);
INSERT INTO acos VALUES (185,11,'Conditions',NULL,'view',74,75);
INSERT INTO acos VALUES (186,20,'Therapies',NULL,'view',106,107);
INSERT INTO acos VALUES (187,20,'Therapies',NULL,'index',108,109);
INSERT INTO acos VALUES (188,133,'traditions',NULL,'index',312,313);
INSERT INTO acos VALUES (189,133,'traditions',NULL,'view',314,315);
INSERT INTO acos VALUES (190,133,'traditions',NULL,'admin_delete',316,317);
INSERT INTO acos VALUES (193,25,'Videos',NULL,'view',146,147);
INSERT INTO acos VALUES (194,0,'positions',NULL,'positions',395,406);
INSERT INTO acos VALUES (195,194,'positions',NULL,'admin_index',396,397);
INSERT INTO acos VALUES (196,194,'positions',NULL,'admin_add',398,399);
INSERT INTO acos VALUES (197,194,'positions',NULL,'admin_delete',400,401);
INSERT INTO acos VALUES (198,194,'positions',NULL,'admin_index',402,403);
INSERT INTO acos VALUES (199,194,'positions',NULL,'admin_edit',404,405);
INSERT INTO acos VALUES (200,11,'Conditions',NULL,'tag_cloud',76,77);
INSERT INTO acos VALUES (201,14,'Symptoms',NULL,'tag_cloud',90,91);
INSERT INTO acos VALUES (202,20,'Therapies',NULL,'tag_cloud',110,111);
INSERT INTO acos VALUES (203,133,'traditions',NULL,'tag_cloud',318,319);
INSERT INTO acos VALUES (204,7,'Pages',NULL,'tag_cloud',56,57);
INSERT INTO acos VALUES (206,28,'Stories',NULL,'featured',178,179);
INSERT INTO acos VALUES (207,28,'Stories',NULL,'tag_cloud',180,181);
INSERT INTO acos VALUES (208,28,'Stories',NULL,'view',182,183);
INSERT INTO acos VALUES (209,25,'Videos',NULL,'featured',148,149);
INSERT INTO acos VALUES (211,5,'Users',NULL,'admin_changePassword',36,37);
INSERT INTO acos VALUES (212,5,'Users',NULL,'unauthorized',38,39);
INSERT INTO acos VALUES (213,24,'Articles',NULL,'view',126,127);
INSERT INTO acos VALUES (214,0,'Subscribers',NULL,'subscribers',407,412);
INSERT INTO acos VALUES (215,214,'Subscribers',NULL,'add',408,409);
INSERT INTO acos VALUES (216,214,'subscribers',NULL,'suscribe',410,411);

#
# Table structure for table 'areas'
#

# DROP TABLE IF EXISTS areas;
CREATE TABLE `areas` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(128) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='This table is the named CONTENT ARENA section (see specs.). ';

#
# Dumping data for table 'areas'
#

INSERT INTO areas VALUES (1,'Body');
INSERT INTO areas VALUES (2,'Inner Journey');
INSERT INTO areas VALUES (3,'Spirituality');
INSERT INTO areas VALUES (6,'Mind / Emotions');

#
# Table structure for table 'aros'
#

# DROP TABLE IF EXISTS aros;
CREATE TABLE `aros` (
  `id` int(10) NOT NULL auto_increment,
  `parent_id` int(10) default NULL,
  `model` varchar(255) default NULL,
  `foreign_key` int(10) default NULL,
  `alias` varchar(255) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'aros'
#

INSERT INTO aros VALUES (1,0,'Group',3,'administrators',1,28);
INSERT INTO aros VALUES (2,0,'Group',2,'publishers',29,42);
INSERT INTO aros VALUES (3,0,'Group',1,'users',43,58);
INSERT INTO aros VALUES (29,1,'User',NULL,'nicolas@conseguir.com.ar',12,13);
INSERT INTO aros VALUES (5,0,'Group',NULL,'anonymous',59,62);
INSERT INTO aros VALUES (6,5,'User',NULL,'anonymous',60,61);
INSERT INTO aros VALUES (13,1,'User',NULL,'pam@eyespeak.com',2,3);
INSERT INTO aros VALUES (35,2,'User',23,'leesa@liiv.com',38,39);
INSERT INTO aros VALUES (34,2,'User',22,'bernadette@liiv.com',36,37);
INSERT INTO aros VALUES (30,2,'User',18,'nico@eyespeak.com',34,35);
INSERT INTO aros VALUES (20,1,'User',10,'jess@eyespeak.com',4,5);
INSERT INTO aros VALUES (22,1,'User',12,'rafael@eyespeak.com',6,7);
INSERT INTO aros VALUES (36,2,'User',24,'greg@liiv.com',40,41);
INSERT INTO aros VALUES (25,2,'User',15,'donna@liiv.com',32,33);
INSERT INTO aros VALUES (26,1,'User',16,'pammasters@yahoo.com',8,9);
INSERT INTO aros VALUES (27,1,'User',17,'benj@eyespeak.com',10,11);
INSERT INTO aros VALUES (37,1,'User',25,'richa@liiv.com',14,15);
INSERT INTO aros VALUES (38,1,'User',26,'michael@liiv.com',16,17);
INSERT INTO aros VALUES (39,1,'User',27,'juan@eyespeak.com',18,19);
INSERT INTO aros VALUES (44,1,'User',33,'richa@liiv.com',22,23);
INSERT INTO aros VALUES (41,3,'User',29,'jasonfrankmartin@gmail.com',46,47);
INSERT INTO aros VALUES (42,3,'User',30,'testuser@test.com',48,49);
INSERT INTO aros VALUES (43,1,'User',32,'johndoe@eyespeak.com',20,21);
INSERT INTO aros VALUES (45,1,'User',34,'rafaela@eyespeak.com',24,25);
INSERT INTO aros VALUES (46,1,'User',35,'rafa@rafa.com',26,27);
INSERT INTO aros VALUES (47,3,'User',36,'dada@dada.com',50,51);
INSERT INTO aros VALUES (48,3,'User',37,'sebastian@conseguir.com.ar',52,53);
INSERT INTO aros VALUES (49,3,'User',38,'asd@asd.com',54,55);
INSERT INTO aros VALUES (50,3,'User',39,'asd@asd33.com',56,57);

#
# Table structure for table 'aros_acos'
#

# DROP TABLE IF EXISTS aros_acos;
CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL auto_increment,
  `aro_id` int(10) NOT NULL default '0',
  `aco_id` int(10) NOT NULL default '0',
  `_create` varchar(2) NOT NULL default '0',
  `_read` varchar(2) NOT NULL default '0',
  `_update` varchar(2) NOT NULL default '0',
  `_delete` varchar(2) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=MyISAM AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'aros_acos'
#

INSERT INTO aros_acos VALUES (1,1,1,'1','1','1','1');
INSERT INTO aros_acos VALUES (2,1,5,'1','1','1','1');
INSERT INTO aros_acos VALUES (3,3,1,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (4,2,1,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (5,3,6,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (6,1,7,'1','1','1','1');
INSERT INTO aros_acos VALUES (7,6,8,'1','1','1','1');
INSERT INTO aros_acos VALUES (8,1,11,'1','1','1','1');
INSERT INTO aros_acos VALUES (9,1,14,'1','1','1','1');
INSERT INTO aros_acos VALUES (10,1,20,'1','1','1','1');
INSERT INTO aros_acos VALUES (11,6,18,'1','1','1','1');
INSERT INTO aros_acos VALUES (12,6,17,'1','1','1','1');
INSERT INTO aros_acos VALUES (13,1,24,'1','1','1','1');
INSERT INTO aros_acos VALUES (14,1,28,'1','1','1','1');
INSERT INTO aros_acos VALUES (82,2,17,'1','1','1','1');
INSERT INTO aros_acos VALUES (81,6,208,'1','1','1','1');
INSERT INTO aros_acos VALUES (17,1,34,'1','1','1','1');
INSERT INTO aros_acos VALUES (18,1,36,'1','1','1','1');
INSERT INTO aros_acos VALUES (19,1,32,'1','1','1','1');
INSERT INTO aros_acos VALUES (20,1,39,'1','1','1','1');
INSERT INTO aros_acos VALUES (21,1,40,'1','1','1','1');
INSERT INTO aros_acos VALUES (22,1,41,'1','1','1','1');
INSERT INTO aros_acos VALUES (23,1,25,'1','1','1','1');
INSERT INTO aros_acos VALUES (24,1,26,'1','1','1','1');
INSERT INTO aros_acos VALUES (25,1,51,'1','1','1','1');
INSERT INTO aros_acos VALUES (26,1,56,'1','1','1','1');
INSERT INTO aros_acos VALUES (27,1,61,'1','1','1','1');
INSERT INTO aros_acos VALUES (28,1,82,'1','1','1','1');
INSERT INTO aros_acos VALUES (29,1,89,'1','1','1','1');
INSERT INTO aros_acos VALUES (30,6,6,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (31,6,14,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (32,1,92,'1','1','1','1');
INSERT INTO aros_acos VALUES (33,1,99,'1','1','1','1');
INSERT INTO aros_acos VALUES (34,6,1,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (36,1,109,'1','1','1','1');
INSERT INTO aros_acos VALUES (37,1,116,'1','1','1','1');
INSERT INTO aros_acos VALUES (38,1,126,'1','1','1','1');
INSERT INTO aros_acos VALUES (39,1,133,'1','1','1','1');
INSERT INTO aros_acos VALUES (40,6,142,'1','1','1','1');
INSERT INTO aros_acos VALUES (41,2,14,'1','1','1','1');
INSERT INTO aros_acos VALUES (42,2,18,'1','1','1','1');
INSERT INTO aros_acos VALUES (43,16,8,'1','1','1','1');
INSERT INTO aros_acos VALUES (44,2,142,'1','1','1','1');
INSERT INTO aros_acos VALUES (45,3,141,'1','1','1','1');
INSERT INTO aros_acos VALUES (46,16,11,'1','1','1','1');
INSERT INTO aros_acos VALUES (80,1,206,'1','1','1','1');
INSERT INTO aros_acos VALUES (48,1,145,'1','1','1','1');
INSERT INTO aros_acos VALUES (49,1,152,'1','1','1','1');
INSERT INTO aros_acos VALUES (50,1,118,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (51,1,160,'1','1','1','1');
INSERT INTO aros_acos VALUES (79,6,206,'1','1','1','1');
INSERT INTO aros_acos VALUES (54,6,168,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (55,3,142,'1','1','1','1');
INSERT INTO aros_acos VALUES (56,6,171,'1','1','1','1');
INSERT INTO aros_acos VALUES (57,3,171,'1','1','1','1');
INSERT INTO aros_acos VALUES (58,6,104,'1','1','1','1');
INSERT INTO aros_acos VALUES (59,6,105,'1','1','1','1');
INSERT INTO aros_acos VALUES (60,1,172,'1','1','1','1');
INSERT INTO aros_acos VALUES (61,6,177,'1','1','1','1');
INSERT INTO aros_acos VALUES (62,1,178,'1','1','1','1');
INSERT INTO aros_acos VALUES (63,6,184,'1','1','1','1');
INSERT INTO aros_acos VALUES (64,6,185,'1','1','1','1');
INSERT INTO aros_acos VALUES (65,6,186,'1','1','1','1');
INSERT INTO aros_acos VALUES (66,6,21,'1','1','1','1');
INSERT INTO aros_acos VALUES (67,6,12,'1','1','1','1');
INSERT INTO aros_acos VALUES (68,6,187,'1','1','1','1');
INSERT INTO aros_acos VALUES (69,6,188,'1','1','1','1');
INSERT INTO aros_acos VALUES (70,6,189,'1','1','1','1');
INSERT INTO aros_acos VALUES (71,1,190,'1','1','1','1');
INSERT INTO aros_acos VALUES (72,1,194,'1','1','1','1');
INSERT INTO aros_acos VALUES (73,6,200,'1','1','1','1');
INSERT INTO aros_acos VALUES (74,6,201,'1','1','1','1');
INSERT INTO aros_acos VALUES (75,6,202,'1','1','1','1');
INSERT INTO aros_acos VALUES (76,6,203,'1','1','1','1');
INSERT INTO aros_acos VALUES (77,6,204,'1','1','1','1');
INSERT INTO aros_acos VALUES (78,3,204,'1','1','1','1');
INSERT INTO aros_acos VALUES (83,2,8,'1','1','1','1');
INSERT INTO aros_acos VALUES (84,2,185,'1','1','1','1');
INSERT INTO aros_acos VALUES (85,2,200,'1','1','1','1');
INSERT INTO aros_acos VALUES (86,2,186,'1','1','1','1');
INSERT INTO aros_acos VALUES (87,2,202,'1','1','1','1');
INSERT INTO aros_acos VALUES (88,2,105,'1','1','1','1');
INSERT INTO aros_acos VALUES (89,2,104,'1','1','1','1');
INSERT INTO aros_acos VALUES (90,2,5,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (91,2,7,'1','1','1','1');
INSERT INTO aros_acos VALUES (92,2,141,'1','1','1','1');
INSERT INTO aros_acos VALUES (93,2,11,'1','1','1','1');
INSERT INTO aros_acos VALUES (94,2,20,'1','1','1','1');
INSERT INTO aros_acos VALUES (95,2,24,'1','1','1','1');
INSERT INTO aros_acos VALUES (96,2,25,'1','1','1','1');
INSERT INTO aros_acos VALUES (97,2,26,'1','1','1','1');
INSERT INTO aros_acos VALUES (98,2,28,'1','1','1','1');
INSERT INTO aros_acos VALUES (99,2,51,'1','1','1','1');
INSERT INTO aros_acos VALUES (100,2,56,'1','1','1','1');
INSERT INTO aros_acos VALUES (101,2,82,'1','1','1','1');
INSERT INTO aros_acos VALUES (102,2,89,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (103,2,92,'1','1','1','1');
INSERT INTO aros_acos VALUES (104,2,99,'1','1','1','1');
INSERT INTO aros_acos VALUES (105,2,109,'1','1','1','1');
INSERT INTO aros_acos VALUES (106,2,126,'1','1','1','1');
INSERT INTO aros_acos VALUES (107,2,145,'1','1','1','1');
INSERT INTO aros_acos VALUES (108,2,152,'1','1','1','1');
INSERT INTO aros_acos VALUES (109,2,160,'1','1','1','1');
INSERT INTO aros_acos VALUES (110,2,172,'1','1','1','1');
INSERT INTO aros_acos VALUES (111,2,178,'1','1','1','1');
INSERT INTO aros_acos VALUES (112,2,194,'1','1','1','1');
INSERT INTO aros_acos VALUES (113,5,209,'1','1','1','1');
INSERT INTO aros_acos VALUES (114,6,209,'1','1','1','1');
INSERT INTO aros_acos VALUES (115,6,25,'1','1','1','1');
INSERT INTO aros_acos VALUES (116,6,33,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (117,6,42,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (118,6,44,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (119,6,45,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (120,6,74,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (121,6,124,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (122,6,170,'-1','-1','-1','-1');
INSERT INTO aros_acos VALUES (123,1,211,'1','1','1','1');
INSERT INTO aros_acos VALUES (124,2,211,'1','1','1','1');
INSERT INTO aros_acos VALUES (125,6,212,'1','1','1','1');
INSERT INTO aros_acos VALUES (126,6,24,'1','1','1','1');
INSERT INTO aros_acos VALUES (127,6,193,'1','1','1','1');
INSERT INTO aros_acos VALUES (128,1,193,'1','1','1','1');
INSERT INTO aros_acos VALUES (129,2,193,'1','1','1','1');
INSERT INTO aros_acos VALUES (130,3,193,'1','1','1','1');
INSERT INTO aros_acos VALUES (131,6,215,'1','1','1','1');
INSERT INTO aros_acos VALUES (132,1,215,'1','1','1','1');
INSERT INTO aros_acos VALUES (133,6,216,'1','1','1','1');

#
# Table structure for table 'articles'
#

# DROP TABLE IF EXISTS articles;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(128) default NULL,
  `author` varchar(128) default NULL,
  `description` text,
  `content` varchar(255) default NULL,
  `area_id` int(11) unsigned default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(11) default NULL,
  `updatedby` int(11) default NULL,
  `published` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles'
#

INSERT INTO articles VALUES (13,'Sleep Deprivation Diseases and Dissonance','Stephen Holt, M.D. LLD(hon) ChB, PhD, ND, FRCP, MRCP, GACG, FACN, FACAM, OSJ','<p>In this excellent and comprehensive article, Dr. Stephen Holt provides strong scientific data in support of approaching sleep irregularities and insomnia from a Complimentary, or Alternative Medical perspective. He challenges the classic allopathic response to insomnia and gives strong scientific data to support his arguments for what he has come to call a &ldquo;Sleep Naturally Plan&rdquo;<br />Dr. Holt cites the causes of insomnia to be the result of a combined reasons, including:</p>\r\n<ol>\r\n	<li>Humanity being out of touch with the circadian rhythms.</li>\r\n	<li>Pharmaceutical drugs being used to deal with other health issues</li>\r\n	<li>Poor sleep habits</li>\r\n	<li>Stressful living</li>\r\n</ol>','http://www.naturalclinician.com/articles/sleep-article.pdf',6,'2009-12-14 16:44:39','2010-02-01 18:04:26',12,5,1);
INSERT INTO articles VALUES (14,'Insomnia and Sleep','The National Sleep Foundation. Reviewed by David N. Neubauer, M.D., M.A','<p>If not treated, insomnia is closely linked to increased illness, disease and can even deteriorate health to such a degree that the end result could be death. A wealth of research indicates that people with insomnia suffer more from depression, have poorer overall health, and more work absenteeism. This article summarizes the symptoms and potential causes for insomnia, and offers different treatment options ranging from relaxation techniques and good sleep habits to medication and behavioral therapy.</p>','http://www.sleepfoundation.org/article/sleep-related-problems/insomnia-and-sleep',6,'2009-12-21 14:59:54','2010-01-07 22:52:18',12,5,1);
INSERT INTO articles VALUES (15,'Improving Your Sleep','Brenda O\'Hanlon,  (Author of Sleep: The Common Sense Approach)','<p>Sleep problems are often a sign of depression or anxiety. For many people, holistic approaches provide the solutions needed to recover quality sleep.&nbsp; This piece provides a lengthy list of holistic suggestions, options and considerations for improving your general sleep hygiene, which may be surprisingly effective in improving sleep quality.</p>','http://www.londonhealth.co.uk/default.asp?id=89',6,'2009-12-21 15:22:03','2010-01-07 22:52:02',12,5,0);
INSERT INTO articles VALUES (16,'Choosing The Right Sleep Medicines, or None at All','Peter Jaret, New York Times','<p>Over 30 million Americans wrestle with chronic insomnia. While the safety of insomnia drugs has improved steadily over the last couple of decades, changing the way you approach sleep is the first step before finding what medication might be right for you.</p>','http://health.nytimes.com/ref/health/healthguide/esn-insomnia-ess.html',6,'2009-12-21 15:24:36','2010-01-07 22:51:49',12,5,1);
INSERT INTO articles VALUES (17,'Homeopathy for Insomnia','www.altMD.com','<p>\r\n	<span style=\"font-family: \'Arial Narrow\';\"><sup><font size=\"3\">Animal studies have shown an improvement in sleep patterns in rats given homeopathic doses of </font></sup></span><span style=\"font-family: \'Arial Narrow\';\"><sup><i><font size=\"3\">coffea cruda</font></i></sup></span><span style=\"font-family: \'Arial Narrow\';\"><sup><font size=\"3\">, which is derived from the coffee bean. Homeopathic practitioners report successful human use of </font></sup></span><span style=\"font-family: \'Arial Narrow\';\"><sup><i><font size=\"3\">coffea cruda</font></i></sup></span><span style=\"font-family: \'Arial Narrow\';\"><sup><font size=\"3\"> in situations where insomnia is caused by stress, anxiety and overactive thought patterns. </font></sup></span><sup> <span style=\"font-family: \'Arial Narrow\';\"><font size=\"3\">This article provides various possible homeopathic responses to insomnia dependent upon the experience or circumstance related to insomnia.&nbsp; </font></span><span style=\"font-family: \'Arial Narrow\';\"><font size=\"3\">It </font></span><span style=\"font-family: \'Arial Narrow\';\"><font size=\"3\">also points out that it is always beneficial to have the advice of a Homeopath as treatments are not always easy to get right the first time.</font></span></sup></p>\r\n','http://www.altmd.com/Articles/Homeopathy-for-Insomnia',6,'2009-12-21 15:25:20','2010-02-16 19:24:42',12,1,0);

#
# Table structure for table 'articles_articles'
#

# DROP TABLE IF EXISTS articles_articles;
CREATE TABLE `articles_articles` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `article_id` int(11) unsigned default NULL,
  `article_sec_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles_articles'
#


#
# Table structure for table 'articles_audios'
#

# DROP TABLE IF EXISTS articles_audios;
CREATE TABLE `articles_audios` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `article_id` int(11) unsigned default NULL,
  `audio_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles_audios'
#


#
# Table structure for table 'articles_authorities'
#

# DROP TABLE IF EXISTS articles_authorities;
CREATE TABLE `articles_authorities` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `article_id` int(11) unsigned default NULL,
  `authority_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles_authorities'
#


#
# Table structure for table 'articles_conditions'
#

# DROP TABLE IF EXISTS articles_conditions;
CREATE TABLE `articles_conditions` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `article_id` int(11) unsigned default NULL,
  `condition_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles_conditions'
#

INSERT INTO articles_conditions VALUES (91,16,9);
INSERT INTO articles_conditions VALUES (90,14,9);
INSERT INTO articles_conditions VALUES (89,13,9);

#
# Table structure for table 'articles_events'
#

# DROP TABLE IF EXISTS articles_events;
CREATE TABLE `articles_events` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `article_id` int(11) unsigned default NULL,
  `event_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles_events'
#


#
# Table structure for table 'articles_organizations'
#

# DROP TABLE IF EXISTS articles_organizations;
CREATE TABLE `articles_organizations` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `article_id` int(11) unsigned default NULL,
  `organization_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles_organizations'
#


#
# Table structure for table 'articles_publications'
#

# DROP TABLE IF EXISTS articles_publications;
CREATE TABLE `articles_publications` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `article_id` int(11) unsigned default NULL,
  `publication_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles_publications'
#


#
# Table structure for table 'articles_stories'
#

# DROP TABLE IF EXISTS articles_stories;
CREATE TABLE `articles_stories` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `article_id` int(11) unsigned default NULL,
  `story_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles_stories'
#


#
# Table structure for table 'articles_therapies'
#

# DROP TABLE IF EXISTS articles_therapies;
CREATE TABLE `articles_therapies` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `article_id` int(11) unsigned default NULL,
  `therapy_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles_therapies'
#


#
# Table structure for table 'articles_videos'
#

# DROP TABLE IF EXISTS articles_videos;
CREATE TABLE `articles_videos` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `article_id` int(11) unsigned default NULL,
  `video_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'articles_videos'
#

INSERT INTO articles_videos VALUES (57,14,1);
INSERT INTO articles_videos VALUES (62,17,47);
INSERT INTO articles_videos VALUES (60,14,47);

#
# Table structure for table 'audios'
#

# DROP TABLE IF EXISTS audios;
CREATE TABLE `audios` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(128) default NULL,
  `author` varchar(128) default NULL,
  `description_short` text,
  `content` varchar(255) default NULL,
  `area_id` int(11) unsigned default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  `published` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'audios'
#

INSERT INTO audios VALUES (3,'CANARIO','JASON','','/audios/520652.mp3',1,'2009-12-14 14:53:31','2009-12-14 14:53:31',1,1,0);
INSERT INTO audios VALUES (4,'VAMOSSSSS','JASON','','/audios/145348994.mp3',1,'2009-12-14 14:54:11','2009-12-14 14:54:11',1,1,0);
INSERT INTO audios VALUES (5,'VAMOSSSSS','JASON','','/audios/9555122.mp3',1,'2009-12-14 14:55:05','2009-12-14 14:55:05',1,1,0);
INSERT INTO audios VALUES (6,'VAS','FRANK','','/audios/18551010.mp3',1,'2009-12-14 14:57:44','2009-12-14 14:57:44',1,1,0);

#
# Table structure for table 'audios_audios'
#

# DROP TABLE IF EXISTS audios_audios;
CREATE TABLE `audios_audios` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `audio_id` int(11) unsigned default NULL,
  `audio_sec_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'audios_audios'
#


#
# Table structure for table 'audios_authorities'
#

# DROP TABLE IF EXISTS audios_authorities;
CREATE TABLE `audios_authorities` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `audio_id` int(11) unsigned default NULL,
  `authority_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'audios_authorities'
#


#
# Table structure for table 'audios_conditions'
#

# DROP TABLE IF EXISTS audios_conditions;
CREATE TABLE `audios_conditions` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `audio_id` int(11) unsigned default NULL,
  `condition_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'audios_conditions'
#


#
# Table structure for table 'audios_events'
#

# DROP TABLE IF EXISTS audios_events;
CREATE TABLE `audios_events` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `audio_id` int(11) unsigned default NULL,
  `event_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'audios_events'
#


#
# Table structure for table 'audios_organizations'
#

# DROP TABLE IF EXISTS audios_organizations;
CREATE TABLE `audios_organizations` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `audio_id` int(11) unsigned default NULL,
  `organization_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'audios_organizations'
#


#
# Table structure for table 'audios_publications'
#

# DROP TABLE IF EXISTS audios_publications;
CREATE TABLE `audios_publications` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `audio_id` int(11) unsigned default NULL,
  `publication_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'audios_publications'
#


#
# Table structure for table 'audios_stories'
#

# DROP TABLE IF EXISTS audios_stories;
CREATE TABLE `audios_stories` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `audio_id` int(11) unsigned default NULL,
  `story_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'audios_stories'
#


#
# Table structure for table 'audios_symptoms'
#

# DROP TABLE IF EXISTS audios_symptoms;
CREATE TABLE `audios_symptoms` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `audio_id` int(11) unsigned default NULL,
  `symptom_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'audios_symptoms'
#


#
# Table structure for table 'audios_therapies'
#

# DROP TABLE IF EXISTS audios_therapies;
CREATE TABLE `audios_therapies` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `audio_id` int(11) unsigned default NULL,
  `therapy_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'audios_therapies'
#


#
# Table structure for table 'authorities'
#

# DROP TABLE IF EXISTS authorities;
CREATE TABLE `authorities` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(128) default NULL,
  `title` varchar(128) default NULL,
  `bio` text,
  `image` varchar(128) default NULL,
  `url` varchar(128) default NULL,
  `area_id` int(11) unsigned default NULL,
  `type_id` int(10) unsigned NOT NULL,
  `practice_area` varchar(128) default NULL,
  `overview` text,
  `interview` varchar(128) default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  `published` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'authorities'
#

INSERT INTO authorities VALUES (1,'My Auth','Test','blabla','/img/authority/1983946.jpg','',3,2,'','','/videos/authority/21277196.flv','2009-11-05 16:40:13','2009-11-23 16:45:03',NULL,NULL,0);
INSERT INTO authorities VALUES (2,'Andrew Weil','','<p>\r\n	<strong><u>Andrew Weil</u></strong> was born in Philadelphia in 1942, received an A.B. degree in biology (botany) from Harvard in 1964 and an M.D. from Harvard Medical School in 1968. From 1971-75, as a Fellow of the Institute of Current World Affairs, Dr. Weil traveled widely in North and South America and Africa collecting information on drug use in other cultures, medicinal plants, and alternative methods of treating disease.&nbsp; At present Dr. Weil is Director of the Center for Integrative Medicine of the College of Medicine, University of Arizona, where he also holds the Lovell-Jones Endowed Chair in Integrative Rheumatology and is Clinical Professor of Medicine and Professor of Public Health. The Center is the leading effort in the world to develop a comprehensive curriculum in integrative medicine. Graduates serve as directors of integrative medicine programs around the United States, and through its Fellowship, the Center is now training doctors and nurse practitioners around the world. Andrew Weil is the author of many scientific and popular articles and of 11 books. Dr. Weil also writes a monthly newsletter, Dr. Andrew Weil&#39;s Self Healing, maintains a popular website, Dr. Weil.com (www.drweil.com), and appears in video programs featured on PBS. He also writes a monthly column for Prevention magazine. Dr. Weil serves as the Director of Integrative Health and Healing at Miraval Life in Balance Resort in Catalina, Arizona. A frequent lecturer and guest on talk shows, Dr. Weil is an internationally recognized expert on medicinal plants, alternative medicine, and the reform of medical education. He lives near Tucson, Arizona, USA</p>\r\n','','',6,2,'Integrative Medecine','','','2009-12-07 16:22:30','2009-12-21 15:37:57',5,12,0);

#
# Table structure for table 'authorities_authorities'
#

# DROP TABLE IF EXISTS authorities_authorities;
CREATE TABLE `authorities_authorities` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `authority_id` int(11) unsigned default NULL,
  `authority_sec_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'authorities_authorities'
#


#
# Table structure for table 'authorities_conditions'
#

# DROP TABLE IF EXISTS authorities_conditions;
CREATE TABLE `authorities_conditions` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `authority_id` int(11) unsigned default NULL,
  `condition_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'authorities_conditions'
#


#
# Table structure for table 'authorities_events'
#

# DROP TABLE IF EXISTS authorities_events;
CREATE TABLE `authorities_events` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `authority_id` int(11) unsigned default NULL,
  `event_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'authorities_events'
#


#
# Table structure for table 'authorities_organizations'
#

# DROP TABLE IF EXISTS authorities_organizations;
CREATE TABLE `authorities_organizations` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `authority_id` int(11) unsigned default NULL,
  `organization_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'authorities_organizations'
#

INSERT INTO authorities_organizations VALUES (1,1,1);

#
# Table structure for table 'authorities_stories'
#

# DROP TABLE IF EXISTS authorities_stories;
CREATE TABLE `authorities_stories` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `authority_id` int(11) unsigned default NULL,
  `story_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'authorities_stories'
#


#
# Table structure for table 'authorities_therapies'
#

# DROP TABLE IF EXISTS authorities_therapies;
CREATE TABLE `authorities_therapies` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `authority_id` int(11) unsigned default NULL,
  `therapy_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'authorities_therapies'
#


#
# Table structure for table 'authorities_workshops'
#

# DROP TABLE IF EXISTS authorities_workshops;
CREATE TABLE `authorities_workshops` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `authority_id` int(10) unsigned NOT NULL,
  `workshop_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table 'authorities_workshops'
#


#
# Table structure for table 'authority_types'
#

# DROP TABLE IF EXISTS authority_types;
CREATE TABLE `authority_types` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(45) NOT NULL,
  PRIMARY KEY  USING BTREE (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Dumping data for table 'authority_types'
#

INSERT INTO authority_types VALUES (1,'Medical Practicioners');
INSERT INTO authority_types VALUES (2,'Experts');
INSERT INTO authority_types VALUES (3,'Teachers');

#
# Table structure for table 'banners'
#

# DROP TABLE IF EXISTS banners;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(45) default NULL,
  `content` varchar(512) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `width` int(10) unsigned default '980',
  `height` int(10) unsigned default '30',
  `link` varchar(255) default '/',
  `createdby` int(11) default NULL,
  `updatedby` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'banners'
#

INSERT INTO banners VALUES (1,'Yoga - Strength and toning','/banners/59479129.jpg','2009-12-18 17:54:44','2010-02-11 20:52:27',980,30,'http://java.sun.com/developer/onlineTraining/Beans/',NULL,1);
INSERT INTO banners VALUES (2,'Another','/banners/59479129.jpg','2009-12-21 14:48:44','2010-02-11 20:52:58',980,30,'http://java.sun.com/developer/onlineTraining/Beans/',NULL,1);
INSERT INTO banners VALUES (4,'Heading 1','/banners/286649201.jpg','2010-01-04 12:41:53','2010-02-01 14:39:57',728,90,'http://www.liiv.com',NULL,12);
INSERT INTO banners VALUES (5,'Heading 2','/banners/55081040.jpg','2010-01-04 12:50:59','2010-01-28 15:50:40',728,90,'http://www.liiv.com',NULL,NULL);
INSERT INTO banners VALUES (6,'Right box ad 1','/banners/520685702.jpg','2010-01-04 15:47:07','2010-01-04 15:47:07',980,35,'http://www.liiv.com',NULL,NULL);
INSERT INTO banners VALUES (7,'Right box ad 2 ','/banners/533615231.jpg','2010-01-04 15:50:06','2010-02-10 11:36:21',980,35,'http://www.liiv.com',NULL,12);
INSERT INTO banners VALUES (8,'Homepage box 1','/banners/41898753.jpg','2010-01-05 10:18:51','2010-01-05 10:18:51',980,35,'http://www.liiv.com',NULL,NULL);
INSERT INTO banners VALUES (9,'300 x 100 ','/banners/232291799.jpg','2010-01-05 10:50:32','2010-01-05 10:50:32',980,35,'http://www.liiv.com',NULL,NULL);
INSERT INTO banners VALUES (10,'300 x 100 2','/banners/421554819.jpg','2010-01-05 10:52:55','2010-01-05 10:52:55',980,35,'http://www.liiv.com',NULL,NULL);

#
# Table structure for table 'banners_positions'
#

# DROP TABLE IF EXISTS banners_positions;
CREATE TABLE `banners_positions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `banner_id` int(10) unsigned default NULL,
  `position_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'banners_positions'
#

INSERT INTO banners_positions VALUES (33,11,3);
INSERT INTO banners_positions VALUES (30,4,4);
INSERT INTO banners_positions VALUES (21,5,4);
INSERT INTO banners_positions VALUES (14,6,5);
INSERT INTO banners_positions VALUES (32,7,5);
INSERT INTO banners_positions VALUES (16,8,6);
INSERT INTO banners_positions VALUES (17,9,7);
INSERT INTO banners_positions VALUES (18,10,7);
INSERT INTO banners_positions VALUES (34,11,5);

#
# Table structure for table 'conditions'
#

# DROP TABLE IF EXISTS conditions;
CREATE TABLE `conditions` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(128) NOT NULL,
  `definition_short` text,
  `definition_long` text,
  `introduction` text,
  `holistic_approach` text,
  `area_id` int(11) unsigned default NULL,
  `published` tinyint(1) default '0',
  `path` varchar(100) NOT NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  `standard_approach` text,
  `symptoms_text` text,
  `symptoms_list` text,
  `references` text,
  PRIMARY KEY  (`id`),
  KEY `URL` (`path`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'conditions'
#

INSERT INTO conditions VALUES (9,'Insomnia','<p>\r\n	<img align=\"right\" alt=\"\" height=\"158\" src=\"/img/wysiwyg/images/liiv3_TherapyDetails_Final.jpg\" width=\"302\" />Insomnia is a common condition in which you have trouble falling or staying asleep. The condition can range from mild to severe, depending on how often it occurs and for how long.</p>\r\n','<p>\r\n	How much sleep is enough? Most adults need seven to eight hours a night. More than one-third of adults have occasional insomnia, while 10 to 15 percent report long-term, chronic sleep problems.</p>\r\n<p>\r\n	Good sleep is as essential for your health, wellness and quality of life as oxygen.&nbsp; The rhythms of sleep and wakefulness are always present in the rhythms of the seasons, and in every species and the natural world.&nbsp; Birdsong stops at the end of the day, and some insects gear up only when the sun goes down.&nbsp; Bears hibernate for months at a time, and many trees and shrubs shed their leaves, laying dormant until the warmth of spring awakens them.&nbsp; It is during the deep sleep phase that growth and healing occur.&nbsp; If you are coping with some form of insomnia, you miss the benefit of the rejuvenation that occurs during the deep sleep cycle.</p>\r\n<p>\r\n	<strong>Chronic</strong> (ongoing) <strong>insomnia</strong> means you experience sleep difficulty at least 3 nights a week for more than a month, whereas acute (short-term) insomnia is the term used to describe a sleep problem of a lesser duration.</p>\r\n<p>\r\n	Insomnia can stem from a medical condition, or other medical conditions that cause chronic pain, (arthritis, fibromyalgia, etc.) or from the use of substances that interfere with sleep.&nbsp; This is known as secondary insomnia.&nbsp; Traditional Medicine as we know it in the west, lists some of the, common causes of insomnia:</p>\r\n<ul>\r\n	<li>\r\n		Stress</li>\r\n	<li>\r\n		Anxiety</li>\r\n	<li>\r\n		Depression</li>\r\n	<li>\r\n		Poor sleep habits</li>\r\n	<li>\r\n		Changes in your environment</li>\r\n	<li>\r\n		Changes in your work schedule</li>\r\n	<li>\r\n		Using certain medications, or substances such as&nbsp; caffeine, nicotine &amp; alcohol.&nbsp;</li>\r\n</ul>\r\n<p>\r\n	Primary insomnia is its own disorder and is not the result of another condition. It is an ongoing condition of sleep disruption, difficulty sleeping, and poor quality of sleep.</p>\r\n','<p>\r\n	Getting a good night&rsquo;s sleep is no simple task for 1 in 3 adults. Fortunately, there are many conventional and holistic treatments to overcome this debilitating problem. It&rsquo;s during deep sleep that the restorative functions of the body take place supporting your overall state of health and wellbeing.</p>\r\n','<p>\r\n	A traditional (western) medical doctor typically explores your medical history and tries to get rid of your symptoms, very often through the use of pills or&nbsp; other pharmaceutical products.&nbsp; Holistic practioners will factor your medical history into a greater equation &ndash; one that looks for the root cause of the imbalance underlying your insomnia.&nbsp; They consider the various components of your life such as &ndash; your medical history, life circumstance, general nutrition, exercise, level of stress, etc.&nbsp; After doing the math so to speak &ndash; assessing the impact of all these elements on your health, general well-being and spirit, a holistic practitioner prescribes the course of treatment that will best heal the imbalance and re-establish a normal sleep cycle.&nbsp; In short, Holistic practitioners treat the whole person &ndash; physically, emotionally, and mentally.&nbsp;</p>\r\n',6,1,'insomnia','2009-12-14 15:51:35','2010-02-12 11:19:43',12,12,'<div>\r\n	<p>\r\n		A traditional (western) medical doctor typically explores your medical history and tries to get rid of your symptoms, very often through the use of pills or&nbsp; other pharmaceutical products.&nbsp; Holistic practioners will factor your medical history into a greater equation &ndash; one that looks for the root cause of the imbalance underlying your insomnia.&nbsp; They consider the various components of your life such as &ndash; your medical history, life circumstance, general nutrition, exercise, level of stress, etc.&nbsp; After doing the math so to speak &ndash; assessing the impact of all these elements on your health, general well-being and spirit, a holistic practitioner prescribes the course of treatment that will best heal the imbalance and re-establish a normal sleep cycle.&nbsp; In short, Holistic practitioners treat the whole person &ndash; physically, emotionally, and mentally.&nbsp;</p>\r\n</div>\r\n','<p>\r\n	Getting a good night&rsquo;s sleep is no simple task for 1 in 3 adults. Fortunately, there are many conventional and holistic treatments to overcome this debilitating problem. It&rsquo;s during deep sleep that the restorative functions of the body take place supporting your overall state of health and wellbeing.</p>\r\n','test\r\ntest\r\ntest\r\n','<p>\r\n	helloooooo</p>\r\n');
INSERT INTO conditions VALUES (19,'testing visits','','','<p>\r\n	test</p>\r\n','',1,1,'testing-visits','2010-02-04 12:23:22','2010-02-04 12:24:08',12,12,'','',NULL,NULL);
INSERT INTO conditions VALUES (14,'Test Condition','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: calibri, sans-serif; font-size: 15px; \">There has been great debate about how to best define chronic fatigue syndrome (CFS). It was once thought that this condition was the result of a viral infection. However, this is not the case. The disease typically affects working-age adults and is often triggered by a flu-like episode<sup>3</sup>. Other conditions that seem to trigger the development of CFS include viral infections, transient traumatic conditions, stress, and toxins.</span></p>\r\n','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: calibri, sans-serif; font-size: 15px; \">The defining characteristic of the disorder is a severe and lasting fatigue that is not linked to an existing medical condition and has lasted six months or longer. The severity of CFS varies from patient to patient. Some people maintain a fairly active lifestyle. For the most symptomatic patients, however, CFS significantly limits work, school, and family activities<sup>4</sup>. The type of fatigue characteristic of this condition is not the kind of fatigue we experience after a particularly busy day, a sleepless night, or a single stressful event. It&rsquo;s a severe, incapacitating fatigue that isn&rsquo;t improved by bed rest and that may be worsened by physical or mental activity. It&rsquo;s an all-encompassing fatigue that results in a dramatic decline in both activity level and stamina.</span></p>\r\n<div style=\"font-family: Arial, Verdana, sans-serif; font-size: 12px; color: rgb(34, 34, 34); background-color: rgb(255, 255, 255); \">\r\n	<p class=\"Default\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><o:p>&nbsp;</o:p></span></font></span></span></p>\r\n	<p class=\"Default\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \">CFS often follows a cyclical course, alternating between periods of illness and relative well-being. Some patients experienced partial or complete remission of symptoms during the course of the illness, but symptoms often recur. This pattern of remission and relapse makes chronic fatigue especially hard for patients to manage. Patients who are in remission may be tempted to overdo activities when they&rsquo;re feeling better, which can actually cause a relapse<sup>5</sup>. Therefore, it is of the utmost importance to identify a balanced rhythm in your life&mdash;one that includes equal measures of rest, exertion, relaxation, work, good nutrition, detoxification, meditation, and community.</span></font></span></span></p>\r\n</div>\r\n','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: calibri, sans-serif; font-size: 15px; \"><em>Chronic fatigue syndrome</em>&nbsp;is a complicated disorder characterized by extreme fatigue that may worsen with physical or mental activity and that does not improve with rest<sup>1</sup>. The best recovery rates have been experienced by people who used both&nbsp;<em>traditional&nbsp;</em><span>and</span><em>&nbsp;alternative healing modalities<sup>2</sup>.</em></span></p>\r\n','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: calibri, sans-serif; font-size: medium; \">The integrative approach also includes a full spectrum of lab tests&mdash;traditional and alternative. In particular, an integrative physician will order studies of saliva and urine to assess your adrenal glands, as well as the levels of neurotransmitters in your system. Holistic treatment is likely to focus on replenishing your adrenal glands, also known as the &ldquo;fight or flight glands.&rdquo; Prolonged periods of elevated levels of adrenaline and cortisol (the fight or flight hormones) erode your immune system. In addition, an integrative physician or practitioner will screen for oxidative stress.</span></p>\r\n<div style=\"font-family: Arial, Verdana, sans-serif; font-size: 12px; color: rgb(34, 34, 34); background-color: rgb(255, 255, 255); \">\r\n	<p class=\"MsoNormal\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \"><span style=\"font-family: calibri, sans-serif; \"><font size=\"3\"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><o:p>&nbsp;</o:p></span></font></font></span></span></p>\r\n	<p class=\"MsoNormal\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \"><span style=\"font-family: calibri, sans-serif; \"><font size=\"3\"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \">Your practitioner may prescribe neurotransmitters&mdash;chemicals in your body that not only regulate how you feel and think, but how your body operates. Very often, through supplementation, balanced nutrition, exercise, and rest, the holistic doctor or practitioner is able to move you towards remission of the disabling symptoms of chronic fatigue syndrome.</span></font></font></span></span></p>\r\n</div>\r\n',1,1,'test','2010-01-12 01:46:42','2010-02-13 12:21:02',5,12,'<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: calibri, sans-serif; font-size: 15px; \">The standard medical approach for treating chronic fatigue syndrome is to first diagnose the condition and to rule out any bacterial or viral infection. Standard lab tests and a complete physical may be done. Typically, a medical doctor will then implement stopgap measures to help patients manage their symptoms<span class=\"MsoFootnoteReference\">7</span>. For example, a sleep medication may be prescribed to help with the broken sleep patterns experienced by many patients with chronic fatigue. Through lab studies and a physical exam, the standard medical approach will also rule-out any underlying conditions.</span></p>\r\n<div style=\"font-family: Arial, Verdana, sans-serif; font-size: 12px; color: rgb(34, 34, 34); background-color: rgb(255, 255, 255); \">\r\n	<p class=\"Default\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \">Where once there was doubt, there is now widespread acceptance of the syndrome as a medical disorder. Increasingly, medical doctors understand the value of energy-based disciplines such as yoga and tai chi, and prescribe alternative therapies for treatment of symptoms that frequently accompany chronic fatigue syndrome. If an infection is present, a medical doctor may prescribe antiviral or antibacterial medications.</span></font></span></font></span></span></span></p>\r\n	<p class=\"Default\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		&nbsp;</p>\r\n	<p class=\"Default\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><strong><u>REFERENCES</u></strong>:</span></font></span></font></span></span></font></span></font></span></span></p>\r\n	<p class=\"Default\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><o:p>&nbsp;</o:p></span></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></span></p>\r\n	<p class=\"MsoFootnoteText\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><font size=\"2\"><sup><span style=\"font-family: calibri, sans-serif; \">1</span></sup><span style=\"font-family: calibri, sans-serif; \">Mayo Clinic.com, &ldquo;Chronic Fatigue Syndrome,&rdquo; at http://www.mayoclinic.com/health/chronic-fatigue-syndrome/DS00395.<o:p></o:p></span></font></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></span></p>\r\n	<p class=\"MsoFootnoteText\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><font size=\"2\"><sup><span style=\"font-family: calibri, sans-serif; \">2</span></sup><span style=\"font-family: calibri, sans-serif; \">Chronic Fatigue.org, &ldquo;The Web Page for Chronic Fatigue Sufferers,&rdquo; at http://www.chronicfatigue.org.<o:p></o:p></span></font></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></span></p>\r\n	<p class=\"MsoFootnoteText\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><font size=\"2\"><sup><span style=\"font-family: calibri, sans-serif; \">3</span></sup><span style=\"font-family: calibri, sans-serif; \">PsychCentral.com, Rick Nauert, Ph.D.,&nbsp;<i>Emerging Theory on Chronic Fatigue</i>, at http://psychcentral.com/news/2008/06/25/new-theory-on-chronic-fatigue/2501.html.<o:p></o:p></span></font></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></span></p>\r\n	<p class=\"MsoFootnoteText\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><font size=\"2\"><sup><span style=\"font-family: calibri, sans-serif; \">4</span></sup><span style=\"font-family: calibri, sans-serif; \">Centers for Disease Control (CDC), &ldquo;Symptoms of CFS,&rdquo; at http://www.CDC.gov/cfs/cfssymptoms.htm#primary.<o:p></o:p></span></font></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></span></p>\r\n	<p class=\"MsoFootnoteText\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><font size=\"2\"><sup><span style=\"font-family: calibri, sans-serif; \">5</span></sup><span style=\"font-family: calibri, sans-serif; \">CDC, &ldquo;Symptoms,&rdquo; http://www.CDC.gov//cfs/cfssymptoms.htm#primary.<o:p></o:p></span></font></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></span></p>\r\n	<p class=\"MsoNormal\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><sup><span style=\"font-family: calibri, sans-serif; font-size: 10pt; \">6</span></sup><span style=\"font-family: calibri, sans-serif; font-size: 10pt; \">CDC, &ldquo;Symptoms,&rdquo;&nbsp;<a href=\"http://www.CDC.gov/cfs/cfssymptoms.htm#primary\"><font color=\"#0000ff\">http://www.CDC.gov//cfs/cfssymptoms.htm#primary</font></a>.<o:p></o:p></span></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></span></p>\r\n	<p class=\"MsoNormal\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><span style=\"font-family: calibri, sans-serif; \"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><font size=\"2\"><span style=\"font-family: calibri, sans-serif; font-size: 11.5pt; \"><sup><span style=\"font-family: calibri, sans-serif; font-size: 10pt; \">7</span></sup><span style=\"font-family: calibri, sans-serif; font-size: 10pt; \">Fibromyalgia and Fatigue Centers, &ldquo;Multi-faceted Treatment Approach,&rdquo; at http://www.five growfibroandfatigue.com/clinical.php#6.</span></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></span></p>\r\n</div>\r\n','<p>\r\n	test condition symptoms</p>\r\n','test left\r\ntest\r\ntest\r\ntest','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: calibri, sans-serif; font-size: medium; \">The integrative approach also includes a full spectrum of lab tests&mdash;traditional and alternative. In particular, an integrative physician will order studies of saliva and urine to assess your adrenal glands, as well as the levels of neurotransmitters in your system. Holistic treatment is likely to focus on replenishing your adrenal glands, also known as the &ldquo;fight or flight glands.&rdquo; Prolonged periods of elevated levels of adrenaline and cortisol (the fight or flight hormones) erode your immune system. In addition, an integrative physician or practitioner will screen for oxidative stress.</span></p>\r\n<div style=\"font-family: Arial, Verdana, sans-serif; font-size: 12px; color: rgb(34, 34, 34); background-color: rgb(255, 255, 255); \">\r\n	<p class=\"MsoNormal\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \"><span style=\"font-family: calibri, sans-serif; \"><font size=\"3\"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \"><o:p>&nbsp;</o:p></span></font></font></span></span></p>\r\n	<p class=\"MsoNormal\" style=\"margin-top: 0in; margin-right: 0in; margin-bottom: 0pt; margin-left: 0in; \">\r\n		<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \"><span style=\"font-family: calibri, sans-serif; \"><font size=\"3\"><font color=\"#000000\"><span style=\"font-family: calibri, sans-serif; \">Your practitioner may prescribe neurotransmitters&mdash;chemicals in your body that not only regulate how you feel and think, but how your body operates. Very often, through supplementation, balanced nutrition, exercise, and rest, the holistic doctor or practitioner is able to move you towards remission of the disabling symptoms of chronic fatigue syndrome.</span></font></font></span></span></p>\r\n</div>\r\n');
INSERT INTO conditions VALUES (16,'Cronic head ache','<p>\r\n	Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</p>\r\n','<p>\r\n	Nam consequat lacinia turpis ornare sollicitudin. Nam sed nibh rhoncus enim consectetur luctus. Pellentesque sed tristique urna. Fusce euismod pellentesque lorem, porta congue magna condimentum eu.</p>\r\n<p>\r\n	Aliquam eget metus sed nunc tincidunt dictum. Aliquam suscipit sagittis ipsum, eu ornare enim imperdiet eu. Pellentesque libero velit, pulvinar in eleifend a, commodo a magna. Suspendisse ac velit orci, eu elementum nisl. Donec malesuada magna eu dui convallis blandit. Mauris tincidunt accumsan fringilla. Suspendisse potenti. Donec commodo urna vel arcu lobortis luctus. Aliquam erat volutpat. Donec nec tellus quam, vitae mollis erat. Suspendisse adipiscing metus et mauris imperdiet vel porta tortor semper. Donec sit amet hendrerit eros. Nam dui ligula, sagittis at bibendum in, pulvinar sed orci. Sed vestibulum dui at neque blandit a fringilla erat congue.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lacus libero, vulputate nec tincidunt eu, pulvinar id purus. Mauris non massa vitae quam auctor suscipit id vitae augue. Integer vehicula faucibus porttitor. Nunc ut porttitor purus. Mauris euismod odio id risus feugiat nec adipiscing nisi lobortis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean cursus arcu id nisi placerat sodales. Nulla a arcu vitae nisl auctor posuere at ut justo. Curabitur ligula metus, sollicitudin eget feugiat vel, fermentum quis metus. Nulla in libero eu velit cursus vulputate sed vel magna. Suspendisse feugiat neque sed sapien fringilla eleifend quis vitae urna. Vivamus quam nibh, vehicula eu auctor nec, commodo ultricies erat.</p>\r\n','<p>\r\n	Nunc risus est, ultricies non interdum vel, rhoncus a diam. Aliquam tincidunt, risus vitae eleifend tempus, leo velit iaculis arcu, nec sollicitudin dolor orci in metus. Quisque mollis gravida faucibus. Donec iaculis sem imperdiet augue rutrum volutpat. Integer pellentesque nunc ut erat suscipit facilisis sed sit amet nunc. In hac habitasse platea dictumst. Nulla at sem lacus. Mauris varius risus in dui pretium dapibus. Integer elementum sollicitudin augue. Aenean rutrum eros in orci lacinia sit amet ullamcorper mauris viverra. Curabitur congue pellentesque libero, at lobortis orci malesuada vitae. Donec sodales porttitor arcu, non interdum sem dignissim eget. Morbi egestas dolor eget augue tempor ut faucibus risus cursus.</p>\r\n',1,1,'cronic-head-ache','2010-01-26 13:29:32','2010-01-26 13:29:32',27,27,'<p>\r\n	Ut eu posuere neque. Aliquam blandit lectus id augue pharetra non adipiscing ipsum auctor. Praesent fermentum nulla sed neque luctus quis molestie lorem laoreet. Mauris sed neque justo, porta ullamcorper tortor. Nunc lacinia condimentum purus, sed tempus ante ultricies in. Etiam tortor ante, feugiat nec malesuada a, tempus non arcu. Vestibulum lorem orci, ullamcorper mattis posuere in, malesuada at mi. Etiam imperdiet cursus congue. Sed enim eros, ultricies eu convallis et, venenatis eu enim. Proin quis dolor id justo aliquam consequat interdum vel augue. Nulla egestas ipsum vel enim tempor blandit. Fusce vel urna et lacus tincidunt accumsan a non justo. Quisque tincidunt varius turpis, ac congue erat convallis ac. Suspendisse pretium pretium dignissim. Vestibulum et arcu ut lacus posuere eleifend. Ut sit amet erat arcu.</p>\r\n','<p>\r\n	Pellentesque cursus commodo eros ac euismod. Nullam ac ligula neque. Suspendisse vitae ligula mi. Mauris ornare porttitor pellentesque. Morbi at eros leo. Integer eu turpis non nulla mattis luctus. Praesent condimentum leo at augue hendrerit tincidunt. Phasellus at odio et purus scelerisque luctus et sit amet mi. Vivamus sed diam sed odio ultrices iaculis. Fusce ullamcorper imperdiet mi nec sollicitudin. Quisque vulputate fringilla convallis.</p>\r\n',NULL,NULL);
INSERT INTO conditions VALUES (17,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut vulputate lorem. Nulla aliquam lectus vitae lacus commodo in int','<p>\r\n	<span style=\"background-color: #ffff00\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut vulputate lorem. Nulla aliquam lectus vitae lacus commodo in interdum orci </span>luctus. Quisque adipiscing felis eu mauris pharetra fermentum. Morbi lacus felis, pulvinar ac laoreet eget, feugiat ac elit. Donec cursus leo at nisl feugiat convallis. Donec ut mi in urna tempor congue bibendum at orci. Vivamus condimentum massa nulla, id consequat erat. Vivamus sed est urna. Sed et elit sit amet orci suscipit sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vestibulum odio imperdiet enim adipiscing rutrum. Phasellus rhoncus porta tellus, sed euismod sapien placerat sit amet. Curabitur quam quam, hendrerit quis viverra quis, consectetur vitae nisi.&nbsp;</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut vulputate lorem. Nulla aliquam lectus vitae lacus commodo in interdum orci luctus. Quisque adipiscing felis eu mauris pharetra fermentum. Morbi lacus felis, pulvinar ac laoreet eget, feugiat ac elit. Donec cursus leo at nisl feugiat convallis. Donec ut mi in urna tempor congue bibendum at orci. Vivamus condimentum massa nulla, id consequat erat. Vivamus sed est urna. Sed et elit sit amet orci suscipit sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vestibulum odio imperdiet enim adipiscing rutrum. Phasellus rhoncus porta tellus, sed euismod sapien placerat sit amet. Curabitur quam quam, hendrerit quis viverra quis, consectetur vitae nisi.</p>\r\n<p>\r\n	Pellentesque suscipit, arcu vitae aliquam ultrices, enim ligula vehicula nulla, at sodales felis risus id felis. Ut semper dapibus lectus, in euismod nibh laoreet eget. Ut tempus, purus in pretium semper, metus sem mollis eros, vitae porta libero libero iaculis quam. Integer viverra rutrum viverra. Duis sapien odio, laoreet ut mattis at, molestie nec elit. Ut in nisi sit amet est porta sagittis. Pellentesque sodales pulvinar sem sit amet dapibus. Vivamus consequat lacus sed odio rhoncus interdum. Nulla facilisi. Cras volutpat egestas massa eu volutpat. Phasellus eu tortor ac sapien aliquam lacinia. Sed rhoncus iaculis tristique. Praesent fermentum ligula sed velit lobortis eleifend. Quisque est nisl, dapibus eget semper ut, viverra in neque.</p>\r\n<p>\r\n	Etiam bibendum facilisis tincidunt. Nulla facilisi. Vestibulum laoreet diam ipsum, sed imperdiet tellus. Sed sed est eget nisi tincidunt imperdiet sit amet sit amet neque. Ut vehicula sem vitae nisi suscipit ut ornare ligula vehicula. Ut odio sem, aliquam at mollis ac, blandit in magna. Vivamus accumsan magna metus. Aenean ac sollicitudin risus. Integer mollis fermentum convallis. Praesent nulla nulla, sollicitudin posuere aliquam tempus, laoreet sit amet leo. Morbi ullamcorper fringilla elit in pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce dictum libero tincidunt arcu suscipit vel euismod tellus auctor. Nulla sed enim in lacus rutrum laoreet et elementum ipsum. Vivamus eget cursus lorem. Sed purus turpis, dignissim id interdum sodales, dictum et mauris. Fusce vestibulum sagittis varius. Maecenas consectetur, metus sed sagittis vehicula, diam sapien consequat erat, et porta dui lorem vitae dui.</p>\r\n<p>\r\n	Vivamus in urna suscipit odio elementum luctus ac nec ante. Donec rutrum euismod hendrerit. Morbi aliquet fringilla fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam nec erat a risus dictum pharetra ut sit amet arcu. Sed in lacus libero. Integer non diam nibh. Integer aliquet luctus iaculis. Fusce tellus quam, cursus feugiat pharetra ut, lobortis ut diam. Proin in ornare lorem. Sed faucibus elit pretium metus laoreet varius. Nullam lacinia consectetur est quis elementum. Vivamus hendrerit velit vel velit molestie non blandit justo imperdiet.</p>\r\n<p>\r\n	Morbi hendrerit massa ut velit condimentum accumsan. Maecenas nec magna ut nulla lobortis hendrerit a at diam. Mauris varius auctor nisi sit amet volutpat. Morbi nec ligula urna, sit amet fermentum libero. Donec tincidunt tincidunt euismod. Vestibulum ut eros mi, id lacinia felis. Praesent varius sagittis felis nec sollicitudin. In mollis dictum metus vitae accumsan. Nunc non urna vel ligula mollis semper eget at nisi. Mauris nunc nisl, tincidunt ac vulputate nec, cursus in est. Donec ut elit id metus consectetur consequat non in tortor. Integer ut justo magna. Curabitur scelerisque iaculis ante et ullamcorper. Phasellus sed mauris a ante commodo varius nec eget lacus. Phasellus egestas magna id lectus adipiscing placerat.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut vulputate lorem. Nulla aliquam lectus vitae lacus commodo in interdum orci luctus. Quisque adipiscing felis eu mauris pharetra fermentum. Morbi lacus felis, pulvinar ac laoreet eget, feugiat ac elit. Donec cursus leo at nisl feugiat convallis. Donec ut mi in urna tempor congue bibendum at orci. Vivamus condimentum massa nulla, id consequat erat. Vivamus sed est urna. Sed et elit sit amet orci suscipit sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vestibulum odio imperdiet enim adipiscing rutrum. Phasellus rhoncus porta tellus, sed euismod sapien placerat sit amet. Curabitur quam quam, hendrerit quis viverra quis, consectetur vitae nisi.</p>\r\n<p>\r\n	Pellentesque suscipit, arcu vitae aliquam ultrices, enim ligula vehicula nulla, at sodales felis risus id felis. Ut semper dapibus lectus, in euismod nibh laoreet eget. Ut tempus, purus in pretium semper, metus sem mollis eros, vitae porta libero libero iaculis quam. Integer viverra rutrum viverra. Duis sapien odio, laoreet ut mattis at, molestie nec elit. Ut in nisi sit amet est porta sagittis. Pellentesque sodales pulvinar sem sit amet dapibus. Vivamus consequat lacus sed odio rhoncus interdum. Nulla facilisi. Cras volutpat egestas massa eu volutpat. Phasellus eu tortor ac sapien aliquam lacinia. Sed rhoncus iaculis tristique. Praesent fermentum ligula sed velit lobortis eleifend. Quisque est nisl, dapibus eget semper ut, viverra in neque.</p>\r\n<p>\r\n	Etiam bibendum facilisis tincidunt. Nulla facilisi. Vestibulum laoreet diam ipsum, sed imperdiet tellus. Sed sed est eget nisi tincidunt imperdiet sit amet sit amet neque. Ut vehicula sem vitae nisi suscipit ut ornare ligula vehicula. Ut odio sem, aliquam at mollis ac, blandit in magna. Vivamus accumsan magna metus. Aenean ac sollicitudin risus. Integer mollis fermentum convallis. Praesent nulla nulla, sollicitudin posuere aliquam tempus, laoreet sit amet leo. Morbi ullamcorper fringilla elit in pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce dictum libero tincidunt arcu suscipit vel euismod tellus auctor. Nulla sed enim in lacus rutrum laoreet et elementum ipsum. Vivamus eget cursus lorem. Sed purus turpis, dignissim id interdum sodales, dictum et mauris. Fusce vestibulum sagittis varius. Maecenas consectetur, metus sed sagittis vehicula, diam sapien consequat erat, et porta dui lorem vitae dui.</p>\r\n<p>\r\n	Vivamus in urna suscipit odio elementum luctus ac nec ante. Donec rutrum euismod hendrerit. Morbi aliquet fringilla fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam nec erat a risus dictum pharetra ut sit amet arcu. Sed in lacus libero. Integer non diam nibh. Integer aliquet luctus iaculis. Fusce tellus quam, cursus feugiat pharetra ut, lobortis ut diam. Proin in ornare lorem. Sed faucibus elit pretium metus laoreet varius. Nullam lacinia consectetur est quis elementum. Vivamus hendrerit velit vel velit molestie non blandit justo imperdiet.</p>\r\n<p>\r\n	Morbi hendrerit massa ut velit condimentum accumsan. Maecenas nec magna ut nulla lobortis hendrerit a at diam. Mauris varius auctor nisi sit amet volutpat. Morbi nec ligula urna, sit amet fermentum libero. Donec tincidunt tincidunt euismod. Vestibulum ut eros mi, id lacinia felis. Praesent varius sagittis felis nec sollicitudin. In mollis dictum metus vitae accumsan. Nunc non urna vel ligula mollis semper eget at nisi. Mauris nunc nisl, tincidunt ac vulputate nec, cursus in est. Donec ut elit id metus consectetur consequat non in tortor. Integer ut justo magna. Curabitur scelerisque iaculis ante et ullamcorper. Phasellus sed mauris a ante commodo varius nec eget lacus. Phasellus egestas magna id lectus adipiscing placerat.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut vulputate lorem. Nulla aliquam lectus vitae lacus commodo in interdum orci luctus. Quisque adipiscing felis eu mauris pharetra fermentum. Morbi lacus felis, pulvinar ac laoreet eget, feugiat ac elit. Donec cursus leo at nisl feugiat convallis. Donec ut mi in urna tempor congue bibendum at orci. Vivamus condimentum massa nulla, id consequat erat. Vivamus sed est urna. Sed et elit sit amet orci suscipit sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vestibulum odio imperdiet enim adipiscing rutrum. Phasellus rhoncus porta tellus, sed euismod sapien placerat sit amet. Curabitur quam quam, hendrerit quis viverra quis, consectetur vitae nisi.</p>\r\n<p>\r\n	Pellentesque suscipit, arcu vitae aliquam ultrices, enim ligula vehicula nulla, at sodales felis risus id felis. Ut semper dapibus lectus, in euismod nibh laoreet eget. Ut tempus, purus in pretium semper, metus sem mollis eros, vitae porta libero libero iaculis quam. Integer viverra rutrum viverra. Duis sapien odio, laoreet ut mattis at, molestie nec elit. Ut in nisi sit amet est porta sagittis. Pellentesque sodales pulvinar sem sit amet dapibus. Vivamus consequat lacus sed odio rhoncus interdum. Nulla facilisi. Cras volutpat egestas massa eu volutpat. Phasellus eu tortor ac sapien aliquam lacinia. Sed rhoncus iaculis tristique. Praesent fermentum ligula sed velit lobortis eleifend. Quisque est nisl, dapibus eget semper ut, viverra in neque.</p>\r\n<p>\r\n	Etiam bibendum facilisis tincidunt. Nulla facilisi. Vestibulum laoreet diam ipsum, sed imperdiet tellus. Sed sed est eget nisi tincidunt imperdiet sit amet sit amet neque. Ut vehicula sem vitae nisi suscipit ut ornare ligula vehicula. Ut odio sem, aliquam at mollis ac, blandit in magna. Vivamus accumsan magna metus. Aenean ac sollicitudin risus. Integer mollis fermentum convallis. Praesent nulla nulla, sollicitudin posuere aliquam tempus, laoreet sit amet leo. Morbi ullamcorper fringilla elit in pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce dictum libero tincidunt arcu suscipit vel euismod tellus auctor. Nulla sed enim in lacus rutrum laoreet et elementum ipsum. Vivamus eget cursus lorem. Sed purus turpis, dignissim id interdum sodales, dictum et mauris. Fusce vestibulum sagittis varius. Maecenas consectetur, metus sed sagittis vehicula, diam sapien consequat erat, et porta dui lorem vitae dui.</p>\r\n<p>\r\n	Vivamus in urna suscipit odio elementum luctus ac nec ante. Donec rutrum euismod hendrerit. Morbi aliquet fringilla fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam nec erat a risus dictum pharetra ut sit amet arcu. Sed in lacus libero. Integer non diam nibh. Integer aliquet luctus iaculis. Fusce tellus quam, cursus feugiat pharetra ut, lobortis ut diam. Proin in ornare lorem. Sed faucibus elit pretium metus laoreet varius. Nullam lacinia consectetur est quis elementum. Vivamus hendrerit velit vel velit molestie non blandit justo imperdiet.</p>\r\n<p>\r\n	Morbi hendrerit massa ut velit condimentum accumsan. Maecenas nec magna ut nulla lobortis hendrerit a at diam. Mauris varius auctor nisi sit amet volutpat. Morbi nec ligula urna, sit amet fermentum libero. Donec tincidunt tincidunt euismod. Vestibulum ut eros mi, id lacinia felis. Praesent varius sagittis felis nec sollicitudin. In mollis dictum metus vitae accumsan. Nunc non urna vel ligula mollis semper eget at nisi. Mauris nunc nisl, tincidunt ac vulputate nec, cursus in est. Donec ut elit id metus consectetur consequat non in tortor. Integer ut justo magna. Curabitur scelerisque iaculis ante et ullamcorper. Phasellus sed mauris a ante commodo varius nec eget lacus. Phasellus egestas magna id lectus adipiscing placerat.</p>\r\n',6,1,'canyousee','2010-01-26 15:25:31','2010-01-26 15:37:55',1,1,'<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut vulputate lorem. Nulla aliquam lectus vitae lacus commodo in interdum orci luctus. Quisque adipiscing felis eu mauris pharetra fermentum. Morbi lacus felis, pulvinar ac laoreet eget, feugiat ac elit. Donec cursus leo at nisl feugiat convallis. Donec ut mi in urna tempor congue bibendum at orci. Vivamus condimentum massa nulla, id consequat erat. Vivamus sed est urna. Sed et elit sit amet orci suscipit sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vestibulum odio imperdiet enim adipiscing rutrum. Phasellus rhoncus porta tellus, sed euismod sapien placerat sit amet. Curabitur quam quam, hendrerit quis viverra quis, consectetur vitae nisi.</p>\r\n<p>\r\n	Pellentesque suscipit, arcu vitae aliquam ultrices, enim ligula vehicula nulla, at sodales felis risus id felis. Ut semper dapibus lectus, in euismod nibh laoreet eget. Ut tempus, purus in pretium semper, metus sem mollis eros, vitae porta libero libero iaculis quam. Integer viverra rutrum viverra. Duis sapien odio, laoreet ut mattis at, molestie nec elit. Ut in nisi sit amet est porta sagittis. Pellentesque sodales pulvinar sem sit amet dapibus. Vivamus consequat lacus sed odio rhoncus interdum. Nulla facilisi. Cras volutpat egestas massa eu volutpat. Phasellus eu tortor ac sapien aliquam lacinia. Sed rhoncus iaculis tristique. Praesent fermentum ligula sed velit lobortis eleifend. Quisque est nisl, dapibus eget semper ut, viverra in neque.</p>\r\n<p>\r\n	Etiam bibendum facilisis tincidunt. Nulla facilisi. Vestibulum laoreet diam ipsum, sed imperdiet tellus. Sed sed est eget nisi tincidunt imperdiet sit amet sit amet neque. Ut vehicula sem vitae nisi suscipit ut ornare ligula vehicula. Ut odio sem, aliquam at mollis ac, blandit in magna. Vivamus accumsan magna metus. Aenean ac sollicitudin risus. Integer mollis fermentum convallis. Praesent nulla nulla, sollicitudin posuere aliquam tempus, laoreet sit amet leo. Morbi ullamcorper fringilla elit in pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce dictum libero tincidunt arcu suscipit vel euismod tellus auctor. Nulla sed enim in lacus rutrum laoreet et elementum ipsum. Vivamus eget cursus lorem. Sed purus turpis, dignissim id interdum sodales, dictum et mauris. Fusce vestibulum sagittis varius. Maecenas consectetur, metus sed sagittis vehicula, diam sapien consequat erat, et porta dui lorem vitae dui.</p>\r\n<p>\r\n	Vivamus in urna suscipit odio elementum luctus ac nec ante. Donec rutrum euismod hendrerit. Morbi aliquet fringilla fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam nec erat a risus dictum pharetra ut sit amet arcu. Sed in lacus libero. Integer non diam nibh. Integer aliquet luctus iaculis. Fusce tellus quam, cursus feugiat pharetra ut, lobortis ut diam. Proin in ornare lorem. Sed faucibus elit pretium metus laoreet varius. Nullam lacinia consectetur est quis elementum. Vivamus hendrerit velit vel velit molestie non blandit justo imperdiet.</p>\r\n<p>\r\n	Morbi hendrerit massa ut velit condimentum accumsan. Maecenas nec magna ut nulla lobortis hendrerit a at diam. Mauris varius auctor nisi sit amet volutpat. Morbi nec ligula urna, sit amet fermentum libero. Donec tincidunt tincidunt euismod. Vestibulum ut eros mi, id lacinia felis. Praesent varius sagittis felis nec sollicitudin. In mollis dictum metus vitae accumsan. Nunc non urna vel ligula mollis semper eget at nisi. Mauris nunc nisl, tincidunt ac vulputate nec, cursus in est. Donec ut elit id metus consectetur consequat non in tortor. Integer ut justo magna. Curabitur scelerisque iaculis ante et ullamcorper. Phasellus sed mauris a ante commodo varius nec eget lacus. Phasellus egestas magna id lectus adipiscing placerat.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut vulputate lorem. Nulla aliquam lectus vitae lacus commodo in interdum orci luctus. Quisque adipiscing felis eu mauris pharetra fermentum. Morbi lacus felis, pulvinar ac laoreet eget, feugiat ac elit. Donec cursus leo at nisl feugiat convallis. Donec ut mi in urna tempor congue bibendum at orci. Vivamus condimentum massa nulla, id consequat erat. Vivamus sed est urna. Sed et elit sit amet orci suscipit sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vestibulum odio imperdiet enim adipiscing rutrum. Phasellus rhoncus porta tellus, sed euismod sapien placerat sit amet. Curabitur quam quam, hendrerit quis viverra quis, consectetur vitae nisi.</p>\r\n<p>\r\n	Pellentesque suscipit, arcu vitae aliquam ultrices, enim ligula vehicula nulla, at sodales felis risus id felis. Ut semper dapibus lectus, in euismod nibh laoreet eget. Ut tempus, purus in pretium semper, metus sem mollis eros, vitae porta libero libero iaculis quam. Integer viverra rutrum viverra. Duis sapien odio, laoreet ut mattis at, molestie nec elit. Ut in nisi sit amet est porta sagittis. Pellentesque sodales pulvinar sem sit amet dapibus. Vivamus consequat lacus sed odio rhoncus interdum. Nulla facilisi. Cras volutpat egestas massa eu volutpat. Phasellus eu tortor ac sapien aliquam lacinia. Sed rhoncus iaculis tristique. Praesent fermentum ligula sed velit lobortis eleifend. Quisque est nisl, dapibus eget semper ut, viverra in neque.</p>\r\n<p>\r\n	Etiam bibendum facilisis tincidunt. Nulla facilisi. Vestibulum laoreet diam ipsum, sed imperdiet tellus. Sed sed est eget nisi tincidunt imperdiet sit amet sit amet neque. Ut vehicula sem vitae nisi suscipit ut ornare ligula vehicula. Ut odio sem, aliquam at mollis ac, blandit in magna. Vivamus accumsan magna metus. Aenean ac sollicitudin risus. Integer mollis fermentum convallis. Praesent nulla nulla, sollicitudin posuere aliquam tempus, laoreet sit amet leo. Morbi ullamcorper fringilla elit in pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce dictum libero tincidunt arcu suscipit vel euismod tellus auctor. Nulla sed enim in lacus rutrum laoreet et elementum ipsum. Vivamus eget cursus lorem. Sed purus turpis, dignissim id interdum sodales, dictum et mauris. Fusce vestibulum sagittis varius. Maecenas consectetur, metus sed sagittis vehicula, diam sapien consequat erat, et porta dui lorem vitae dui.</p>\r\n<p>\r\n	Vivamus in urna suscipit odio elementum luctus ac nec ante. Donec rutrum euismod hendrerit. Morbi aliquet fringilla fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam nec erat a risus dictum pharetra ut sit amet arcu. Sed in lacus libero. Integer non diam nibh. Integer aliquet luctus iaculis. Fusce tellus quam, cursus feugiat pharetra ut, lobortis ut diam. Proin in ornare lorem. Sed faucibus elit pretium metus laoreet varius. Nullam lacinia consectetur est quis elementum. Vivamus hendrerit velit vel velit molestie non blandit justo imperdiet.</p>\r\n<p>\r\n	Morbi hendrerit massa ut velit condimentum accumsan. Maecenas nec magna ut nulla lobortis hendrerit a at diam. Mauris varius auctor nisi sit amet volutpat. Morbi nec ligula urna, sit amet fermentum libero. Donec tincidunt tincidunt euismod. Vestibulum ut eros mi, id lacinia felis. Praesent varius sagittis felis nec sollicitudin. In mollis dictum metus vitae accumsan. Nunc non urna vel ligula mollis semper eget at nisi. Mauris nunc nisl, tincidunt ac vulputate nec, cursus in est. Donec ut elit id metus consectetur consequat non in tortor. Integer ut justo magna. Curabitur scelerisque iaculis ante et ullamcorper. Phasellus sed mauris a ante commodo varius nec eget lacus. Phasellus egestas magna id lectus adipiscing placerat.</p>\r\n',NULL,NULL);
INSERT INTO conditions VALUES (18,'unpublished condition','<p>\r\n	test</p>\r\n','<p>\r\n	test</p>\r\n','<p>\r\n	test</p>\r\n','<p>\r\n	test</p>\r\n',1,0,'unpublished','2010-01-30 16:22:57','2010-02-04 12:14:27',5,12,'<p>\r\n	test</p>\r\n','<p>\r\n	test</p>\r\n',NULL,NULL);
INSERT INTO conditions VALUES (20,'testing conditions visits','','','','',1,1,'testing-conditions-visits','2010-02-04 14:20:36','2010-02-04 14:20:36',12,12,'','',NULL,NULL);
INSERT INTO conditions VALUES (21,'Testing new fields','<p>\r\n	A traditional (western) medical doctor typically explores your medical history and tries to get rid of your symptoms, very often through the use of pills or&nbsp; other pharmaceutical products.&nbsp; Holistic practioners will factor your medical history into a greater equation &ndash; one that looks for the root cause of the imbalance underlying your insomnia.&nbsp; They consider the various components of your life such as &ndash; your medical history, life circumstance, general nutrition, exercise, level of stress, etc.&nbsp; After doing the math so to speak &ndash; assessing the impact of all these elements on your health, general well-being and spirit, a holistic practitioner prescribes the course of treatment that will best heal the imbalance and re-establish a normal sleep cycle.&nbsp; In short, Holistic practitioners treat the whole person &ndash; physically, emotionally, and mentally.&nbsp;</p>\r\n','<p>\r\n	A traditional (western) medical doctor typically explores your medical history and tries to get rid of your symptoms, very often through the use of pills or&nbsp; other pharmaceutical products.&nbsp; Holistic practioners will factor your medical history into a greater equation &ndash; one that looks for the root cause of the imbalance underlying your insomnia.&nbsp; They consider the various components of your life such as &ndash; your medical history, life circumstance, general nutrition, exercise, level of stress, etc.&nbsp; After doing the math so to speak &ndash; assessing the impact of all these elements on your health, general well-being and spirit, a holistic practitioner prescribes the course of treatment that will best heal the imbalance and re-establish a normal sleep cycle.&nbsp; In short, Holistic practitioners treat the whole person &ndash; physically, emotionally, and mentally.&nbsp;</p>\r\n','<p>\r\n	A traditional (western) medical doctor typically explores your medical history and tries to get rid of your symptoms, very often through the use of pills or&nbsp; other pharmaceutical products.&nbsp; Holistic practioners will factor your medical history into a greater equation &ndash; one that looks for the root cause of the imbalance underlying your insomnia.&nbsp; They consider the various components of your life such as &ndash; your medical history, life circumstance, general nutrition, exercise, level of stress, etc.&nbsp; After doing the math so to speak &ndash; assessing the impact of all these elements on your health, general well-being and spirit, a holistic practitioner prescribes the course of treatment that will best heal the imbalance and re-establish a normal sleep cycle.&nbsp; In short, Holistic practitioners treat the whole person &ndash; physically, emotionally, and mentally.&nbsp;</p>\r\n','<p>\r\n	A traditional (western) medical doctor typically explores your medical history and tries to get rid of your symptoms, very often through the use of pills or&nbsp; other pharmaceutical products.&nbsp; Holistic practioners will factor your medical history into a greater equation &ndash; one that looks for the root cause of the imbalance underlying your insomnia.&nbsp; They consider the various components of your life such as &ndash; your medical history, life circumstance, general nutrition, exercise, level of stress, etc.&nbsp; After doing the math so to speak &ndash; assessing the impact of all these elements on your health, general well-being and spirit, a holistic practitioner prescribes the course of treatment that will best heal the imbalance and re-establish a normal sleep cycle.&nbsp; In short, Holistic practitioners treat the whole person &ndash; physically, emotionally, and mentally.&nbsp;</p>\r\n',1,1,'testing-fields','2010-02-05 11:43:27','2010-02-05 14:02:30',12,12,'<p>\r\n	A traditional (western) medical doctor typically explores your medical history and tries to get rid of your symptoms, very often through the use of pills or&nbsp; other pharmaceutical products.&nbsp; Holistic practioners will factor your medical history into a greater equation &ndash; one that looks for the root cause of the imbalance underlying your insomnia.&nbsp; They consider the various components of your life such as &ndash; your medical history, life circumstance, general nutrition, exercise, level of stress, etc.&nbsp; After doing the math so to speak &ndash; assessing the impact of all these elements on your health, general well-being and spirit, a holistic practitioner prescribes the course of treatment that will best heal the imbalance and re-establish a normal sleep cycle.&nbsp; In short, Holistic practitioners treat the whole person &ndash; physically, emotionally, and mentally.&nbsp;</p>\r\n','<p>\r\n	A traditional (western) medical doctor typically explores your medical history and tries to get rid of your symptoms, very often through the use of pills or&nbsp; other pharmaceutical products.&nbsp; Holistic practioners will factor your medical history into a greater equation &ndash; one that looks for the root cause of the imbalance underlying your insomnia.&nbsp; They consider the various components of your life such as &ndash; your medical history, life circumstance, general nutrition, exercise, level of stress, etc.&nbsp; After doing the math so to speak &ndash; assessing the impact of all these elements on your health, general well-being and spirit, a holistic practitioner prescribes the course of treatment that will best heal the imbalance and re-establish a normal sleep cycle.&nbsp; In short, Holistic practitioners treat the whole person &ndash; physically, emotionally, and mentally.&nbsp;</p>\r\n','hola!!\r\ntesting\r\n951059%)\"(nfog9t13nkgfeg\r\nBULLETS!!!\r\nhola!!\r\ntesting\r\n951059%)\"(nfog9t13nkgfeg\r\nBULLETS!!!\r\nhola!!\r\ntesting\r\n951059%)\"(nfog9t13nkgfeg\r\nBULLETS!!!',NULL);
INSERT INTO conditions VALUES (22,'testing reference field','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \">dasdsadsa</span></p>\r\n','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \">dasdsadsa</span></p>\r\n','<p>\r\n	dasdsadsa</p>\r\n','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \">dasdsadsa</span></p>\r\n',1,1,'testing-reference','2010-02-12 11:23:59','2010-02-12 17:59:52',12,12,'<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \">dasdsadsa</span></p>\r\n','','1test \r\n2te\r\n3s\r\n4tingggggggggggggggg ggggggggggg ggggggggggggggg ggggggggggggg \r\n5test\r\n6test\r\n7test\r\n8test\r\n9test\r\n10test\r\n11test\r\n12test\r\n13test\r\n14test\r\n15test','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-family: \'Times New Roman\'; font-size: medium; \">dasdsadsa</span></p>\r\n');
INSERT INTO conditions VALUES (23,'asd','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0);\">The defining characteristic of the disorder is a severe and lasting fatigue that is not linked to an existing medical condition and has lasted six months or longer. The severity of CFS varies from patient to patient. Some people maintain a fairly active lifestyle. For the most symptomatic patients, however, CFS significantly limits work, school, and family activities<sup>4</sup>. The type of fatigue characteristic of this condition is not the kind of fatigue we experience after a particularly busy day, a sleepless night, or a single stressful event. It&rsquo;s a severe, incapacitating fatigue that isn&rsquo;t improved by bed rest and that may be worsened by physical or mental activity. It&rsquo;s an all-encompassing fatigue that results in a dramatic decline in both activity level and stamina.</span></p>\r\n<div>\r\n	<div>\r\n		<div>\r\n			<div>\r\n				<div>\r\n					<div>\r\n						<font color=\"#000000\">&nbsp;</font></div>\r\n					<div>\r\n						<font color=\"#000000\"><span>CFS often follows a cyclical course, alternating between periods of illness and relative well-being. Some patients experienced partial or complete remission of symptoms during the course of the illness, but symptoms often recur. This pattern of remission and relapse makes chronic fatigue especially hard for patients to manage. Patients who are in remission may be tempted to overdo activities when they&rsquo;re feeling better, which can actually cause a relapse<sup>5</sup>. Therefore, it is of the utmost importance to identify a balanced rhythm in your life&mdash;one that includes equal measures of rest, exertion, relaxation, work, good nutrition, detoxification, meditation, and community.</span></font></div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n','<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0);\">The defining characteristic of the disorder is a severe and lasting fatigue that is not linked to an existing medical condition and has lasted six months or longer. The severity of CFS varies from patient to patient. Some people maintain a fairly active lifestyle. For the most symptomatic patients, however, CFS significantly limits work, school, and family activities<sup>4</sup>. The type of fatigue characteristic of this condition is not the kind of fatigue we experience after a particularly busy day, a sleepless night, or a single stressful event. It&rsquo;s a severe, incapacitating fatigue that isn&rsquo;t improved by bed rest and that may be worsened by physical or mental activity. It&rsquo;s an all-encompassing fatigue that results in a dramatic decline in both activity level and stamina.</span></p>\r\n<div>\r\n	<div>\r\n		<font color=\"#000000\">&nbsp;</font></div>\r\n	<div>\r\n		<font color=\"#000000\"><span>CFS often follows a cyclical course, alternating between periods of illness and relative well-being. Some patients experienced partial or complete remission of symptoms during the course of the illness, but symptoms often recur. This pattern of remission and relapse makes chronic fatigue especially hard for patients to manage. Patients who are in remission may be tempted to overdo activities when they&rsquo;re feeling better, which can actually cause a relapse<sup>5</sup>. Therefore, it is of the utmost importance to identify a balanced rhythm in your life&mdash;one that includes equal measures of rest, exertion, relaxation, work, good nutrition, detoxification, meditation, and community.</span></font></div>\r\n</div>\r\n','<p>\r\n	&nbsp;</p>\r\n<div>\r\n	<div>\r\n		<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><i><em>Chronic fatigue syndrome</em>&nbsp;is a complicated disorder characterized by extreme fatigue that may worsen with physical or mental activity and that does not improve with rest<sup>1</sup>. The best recovery rates have been experienced by people who used both&nbsp;<em>traditional&nbsp;</em><span>and<em>&nbsp;alternative healing modalities<sup>2</sup>.</em></span></i></span></font></div>\r\n</div>\r\n','<p>\r\n	&nbsp;</p>\r\n<div>\r\n	The integrative approach also includes a full spectrum of lab tests&mdash;traditional and alternative. In particular, an integrative physician will order studies of saliva and urine to assess your adrenal glands, as well as the levels of neurotransmitters in your system. Holistic treatment is likely to focus on replenishing your adrenal glands, also known as the &ldquo;fight or flight glands.&rdquo; Prolonged periods of elevated levels of adrenaline and cortisol (the fight or flight hormones) erode your immune system. In addition, an integrative physician or practitioner will screen for oxidative stress.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Your practitioner may prescribe neurotransmitters&mdash;chemicals in your body that not only regulate how you feel and think, but how your body operates. Very often, through supplementation, balanced nutrition, exercise, and rest, the holistic doctor or practitioner is able to move you towards remission of the disabling symptoms of chronic fatigue syndrome.</div>\r\n',1,1,'asd','2010-02-13 11:07:51','2010-02-15 15:16:33',12,12,'<p>\r\n	<span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 0); font-size: small;\">Where once there was doubt, there is now widespread acceptance of the syndrome as a medical disorder. Increasingly, medical doctors understand the value of energy-based disciplines such as yoga and tai chi, and prescribe alternative therapies for treatment of symptoms that frequently accompany chronic fatigue syndrome. If an infection is present, a medical doctor may prescribe antiviral or antibacterial medications.</span></p>\r\n<div>\r\n	<div>\r\n		<div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><br />\r\n				</span></font></span></font></div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><strong><u>REFERENCES</u></strong>:</span></font></span></font></span></span></font></span></font></span></font></span></font></div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\">&nbsp;</font></span></font></span></span></font></span></font></span></span></font></span></font></span></font></span></font></div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><font size=\"2\"><sup><span>1</span></sup>Mayo Clinic.com, &ldquo;Chronic Fatigue Syndrome,&rdquo; at http://www.mayoclinic.com/health/chronic-fatigue-syndrome/DS00395.</font></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></font></span></font></div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><font size=\"2\"><sup><span>2</span></sup>Chronic Fatigue.org, &ldquo;The Web Page for Chronic Fatigue Sufferers,&rdquo; at http://www.chronicfatigue.org.</font></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></font></span></font></div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><font size=\"2\"><sup><span>3</span></sup>PsychCentral.com, Rick Nauert, Ph.D.,&nbsp;<i>Emerging Theory on Chronic Fatigue</i>, at http://psychcentral.com/news/2008/06/25/new-theory-on-chronic-fatigue/2501.html.</font></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></font></span></font></div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><font size=\"2\"><sup><span>4</span></sup>Centers for Disease Control (CDC), &ldquo;Symptoms of CFS,&rdquo; at http://www.CDC.gov/cfs/cfssymptoms.htm#primary.</font></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></font></span></font></div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><font size=\"2\"><sup><span>5</span></sup>CDC, &ldquo;Symptoms,&rdquo; http://www.CDC.gov//cfs/cfssymptoms.htm#primary.</font></span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></font></span></font></div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><sup><span>6</span></sup>CDC, &ldquo;Symptoms,&rdquo;&nbsp;<a href=\"http://www.CDC.gov/cfs/cfssymptoms.htm#primary\"><font color=\"#0000ff\">http://www.CDC.gov//cfs/cfssymptoms.htm#primary</font></a>.</span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></font></span></font></div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><sup><span>7</span></sup>Fibromyalgia and Fatigue Centers, &ldquo;Multi-faceted Treatment Approach,&rdquo; at http://www.five growfibroandfatigue.com/clinical.php#6.</span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></font></span></font></div>\r\n			<div>\r\n				<font class=\"Apple-style-span\" color=\"#000000\"><span class=\"Apple-style-span\" style=\"font-size: small;\"><font class=\"Apple-style-span\" color=\"#222222\" size=\"3\"><span class=\"Apple-style-span\" style=\"font-size: 12px;\"><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><span><font color=\"#000000\"><span><font size=\"2\"><span><br />\r\n				</span></font></span></font></span></span></font></span></font></span></span></font></span></font></span></font></span></font></div>\r\n		</div>\r\n	</div>\r\n</div>\r\n',NULL,'dfsafasfasf fsafasfasfas fasfas fsa fsafsa fsa fsa safa fs fs fsf\r\ndasdsasda\r\ndsadasd\r\nasdas\r\ndas\r\ndas\r\ndfsafasfasf fsafasfasfas fasfas fsa fsafsa fsa fsa safa fs fs fsf dfsafasfasf fsafasfasfas fasfas fsa fsafsa fsa fsa safa fs fs fsf dfsafasfasf fsafasfasfas fasfas fsa fsafsa fsa fsa safa fs fs fsf','<p>\r\n	&nbsp;</p>\r\n<div>\r\n	The integrative approach also includes a full spectrum of lab tests&mdash;traditional and alternative. In particular, an integrative physician will order studies of saliva and urine to assess your adrenal glands, as well as the levels of neurotransmitters in your system. Holistic treatment is likely to focus on replenishing your adrenal glands, also known as the &ldquo;fight or flight glands.&rdquo; Prolonged periods of elevated levels of adrenaline and cortisol (the fight or flight hormones) erode your immune system. In addition, an integrative physician or practitioner will screen for oxidative stress.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Your practitioner may prescribe neurotransmitters&mdash;chemicals in your body that not only regulate how you feel and think, but how your body operates. Very often, through supplementation, balanced nutrition, exercise, and rest, the holistic doctor or practitioner is able to move you towards remission of the disabling symptoms of chronic fatigue syndrome.</div>\r\n');

#
# Table structure for table 'conditions_organizations'
#

# DROP TABLE IF EXISTS conditions_organizations;
CREATE TABLE `conditions_organizations` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `condition_id` int(11) unsigned default NULL,
  `organization_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'conditions_organizations'
#


#
# Table structure for table 'conditions_products'
#

# DROP TABLE IF EXISTS conditions_products;
CREATE TABLE `conditions_products` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `condition_id` int(11) unsigned default NULL,
  `product_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'conditions_products'
#


#
# Table structure for table 'conditions_symptoms'
#

# DROP TABLE IF EXISTS conditions_symptoms;
CREATE TABLE `conditions_symptoms` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `condition_id` int(11) unsigned default NULL,
  `symptom_id` int(11) unsigned default NULL,
  `classification` tinyint(1) default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'conditions_symptoms'
#

INSERT INTO conditions_symptoms VALUES (123,16,7,1);
INSERT INTO conditions_symptoms VALUES (122,19,7,1);

#
# Table structure for table 'conditions_therapies'
#

# DROP TABLE IF EXISTS conditions_therapies;
CREATE TABLE `conditions_therapies` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `condition_id` int(11) unsigned default NULL,
  `therapy_id` int(11) unsigned default NULL,
  `pro` text COMMENT 'Primary=1,Secondary=0,Pro=1,Con=0',
  `con` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'conditions_therapies'
#

INSERT INTO conditions_therapies VALUES (131,14,6,'test1','test2');
INSERT INTO conditions_therapies VALUES (87,17,12,'LOREMMMMMMMMMMMMMMMMMMM','LOREMMMMMMMMMMMMMMMMMMMMM');
INSERT INTO conditions_therapies VALUES (90,18,6,NULL,NULL);
INSERT INTO conditions_therapies VALUES (98,21,6,'adsada','dsadsad');
INSERT INTO conditions_therapies VALUES (99,21,8,'','');
INSERT INTO conditions_therapies VALUES (126,9,8,'','');
INSERT INTO conditions_therapies VALUES (127,9,9,'','');
INSERT INTO conditions_therapies VALUES (128,9,12,'','');
INSERT INTO conditions_therapies VALUES (129,9,13,'','');
INSERT INTO conditions_therapies VALUES (125,9,7,'','');
INSERT INTO conditions_therapies VALUES (124,9,6,'CBT is often as effective as prescription medications used for short-term treatment of chronic insomnia. In addition, there is an increasing body of evidence suggesting that the beneficial effects of CBT, may last well beyond the termination of active treatment.  By contrast, the benefits of CBT often outweigh those produced by medications.','CBT requires a committed and steady practice.  Some approaches may cause you to lose sleep at first.');

#
# Table structure for table 'entities'
#

# DROP TABLE IF EXISTS entities;
CREATE TABLE `entities` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'entities'
#

INSERT INTO entities VALUES (1,'Condition');
INSERT INTO entities VALUES (2,'Therapy');
INSERT INTO entities VALUES (3,'Symptom');
INSERT INTO entities VALUES (4,'Article');
INSERT INTO entities VALUES (5,'Video');
INSERT INTO entities VALUES (6,'Authority');
INSERT INTO entities VALUES (7,'Publication');
INSERT INTO entities VALUES (8,'Event');
INSERT INTO entities VALUES (9,'Audio');
INSERT INTO entities VALUES (10,'Story');
INSERT INTO entities VALUES (11,'Organization');
INSERT INTO entities VALUES (12,'Poll');
INSERT INTO entities VALUES (13,'Quiz');
INSERT INTO entities VALUES (14,'Workshop');
INSERT INTO entities VALUES (15,'Journal');
INSERT INTO entities VALUES (16,'Worksheet');
INSERT INTO entities VALUES (17,'Product');
INSERT INTO entities VALUES (18,'Page');
INSERT INTO entities VALUES (19,'Discussion');
INSERT INTO entities VALUES (20,'Unknown');
INSERT INTO entities VALUES (21,'LiivToday');
INSERT INTO entities VALUES (22,'Tradition');

#
# Table structure for table 'events'
#

# DROP TABLE IF EXISTS events;
CREATE TABLE `events` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(128) default NULL,
  `description_short` text,
  `description_long` text,
  `event_date` date default NULL,
  `link` varchar(128) default NULL,
  `event_blob` text,
  `tags` text,
  `location` varchar(128) default NULL,
  `teacher` varchar(128) default NULL,
  `subject` varchar(128) default NULL,
  `source` varchar(64) default NULL,
  `area_id` int(11) unsigned default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(11) default NULL,
  `updatedby` int(11) default NULL,
  `published` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'events'
#

INSERT INTO events VALUES (1,'Holiday','holy','bla','2010-03-06','','','','','','','',2,'2009-11-05 16:43:13','2009-11-23 14:42:00',NULL,NULL,0);

#
# Table structure for table 'events_conditions'
#

# DROP TABLE IF EXISTS events_conditions;
CREATE TABLE `events_conditions` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `event_id` int(11) unsigned default NULL,
  `condition_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'events_conditions'
#


#
# Table structure for table 'events_organizations'
#

# DROP TABLE IF EXISTS events_organizations;
CREATE TABLE `events_organizations` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `event_id` int(11) unsigned default NULL,
  `organization_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'events_organizations'
#


#
# Table structure for table 'events_stories'
#

# DROP TABLE IF EXISTS events_stories;
CREATE TABLE `events_stories` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `event_id` int(11) unsigned default NULL,
  `story_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'events_stories'
#

INSERT INTO events_stories VALUES (30,1,1);

#
# Table structure for table 'events_therapies'
#

# DROP TABLE IF EXISTS events_therapies;
CREATE TABLE `events_therapies` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `event_id` int(11) unsigned default NULL,
  `therapy_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'events_therapies'
#


#
# Table structure for table 'groups'
#

# DROP TABLE IF EXISTS groups;
CREATE TABLE `groups` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'groups'
#

INSERT INTO groups VALUES (1,'users');
INSERT INTO groups VALUES (2,'publishers');
INSERT INTO groups VALUES (3,'administrators');
INSERT INTO groups VALUES (4,'TESTERS');

#
# Table structure for table 'iplogs'
#

# DROP TABLE IF EXISTS iplogs;
CREATE TABLE `iplogs` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `ip` int(11) default NULL,
  `proxy` int(11) default NULL,
  `ip_text` varchar(20) default NULL,
  `proxy_text` varchar(20) default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UNIQUE` (`user_id`,`ip`,`proxy`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'iplogs'
#

INSERT INTO iplogs VALUES (1,1,2130706689,0,'127.0.1.1','','2009-11-27 16:16:18','2010-01-26 13:54:21');
INSERT INTO iplogs VALUES (2,1,2130706433,0,'127.0.0.1','','2009-11-27 16:23:53','2010-02-17 20:25:18');
INSERT INTO iplogs VALUES (3,1,2147483647,0,'192.168.1.1','','2009-11-27 19:19:14','2010-02-17 17:14:38');
INSERT INTO iplogs VALUES (4,5,1155221446,0,'68.219.71.198','','2009-11-30 20:19:16','2009-12-07 14:02:38');
INSERT INTO iplogs VALUES (5,5,409110320,0,'24.98.135.48','','2009-12-01 15:04:07','2010-02-16 19:14:27');
INSERT INTO iplogs VALUES (6,1,417875154,0,'24.232.68.210','','2009-12-08 02:38:23','2009-12-08 02:42:04');
INSERT INTO iplogs VALUES (7,7,417872413,0,'24.232.58.29','','2009-12-08 17:53:48','2009-12-08 18:06:32');
INSERT INTO iplogs VALUES (8,7,2130706689,0,'127.0.1.1','','2009-12-09 18:42:29','2009-12-09 18:50:08');
INSERT INTO iplogs VALUES (9,12,2130706689,0,'127.0.1.1','','2009-12-09 18:55:28','2010-02-15 21:09:57');
INSERT INTO iplogs VALUES (10,5,1155245017,0,'68.219.163.217','','2009-12-11 15:31:25','2009-12-16 20:10:20');
INSERT INTO iplogs VALUES (11,13,2147483647,0,'192.168.1.1','','2009-12-14 15:25:46','2009-12-14 15:25:52');
INSERT INTO iplogs VALUES (12,5,1092307587,0,'65.27.74.131','','2009-12-24 18:40:33','2009-12-24 18:40:41');
INSERT INTO iplogs VALUES (13,1,417872413,0,'24.232.58.29','','2009-12-31 01:34:56','2009-12-31 01:35:56');
INSERT INTO iplogs VALUES (14,12,2147483647,0,'190.137.17.117','','2010-01-03 03:24:16','2010-01-03 03:24:31');
INSERT INTO iplogs VALUES (15,14,409110320,0,'24.98.135.48','','2010-01-06 21:57:29','2010-01-06 22:01:20');
INSERT INTO iplogs VALUES (16,5,1155253132,0,'68.219.195.140','','2010-01-07 01:49:44','2010-01-12 02:03:39');
INSERT INTO iplogs VALUES (17,14,1155253132,0,'68.219.195.140','','2010-01-07 01:50:49','2010-01-07 02:06:49');
INSERT INTO iplogs VALUES (18,17,1155253132,0,'68.219.195.140','','2010-01-07 02:09:48','2010-01-07 02:10:37');
INSERT INTO iplogs VALUES (19,17,1663565487,0,'99.39.254.175','','2010-01-07 02:13:32','2010-01-07 02:14:41');
INSERT INTO iplogs VALUES (20,10,1281465818,0,'76.97.157.218','','2010-01-08 15:02:27','2010-01-08 17:34:23');
INSERT INTO iplogs VALUES (21,18,2147483647,0,'190.18.119.113','','2010-01-11 17:57:53','2010-01-11 18:46:27');
INSERT INTO iplogs VALUES (22,23,1155253132,0,'68.219.195.140','','2010-01-12 01:21:59','2010-01-12 01:22:12');
INSERT INTO iplogs VALUES (23,18,2130706433,0,'127.0.0.1','','2010-01-12 16:27:14','2010-01-12 16:34:59');
INSERT INTO iplogs VALUES (24,23,409110320,0,'24.98.135.48','','2010-01-12 16:42:30','2010-01-12 16:55:33');
INSERT INTO iplogs VALUES (25,15,1658043413,0,'98.211.188.21','','2010-01-12 17:45:13','2010-01-23 17:01:54');
INSERT INTO iplogs VALUES (26,15,1676163043,0,'99.232.55.227','','2010-01-23 16:55:20','2010-01-23 17:01:03');
INSERT INTO iplogs VALUES (27,25,403055908,0,'24.6.37.36','','2010-01-23 16:56:51','2010-01-23 17:00:46');
INSERT INTO iplogs VALUES (28,27,2130706433,0,'127.0.0.1','','2010-01-26 15:02:35','2010-02-12 21:58:17');
INSERT INTO iplogs VALUES (29,29,2147483647,0,'192.168.1.1','','2010-01-28 20:35:27','2010-01-28 20:52:11');
INSERT INTO iplogs VALUES (30,30,2130706689,0,'127.0.1.1','','2010-01-29 19:40:14','2010-01-29 20:35:02');
INSERT INTO iplogs VALUES (31,5,1155251428,0,'68.219.188.228','','2010-01-30 16:16:35','2010-02-11 04:07:26');
INSERT INTO iplogs VALUES (32,31,2130706433,0,'127.0.0.1','','2010-02-08 20:46:05','2010-02-08 21:02:25');
INSERT INTO iplogs VALUES (33,32,2130706433,0,'127.0.0.1','','2010-02-08 21:10:13','2010-02-08 21:20:08');
INSERT INTO iplogs VALUES (34,27,2147483647,0,'192.168.1.1','','2010-02-10 14:23:45','2010-02-12 19:46:48');
INSERT INTO iplogs VALUES (35,12,2130706433,0,'127.0.0.1','','2010-02-13 13:59:44','2010-02-14 23:20:56');
INSERT INTO iplogs VALUES (36,5,1204556327,0,'71.204.18.39','','2010-02-13 18:06:07','2010-02-13 18:06:41');
INSERT INTO iplogs VALUES (37,23,1204707081,0,'71.206.95.9','','2010-02-16 00:00:17','2010-02-16 22:15:06');
INSERT INTO iplogs VALUES (38,5,1154991476,0,'68.215.197.116','','2010-02-16 05:30:26','2010-02-16 05:30:49');
INSERT INTO iplogs VALUES (39,5,2147483647,0,'174.48.252.225','','2010-02-17 16:56:49','2010-02-17 20:27:05');

#
# Table structure for table 'journal_types'
#

# DROP TABLE IF EXISTS journal_types;
CREATE TABLE `journal_types` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table 'journal_types'
#


#
# Table structure for table 'journals'
#

# DROP TABLE IF EXISTS journals;
CREATE TABLE `journals` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `published` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'journals'
#


#
# Table structure for table 'liiv_today'
#

# DROP TABLE IF EXISTS liiv_today;
CREATE TABLE `liiv_today` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `when` date NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `content` text,
  `link` varchar(255) default NULL,
  `type_id` int(10) unsigned NOT NULL,
  `topic` varchar(45) default NULL,
  `image` varchar(255) default NULL,
  `published` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

#
# Dumping data for table 'liiv_today'
#

INSERT INTO liiv_today VALUES (6,'2009-11-04','2009-11-27 12:43:18','2009-12-01 14:21:57',1,'another test<br><br>',NULL,1,'hello world',NULL,0);
INSERT INTO liiv_today VALUES (8,'2009-12-16','2009-11-30 20:20:00','2009-12-17 12:10:34',1,'eat more oranges...<br>',NULL,1,'food oranges health wellness prevent','/img/liiv_today/1/34010390.jpg',0);
INSERT INTO liiv_today VALUES (9,'2009-11-30','2009-11-30 20:24:23','2009-11-30 20:24:23',5,'I need to clean out my inbox today.<br>',NULL,2,'email',NULL,0);
INSERT INTO liiv_today VALUES (10,'2009-12-10','2009-11-30 20:43:00','2009-12-17 15:31:34',1,'Hahaha so funny<br>',NULL,6,'requirements','/img/liiv_today/6/162265424.jpg',0);
INSERT INTO liiv_today VALUES (11,'2009-11-30','2009-12-01 03:31:47','2009-12-01 03:31:47',5,'knock knock...<br>',NULL,7,'laugh joke knock knock bar',NULL,0);
INSERT INTO liiv_today VALUES (12,'2009-12-14','2009-12-14 15:09:37','2009-12-14 15:12:46',1,'<P><FONT color=#000000><STRONG>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus convallis egestas nunc nec tempor. Proin posuere consequat ultricies. Morbi vel metus est. Vestibulum ultrices ipsum nec nisl hendrerit laoreet. Donec tincidunt mi eu urna sagittis sollicitudin. Mauris porta, libero ut auctor consequat, leo nisi fermentum tortor, faucibus laoreet tellus lorem sit amet dolor. Aliquam commodo blandit massa eu ullamcorper. Aliquam nunc est, adipiscing eget iaculis ac, tempus nec sem. Morbi eleifend, metus at varius adipiscing, augue risus sagittis lacus, quis posuere magna diam non lorem. Phasellus sem lorem, vulputate vel venenatis non, posuere a sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a felis ac ante congue rhoncus eu eu libero. Duis ultrices, orci eget malesuada imperdiet, ipsum orci gravida quam, in egestas dui tortor sit amet lorem. Nullam at leo diam. Nunc dignissim velit vel enim dignissim dictum. Aliquam sed nibh in magna euismod ultricies. Curabitur eu orci nibh, at egestas elit. Nam varius gravida ligula quis fringilla. Proin ipsum dui, congue a rutrum non, vestibulum non turpis. Vestibulum at justo id sem convallis vehicula. </STRONG></FONT></P>\r\n<P><FONT color=#000000><STRONG>Nulla convallis tellus non justo placerat posuere. Donec blandit sapien nec felis imperdiet eu lobortis nunc congue. In sed nulla sit amet nibh mattis faucibus. Donec blandit dignissim tristique. Vivamus sed odio at nulla condimentum tempor vitae in libero. Vivamus adipiscing dolor ut turpis imperdiet nec rutrum urna viverra. Aenean blandit aliquet nisl, eget porta libero malesuada id. Sed eu tempor mi. Sed feugiat, tortor sollicitudin hendrerit dignissim, lorem massa pretium augue, ut lacinia libero massa id arcu. In sapien nibh, rhoncus eu ornare id, hendrerit quis ipsum. </STRONG></FONT></P>\r\n<P><FONT color=#000000><STRONG>Proin scelerisque lorem eu urna ullamcorper in volutpat nisi dictum. Sed tincidunt felis id erat imperdiet cursus. Mauris eros augue, lacinia iaculis suscipit in, mattis eget dolor. Duis condimentum varius velit in ornare. Mauris malesuada pulvinar pretium. Cras porttitor, eros sed cursus rhoncus, felis mi ullamcorper risus, quis lobortis nunc metus at magna. Pellentesque ultrices sapien sit amet libero venenatis luctus. Curabitur pellentesque, arcu venenatis euismod mollis, tellus libero egestas nibh, eget consequat nisi nisi quis nulla. Donec vitae dapibus est. Duis leo erat, vehicula nec ultrices eget, feugiat at turpis. Proin tellus diam, sollicitudin vel euismod eu, suscipit vitae odio. Aenean sed orci elementum elit suscipit commodo at nec turpis. Suspendisse interdum adipiscing dapibus. Proin nisi augue, consequat a placerat id, adipiscing a nibh. Nullam sollicitudin nunc metus, non interdum ante. Mauris porta tempus nibh quis elementum. Vivamus accumsan tortor sed sem scelerisque a auctor justo ultrices. Donec elementum imperdiet ornare. Nunc vehicula faucibus blandit. Ut aliquam mattis viverra.</STRONG> </FONT></P>',NULL,4,'OKOK',NULL,0);
INSERT INTO liiv_today VALUES (13,'2009-12-16','2009-12-15 22:54:47','2010-01-06 13:54:09',1,'<a href=\"http://www.amazon.com/Everybodys-Normal-Till-Know-Them/dp/0310286638/ref=sr_1_1?ie=UTF8&amp;s=books&amp;qid=1260917478&amp;sr=1-1\">Everyone\'s Normal Till You Get to Know Them</a>, by John Ortberg<br><br>\"Rarely has a book done the job so effectively.\"<br>-- <i> CBA Marketplace</i><br><br>\". . . upbeat . . .\"<br>-- <i>Publishers Weekly</i> (<i>Publisher\'s Weekly</i> )\r\n  <em>--This text refers to an out of print or unavailable edition of this title.</em><br>',NULL,5,'Everyone\'s Normal','',0);
INSERT INTO liiv_today VALUES (14,'2010-01-06','2010-01-06 17:56:15','2010-01-06 17:56:15',5,'get more sleep<br>',NULL,1,'sleep','',0);
INSERT INTO liiv_today VALUES (15,'2010-01-08','2010-01-06 17:56:55','2010-01-06 17:56:55',5,'eat more apples',NULL,1,'apples','',0);
INSERT INTO liiv_today VALUES (16,'2010-02-28','2010-01-12 16:00:03','2010-01-12 16:00:03',5,'Testing a future date...<br>',NULL,1,'future date','',0);
INSERT INTO liiv_today VALUES (17,'2010-01-13','2010-01-12 17:38:23','2010-01-12 17:38:23',5,'Enter text here.<br>',NULL,1,'test keyword wellness','',0);
INSERT INTO liiv_today VALUES (18,'2010-01-20','2010-01-21 20:39:19','2010-01-21 20:39:19',5,'Testing Yesterday',NULL,1,'test','',0);
INSERT INTO liiv_today VALUES (19,'2010-01-20','2010-01-21 20:40:34','2010-01-21 20:40:46',5,'I have GOT to quit getting sick!<br>',NULL,2,'sick','',0);
INSERT INTO liiv_today VALUES (20,'2010-02-21','2010-01-21 20:41:27','2010-01-21 20:41:27',5,'Testing future.<br>',NULL,2,'tomorrow','',0);
INSERT INTO liiv_today VALUES (24,'2010-01-28','2010-01-28 14:12:12','2010-01-28 14:13:22',12,'<div>test!</div>',NULL,1,'Test!','/img/liiv_today/1/34657360.jpg',0);
INSERT INTO liiv_today VALUES (25,'2010-02-01','2010-02-01 10:32:52','2010-02-01 11:29:35',12,'<p>\r\n	dsdasdadasdasd</p>\r\n','http://www.google.com',2,'test topic','',0);
INSERT INTO liiv_today VALUES (29,'2010-01-29','2010-02-01 11:30:46','2010-02-01 11:30:46',12,'<p>\r\n	content test!!</p>\r\n','http://testbook.com',5,'test book','/img/liiv_today//458680949.png',0);
INSERT INTO liiv_today VALUES (31,'2010-02-10','2010-02-12 13:55:41','2010-02-14 17:59:32',12,'<p>\r\n	sdasdasdadas</p>\r\n<p>\r\n	das</p>\r\n<p>\r\n	dasd</p>\r\n','http://asd.com',1,'asd','/img/liiv_today/1/770512999.jpg',0);

#
# Table structure for table 'liiv_today_habtm'
#

# DROP TABLE IF EXISTS liiv_today_habtm;
CREATE TABLE `liiv_today_habtm` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `entity_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `liiv_today_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Dumping data for table 'liiv_today_habtm'
#


#
# Table structure for table 'liiv_today_types'
#

# DROP TABLE IF EXISTS liiv_today_types;
CREATE TABLE `liiv_today_types` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(64) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#
# Dumping data for table 'liiv_today_types'
#

INSERT INTO liiv_today_types VALUES (1,'Wellness Tip of the Day');
INSERT INTO liiv_today_types VALUES (2,'Intention for the Day');
INSERT INTO liiv_today_types VALUES (5,'Book of the Day');
INSERT INTO liiv_today_types VALUES (6,'The Daily Wild Card');

#
# Table structure for table 'organizations'
#

# DROP TABLE IF EXISTS organizations;
CREATE TABLE `organizations` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(128) default NULL,
  `website` varchar(128) default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'organizations'
#

INSERT INTO organizations VALUES (1,'Test organization','http://asd.com',NULL,'2009-12-02 18:18:48',NULL,1);
INSERT INTO organizations VALUES (2,'Test org','dasdasdas',NULL,NULL,NULL,NULL);
INSERT INTO organizations VALUES (3,'JASON','WWWOGLE.COM','2009-12-14 15:04:05','2009-12-14 15:04:29',1,1);

#
# Table structure for table 'pages'
#

# DROP TABLE IF EXISTS pages;
CREATE TABLE `pages` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(40) default NULL,
  `content` text,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'pages'
#

INSERT INTO pages VALUES (1,'About us','<p>Praesent ac eros diam. Vivamus facilisis, odio nec semper accumsan, diam ipsum tempus nisi, a convallis urna arcu aliquam dolor. Phasellus non mattis nulla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque sit amet dolor ut lacus ullamcorper dapibus. Nam id nibh vitae lorem lobortis dictum id commodo nunc. Nam venenatis sollicitudin commodo. Donec in sem augue. Praesent tempus cursus ultricies. Praesent blandit neque at velit dictum eu consequat urna laoreet. Fusce in erat sed elit dictum viverra.</p>\r\n<p>Praesent malesuada elementum convallis. Mauris quis diam et ante egestas ultricies in et arcu. Nunc interdum, magna nec tincidunt tristique, dolor justo aliquet nulla, vel viverra leo quam ac libero. Nulla massa tortor, adipiscing dictum varius lobortis, tempor ultricies felis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent varius rutrum velit eu pellentesque. Vestibulum eget augue magna. Mauris accumsan urna quis purus egestas placerat. Donec pellentesque neque eget ante tempor consequat. Morbi volutpat dui at massa consequat at malesuada est molestie. Vestibulum sollicitudin libero ornare turpis volutpat feugiat. Aenean laoreet bibendum enim et feugiat. Fusce non dapibus nulla. Maecenas dictum orci vel tortor blandit placerat. Ut in erat neque.</p>','2009-11-18 11:28:38','2010-01-06 11:39:15',NULL,12);
INSERT INTO pages VALUES (2,'Health Overview','<p><img src=\"/img/wysiwyg/images/HealthHP.jpg\" alt=\"\" width=\"301\" height=\"203\" style=\"margin:0 0 10px 10px;float:right\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n<br />\r\n<ul>\r\n	<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>\r\n	<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>\r\n	<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>\r\n</ul>','2009-11-24 12:12:58','2010-02-02 14:14:35',NULL,12);
INSERT INTO pages VALUES (3,'Stress','<p><img src=\"/img/wysiwyg/images/StressHP.jpg\" alt=\"\" width=\"299\" height=\"201\" style=\"margin:0 0 10px 10px;float:right\" />Claritas est etiam processus <strong>dynamicus, qui sequitur mutationem consuetudium lectorum</strong>. Mirum est notare quam littera gothica, quam nunc putamus parum claram, <em>anteposuerit litterarum formas humanitatis</em> per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>','2009-11-24 12:16:08','2010-02-09 17:30:04',NULL,27);
INSERT INTO pages VALUES (4,'Spiritual Traditions Homepage','<p><img src=\"/img/wysiwyg/images/SpiritualTradHP.jpg\" alt=\"\" width=\"299\" height=\"201\" style=\"margin:0 0 10px 10px;float:right\" />Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n<h3>Subheading</h3>\r\n<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n<h2>Watch Video</h2>\r\n<p>video should go here...</p>','2009-11-24 12:16:26','2010-01-05 11:56:49',NULL,12);
INSERT INTO pages VALUES (5,'Conditions Homepage Narrative','<p><img src=\"/img/wysiwyg/images/ConditionsHP.jpg\" alt=\"\" width=\"299\" height=\"201\" style=\"margin:0 0 10px 10px;float:right\" />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi et tellus purus, id scelerisque elit. Vivamus ac enim et dolor pharetra congue a sit amet dui. Vestibulum tortor magna, placerat ac tristique eu, scelerisque non sem. Maecenas et tristique nisl. In hac habitasse platea dictumst. In vehicula pulvinar pellentesque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam erat volutpat. Nunc vel libero ligula, id facilisis massa. Mauris massa purus, posuere id feugiat a, lobortis vitae augue. Donec tincidunt lorem id purus rhoncus at lobortis enim vehicula. Phasellus mollis elementum justo, sit amet tristique quam rutrum a.</p>\r\n<p>Mauris at neque nec tortor egestas interdum vitae eu urna. Proin sit amet mi arcu, eu vestibulum ipsum. Nunc tristique sodales arcu quis porta. Morbi euismod ipsum sed odio porttitor facilisis porta metus congue. Quisque aliquam, est quis auctor vehicula, felis nunc pulvinar sapien, et aliquet tellus sem et enim. Etiam interdum diam a ligula faucibus vel gravida eros porta. Curabitur interdum, nisl convallis vehicula imperdiet, lorem leo auctor sem, a eleifend purus sem ac nibh. Etiam ut elit lorem. Sed ut orci non quam aliquam tempor. Fusce metus est, blandit a aliquam at, porta et eros.</p>\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean pellentesque vestibulum lacus eget interdum. Pellentesque non sapien sem. Aliquam porta pharetra libero at adipiscing. Aliquam erat volutpat. Vivamus consequat turpis lorem. Vivamus vel ante non magna faucibus tincidunt in sit amet felis. Aenean rutrum sapien libero, nec vehicula augue. Duis sodales elit et nunc eleifend at commodo mauris tincidunt. Curabitur nec lorem eget tortor dictum rutrum.</p>\r\n<p>Vivamus neque est, porttitor nec vehicula at, malesuada vel nulla. Maecenas pretium pellentesque lacinia. Nulla malesuada rhoncus enim, sit amet congue ipsum pulvinar non. Proin tincidunt condimentum elit non fermentum. Etiam arcu nisi, volutpat quis dictum a, suscipit ut urna. Mauris a ipsum tortor. Integer viverra, diam vel semper tempor, arcu diam facilisis nisl, eget pulvinar sem augue id eros. Cras in odio arcu. Suspendisse sit amet diam id nibh tincidunt tempor ut ut dolor. Proin ante neque, porta in tincidunt eget, euismod in dolor. Integer auctor, tellus at aliquam mollis, enim lorem malesuada lorem, et rhoncus dui libero ut ligula.</p>\r\n<h3>Subheading</h3>\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean pellentesque vestibulum lacus eget interdum. Pellentesque non sapien sem. Aliquam porta pharetra libero at adipiscing. Aliquam erat volutpat. Vivamus consequat turpis lorem. Vivamus vel ante non magna faucibus tincidunt in sit amet felis. Aenean rutrum sapien libero, nec vehicula augue. Duis sodales elit et nunc eleifend at commodo mauris tincidunt. Curabitur nec lorem eget tortor dictum rutrum.</p>\r\n<p>Vivamus neque est, porttitor nec vehicula at, malesuada vel nulla. Maecenas pretium pellentesque lacinia. Nulla malesuada rhoncus enim, sit amet congue ipsum pulvinar non. Proin tincidunt condimentum elit non fermentum. Etiam arcu nisi, volutpat quis dictum a, suscipit ut urna. Mauris a ipsum tortor. Integer viverra, diam vel semper tempor, arcu diam facilisis nisl, eget pulvinar sem augue id eros. Cras in odio arcu. Suspendisse sit amet diam id nibh tincidunt tempor ut ut dolor. Proin ante neque, porta in tincidunt eget, euismod in dolor. Integer auctor, tellus at aliquam mollis, enim lorem malesuada lorem, et rhoncus dui libero ut ligula.</p>\r\n<h2>Watch Video:</h2>\r\n<p>Should be implemented</p>','2009-12-31 10:17:39','2009-12-31 11:36:24',12,12);
INSERT INTO pages VALUES (6,'Therapies Homepage Narrative ','<p><img src=\"/img/wysiwyg/images/TherapiesHP.jpg\" alt=\"\" width=\"299\" height=\"201\" style=\"margin:0 0 10px 10px;float:right\" />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi et tellus purus, id scelerisque elit. Vivamus ac enim et dolor pharetra congue a sit amet dui. Vestibulum tortor magna, placerat ac tristique eu, scelerisque non sem. Maecenas et tristique nisl. In hac habitasse platea dictumst. In vehicula pulvinar pellentesque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam erat volutpat. Nunc vel libero ligula, id facilisis massa. Mauris massa purus, posuere id feugiat a, lobortis vitae augue. Donec tincidunt lorem id purus rhoncus at lobortis enim vehicula. Phasellus mollis elementum justo, sit amet tristique quam rutrum a.</p>\r\n<p>Mauris at neque nec tortor egestas interdum vitae eu urna. Proin sit amet mi arcu, eu vestibulum ipsum. Nunc tristique sodales arcu quis porta. Morbi euismod ipsum sed odio porttitor facilisis porta metus congue. Quisque aliquam, est quis auctor vehicula, felis nunc pulvinar sapien, et aliquet tellus sem et enim. Etiam interdum diam a ligula faucibus vel gravida eros porta. Curabitur interdum, nisl convallis vehicula imperdiet, lorem leo auctor sem, a eleifend purus sem ac nibh. Etiam ut elit lorem. Sed ut orci non quam aliquam tempor. Fusce metus est, blandit a aliquam at, porta et eros.</p>\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean pellentesque vestibulum lacus eget interdum. Pellentesque non sapien sem. Aliquam porta pharetra libero at adipiscing. Aliquam erat volutpat. Vivamus consequat turpis lorem. Vivamus vel ante non magna faucibus tincidunt in sit amet felis. Aenean rutrum sapien libero, nec vehicula augue. Duis sodales elit et nunc eleifend at commodo mauris tincidunt. Curabitur nec lorem eget tortor dictum rutrum.</p>\r\n<p>Vivamus neque est, porttitor nec vehicula at, malesuada vel nulla. Maecenas pretium pellentesque lacinia. Nulla malesuada rhoncus enim, sit amet congue ipsum pulvinar non. Proin tincidunt condimentum elit non fermentum. Etiam arcu nisi, volutpat quis dictum a, suscipit ut urna. Mauris a ipsum tortor. Integer viverra, diam vel semper tempor, arcu diam facilisis nisl, eget pulvinar sem augue id eros. Cras in odio arcu. Suspendisse sit amet diam id nibh tincidunt tempor ut ut dolor. Proin ante neque, porta in tincidunt eget, euismod in dolor. Integer auctor, tellus at aliquam mollis, enim lorem malesuada lorem, et rhoncus dui libero ut ligula.</p>\r\n<h3>Subheading</h3>\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean pellentesque vestibulum lacus eget interdum. Pellentesque non sapien sem. Aliquam porta pharetra libero at adipiscing. Aliquam erat volutpat. Vivamus consequat turpis lorem. Vivamus vel ante non magna faucibus tincidunt in sit amet felis. Aenean rutrum sapien libero, nec vehicula augue. Duis sodales elit et nunc eleifend at commodo mauris tincidunt. Curabitur nec lorem eget tortor dictum rutrum.</p>\r\n<p>Vivamus neque est, porttitor nec vehicula at, malesuada vel nulla. Maecenas pretium pellentesque lacinia. Nulla malesuada rhoncus enim, sit amet congue ipsum pulvinar non. Proin tincidunt condimentum elit non fermentum. Etiam arcu nisi, volutpat quis dictum a, suscipit ut urna. Mauris a ipsum tortor. Integer viverra, diam vel semper tempor, arcu diam facilisis nisl, eget pulvinar sem augue id eros. Cras in odio arcu. Suspendisse sit amet diam id nibh tincidunt tempor ut ut dolor. Proin ante neque, porta in tincidunt eget, euismod in dolor. Integer auctor, tellus at aliquam mollis, enim lorem malesuada lorem, et rhoncus dui libero ut ligula.</p>\r\n<h2>Watch Video:</h2>\r\n<p>Should be implemented</p>','2009-12-31 11:20:08','2009-12-31 11:36:48',12,12);
INSERT INTO pages VALUES (7,'Terms and Conditions','<h3 id=\"top\">\r\n	PLEASE READ THESE TERMS AND CONDITIONS OF USE CAREFULLY BEFORE USING THIS SITE.</h3>\r\n<p>\r\n	By using this site, you signify your assent to these Terms and Conditions. If you do not agree to all of these Terms and Conditions of use, do not use this site!</p>\r\n<p>\r\n	Liiv LLC (DBA of Liiv LLC (DBA of Planet Hope)) (&quot;Liiv LLC (DBA of Planet Hope)&quot;) may revise and update these Terms and Conditions at any time. Your continued usage of the Liiv LLC (DBA of Planet Hope) website (&quot;Liiv.com Site,&quot; or the &quot;Site,&quot;) will mean you accept those changes.</p>\r\n<p>\r\n	Index</p>\r\n<p>\r\n	The Site Does Not Provide Medical Advice </p>\r\n<p>\r\n	<a href=\"#childrens\">Children&#39;s Privacy</a>  <a href=\"#use-of-content\">Use of Content</a>  <a href=\"#liability\">Liability of Liiv LLC (DBA of Planet Hope) and Its Licensors </a> User Submissions  User Media Submissions &ndash; Image, Video, Audio files  Passwords  Liiv LLC (DBA of Planet Hope) Community and Member to Member Areas (&quot;Public Areas&quot; ) Advertisements, Search and Links to Other Sites  Indemnity  General  Jurisdiction  Notice and Take Down Procedures, Copyright Agent  Complete Agreement </p>\r\n<p>\r\n	The site does not provide medical advice</p>\r\n<p>\r\n	The contents of the Liiv LLC (DBA of Planet Hope) Site, such as text, graphics, images, information obtained from Liiv LLC (DBA of Planet Hope)&#39;s licensors, and other material contained on the Liiv LLC (DBA of Planet Hope) Site (&quot;Content&quot;) are for informational purposes only. The Content is not intended to be a substitute for professional medical advice, diagnosis, or treatment. Always seek the advice of your physician or other qualified health provider with any questions you may have regarding a medical condition. Never disregard professional medical advice or delay in seeking it because of something you have read on the Liiv LLC (DBA of Planet Hope) Site!</p>\r\n<p>\r\n	If you think you may have a medical emergency, call your doctor or 911 immediately. Liiv LLC (DBA of Planet Hope) does not recommend or endorse any specific tests, physicians, products, procedures, opinions, or other information that may be mentioned on the Site. Reliance on any information provided by Liiv LLC (DBA of Planet Hope), Liiv LLC (DBA of Planet Hope) employees, others appearing on the Site at the invitation of Liiv LLC (DBA of Planet Hope), or other visitors to the Site is solely at your own risk.</p>\r\n<p>\r\n	The Site may contain health- or medical-related materials that are sexually explicit. If you find these materials offensive, you may not want to use our Site.</p>\r\n<p>\r\n	<a href=\"#header\">^Back to Top</a></p>\r\n<h3 id=\"childrens\">\r\n	Children&#39;s privacy</h3>\r\n<p>\r\n	We are committed to protecting the privacy of children. You should be aware that this Site is not intended or designed to attract children under the age of 13. We do not collect personally identifiable information from any person we actually know is a child under the age of 13.</p>\r\n<p>\r\n	<a href=\"#header\">^Back to Top</a></p>\r\n<h3 id=\"use-of-content\">\r\n	Use of content</h3>\r\n<p>\r\n	Liiv LLC (DBA of Planet Hope) authorizes you to view or download a single copy of the material on the Liiv LLC (DBA of Planet Hope) Site solely for your personal, noncommercial use if you include the following copyright notice: &quot;Copyright &copy;2009, Liiv LLC (DBA of Liiv LLC (DBA of Planet Hope)) All rights reserved&quot; and other copyright and proprietary rights notices that are contained in the Content. Any special rules for the use of certain software and other items accessible on the Liiv LLC (DBA of Planet Hope) Site may be included elsewhere within the Site and are incorporated into these Terms and Conditions by reference.</p>\r\n<p>\r\n	The Content is protected by copyright under both United States and foreign laws. Title to the Content remains with Liiv LLC (DBA of Planet Hope) or its licensors. Any use of the Content not expressly permitted by these Terms and Conditions is a breach of these Terms and Conditions and may violate copyright, trademark, and other laws. Content and features are subject to change or termination without notice in the editorial discretion of Liiv LLC (DBA of Planet Hope). All rights not expressly granted herein are reserved to Liiv LLC (DBA of Planet Hope) and its licensors. If you violate any of these Terms and Conditions, your permission to use the Content automatically terminates and you must immediately destroy any copies you have made of any portion of the Content.</p>\r\n<p>\r\n	<a href=\"#header\">^Back to Top</a></p>\r\n<h3 id=\"liability\">\r\n	Liability of Liiv LLC (DBA of Planet Hope) and its licensors</h3>\r\n<p>\r\n	The use of the Liiv LLC (DBA of Planet Hope) Site and the Content is at your own risk.</p>\r\n<p>\r\n	When using the Liiv LLC (DBA of Planet Hope) Site, information will be transmitted over a medium that may be beyond the control and jurisdiction of Liiv LLC (DBA of Planet Hope) and its suppliers. Accordingly, Liiv LLC (DBA of Planet Hope) assumes no liability for or relating to the delay, failure, interruption, or corruption of any data or other information transmitted in connection with use of the Liiv LLC (DBA of Planet Hope) Site.</p>\r\n<p>\r\n	The Liiv LLC (DBA of Planet Hope) Site and the content are provided on an &quot;as is&quot; basis. LIIV LLC (DBA OF PLANET HOPE), ITS LICENSORS, AND ITS SUPPLIERS, TO THE FULLEST EXTENT PERMITTED BY LAW, DISCLAIM ALL WARRANTIES, EITHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT OF THIRD PARTIES&#39; RIGHTS, AND FITNESS FOR PARTICULAR PURPOSE. Without limiting the foregoing, Liiv LLC (DBA of Planet Hope), its licensors, and its suppliers make no representations or warranties about the following:</p>\r\n<ol>\r\n	<li>\r\n		The accuracy, reliability, completeness, currentness, or timeliness of the Content, software, text, graphics, links, or communications provided on or through the use of the Liiv LLC (DBA of Planet Hope) Site or Liiv LLC (DBA of Planet Hope).</li>\r\n	<li>\r\n		The satisfaction of any government regulations requiring disclosure of information on prescription drug products or the approval or compliance of any software tools with regard to the Content contained on the Liiv LLC (DBA of Planet Hope) Site. <br />\r\n		In no event shall Liiv LLC (DBA of Planet Hope), its licensors, its suppliers, or any third parties mentioned on the Liiv LLC (DBA of Planet Hope) Site be liable for any damages (including, without limitation, incidental and consequential damages, personal injury/wrongful death, lost profits, or damages resulting from lost data or business interruption) resulting from the use of or inability to use the Liiv LLC (DBA of Planet Hope) Site or the Content, whether based on warranty, contract, tort, or any other legal theory, and whether or not Liiv LLC (DBA of Planet Hope), its licensors, its suppliers, or any third parties mentioned on the Liiv LLC (DBA of Planet Hope) Site are advised of the possibility of such damages. Liiv LLC (DBA of Planet Hope), its licensors, its suppliers, or any third parties mentioned on the Liiv LLC (DBA of Planet Hope) Site shall be liable only to the extent of actual damages incurred by you, not to exceed U.S. $1000. Liiv LLC (DBA of Planet Hope), its licensors, its suppliers, or any third parties mentioned on the Liiv LLC (DBA of Planet Hope) Site are not liable for any personal injury, including death, caused by your use or misuse of the Site, Content, or Public Areas (as defined below). Any claims arising in connection with your use of the Site, any Content, or the Public Areas must be brought within one (1) year of the date of the event Health Manager are also used to restrict underage use of the tools. <br />\r\n		Most browser software can be set to reject all Cookies. Most browsers offer instructions on how to reset the browser to reject Cookies in the &quot;Help&quot; section of the toolbar. If you reject our Cookies, certain of the functions and conveniences of our Web site may not work properly but you do not have to accept our Cookies in order to productively use our site. We do not link Non-Personal Information from Cookies to Personally Identifiable Information without your permission and do not use Cookies to collect or store Personal Health Information about you.</li>\r\n</ol>\r\n','2010-01-04 10:57:00','2010-01-04 11:48:06',12,12);
INSERT INTO pages VALUES (8,'Privacy Policy','<p>\r\n	Liiv LLC (DBA of Planet Hope) understands how important the privacy of personal information is to our users. This Privacy Policy will tell you what information we collect about you and about your use of Liiv LLC (DBA of Planet Hope) and its services. It will explain the choices you have about how your personal information is used and how we protect that information. We urge you to read this Privacy Policy carefully.</p>\r\n<h3>\r\n	Part 1: About This Privacy Policy and Using Our Site and Tools</h3>\r\n<p>\r\n	This Privacy Policy applies to the Liiv LLC (DBA of Planet Hope) Web sites owned and operated by Liiv LLC (DBA of Planet Hope), including the Liiv LLC (DBA of Planet Hope).com site, the Liiv LLC (DBA of Planet Hope) Health Manager site, the MedicineNet.com site, the RxList.com site and the Liiv LLC (DBA of Planet Hope) Health-101 sites. Reference to &quot;Liiv LLC (DBA of Planet Hope)&quot; means Liiv LLC (DBA of Planet Hope), LLC., including any company that Liiv LLC (DBA of Planet Hope), LLC. controls (for example, a subsidiary that Liiv LLC (DBA of Planet Hope), LLC. owns). Liiv LLC (DBA of Planet Hope) Web sites include any site that Liiv LLC (DBA of Planet Hope) owns or controls (for example, a Liiv LLC (DBA of Planet Hope) Health-101 site). Liiv LLC (DBA of Planet Hope) may share information among its subsidiaries or sites that it owns or controls, but it is always protected under the terms of this Privacy Policy.</p>\r\n<p>\r\n	The Liiv LLC (DBA of Planet Hope) Web site contains links to other sites. Once you enter another Web site (whether through an advertisement, service, or content link), be aware that Liiv LLC (DBA of Planet Hope) is not responsible for the privacy practices of these other sites. We encourage you to look for and review the privacy statements of each and every Web site that you visit through a link or advertisement on Liiv LLC (DBA of Planet Hope).</p>\r\n<p>\r\n	We hope that reading our Privacy Policy helps you understand how we manage information about you. Throughout our Privacy Policy, we have underlined various terms and hot-linked them to our Glossary or to the corresponding Section within the Privacy Policy to help you better understand their meaning.</p>\r\n<p>\r\n	While you may use some of the functionality of Liiv LLC (DBA of Planet Hope) without registration, many of the specific tools and services on our website require registration. If you use our Web site without registering, the only information we collect will be Non-Personal Information through the use of Cookies or Web Beacons. If you choose to register with our Web site for certain Interactive Tools or other services, we require you to submit Personally Identifiable Information. Depending on the tool or service you have selected, we may also collect Personal Health Information. You are responsible for ensuring the accuracy of the Personally Identifiable Information and Personal Health Information you submit to Liiv LLC (DBA of Planet Hope). Inaccurate information will affect the information you receive when using our site and tools and our ability to contact you as described in this Privacy Policy. For example, your email address should be kept current because that it is how we communicate with you.</p>\r\n<p>\r\n	<a href=\"#header\">^Back to Top</a></p>\r\n<h3>\r\n	Part 2: Non-Personal Information We Collect About You</h3>\r\n<p>\r\n	Even if you do not register with Liiv LLC (DBA of Planet Hope), we collect Non-Personal Information about your use of our Web site, special promotions and newsletters.</p>\r\n<h4>\r\n	A. Cookies</h4>\r\n<p>\r\n	We collect Non-Personal Information about your use of our Web site and your use of the Web sites of selected sponsors and advertisers through the use of Cookies. Every computer that accesses a Liiv LLC (DBA of Planet Hope) Web site is assigned a different Cookie by Liiv LLC (DBA of Planet Hope). The information collected by Cookies (i) helps us dynamically generate advertising and content on Web pages or in newsletters, (ii) allows us to statistically monitor how many people are using our Web site and selected sponsors&#39; and advertisers&#39; sites, (iii) how many people open our emails, and (iv) for what purposes these actions are being taken. We may use Cookie information to target certain advertisements to your browser or to determine the popularity of certain content or advertisements. Cookies are also used to facilitate a user&#39;s log-in, as navigation aids and as session timers. Cookies used by Liiv LLC (DBA of Planet Hope) and Liiv LLC (DBA of Planet Hope) Health Manager are also used to restrict underage use of the tools.</p>\r\n<p>\r\n	Most browser software can be set to reject all Cookies. Most browsers offer instructions on how to reset the browser to reject Cookies in the &quot;Help&quot; section of the toolbar. If you reject our Cookies, certain of the functions and conveniences of our Web site may not work properly but you do not have to accept our Cookies in order to productively use our site. We do not link Non-Personal Information from Cookies to Personally Identifiable Information without your permission and do not use Cookies to collect or store Personal Health Information about you.</p>\r\n<h4>\r\n	B. Web Beacons</h4>\r\n<p>\r\n	We also may use Web Beacons to collect Non-Personal Information about your use of our Web site and the Web sites of selected sponsors and advertisers, and your use of special promotions or newsletters. The information collected by Web Beacons (i) allows us to statistically monitor how many people are using our website and selected sponsors&#39; and advertisers&#39; sites, (ii) how many people open our emails, and (iii) for what purposes these actions are being taken. Our Web Beacons are not used to track your activity outside of our Web sites or those of our sponsors&#39;. We do not link Non-Personal Information from Web Beacons to Personally Identifiable Information without your permission and do not use Web Beacons to collect or store Personal Health Information about you.</p>\r\n<p>\r\n	<a href=\"#header\">^Back to Top</a></p>\r\n<h4>\r\n	C. Third Parties Collecting Non-Personal Information On Liiv LLC (DBA of Planet Hope)&#39;s Behalf</h4>\r\n<p>\r\n	Third parties under contract with Liiv LLC (DBA of Planet Hope) may use Cookies or Web Beacons to collect Non-Personal Information about your usage of Liiv LLC (DBA of Planet Hope)&#39;s sites. These third parties may use this information, on our behalf, to help Liiv LLC (DBA of Planet Hope) target our advertising on their sites within their network, and Liiv LLC (DBA of Planet Hope) may further tailor the advertising on these third party sites based on your geographic location (zip code), gender and/or age range to the extent known by these third parties. These third parties have agreed not to use Non-Personal Information they collect from our sites except to deliver advertisements on our behalf. If your browser is set to reject cookies, the Non-Personal Information will not be sent to these third parties. Please read Part 3.A for more information on rejecting cookies.</p>\r\n<p>\r\n	<a href=\"#header\">^Back to Top</a></p>\r\n<h3>\r\n	Part 3: Personally Identifiable Information We Collect About You</h3>\r\n<p>\r\n	We collect Personally Identifiable Information, like your name, email address, date of birth, and zip code, you provide to us when you register as a member of Liiv LLC (DBA of Planet Hope) and/or when you update your member profile. We use the Personally Identifiable Information that you provide to respond to your questions, provide you the specific services you select, send you emails about Web site maintenance and updates, and inform you of significant changes to this Privacy Policy.</p>\r\n<p>\r\n	<a href=\"#header\">^Back to Top</a></p>\r\n<h4>\r\n	A. Newsletters &amp; Emails To You</h4>\r\n<p>\r\n	At registration and at various times as you use Liiv LLC (DBA of Planet Hope), you will be given the option of receiving recurring informational/promotional newsletters via email from Liiv LLC (DBA of Planet Hope) and/or directly from third parties. These emails will not contain Personal Health Information. When you sign up for our email newsletters or at any time, you can choose to Opt-In to receiving additional promotional emails from Liiv LLC (DBA of Planet Hope) or our Sponsors. In order to subscribe to Liiv LLC (DBA of Planet Hope) newsletters via email, we need your contact information, like name and email address. You can unsubscribe from the newsletters by simply clicking on the &quot;unsubscribe&quot; link at the bottom of any email newsletter. An email to our automated unsubscribe service will be created on your computer. Click the &quot;send&quot; button. You will then be unsubscribed from that newsletter within two to three business days. You may also unsubscribe or change any of your email preferences by clicking on the applicable links in your email newsletter or by changing your profile settings by clicking on any of the view/manage links at the bottom of your Liiv LLC (DBA of Planet Hope) Newsletter. If you experience difficulties with our automated unsubscribe service, please use our Customer Support Form. Liiv LLC (DBA of Planet Hope) Customer Service will manually unsubscribe you from that newsletter in two to three business days. In some cases, when you click on a link or an advertisement on our site, in an e-mail or newsletter, your browser may be momentarily directed to the website of a third party which, acting on behalf of Liiv LLC (DBA of Planet Hope) (see Disclosure to Third Party Contractor Web sites, below), notes or &quot;counts&quot; your response to the e-mail or newsletter before re-directing your browser to your selected destination; this re-direction process may not be apparent to you. </p>\r\n<p>\r\n	<a href=\"#header\">^Back to Top</a></p>\r\n','2010-01-04 10:58:35','2010-01-04 11:47:39',12,12);
INSERT INTO pages VALUES (9,'Partnership Opportunities','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam semper mollis viverra. Etiam vitae nisi a sem vulputate luctus sit amet dignissim magna. Nam nec egestas dui. Ut nunc nisl, dapibus et viverra et, volutpat a mauris. Morbi vulputate, sem hendrerit fringilla dapibus, lectus risus mollis mi, non tempus nisi eros id massa. Proin dolor neque, porttitor nec posuere id, dapibus vel erat. Nullam sollicitudin scelerisque dui sit amet aliquam. Praesent ultrices vulputate nisl nec aliquam. Duis volutpat magna id diam suscipit accumsan. Quisque vitae scelerisque lacus. In non erat nulla, sodales fringilla dolor. Nullam vitae felis ligula, sit amet pharetra nulla. Nam arcu nibh, fermentum non vulputate a, tincidunt quis urna. Quisque eu tincidunt turpis. Praesent id tempor tortor. Integer erat sem, suscipit vitae vehicula eu, consequat sed felis. Aenean pulvinar adipiscing tincidunt.</p>\r\n<p>\r\n	Suspendisse potenti. Nulla tempor quam a magna posuere a lobortis lorem ornare. Vivamus id lacus a velit sollicitudin sodales vel ac arcu. Vivamus a est eget lectus fringilla viverra. Nullam eu lectus at tortor consectetur feugiat. Integer viverra mollis nunc, sit amet iaculis est accumsan et. Integer placerat vulputate dolor vel fringilla. Fusce venenatis convallis congue. Praesent vel consequat urna. Nullam vitae risus tortor, et eleifend tortor. Phasellus id nunc quis arcu lobortis volutpat. Vivamus sagittis ultrices urna, sed hendrerit ante interdum posuere. Proin erat elit, blandit sed ornare vitae, porta tincidunt nulla. Donec scelerisque facilisis neque quis fringilla. Proin fringilla laoreet elit, a malesuada risus aliquet sodales. Donec ut justo eget quam semper fermentum ut a nunc.</p>\r\n','2010-01-06 11:15:58','2010-02-01 10:16:38',12,12);
INSERT INTO pages VALUES (10,'Contact us','<p>\r\n	Lorem ipsum</p>\r\n','2010-02-13 18:11:27','2010-02-13 18:11:27',12,12);

#
# Table structure for table 'polloptions'
#

# DROP TABLE IF EXISTS polloptions;
CREATE TABLE `polloptions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poll_id` int(10) unsigned default NULL,
  `answer` varchar(255) default NULL,
  `message` varchar(512) default NULL,
  `order` int(11) default NULL,
  `hits` int(10) unsigned default NULL,
  `published` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=209 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'polloptions'
#

INSERT INTO polloptions VALUES (199,81,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',5,NULL,NULL);
INSERT INTO polloptions VALUES (55,3,'A guy that wants to be famousab','You did it!a a',0,NULL,NULL);
INSERT INTO polloptions VALUES (198,81,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',4,NULL,NULL);
INSERT INTO polloptions VALUES (207,86,'No','Shoot, you really should leave!',1,NULL,NULL);
INSERT INTO polloptions VALUES (204,85,'yo','sadasd',1,NULL,NULL);
INSERT INTO polloptions VALUES (205,85,'asdsad','dsadasdsad',0,NULL,NULL);
INSERT INTO polloptions VALUES (177,3,'I dunnoa','Well, get some info aa',2,NULL,NULL);
INSERT INTO polloptions VALUES (179,3,'Was a news presentator without a future','Yeah! you didaf',1,NULL,NULL);
INSERT INTO polloptions VALUES (197,81,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',3,NULL,NULL);
INSERT INTO polloptions VALUES (191,61,'el front no existe??','todavia no',0,NULL,NULL);
INSERT INTO polloptions VALUES (192,63,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL);
INSERT INTO polloptions VALUES (193,63,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',1,NULL,NULL);
INSERT INTO polloptions VALUES (194,81,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL);
INSERT INTO polloptions VALUES (195,81,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',1,NULL,NULL);
INSERT INTO polloptions VALUES (196,81,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',2,NULL,NULL);
INSERT INTO polloptions VALUES (200,81,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',6,NULL,NULL);
INSERT INTO polloptions VALUES (201,81,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',7,NULL,NULL);
INSERT INTO polloptions VALUES (202,81,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',8,NULL,NULL);
INSERT INTO polloptions VALUES (203,81,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est ma','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',9,NULL,NULL);
INSERT INTO polloptions VALUES (206,86,'Yes','Then go!',0,NULL,NULL);
INSERT INTO polloptions VALUES (208,NULL,'algo','algo',NULL,NULL,NULL);

#
# Table structure for table 'polls'
#

# DROP TABLE IF EXISTS polls;
CREATE TABLE `polls` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `question` varchar(512) NOT NULL,
  `published` tinyint(1) default '1',
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(11) default NULL,
  `updatedby` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'polls'
#

INSERT INTO polls VALUES (3,'Who is Mike Amingorena?',1,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (61,'dsadasdadasd',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (62,'duele no tener el front para revisar???',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (63,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',1,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (64,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (65,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (66,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (67,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (68,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (69,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (70,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (71,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',1,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (72,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (73,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (74,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (75,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (76,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (77,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (78,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',1,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (79,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (80,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare risus. Cras ipsum nibh, dictum lu',1,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (81,'01 - Tiene muchas answers este por las dudas',1,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (83,'test',1,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (84,'tst2',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (85,'Quienes usan ie?',1,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (82,'                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed metus leo. Morbi convallis euismod erat, sit amet sagittis metus pretium vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent a est massa, dictum eleifend dui. Nam sem nulla, commodo ac tempus vel, blandit a orci. Nunc in sem vitae lacus malesuada dignissim vitae id sapien. Mauris vitae erat libero. Curabitur non cursus orci. Vestibulum ullamcorper ornare ris',0,NULL,NULL,NULL,NULL);
INSERT INTO polls VALUES (86,'Should I leave right now?',0,'2009-12-18 20:12:14','2009-12-18 20:12:14',5,5);

#
# Table structure for table 'positions'
#

# DROP TABLE IF EXISTS positions;
CREATE TABLE `positions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(80) default NULL,
  `identifier` varchar(45) default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'positions'
#

INSERT INTO positions VALUES (2,'980 x 30 Home Page Top','homepage-top','2009-12-30 14:53:26','2010-02-01 12:42:24');
INSERT INTO positions VALUES (3,'980 x 30 Home Page Middle','homepage-middle','2009-12-30 14:54:43','2010-02-01 12:42:38');
INSERT INTO positions VALUES (4,'728 x 90 Conditions, Therapies, Traditions','header','2010-01-04 12:36:37','2010-02-01 12:41:27');
INSERT INTO positions VALUES (5,'300 x 250 1','box_a','2010-01-04 15:40:52','2010-01-04 15:41:43');
INSERT INTO positions VALUES (6,'300 x 250 Home Page','homepage-box','2010-01-05 10:15:16','2010-02-01 12:41:44');
INSERT INTO positions VALUES (7,'300 x 100 Conditions, Therapies, Traditions','box_b','2010-01-05 10:50:00','2010-02-01 12:41:18');

#
# Table structure for table 'products'
#

# DROP TABLE IF EXISTS products;
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'products'
#


#
# Table structure for table 'publications'
#

# DROP TABLE IF EXISTS publications;
CREATE TABLE `publications` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(128) default NULL,
  `author` varchar(128) default NULL,
  `authority_id` int(10) unsigned default NULL,
  `organization_id` int(10) unsigned default NULL,
  `description_short` text,
  `content` text,
  `link` varchar(150) default NULL,
  `area_id` int(11) unsigned default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  `published` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'publications'
#

INSERT INTO publications VALUES (1,'Test Publication',NULL,NULL,NULL,'test','',NULL,1,'2009-11-05 12:02:20','2009-11-24 10:21:36',NULL,NULL,0);
INSERT INTO publications VALUES (2,'test public',NULL,NULL,NULL,'Description','contentntnentet','',1,'2009-11-24 10:36:54','2009-11-24 10:53:56',NULL,NULL,0);
INSERT INTO publications VALUES (3,'Test p',NULL,1,2,'dasdsada','dsadasd',NULL,1,'2009-11-24 10:38:08','2009-11-24 10:39:20',NULL,NULL,0);

#
# Table structure for table 'publications_conditions'
#

# DROP TABLE IF EXISTS publications_conditions;
CREATE TABLE `publications_conditions` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `publication_id` int(11) unsigned default NULL,
  `condition_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'publications_conditions'
#


#
# Table structure for table 'publications_events'
#

# DROP TABLE IF EXISTS publications_events;
CREATE TABLE `publications_events` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `publication_id` int(11) unsigned default NULL,
  `event_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'publications_events'
#

INSERT INTO publications_events VALUES (12,1,1);

#
# Table structure for table 'publications_publications'
#

# DROP TABLE IF EXISTS publications_publications;
CREATE TABLE `publications_publications` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `publication_id` int(11) unsigned default NULL,
  `publication_sec_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'publications_publications'
#

INSERT INTO publications_publications VALUES (2,1,1);

#
# Table structure for table 'publications_stories'
#

# DROP TABLE IF EXISTS publications_stories;
CREATE TABLE `publications_stories` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `publication_id` int(11) unsigned default NULL,
  `story_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'publications_stories'
#


#
# Table structure for table 'publications_therapies'
#

# DROP TABLE IF EXISTS publications_therapies;
CREATE TABLE `publications_therapies` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `publication_id` int(11) unsigned default NULL,
  `therapy_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'publications_therapies'
#


#
# Table structure for table 'recommended_media'
#

# DROP TABLE IF EXISTS recommended_media;
CREATE TABLE `recommended_media` (
  `id` int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table 'recommended_media'
#


#
# Table structure for table 'resolutions'
#

# DROP TABLE IF EXISTS resolutions;
CREATE TABLE `resolutions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `ticket_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `comment` varchar(1024) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Dumping data for table 'resolutions'
#


#
# Table structure for table 'search_index'
#

# DROP TABLE IF EXISTS search_index;
CREATE TABLE `search_index` (
  `id` int(11) NOT NULL auto_increment,
  `association_key` varchar(36) NOT NULL,
  `model` varchar(128) NOT NULL,
  `data` longtext NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `association_key` (`association_key`,`model`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'search_index'
#

INSERT INTO search_index VALUES (1,'56','Video','Marad\'o Marad\'o. . god. The best football goal in human history.. http://www.youtube.com/watch?v=324UsTQsHAM. ','2010-02-11 19:20:10','2010-02-11 20:31:03');
INSERT INTO search_index VALUES (2,'57','Video','Soda Stereo - De Musica Ligera (El Ultimo Concierto). . . Soda Stereo - De M\'usica Ligera\r\nEl Ultimo Concierto (En Vivo)\r\n20 De Septiembre de 1997\r\nEstadio de River Plate, Argentina. http://www.youtube.com/watch?v=otLlQbDJiOs. ','2010-02-11 20:38:54','2010-02-11 20:38:54');
INSERT INTO search_index VALUES (3,'9','Condition','Insomnia. \r\n	Getting a good night\'s sleep is no simple task for 1 in 3 adults. Fortunately, there are many conventional and holistic treatments to overcome this debilitating problem. It\'s during deep sleep that the restorative functions of the body take place supporting your overall state of health and wellbeing.\r\n. \r\n	Insomnia is a common condition in which you have trouble falling or staying asleep. The condition can range from mild to severe, depending on how often it occurs and for how long.\r\n. \r\n	How much sleep is enough? Most adults need seven to eight hours a night. More than one-third of adults have occasional insomnia, while 10 to 15 percent report long-term, chronic sleep problems.\r\n\r\n	Good sleep is as essential for your health, wellness and quality of life as oxygen. The rhythms of sleep and wakefulness are always present in the rhythms of the seasons, and in every species and the natural world. Birdsong stops at the end of the day, and some insects gear up only when the sun goes down. Bears hibernate for months at a time, and many trees and shrubs shed their leaves, laying dormant until the warmth of spring awakens them. It is during the deep sleep phase that growth and healing occur. If you are coping with some form of insomnia, you miss the benefit of the rejuvenation that occurs during the deep sleep cycle.\r\n\r\n	Chronic (ongoing) insomnia means you experience sleep difficulty at least 3 nights a week for more than a month, whereas acute (short-term) insomnia is the term used to describe a sleep problem of a lesser duration.\r\n\r\n	Insomnia can stem from a medical condition, or other medical conditions that cause chronic pain, (arthritis, fibromyalgia, etc.) or from the use of substances that interfere with sleep. This is known as secondary insomnia. Traditional Medicine as we know it in the west, lists some of the, common causes of insomnia:\r\n\r\n	\r\n		Stress\r\n	\r\n		Anxiety\r\n	\r\n		Depression\r\n	\r\n		Poor sleep habits\r\n	\r\n		Changes in your environment\r\n	\r\n		Changes in your work schedule\r\n	\r\n		Using certain medications, or substances such as caffeine, nicotine & alcohol. \r\n\r\n\r\n	Primary insomnia is its own disorder and is not the result of another condition. It is an ongoing condition of sleep disruption, difficulty sleeping, and poor quality of sleep.\r\n. \r\n	Getting a good night\'s sleep is no simple task for 1 in 3 adults. Fortunately, there are many conventional and holistic treatments to overcome this debilitating problem. It\'s during deep sleep that the restorative functions of the body take place supporting your overall state of health and wellbeing.\r\n. test\r\ntest\r\ntest\r\n. test\r\ntest\r\ntest\r\n. \r\n	\r\n		A traditional (western) medical doctor typically explores your medical history and tries to get rid of your symptoms, very often through the use of pills or other pharmaceutical products. Holistic practioners will factor your medical history into a greater equation - one that looks for the root cause of the imbalance underlying your insomnia. They consider the various components of your life such as - your medical history, life circumstance, general nutrition, exercise, level of stress, etc. After doing the math so to speak - assessing the impact of all these elements on your health, general well-being and spirit, a holistic practitioner prescribes the course of treatment that will best heal the imbalance and re-establish a normal sleep cycle. In short, Holistic practitioners treat the whole person - physically, emotionally, and mentally. \r\n\r\n. \r\n	A traditional (western) medical doctor typically explores your medical history and tries to get rid of your symptoms, very often through the use of pills or other pharmaceutical products. Holistic practioners will factor your medical history into a greater equation - one that looks for the root cause of the imbalance underlying your insomnia. They consider the various components of your life such as - your medical history, life circumstance, general nutrition, exercise, level of stress, etc. After doing the math so to speak - assessing the impact of all these elements on your health, general well-being and spirit, a holistic practitioner prescribes the course of treatment that will best heal the imbalance and re-establish a normal sleep cycle. In short, Holistic practitioners treat the whole person - physically, emotionally, and mentally. \r\n. \r\n	helloooooo\r\n','2010-02-12 10:57:58','2010-02-12 11:19:43');
INSERT INTO search_index VALUES (4,'22','Condition','testing reference field. \r\n	dasdsadsa\r\n. \r\n	dasdsadsa\r\n. \r\n	dasdsadsa\r\n. 1test \r\n2te\r\n3s\r\n4tingggggggggggggggg ggggggggggg ggggggggggggggg ggggggggggggg \r\n5test\r\n6test\r\n7test\r\n8test\r\n9test\r\n10test\r\n11test\r\n12test\r\n13test\r\n14test\r\n15test. \r\n	dasdsadsa\r\n. \r\n	dasdsadsa\r\n. \r\n	dasdsadsa\r\n','2010-02-12 11:23:59','2010-02-12 17:59:52');
INSERT INTO search_index VALUES (5,'58','Video','QuickTime H.264 test. Alex. Test of QuickTime (.mov) H.264-encoded video. /videos/158776260.mov. ','2010-02-12 17:45:11','2010-02-15 15:52:21');
INSERT INTO search_index VALUES (6,'23','Condition','asd. \r\n	 \r\n\r\n	\r\n		Chronic fatigue syndrome is a complicated disorder characterized by extreme fatigue that may worsen with physical or mental activity and that does not improve with rest1. The best recovery rates have been experienced by people who used both traditional and alternative healing modalities2.\r\n\r\n. \r\n	The defining characteristic of the disorder is a severe and lasting fatigue that is not linked to an existing medical condition and has lasted six months or longer. The severity of CFS varies from patient to patient. Some people maintain a fairly active lifestyle. For the most symptomatic patients, however, CFS significantly limits work, school, and family activities4. The type of fatigue characteristic of this condition is not the kind of fatigue we experience after a particularly busy day, a sleepless night, or a single stressful event. It\'s a severe, incapacitating fatigue that isn\'t improved by bed rest and that may be worsened by physical or mental activity. It\'s an all-encompassing fatigue that results in a dramatic decline in both activity level and stamina.\r\n\r\n	\r\n		\r\n			\r\n				\r\n					\r\n						 \r\n					\r\n						CFS often follows a cyclical course, alternating between periods of illness and relative well-being. Some patients experienced partial or complete remission of symptoms during the course of the illness, but symptoms often recur. This pattern of remission and relapse makes chronic fatigue especially hard for patients to manage. Patients who are in remission may be tempted to overdo activities when they\'re feeling better, which can actually cause a relapse5. Therefore, it is of the utmost importance to identify a balanced rhythm in your life--one that includes equal measures of rest, exertion, relaxation, work, good nutrition, detoxification, meditation, and community.\r\n				\r\n			\r\n		\r\n	\r\n\r\n. \r\n	The defining characteristic of the disorder is a severe and lasting fatigue that is not linked to an existing medical condition and has lasted six months or longer. The severity of CFS varies from patient to patient. Some people maintain a fairly active lifestyle. For the most symptomatic patients, however, CFS significantly limits work, school, and family activities4. The type of fatigue characteristic of this condition is not the kind of fatigue we experience after a particularly busy day, a sleepless night, or a single stressful event. It\'s a severe, incapacitating fatigue that isn\'t improved by bed rest and that may be worsened by physical or mental activity. It\'s an all-encompassing fatigue that results in a dramatic decline in both activity level and stamina.\r\n\r\n	\r\n		 \r\n	\r\n		CFS often follows a cyclical course, alternating between periods of illness and relative well-being. Some patients experienced partial or complete remission of symptoms during the course of the illness, but symptoms often recur. This pattern of remission and relapse makes chronic fatigue especially hard for patients to manage. Patients who are in remission may be tempted to overdo activities when they\'re feeling better, which can actually cause a relapse5. Therefore, it is of the utmost importance to identify a balanced rhythm in your life--one that includes equal measures of rest, exertion, relaxation, work, good nutrition, detoxification, meditation, and community.\r\n\r\n. dfsafasfasf fsafasfasfas fasfas fsa fsafsa fsa fsa safa fs fs fsf\r\ndasdsasda\r\ndsadasd\r\nasdas\r\ndas\r\ndas\r\ndfsafasfasf fsafasfasfas fasfas fsa fsafsa fsa fsa safa fs fs fsf dfsafasfasf fsafasfasfas fasfas fsa fsafsa fsa fsa safa fs fs fsf dfsafasfasf fsafasfasfas fasfas fsa fsafsa fsa fsa safa fs fs fsf. \r\n	Where once there was doubt, there is now widespread acceptance of the syndrome as a medical disorder. Increasingly, medical doctors understand the value of energy-based disciplines such as yoga and tai chi, and prescribe alternative therapies for treatment of symptoms that frequently accompany chronic fatigue syndrome. If an infection is present, a medical doctor may prescribe antiviral or antibacterial medications.\r\n\r\n	\r\n		\r\n			\r\n				\r\n				\r\n			\r\n				REFERENCES:\r\n			\r\n				 \r\n			\r\n				1Mayo Clinic.com, \"Chronic Fatigue Syndrome,\" at http://www.mayoclinic.com/health/chronic-fatigue-syndrome/DS00395.\r\n			\r\n				2Chronic Fatigue.org, \"The Web Page for Chronic Fatigue Sufferers,\" at http://www.chronicfatigue.org.\r\n			\r\n				3PsychCentral.com, Rick Nauert, Ph.D., Emerging Theory on Chronic Fatigue, at http://psychcentral.com/news/2008/06/25/new-theory-on-chronic-fatigue/2501.html.\r\n			\r\n				4Centers for Disease Control (CDC), \"Symptoms of CFS,\" at http://www.CDC.gov/cfs/cfssymptoms.htm#primary.\r\n			\r\n				5CDC, \"Symptoms,\" http://www.CDC.gov//cfs/cfssymptoms.htm#primary.\r\n			\r\n				6CDC, \"Symptoms,\" http://www.CDC.gov//cfs/cfssymptoms.htm#primary.\r\n			\r\n				7Fibromyalgia and Fatigue Centers, \"Multi-faceted Treatment Approach,\" at http://www.five growfibroandfatigue.com/clinical.php#6.\r\n			\r\n				\r\n				\r\n		\r\n	\r\n\r\n. \r\n	 \r\n\r\n	The integrative approach also includes a full spectrum of lab tests--traditional and alternative. In particular, an integrative physician will order studies of saliva and urine to assess your adrenal glands, as well as the levels of neurotransmitters in your system. Holistic treatment is likely to focus on replenishing your adrenal glands, also known as the \"fight or flight glands.\" Prolonged periods of elevated levels of adrenaline and cortisol (the fight or flight hormones) erode your immune system. In addition, an integrative physician or practitioner will screen for oxidative stress.\r\n\r\n	 \r\n\r\n	Your practitioner may prescribe neurotransmitters--chemicals in your body that not only regulate how you feel and think, but how your body operates. Very often, through supplementation, balanced nutrition, exercise, and rest, the holistic doctor or practitioner is able to move you towards remission of the disabling symptoms of chronic fatigue syndrome.\r\n. \r\n	 \r\n\r\n	The integrative approach also includes a full spectrum of lab tests--traditional and alternative. In particular, an integrative physician will order studies of saliva and urine to assess your adrenal glands, as well as the levels of neurotransmitters in your system. Holistic treatment is likely to focus on replenishing your adrenal glands, also known as the \"fight or flight glands.\" Prolonged periods of elevated levels of adrenaline and cortisol (the fight or flight hormones) erode your immune system. In addition, an integrative physician or practitioner will screen for oxidative stress.\r\n\r\n	 \r\n\r\n	Your practitioner may prescribe neurotransmitters--chemicals in your body that not only regulate how you feel and think, but how your body operates. Very often, through supplementation, balanced nutrition, exercise, and rest, the holistic doctor or practitioner is able to move you towards remission of the disabling symptoms of chronic fatigue syndrome.\r\n','2010-02-13 11:07:52','2010-02-15 15:16:33');
INSERT INTO search_index VALUES (7,'14','Condition','Test Condition. \r\n	Chronic fatigue syndrome is a complicated disorder characterized by extreme fatigue that may worsen with physical or mental activity and that does not improve with rest1. The best recovery rates have been experienced by people who used both traditional and alternative healing modalities2.\r\n. \r\n	There has been great debate about how to best define chronic fatigue syndrome (CFS). It was once thought that this condition was the result of a viral infection. However, this is not the case. The disease typically affects working-age adults and is often triggered by a flu-like episode3. Other conditions that seem to trigger the development of CFS include viral infections, transient traumatic conditions, stress, and toxins.\r\n. \r\n	The defining characteristic of the disorder is a severe and lasting fatigue that is not linked to an existing medical condition and has lasted six months or longer. The severity of CFS varies from patient to patient. Some people maintain a fairly active lifestyle. For the most symptomatic patients, however, CFS significantly limits work, school, and family activities4. The type of fatigue characteristic of this condition is not the kind of fatigue we experience after a particularly busy day, a sleepless night, or a single stressful event. It\'s a severe, incapacitating fatigue that isn\'t improved by bed rest and that may be worsened by physical or mental activity. It\'s an all-encompassing fatigue that results in a dramatic decline in both activity level and stamina.\r\n\r\n	\r\n		 \r\n	\r\n		CFS often follows a cyclical course, alternating between periods of illness and relative well-being. Some patients experienced partial or complete remission of symptoms during the course of the illness, but symptoms often recur. This pattern of remission and relapse makes chronic fatigue especially hard for patients to manage. Patients who are in remission may be tempted to overdo activities when they\'re feeling better, which can actually cause a relapse5. Therefore, it is of the utmost importance to identify a balanced rhythm in your life--one that includes equal measures of rest, exertion, relaxation, work, good nutrition, detoxification, meditation, and community.\r\n\r\n. test left\r\ntest\r\ntest\r\ntest. \r\n	The standard medical approach for treating chronic fatigue syndrome is to first diagnose the condition and to rule out any bacterial or viral infection. Standard lab tests and a complete physical may be done. Typically, a medical doctor will then implement stopgap measures to help patients manage their symptoms7. For example, a sleep medication may be prescribed to help with the broken sleep patterns experienced by many patients with chronic fatigue. Through lab studies and a physical exam, the standard medical approach will also rule-out any underlying conditions.\r\n\r\n	\r\n		Where once there was doubt, there is now widespread acceptance of the syndrome as a medical disorder. Increasingly, medical doctors understand the value of energy-based disciplines such as yoga and tai chi, and prescribe alternative therapies for treatment of symptoms that frequently accompany chronic fatigue syndrome. If an infection is present, a medical doctor may prescribe antiviral or antibacterial medications.\r\n	\r\n		 \r\n	\r\n		REFERENCES:\r\n	\r\n		 \r\n	\r\n		1Mayo Clinic.com, \"Chronic Fatigue Syndrome,\" at http://www.mayoclinic.com/health/chronic-fatigue-syndrome/DS00395.\r\n	\r\n		2Chronic Fatigue.org, \"The Web Page for Chronic Fatigue Sufferers,\" at http://www.chronicfatigue.org.\r\n	\r\n		3PsychCentral.com, Rick Nauert, Ph.D., Emerging Theory on Chronic Fatigue, at http://psychcentral.com/news/2008/06/25/new-theory-on-chronic-fatigue/2501.html.\r\n	\r\n		4Centers for Disease Control (CDC), \"Symptoms of CFS,\" at http://www.CDC.gov/cfs/cfssymptoms.htm#primary.\r\n	\r\n		5CDC, \"Symptoms,\" http://www.CDC.gov//cfs/cfssymptoms.htm#primary.\r\n	\r\n		6CDC, \"Symptoms,\" http://www.CDC.gov//cfs/cfssymptoms.htm#primary.\r\n	\r\n		7Fibromyalgia and Fatigue Centers, \"Multi-faceted Treatment Approach,\" at http://www.five growfibroandfatigue.com/clinical.php#6.\r\n\r\n. \r\n	The integrative approach also includes a full spectrum of lab tests--traditional and alternative. In particular, an integrative physician will order studies of saliva and urine to assess your adrenal glands, as well as the levels of neurotransmitters in your system. Holistic treatment is likely to focus on replenishing your adrenal glands, also known as the \"fight or flight glands.\" Prolonged periods of elevated levels of adrenaline and cortisol (the fight or flight hormones) erode your immune system. In addition, an integrative physician or practitioner will screen for oxidative stress.\r\n\r\n	\r\n		 \r\n	\r\n		Your practitioner may prescribe neurotransmitters--chemicals in your body that not only regulate how you feel and think, but how your body operates. Very often, through supplementation, balanced nutrition, exercise, and rest, the holistic doctor or practitioner is able to move you towards remission of the disabling symptoms of chronic fatigue syndrome.\r\n\r\n. \r\n	The integrative approach also includes a full spectrum of lab tests--traditional and alternative. In particular, an integrative physician will order studies of saliva and urine to assess your adrenal glands, as well as the levels of neurotransmitters in your system. Holistic treatment is likely to focus on replenishing your adrenal glands, also known as the \"fight or flight glands.\" Prolonged periods of elevated levels of adrenaline and cortisol (the fight or flight hormones) erode your immune system. In addition, an integrative physician or practitioner will screen for oxidative stress.\r\n\r\n	\r\n		 \r\n	\r\n		Your practitioner may prescribe neurotransmitters--chemicals in your body that not only regulate how you feel and think, but how your body operates. Very often, through supplementation, balanced nutrition, exercise, and rest, the holistic doctor or practitioner is able to move you towards remission of the disabling symptoms of chronic fatigue syndrome.\r\n\r\n','2010-02-13 12:19:32','2010-02-13 12:21:04');
INSERT INTO search_index VALUES (8,'53','Video','Codehunters. Axis Animationnnnnnnnnnasdjgbh. Codehunters was created by the awesome Ben Hibon for MTV Asia and was broadcast as both a standalone short film and used to brand the MTV Asia Music Video Awards.. http://vimeo.com/7432584. ','2010-02-15 15:17:10','2010-02-15 17:14:16');
INSERT INTO search_index VALUES (9,'52','Video','Black Sabbath - Iron Man. . A bunch of awesome Iron Man pictures with Iron Man sung by Black Sabbath playing in the background.. http://www.youtube.com/watch?v=GECit4oSpbk. ','2010-02-15 16:22:33','2010-02-15 16:26:16');
INSERT INTO search_index VALUES (10,'59','Video','true h264 test. truetest. . . /videos/31106187.mp4. ','2010-02-15 18:10:10','2010-02-15 18:10:10');
INSERT INTO search_index VALUES (11,'50','Video','eTrade Babies. etrade. test short description. http://www.youtube.com/watch?v=uHPg262Kr9c&feature=player_embedded. ','2010-02-16 10:10:03','2010-02-16 10:28:52');

#
# Table structure for table 'slides'
#

# DROP TABLE IF EXISTS slides;
CREATE TABLE `slides` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `content` varchar(128) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `link_title` varchar(64) default 'Click Here',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'slides'
#

INSERT INTO slides VALUES (1,'/img/slider/72896662.jpg','Conditions Home Page','<p>\r\n	just a test - to navigate to the conditions home page.</p>\r\n','http://liiv.codigoaustral.com/conditions','2009-12-22 12:43:17','2010-02-13 12:07:26','Click Here dude!');
INSERT INTO slides VALUES (2,'/img/slider/159331437.jpg','Therapies Home Page','<p>\r\n	test navigation to Therapies Home Page</p>\r\n','http://liiv.codigoaustral.com/therapies','2009-12-22 12:43:55','2010-01-12 16:28:50','Click Here');
INSERT INTO slides VALUES (3,'/img/slider/43924362.jpg','Stress','<p>\r\n	stress page</p>\r\n','http://liiv.codigoaustral.com/stress','2009-12-22 12:44:57','2010-01-12 16:36:01','Click Here');

#
# Table structure for table 'stories'
#

# DROP TABLE IF EXISTS stories;
CREATE TABLE `stories` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(128) default NULL,
  `content` text,
  `photo` varchar(255) default NULL,
  `area_id` int(11) default NULL,
  `user_id` int(11) unsigned default NULL,
  `media` varchar(255) default NULL,
  `updated` datetime default NULL,
  `created` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  `when` date default NULL,
  `begin_publishing` datetime default NULL,
  `end_publishing` datetime default NULL,
  `liiv_today_type_id` int(10) unsigned NOT NULL default '4',
  `featured` tinyint(1) default '0',
  `path` varchar(120) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'stories'
#

INSERT INTO stories VALUES (3,'testing sluggabble','<p>\r\n	Aliquam arcu nisi, congue non interdum vel, tristique non erat. Donec ultricies tincidunt magna sodales elementum. Aenean vitae nisl non justo tincidunt vestibulum. Suspendisse massa felis, pretium sed iaculis id, tempor et odio. Nam porttitor purus sit amet elit imperdiet pretium. Donec sollicitudin facilisis enim sit amet aliquet. Sed eu dui vitae dolor accumsan varius vel et magna. Morbi vitae sem nisl, dignissim tristique tellus.</p>\r\n<p>\r\n	Proin interdum turpis id turpis pellentesque eu fermentum felis gravida. Curabitur elementum iaculis nulla in tristique. Vivamus risus est, euismod et gravida sit amet, cursus convallis mauris. Quisque facilisis est sit amet sapien fermentum vehicula condimentum ut velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut at metus lectus, faucibus suscipit nulla. Praesent sed sapien felis, ac vulputate lorem. Suspendisse velit nunc, commodo in eleifend et, volutpat egestas eros. Donec erat libero, vehicula ac egestas a, ornare et velit. Sed non nisl in sapien pretium bibendum. Curabitur consequat, erat non accumsan dignissim, massa ante rutrum mauris, eget vulputate ipsum lacus in mi. Quisque viverra, tortor quis tempor dictum, nisl enim elementum turpis, et ullamcorper felis massa ac risus. Fusce dapibus, metus et tristique hendrerit, mi neque cursus libero, vel viverra ligula leo ac ipsum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In varius, tortor sit amet venenatis eleifend, lacus augue sollicitudin justo, nec euismod metus turpis non eros. Nulla accumsan, libero eu eleifend ultrices, libero augue ornare lectus, at porttitor odio eros id nulla.</p>\r\n<p>\r\n	Duis elementum purus et ipsum pretium porta. Aenean blandit vestibulum ullamcorper. Donec mauris odio, convallis ut interdum a, scelerisque venenatis leo. Mauris velit ligula, varius id dictum ac, egestas in sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin auctor, odio vitae feugiat pretium, odio nunc mollis diam, at vestibulum dolor tortor vel turpis. Vestibulum ac nibh tortor. In erat risus, fermentum et laoreet non, luctus et metus. Donec dui magna, gravida sed elementum id, semper et purus. Vivamus nisl ipsum, dictum quis tincidunt a, faucibus quis ligula. Donec semper, tortor at feugiat sagittis, felis nibh pharetra tellus, nec ultrices mauris turpis id sem. Duis dignissim justo non eros lacinia quis sollicitudin risus lacinia. Mauris et ante ac massa semper imperdiet vel vel dolor. Praesent quis ipsum justo, eu porttitor magna.</p>\r\n','',1,22,'','2010-02-02 15:12:49','2010-02-02 15:12:49',12,12,'0000-00-00',NULL,NULL,4,0,'testing-sluggabble');
INSERT INTO stories VALUES (7,'testing sluggabble','<p>\r\n	2Aliquam arcu nisi, congue non interdum vel, tristique non erat. Donec ultricies tincidunt magna sodales elementum. Aenean vitae nisl non justo tincidunt vestibulum. Suspendisse massa felis, pretium sed iaculis id, tempor et odio. Nam porttitor purus sit amet elit imperdiet pretium. Donec sollicitudin facilisis enim sit amet aliquet. Sed eu dui vitae dolor accumsan varius vel et magna. Morbi vitae sem nisl, dignissim tristique tellus.</p>\r\n<p>\r\n	Proin interdum turpis id turpis pellentesque eu fermentum felis gravida. Curabitur elementum iaculis nulla in tristique. Vivamus risus est, euismod et gravida sit amet, cursus convallis mauris. Quisque facilisis est sit amet sapien fermentum vehicula condimentum ut velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut at metus lectus, faucibus suscipit nulla. Praesent sed sapien felis, ac vulputate lorem. Suspendisse velit nunc, commodo in eleifend et, volutpat egestas eros. Donec erat libero, vehicula ac egestas a, ornare et velit. Sed non nisl in sapien pretium bibendum. Curabitur consequat, erat non accumsan dignissim, massa ante rutrum mauris, eget vulputate ipsum lacus in mi. Quisque viverra, tortor quis tempor dictum, nisl enim elementum turpis, et ullamcorper felis massa ac risus. Fusce dapibus, metus et tristique hendrerit, mi neque cursus libero, vel viverra ligula leo ac ipsum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In varius, tortor sit amet venenatis eleifend, lacus augue sollicitudin justo, nec euismod metus turpis non eros. Nulla accumsan, libero eu eleifend ultrices, libero augue ornare lectus, at porttitor odio eros id nulla.</p>\r\n<p>\r\n	Duis elementum purus et ipsum pretium porta. Aenean blandit vestibulum ullamcorper. Donec mauris odio, convallis ut interdum a, scelerisque venenatis leo. Mauris velit ligula, varius id dictum ac, egestas in sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin auctor, odio vitae feugiat pretium, odio nunc mollis diam, at vestibulum dolor tortor vel turpis. Vestibulum ac nibh tortor. In erat risus, fermentum et laoreet non, luctus et metus. Donec dui magna, gravida sed elementum id, semper et purus. Vivamus nisl ipsum, dictum quis tincidunt a, faucibus quis ligula. Donec semper, tortor at feugiat sagittis, felis nibh pharetra tellus, nec ultrices mauris turpis id sem. Duis dignissim justo non eros lacinia quis sollicitudin risus lacinia. Mauris et ante ac massa semper imperdiet vel vel dolor. Praesent quis ipsum justo, eu porttitor magna.</p>\r\n','',1,1,'','2010-02-02 18:05:30','2010-02-02 18:05:30',12,12,'0000-00-00',NULL,NULL,4,0,'testing-sluggabble-1');

#
# Table structure for table 'stories_conditions'
#

# DROP TABLE IF EXISTS stories_conditions;
CREATE TABLE `stories_conditions` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `story_id` int(11) unsigned default NULL,
  `condition_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'stories_conditions'
#


#
# Table structure for table 'stories_organizations'
#

# DROP TABLE IF EXISTS stories_organizations;
CREATE TABLE `stories_organizations` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `story_id` int(11) unsigned default NULL,
  `organization_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'stories_organizations'
#


#
# Table structure for table 'stories_products'
#

# DROP TABLE IF EXISTS stories_products;
CREATE TABLE `stories_products` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `story_id` int(11) unsigned default NULL,
  `product_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'stories_products'
#


#
# Table structure for table 'stories_stories'
#

# DROP TABLE IF EXISTS stories_stories;
CREATE TABLE `stories_stories` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `story_id` int(11) unsigned default NULL,
  `story_sec_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'stories_stories'
#


#
# Table structure for table 'stories_therapies'
#

# DROP TABLE IF EXISTS stories_therapies;
CREATE TABLE `stories_therapies` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `story_id` int(11) unsigned default NULL,
  `therapy_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'stories_therapies'
#


#
# Table structure for table 'stories_videos'
#

# DROP TABLE IF EXISTS stories_videos;
CREATE TABLE `stories_videos` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `story_id` int(11) unsigned default NULL,
  `video_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'stories_videos'
#


#
# Table structure for table 'subscribers'
#

# DROP TABLE IF EXISTS subscribers;
CREATE TABLE `subscribers` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(100) default NULL,
  `last_name` varchar(100) default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'subscribers'
#

INSERT INTO subscribers VALUES (8,'rafamaaaafa@rafaamaafa.com','Rafa','Mafa',NULL,NULL);
INSERT INTO subscribers VALUES (9,'rasdafamaaaafa@rafaamaafa.com','Rafa','Mafa',NULL,NULL);
INSERT INTO subscribers VALUES (10,'rasdafadsadmaaaafa@rafaamaafa.com','Rafa','Mafa',NULL,NULL);
INSERT INTO subscribers VALUES (11,'rasdafadasdsadmaaaafa@rafaamaafa.com','Rafa','Mafa',NULL,NULL);
INSERT INTO subscribers VALUES (12,'rasdafadasdfafsadmaaaafa@rafaamaafa.com','Rafa','Mafa',NULL,NULL);
INSERT INTO subscribers VALUES (13,'adsada@dada.com','dsadadadas','dsadad',NULL,NULL);
INSERT INTO subscribers VALUES (14,'dsadasd@dada.com','rafa','adasda',NULL,NULL);
INSERT INTO subscribers VALUES (15,'dada@dada.com','dada','dada',NULL,NULL);
INSERT INTO subscribers VALUES (16,'rara@rara.com','rara','rara',NULL,NULL);
INSERT INTO subscribers VALUES (17,'dadas@dada.com','dsadas','dsadad',NULL,NULL);
INSERT INTO subscribers VALUES (18,'ddadsafas@ddad.com','dada','dada',NULL,NULL);
INSERT INTO subscribers VALUES (19,'dasdasd@dadad.com','dadsad','dasda',NULL,NULL);
INSERT INTO subscribers VALUES (20,'dasdsa@dada.co','dadasda','sadasdsa',NULL,NULL);
INSERT INTO subscribers VALUES (21,'rara@erara.com','rarfr','rarar',NULL,NULL);
INSERT INTO subscribers VALUES (22,'dsadas@dadad.com','dsadsa','dsadasd',NULL,NULL);
INSERT INTO subscribers VALUES (23,'rara@rararar.com','rafa','rara','2010-02-09 17:34:41','2010-02-09 17:34:41');
INSERT INTO subscribers VALUES (24,'rara@rarardadadar.com','rafa','rara','2010-02-09 17:35:03','2010-02-09 17:35:03');
INSERT INTO subscribers VALUES (25,'dasdada@dadad.com','rafa','poni','2010-02-09 17:59:27','2010-02-09 17:59:27');
INSERT INTO subscribers VALUES (26,'gdsggdsgsd@dada.com','dsadasd','last name','2010-02-09 18:09:07','2010-02-09 18:09:07');
INSERT INTO subscribers VALUES (27,'rafafa@dada.com','dsadasd','dsadasdas','2010-02-09 18:11:42','2010-02-09 18:11:42');

#
# Table structure for table 'subscriptions'
#

# DROP TABLE IF EXISTS subscriptions;
CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `type` int(11) NOT NULL default '0',
  `entity_id` int(11) NOT NULL,
  `item` int(11) NOT NULL default '0',
  `created` datetime default NULL,
  `updated` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

#
# Dumping data for table 'subscriptions'
#


#
# Table structure for table 'symptoms'
#

# DROP TABLE IF EXISTS symptoms;
CREATE TABLE `symptoms` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(128) NOT NULL,
  `definition` text,
  `area_id` int(11) unsigned default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  `published` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'symptoms'
#

INSERT INTO symptoms VALUES (1,'test symptom','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.',3,NULL,'2009-11-26 16:20:16',NULL,1,0);
INSERT INTO symptoms VALUES (3,'test symptom 2','Praesent sed dolor nisl, vitae rutrum nisi. Suspendisse non velit nisl, nec consequat augue. Integer mauris metus, ornare sed sodales vitae, ',1,NULL,'2009-11-26 14:19:14',NULL,1,0);
INSERT INTO symptoms VALUES (4,'test symptom 3','Praesent sed dolor nisl, vitae rutrum nisi. Suspendisse non velit nisl, nec consequat augue. Integer mauris metus, ornare sed sodales vitae, ',1,NULL,NULL,NULL,NULL,0);
INSERT INTO symptoms VALUES (5,'can\'t sleep','',1,NULL,NULL,NULL,NULL,0);
INSERT INTO symptoms VALUES (7,'testttttttttt','tttest symptoms!!!',1,'2010-02-04 17:03:24','2010-02-04 17:04:08',12,12,0);

#
# Table structure for table 'terms'
#

# DROP TABLE IF EXISTS terms;
CREATE TABLE `terms` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `term` varchar(150) NOT NULL,
  `definition` text NOT NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Glossary terms table';

#
# Dumping data for table 'terms'
#


#
# Table structure for table 'therapies'
#

# DROP TABLE IF EXISTS therapies;
CREATE TABLE `therapies` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(128) default NULL,
  `short_definition` text,
  `long_definition` text,
  `area_id` int(11) unsigned default NULL,
  `path` varchar(100) NOT NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  `introduction` varchar(512) default NULL,
  `benefits_text` text,
  `benefits_list` text,
  `published` tinyint(1) default '0',
  `references` text,
  `conditions_list` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'therapies'
#

INSERT INTO therapies VALUES (6,'Cognitive Behavior Therapy','<p>\r\n	Through the use of relaxation training, relaxation techniques, bio-feedback and talk therapy, Cognitive Behavior Therapy targets the world of thoughts and actions that actually disrupt sleep.&nbsp;</p>\r\n<p>\r\n	It is an action-oriented form of psycho-social therapy that offers new solutions to the thinking patterns causing anxiety, intense emotions, and the absence of ease that normally promotes good sleep. The treatment focuses on helping individuals to comfortably work with their thoughts (cognitive patterns) in such a way that he or she is able to change the disruptive behavior and emotional state.<a name=\"_ftnref6\"></a></p>\r\n','<p>\r\n	Through the use of relaxation training, relaxation techniques, bio-feedback and talk therapy, Cognitive Behavior Therapy targets the world of thoughts and actions that actually disrupt sleep.&nbsp;</p>\r\n<p>\r\n	It is an action-oriented form of psycho-social therapy that offers new solutions to the thinking patterns causing anxiety, intense emotions, and the absence of ease &ndash; the ease that normally promotes good sleep. The treatment focuses on helping individuals to comfortably work with their thoughts (cognitive patterns) in such a way that he or she is able to change the disruptive behavior and emotional state.6</p>\r\n<p>\r\n	Cognitive-behavioral therapy combines the individual goals of cognitive therapy and behavioral therapy.&nbsp; Pioneered by psychologists Aaron Beck and Albert Ellis in the 1960s, cognitive therapy assumes that maladaptive behaviors and disturbed mood or emotions are the result of irrational thinking patterns, called automatic thoughts. Behavioral therapy, or behavior modification, trains individuals to replace distortions in thought and undesirable behaviors with healthier behavioral patterns.</p>\r\n<p>\r\n	Cognitive-behavioral therapy integrates the cognitive restructuring approach of cognitive therapy with the behavioral modification techniques of behavioral therapy. The therapist works with the patient to identify both the thoughts and the behaviors that are causing distress, and to change those thoughts in order to readjust the behavior.<a name=\"_ftnref7\"></a></p>\r\n',1,'cognitive-behaviour-therapy','2009-12-17 12:14:54','2009-12-21 14:27:13',12,12,'',NULL,'',1,NULL,NULL);
INSERT INTO therapies VALUES (7,'Prescription Medications','<p>\r\n	A drug requiring a written prescription from a medical doctor for a substance that is dispensed only by a registered pharmacist, as opposed to an over-the-counter drug, which can be purchased without a prescription at any pharmacy.</p>\r\n','<p>\r\n	A drug requiring a written prescription from a medical doctor for a substance that is dispensed only by a registered pharmacist, as opposed to an over-the-counter drug, which can be purchased without a prescription at any pharmacy. The term prescription drug is most frequently applied to pharmaceutical medications as they are highly specific chemical compounds and cannot be offered as a treatment option by anyone other than a knowing and skilled physician who is versed in the science of Medicine we have come to know as Allopathy, or standard western medicine.&nbsp; Historically, a prescription was written before the drug was prepared and administered and it has several parts as follows:</p>\r\n<ul>\r\n	<li>\r\n		The superscription (or heading) with the symbol R or Rx which stands for the word Recipe, meaning (in Latin) to take;</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		The inscription which contains the names and quantities of the ingredients;</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		The subscription or directions for compounding the drug; and</li>\r\n</ul>\r\n<ul>\r\n	<li>\r\n		The signature which is often preceded by the sign s. standing for signa, mark, giving the directions to be marked on the container.<a name=\"_ftnref8\"></a></li>\r\n</ul>\r\n',1,'prescription-medications','2009-12-17 12:32:24','2009-12-21 14:27:05',12,12,'',NULL,'',1,NULL,NULL);
INSERT INTO therapies VALUES (8,'Over-the-counter Medications','<p>\r\n	Two inexpensive medications available without a prescription that can relieve mild or occasional sleeping problems are diphenhydramine and dimenhydrinate. Over-the-counter medications for insomnia are purchased more than any other type of drug.</p>\r\n','<p>\r\n	<span style=\"font-family: \'Arial Narrow\';\"><font size=\"3\"><br />\r\n	</font></span></p>\r\n',1,'over-the-counter-medications','2009-12-17 14:39:36','2009-12-21 14:26:49',12,12,'',NULL,'',1,NULL,NULL);
INSERT INTO therapies VALUES (9,'Traditional Chinese Medicine','<p>\r\n	TCM is the Chinese system of comprehensive healing that dates back further than 200 B.C.&nbsp; TCM practioners use several modalities (acupuncture, herbs, and massage with manipulation) to help you establish a sense of balance on all levels.&nbsp; It works at the level of body, mind, and soul and this is why it is considered comprehensive.&nbsp; Increasingly, many western medical doctors, dentists and chiropractors have embraced TCM and practice it in addition to their regular system of healing as the results of TCM are measurable and lasting, address the root cause of the issue at hand, restoring a sense of normalcy to people&rsquo;s lives.&nbsp;</p>\r\n','<p>\r\n	TCM is the Chinese system of comprehensive healing that dates back further than 200 B.C.&nbsp; TCM practioners use several modalities (acupuncture, herbs, and massage with manipulation) to help you establish a sense of balance on all levels.&nbsp; It works at the level of body, mind, and soul and this is why it is considered comprehensive.&nbsp; Increasingly, many western medical doctors, dentists and chiropractors have embraced TCM and practice it in addition to their regular system of healing as the results of TCM are measurable and lasting, address the root cause of the issue at hand, restoring a sense of normalcy to people&rsquo;s lives.&nbsp;</p>\r\n<p>\r\n	One of the major assumptions in TCM is that health is achieved by maintaining the body in a &quot;balanced state&quot; and that disease is due to an internal imbalance of energy.&nbsp; In Chinese medicine energy is called Chi (pronounced Chee). Chi is also attributed with additional characteristics and may be referred to as yin and yang energies. <a name=\"_ftnref10\"></a><a href=\"http://docs.google.com/a/eyespeak.com/Doc?docid=0AWeg3yXANJeSZGZwYnIyYnBfM3J4c21jMmQ0&amp;hl=en#_ftn10\">[10]</a> Yin represents the slow, passive, feminine energy and is seen as cooling or cold, while yang represents the hot, excited, active, male energy. </p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	TCM works to restore a delicate balance between the two opposing and inseparable energy forces of yin and yang.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	In Traditional Chinese Medicine it is understood that each organ of the body houses different aspects of your spirit or essence and temperament. When you suffer from insomnia, the two organs most often out of balance are the Heart and the Liver.<a name=\"_ftnref11\"></a><a href=\"http://docs.google.com/a/eyespeak.com/Doc?docid=0AWeg3yXANJeSZGZwYnIyYnBfM3J4c21jMmQ0&amp;hl=en#_ftn11\">[11]</a> To restore balance a TCM works with a combination of approaches including acupuncture, Chinese herbs, Qigong/Tai Chi, nutrition to bring about the balance and wellness that your body is asking for.&nbsp; Another primary concept in TCM is the idea of &quot;root and branch.&quot; For example, a symptoms like insomnia is considered to be the branch of a disease. The root of a disease is a dysfunction or imbalance of the fundamental substances (Chi, blood, Yin, Yang, Jing, Shen), or of the major organ systems (Lungs, Heart, Spleen, Liver, Kidneys). </p>\r\n<p>\r\n	TCM is a complex science and medical system, originating from Shamanism, then progressing to Taoism. If the heart and liver are out of balance, they will not be able to house the spirit properly, and the spirit will wander, hence the restlessness experienced in insomnia. A wandering spirit, or Shen disturbance, can manifest in a number of ways, including mood disorders and heart palpitations, but insomnia is one of the most common symptoms.</p>\r\n',1,'traditional-chinese-medicine','2009-12-17 14:40:17','2009-12-21 14:26:41',12,12,'',NULL,'',1,NULL,NULL);
INSERT INTO therapies VALUES (10,'Acupuncture','<p>\r\n	Acupuncture is over 5000 years old and was practiced not only in China, but throughout the Egyptian civilization as well. Very slim, wisp-like filament needles are inserted into meridian points on your skin.&nbsp; Meridians are the various energy channels of your body.&nbsp; Acupuncture needles are used to stimulate specific points on the body so as to remove blockages in the flow of Chi, restoring health and supporting your optimal wellbeing.</p>\r\n','<p>\r\n	The documented history of Acupuncture suggests it has been in practice for at least 3000 years and perhaps as long as 5000 years.&nbsp; It was practiced not only in China, but throughout the Egyptian civilization as well. Its central principle is that there are energy channels in your body that sometimes get congested and need stimulation so that the energy, known as Chi (pronounced Chee), can flow evenly throughout your body and organs.&nbsp; Acupuncture is a deeply respected healing art practiced by medical doctors, nurses, doctors of Chinese medicine, acupuncturists, dentists and other practitioners.</p>\r\n<p>\r\n	When receiving acupuncture, your practitioner will assess your condition by taking your pulses.&nbsp; This system of pulse taking is very different than it is in the standard medical system which simply counts the pulse, assesses regularity of your heart-rate and determines if it is a strong or weak pulse. An acupuncturist will assess more subtle aspects of your pulse that represent the function of your organs.&nbsp; Once your practitioner understands what you need most, your treatment will begin and you need do no more than simply lie on the table and rest. Very slim, wisp-like filament needles are inserted into meridian points on your skin.&nbsp; Meridians are the various energy channels of your body.&nbsp; Acupuncture needles are used to stimulate specific points on the body so as to remove blockages in the flow of Chi, restoring health and supporting your optimal wellbeing.<br />\r\n	<br />\r\n	Acupuncture has many positive benefits. It is safe (sterile disposable needles are always used), it is effective for a wide variety of health problems, and it is virtually free of side effects. It has been scientifically demonstrated that acupuncture can have an effect on the body&#39;s central nervous system, and can increase levels of several neurotransmitters, including serotonin. Acupuncture promotes natural sleeping patterns, and doesn&rsquo;t have the hangover effect that most sleeping pills do.<a name=\"_ftnref15\"></a></p>\r\n',1,'acupuncture','2009-12-17 15:06:44','2010-01-08 17:15:37',12,10,'','','',1,NULL,NULL);
INSERT INTO therapies VALUES (11,'Homeopathy','<p>\r\n	Homeopathy is a very old system of medicine used by the ancient Egyptians, Greeks and later it was referred to by Paracelcus.&nbsp; Samuel Hahnemann an 18th century physician did not &lsquo;discover&rsquo; homeopathy per se, but just as Isaac Newton named a force all had understood, no one called it Gravity before. Hahnemann named Homeopathy and formalized it&rsquo;s practice.&nbsp; He was a traditionally trained MD was appalled by the intrusive / invasive techniques employed during his lifetime, so much so that he left medicine and decided to earn a living by translating medical texts. While translating texts he came upon much material to support his supposition, that like cures like &ndash; a foundational principle in Homeopathy.&nbsp;</p>\r\n','<p>\r\n	Homeopathy is a very old system of medicine used by the ancient Egyptians, Greeks and later it was referred to by Paracelcus.&nbsp; Samuel Hahnemann an 18th century physician did not &lsquo;discover&rsquo; homeopathy per se, but just as Isaac Newton named a force all had understood, no one called it Gravity before. Hahnemann named Homeopathy and formalized it&rsquo;s practice.&nbsp; He was a traditionally trained MD was appalled by the intrusive / invasive techniques employed during his lifetime, so much so that he left medicine and decided to earn a living by translating medical texts. While translating texts he came upon much material to support his supposition, that like cures like &ndash; a foundational principle in Homeopathy.&nbsp;</p>\r\n<p>\r\n	Homeopathy is a patient-centric model, meaning your story and set of circumstances, your temperament and history will determine the solution to your Insomnia.&nbsp; Every patient is an individual with a story specific to him or her.&nbsp; In homeopathy, there are no real &lsquo;equations&rsquo; as they exist in allopathic medicine.&nbsp; Your situation is studied, reviewed, discussed and only then is a course of action decided.&nbsp; Even so, there may be several steps before you find the right homeopathic medicine to resolve your situation.&nbsp; Homeopaths practice Homeopathy with Intention &ndash; the Intention in Homeopathy is the &lsquo;the Intention to cure Profoundly&rsquo;.</p>\r\n<p>\r\n	Once a remedy is decided upon, your homeopath will prescribe a pellet to be dissolved under the tongue &ndash; sublingually.&nbsp; Homeopaths also work with ointments, tinctures and solutions.&nbsp; Each prescription is created for you specificially, based upon your needs.</p>\r\n',1,'homeopathy','2009-12-17 15:07:34','2009-12-21 14:26:15',12,12,'',NULL,'',1,NULL,NULL);
INSERT INTO therapies VALUES (12,'Yoga','<p>\r\n	Yoga is a comprehensive system of study originating in India, that addresses the body&rsquo;s energy on all levels &ndash; physical, mental, emotional and spiritual. Exploring these aspects, you are sensitized to the dynamics that have been at play in your body and being that contribute to any tension or dis-ease.&nbsp; Once you become versed in these subtler aspects of self, you are better able to resolve problems that you had not come to terms with or acknowledged previously. Yoga deals with each area at the simultaneously, integrating mind, body, and spirit.</p>\r\n','<p>\r\n	Yoga is a comprehensive system of study originating in India, that addresses the body&rsquo;s energy on all levels &ndash; physical, mental, emotional and spiritual. Exploring these aspects, you are sensitized to the dynamics that have been at play in your body and being that contribute to any tension or dis-ease.&nbsp; Once you become versed in these subtler aspects of self, you are better able to resolve problems that you had not come to terms with or acknowledged previously. Yoga deals with each area at the simultaneously, integrating mind, body, and spirit.<a name=\"_ftnref17\"></a></p>\r\n<p>\r\n	Hatha Yoga is the practice of assuming physical positions and holding them for a specified period of time.&nbsp; It also includes certain breathing techniques and stretching.&nbsp; Meditation is another aspect of yoga and is often the natural result of doing the physical practices just mentioned.&nbsp; Meditation performed during Yoga can induce deep relaxation, which effectively sets the stage for sleep. Yoga can also reduce stress hormones, which are often elevated in people with insomnia.</p>\r\n<p>\r\n	Yogic practices such as asanas (yoga positions) and pranayama (yogic breathing) purify the body temple, and unblock emotional and physical blockages. The human body is seen in a different way as containing subtle energetic elements, and as more than being merely physical. Yoga is a system of living, a scientific method learned experientially by different yogis over thousands of years. <a name=\"_ftnref19\"></a>As mentioned, yoga is a comprehensive system not only for wellness but its philosophy is applicable to all aspects of your life. A life rooted in yoga creates a balanced state which naturally supports healthy sleep cycles.</p>\r\n',1,'yoga','2009-12-17 15:10:59','2009-12-21 14:25:52',12,12,'',NULL,'',1,NULL,NULL);
INSERT INTO therapies VALUES (13,'Herbalism','<p>\r\n	Herbalism is the art and science of treating a condition or multiple conditions with Herbs and dates back as far as 5000 years ago.&nbsp; Today, herbs are used widely and&nbsp; it is a known fact that several major drugs have been created to emulate the actions of herbs and to provide a magnified result. In their purest form however, herbs are highly effective in treating many conditions and restoring a sense of balance to the body &ndash; heart, mind and spirit!&nbsp; The herbs&nbsp; used in Herbal medicine most often come from plants.&nbsp; The list is sometimes expanded to include <a href=\"http://en.wikipedia.org/wiki/Fungi\">fungal</a> and <a href=\"http://en.wikipedia.org/wiki/Bee\">bee</a> products, as well as minerals, <a href=\"http://en.wikipedia.org/wiki/Animal_shell\">shells</a> and certain animal parts.&nbsp; Herbalism is widely embraced cross culturally and has several names - botanical medicine, medical herbalism, herbal medicine, and <a href=\"http://en.wikipedia.org/wiki/Phytotherapy\">phytotherapy</a> to cite just a few. Some herbal and nutritional supplements have been found to promote relaxation of mind and body, with several used specifically for insomnia.&nbsp; The use of herbs is best done with a licensed practioners who understands your body and the properties of the herbs.</p>\r\n','<p>\r\n	Herbalism is the art and science of treating a condition or multiple conditions with Herbs and dates back as far as 5000 years ago.&nbsp; Today, herbs are used widely and&nbsp; it is a known fact that several major drugs have been created to emulate the actions of herbs and to provide a magnified result. In their purest form however, herbs are highly effective in treating many conditions and restoring a sense of balance to the body &ndash; heart, mind and spirit!&nbsp; The herbs&nbsp; used in Herbal medicine most often come from plants.&nbsp; The list is sometimes expanded to include <a href=\"http://en.wikipedia.org/wiki/Fungi\">fungal</a> and <a href=\"http://en.wikipedia.org/wiki/Bee\">bee</a> products, as well as minerals, <a href=\"http://en.wikipedia.org/wiki/Animal_shell\">shells</a> and certain animal parts.&nbsp; Herbalism is widely embraced cross culturally and has several names - botanical medicine, medical herbalism, herbal medicine, and <a href=\"http://en.wikipedia.org/wiki/Phytotherapy\">phytotherapy</a> to cite just a few. Some herbal and nutritional supplements have been found to promote relaxation of mind and body, with several used specifically for insomnia.&nbsp; The use of herbs is best done with a licensed practioners who understands your body and the properties of the herbs.</p>\r\n',1,'herbalism','2009-12-17 15:17:19','2010-02-11 10:43:13',12,12,'','<p>\r\n	Herbalism is the art and science of treating a condition or multiple conditions with Herbs and dates back as far as 5000 years ago.&nbsp; Today, herbs are used widely and&nbsp; it is a known fact that several major drugs have been created to emulate the actions of herbs and to provide a magnified result. In their purest form however, herbs are highly effective in treating many conditions and restoring a sense of balance to the body &ndash; heart, mind and spirit!&nbsp; The herbs&nbsp; used in Herbal medicine most often come from plants.&nbsp; The list is sometimes expanded to include <a href=\"http://en.wikipedia.org/wiki/Fungi\">fungal</a> and <a href=\"http://en.wikipedia.org/wiki/Bee\">bee</a> products, as well as minerals, <a href=\"http://en.wikipedia.org/wiki/Animal_shell\">shells</a> and certain animal parts.&nbsp; Herbalism is widely embraced cross culturally and has several names - botanical medicine, medical herbalism, herbal medicine, and <a href=\"http://en.wikipedia.org/wiki/Phytotherapy\">phytotherapy</a> to cite just a few. Some herbal and nutritional supplements have been found to promote relaxation of mind and body, with several used specifically for insomnia.&nbsp; The use of herbs is best done with a licensed practioners who understands your body and the properties of the herbs.</p>\r\n','testing!\r\nmore testing!',1,NULL,NULL);
INSERT INTO therapies VALUES (16,'Lorem ipsum dolor sit amet','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent volutpat, diam eu lobortis dignissim, nulla urna pulvinar lorem, sed mollis purus turpis lobortis ante. Pellentesque vestibulum, justo quis consequat cursus, felis sem pharetra neque, at semper est nibh sed dolor. Vestibulum molestie egestas elit quis faucibus. Nullam gravida pellentesque augue a sodales. Nam quis ipsum mauris, ut porta elit. Phasellus a neque lorem, id vehicula dolor. Phasellus ac lorem risus. Praesent odio lorem, bibendum in fringilla vel, faucibus eget justo. Donec lobortis urna vel quam feugiat consequat. Etiam sodales ante id metus vestibulum feugiat. Pellentesque ac quam nec erat dignissim placerat viverra quis massa. Sed augue neque, adipiscing ut luctus et, euismod a neque. Fusce dapibus tempor neque, non condimentum purus volutpat a. Donec interdum ultricies leo quis aliquet. Morbi eu nibh a massa hendrerit sollicitudin eu non arcu. Donec felis tortor, vulputate et fermentum sed, imperdiet non lorem. Duis non nibh quis sapien tristique interdum.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent volutpat, diam eu lobortis dignissim, nulla urna pulvinar lorem, sed mollis purus turpis lobortis ante. Pellentesque vestibulum, justo quis consequat cursus, felis sem pharetra neque, at semper est nibh sed dolor. Vestibulum molestie egestas elit quis faucibus. Nullam gravida pellentesque augue a sodales. Nam quis ipsum mauris, ut porta elit. Phasellus a neque lorem, id vehicula dolor. Phasellus ac lorem risus. Praesent odio lorem, bibendum in fringilla vel, faucibus eget justo. Donec lobortis urna vel quam feugiat consequat. Etiam sodales ante id metus vestibulum feugiat. Pellentesque ac quam nec erat dignissim placerat viverra quis massa. Sed augue neque, adipiscing ut luctus et, euismod a neque. Fusce dapibus tempor neque, non condimentum purus volutpat a. Donec interdum ultricies leo quis aliquet. Morbi eu nibh a massa hendrerit sollicitudin eu non arcu. Donec felis tortor, vulputate et fermentum sed, imperdiet non lorem. Duis non nibh quis sapien tristique interdum.</p>\r\n<p>\r\n	Donec accumsan dictum mauris sed pulvinar. Aenean convallis risus non lectus sollicitudin vitae aliquam orci condimentum. Pellentesque malesuada laoreet tincidunt. Vestibulum egestas libero sed dolor aliquam placerat. Vestibulum imperdiet velit a augue euismod hendrerit. Curabitur commodo tellus vitae tortor vulputate fermentum. Cras ac tincidunt ipsum. Nullam dignissim augue et massa ornare tincidunt. Nullam vitae lacus at purus semper euismod hendrerit quis est. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dui justo, pretium elementum auctor nec, dapibus et elit. Sed dolor neque, semper et sagittis facilisis, vehicula ac mauris. Aenean massa eros, pretium vitae aliquet et, luctus vel lorem.</p>\r\n',1,'canyousee','2010-01-26 16:19:14','2010-02-12 13:46:37',1,12,'<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent volutpat, diam eu lobortis dignissim, nulla urna pulvinar lorem, sed mollis purus turpis lobortis ante. Pellentesque vestibulum, justo quis consequat cursus, felis sem pharetra neque, at semper est nibh sed dolor</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent volutpat, diam eu lobortis dignissim, nulla urna pulvinar lorem, sed mollis purus turpis lobortis ante. Pellentesque vestibulum, justo quis consequat cursus, felis sem pharetra neque, at semper est nibh sed dolor. Vestibulum molestie egestas elit quis faucibus. Nullam gravida pellentesque augue a sodales. Nam quis ipsum mauris, ut porta elit. Phasellus a neque lorem, id vehicula dolor. Phasellus ac lorem risus. Praesent odio lorem, bibendum in fringilla vel, faucibus eget justo. Donec lobortis urna vel quam feugiat consequat. Etiam sodales ante id metus vestibulum feugiat. Pellentesque ac quam nec erat dignissim placerat viverra quis massa. Sed augue neque, adipiscing ut luctus et, euismod a neque. Fusce dapibus tempor neque, non condimentum purus volutpat a. Donec interdum ultricies leo quis aliquet. Morbi eu nibh a massa hendrerit sollicitudin eu non arcu. Donec felis tortor, vulputate et fermentum sed, imperdiet non lorem. Duis non nibh quis sapien tristique interdum.</p>\r\n','Lorem ipsum dolor sit amet, consectetur adipiscing \r\nelit. Praesent volutpat, diam eu lobortis \r\ndignissim, nulla urna pulvinar lorem, sed mollis \r\npurus turpis lobortis ante. Pellentesque \r\nvestibulum, justo quis consequat cursus, felis sem \r\npharetra neque, at semper est nibh sed dolor. \r\nVestibulum molestie egestas elit quis faucibus. \r\nNullam gravida pellentesque augue a sodales. Nam \r\n',1,'<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing&nbsp;elit. Praesent volutpat, diam eu lobortis&nbsp;dignissim, nulla urna pulvinar lorem, sed mollis&nbsp;purus turpis lobortis ante. Pellentesque&nbsp;vestibulum, justo quis consequat cursus, felis sem&nbsp;pharetra neque, at semper est nibh sed dolor.&nbsp;Vestibulum molestie egestas elit quis faucibus.&nbsp;Nullam gravida pellentesque augue a sodales. Nam&nbsp;</p>\r\n',NULL);
INSERT INTO therapies VALUES (17,'Naturopathy','<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt;\">\r\n	<font size=\"3\"><font color=\"#000000\"><i>Naturopathy</i> is a comprehensive and integrated system of primary health care offered by licensed naturopathic physicians. The core belief of a naturopathic doctor (N.D.) is that all living things have the ability to heal. Your treatment plan is based on what is best for your constitution.&nbsp; <span style=\"\"><br />\r\n	</span></font></font></p>\r\n','<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt;\">\r\n	<span style=\"font-size: 12px;\"><font color=\"#000000\">While there are several principles that help naturopathic doctors understand the best way to work with your condition, there are no equations or templates governing his or her decision about your care. Rather, your naturopath will assess your biochemistry, biomechanics, and emotional disposition or temperament in order to help you restore the balance that we describe as &ldquo;good health&rdquo;<sup>2</sup>. </font></span></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt;\">\r\n	<o:p><span style=\"font-size: 12px;\"><font color=\"#000000\">&nbsp;</font></span></o:p></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt;\">\r\n	<span style=\"font-size: 12px;\"><font color=\"#000000\">Like any other practitioner, N.D.s take your history, conduct a physical examination, and identify your basic temperament and the effect it has on your health. Typically, naturopathic doctors will use any one of the following disciplines: clinical nutrition, botanical medicine or herbology, homeopathy, ayurvedic medicine, traditional Chinese medicine, and acupuncture. Some naturopathic physicians are very interested in using body temperature&ndash;regulation as a treatment for disease. This is called <i>thermal regulation, </i>and it is often done in hydrothermal pools or on hydrothermal tables. </font></span></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt;\">\r\n	<o:p><span style=\"font-size: 12px;\"><font color=\"#000000\">&nbsp;</font></span></o:p></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt;\">\r\n	<span style=\"font-size: 12px;\"><font color=\"#000000\">No matter what therapy they use, the goal for all N.D.s is to stimulate the body to heal itself. Rather than trying to attack specific diseases, your practitioner will focus on cleansing and strengthening the body<sup>3</sup>. Regardless of the specific methodology, and regardless of whether the healer practiced in the last century or is active today, the approach remains basically the same. Naturopathic medicine is good for many conditions. </font></span></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt;\">\r\n	<o:p><span style=\"font-size: 12px;\"><font color=\"#000000\">&nbsp;</font></span></o:p></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt;\">\r\n	<span style=\"font-size: 12px;\"><font color=\"#000000\">Naturopathy <i style=\"\">is <b style=\"\"><u>not recommended</u></b></i> for severe traumas from, for example, a car accident, fall, fire, or any emergency or orthopedic problem that might need immediate or corrective surgery. It <i>can</i> help the <i>healing </i>process once the emergency has been addressed.</font></span></p>\r\n<p class=\"MsoHeader\" style=\"margin: 0in 0in 0pt;\">\r\n	<o:p><span style=\"font-size: 12px;\"><font color=\"#000000\">&nbsp;</font></span></o:p></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt;\">\r\n	<span style=\"font-size: 12px;\"><font color=\"#000000\">Naturopathy is based on five overarching principles<sup>4,5</sup>: </font></span></p>\r\n<p class=\"MsoListParagraphCxSpFirst\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<span style=\"font-size: 12px;\"><font color=\"#000000\">1.<span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Nature is a powerful and healing force.</font></span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<span style=\"font-size: 12px;\"><font color=\"#000000\">2.<span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>View the person as a whole being&mdash;body, mind, emotions; past, present, and future.</font></span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<span style=\"font-size: 12px;\"><font color=\"#000000\">3.<span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Identify and treat the underlying cause.</font></span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<span style=\"font-size: 12px;\"><font color=\"#000000\">4.<span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>The Naturopath is a teacher.</font></span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<span style=\"font-size: 12px;\"><font color=\"#000000\">5.<span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Prevention is of the utmost importance in the best approach.</font></span></p>\r\n',1,'naturopathy','2010-02-11 03:56:14','2010-02-11 04:07:24',5,5,'<p>\r\n	<font color=\"#000000\"><font size=\"3\">Naturopathy and naturopathic medicine are the fastest-growing of all alternative health care disciplines. This demonstrates the enormous change Americans are experiencing in our attitudes about health care and self-care</font><sup><font size=\"2\">1</font></sup><font size=\"3\">.</font></font></p>\r\n','<p class=\"MsoListParagraphCxSpFirst\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<font color=\"#000000\"><span style=\"font-family: symbol;\"><span style=\"\"><font size=\"3\">&middot;</font><span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><font size=\"3\">Natural immunity is strengthened</font></font></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<font color=\"#000000\"><span style=\"font-family: symbol;\"><span style=\"\"><font size=\"3\">&middot;</font><span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><font size=\"3\">Conditions are treated holistically</font></font></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<font color=\"#000000\"><span style=\"font-family: symbol;\"><span style=\"\"><font size=\"3\">&middot;</font><span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><font size=\"3\">There is less wear and tear on the liver and other organs of the body</font></font></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<font color=\"#000000\"><span style=\"font-family: symbol;\"><span style=\"\"><font size=\"3\">&middot;</font><span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><font size=\"3\">Risk of side effects from drugs is decreased</font></font></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<font color=\"#000000\"><span style=\"font-family: symbol;\"><span style=\"\"><font size=\"3\">&middot;</font><span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><font size=\"3\">Overall health and wellness benefits</font></font></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<font color=\"#000000\"><span style=\"font-family: symbol;\"><span style=\"\"><font size=\"3\">&middot;</font><span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><font size=\"3\">You are empowered with knowledge</font></font></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<font color=\"#000000\"><span style=\"font-family: symbol;\"><span style=\"\"><font size=\"3\">&middot;</font><span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><font size=\"3\">You learn to make better decisions</font></font></p>\r\n<p class=\"MsoListParagraphCxSpLast\" style=\"margin: 0in 0in 0pt 0.5in; text-indent: -0.25in;\">\r\n	<font color=\"#000000\"><span style=\"font-family: symbol;\"><span style=\"\"><font size=\"3\">&middot;</font><span style=\"font-family: \'times new roman\'; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><font size=\"3\">You become more familiar with how your body works</font></font></p>\r\n','ADHD\r\nAllergies\r\nAnxiety\r\nArthritis\r\nAsthma\r\nAtherosclerosis\r\nADHD\r\nAllergies\r\nAnxiety\r\nArthritis\r\nAsthma\r\nAtherosclerosis\r\nADHD\r\nAllergies\r\nAnxiety\r\nArthritis',1,NULL,NULL);
INSERT INTO therapies VALUES (18,'testing','','<p>\r\n	dasdasdas</p>\r\n',1,'testttt','2010-02-13 16:48:17','2010-02-13 17:11:13',12,12,'<p>\r\n	dasdasd</p>\r\n',NULL,'',1,'<p>\r\n	1231541235gdsgsdgsdg</p>\r\n','dasdasd\r\ndsadasda\r\ndasdasd');

#
# Table structure for table 'therapies_organizations'
#

# DROP TABLE IF EXISTS therapies_organizations;
CREATE TABLE `therapies_organizations` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `therapy_id` int(11) unsigned default NULL,
  `organization_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'therapies_organizations'
#


#
# Table structure for table 'therapies_products'
#

# DROP TABLE IF EXISTS therapies_products;
CREATE TABLE `therapies_products` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `therapy_id` int(11) unsigned default NULL,
  `product_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'therapies_products'
#


#
# Table structure for table 'tickets'
#

# DROP TABLE IF EXISTS tickets;
CREATE TABLE `tickets` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `subject` varchar(45) NOT NULL,
  `resume` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Dumping data for table 'tickets'
#

INSERT INTO tickets VALUES (1,1,'Test','<p>\r\n	Test</p>\r\n','2009-12-17 18:41:52','2009-12-18 10:46:52');
INSERT INTO tickets VALUES (2,1,'sfg','sdfg','2009-12-18 10:45:18','2009-12-18 10:45:18');
INSERT INTO tickets VALUES (3,1,'asdf','<p>\r\n	asdf</p>\r\n','2009-12-18 10:46:14','2009-12-18 10:46:14');

#
# Table structure for table 'traditions'
#

# DROP TABLE IF EXISTS traditions;
CREATE TABLE `traditions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(150) NOT NULL,
  `introduction` text,
  `definition_short` text,
  `definition_long` text,
  `beliefs` text,
  `path` varchar(100) NOT NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  `published` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions'
#

INSERT INTO traditions VALUES (3,'Test tradition 1','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non nisi est. Donec auctor elit vitae tellus vehicula a eleifend arcu lacinia. Vivamus gravida, felis et consequat blandit, justo urna gravida risus, sit amet mollis magna est vitae nisi. Pellentesque scelerisque, sapien at mollis fringilla, libero quam accumsan urna, nec malesuada erat enim quis nibh. Suspendisse potenti. Duis tincidunt consequat dui, vel convallis nisi malesuada eget. Nullam imperdiet auctor magna, nec bibendum neque porttitor et. Duis vitae porttitor orci. Donec ut egestas metus. Ut ac purus arcu. Vestibulum laoreet purus vel libero rutrum feugiat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam varius pellentesque aliquam. Vivamus viverra tortor a purus pulvinar vehicula. In a tortor quis odio elementum egestas. Morbi dapibus sollicitudin velit, nec dapibus lorem viverra ac. Nulla libero felis, suscipit quis rutrum aliquet, egestas et nibh. Vivamus magna mauris, sagittis et mollis rutrum, gravida ac diam. Aenean at dapibus justo. Vivamus a lectus augue, non ornare tortor.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non nisi est. Donec auctor elit vitae tellus vehicula a eleifend arcu lacinia. Vivamus gravida, felis et consequat blandit, justo urna gravida risus, sit amet mollis magna est vitae nisi. Pellentesque scelerisque, sapien at mollis fringilla, libero quam accumsan urna, nec malesuada erat enim quis nibh. Suspendisse potenti. Duis tincidunt consequat dui, vel convallis nisi malesuada eget. Nullam imperdiet auctor magna, nec bibendum neque porttitor et. Duis vitae porttitor orci. Donec ut egestas metus. Ut ac purus arcu. Vestibulum laoreet purus vel libero rutrum feugiat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam varius pellentesque aliquam. Vivamus viverra tortor a purus pulvinar vehicula. In a tortor quis odio elementum egestas. Morbi dapibus sollicitudin velit, nec dapibus lorem viverra ac. Nulla libero felis, suscipit quis rutrum aliquet, egestas et nibh. Vivamus magna mauris, sagittis et mollis rutrum, gravida ac diam. Aenean at dapibus justo. Vivamus a lectus augue, non ornare tortor.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non nisi est. Donec auctor elit vitae tellus vehicula a eleifend arcu lacinia. Vivamus gravida, felis et consequat blandit, justo urna gravida risus, sit amet mollis magna est vitae nisi. Pellentesque scelerisque, sapien at mollis fringilla, libero quam accumsan urna, nec malesuada erat enim quis nibh. Suspendisse potenti. Duis tincidunt consequat dui, vel convallis nisi malesuada eget. Nullam imperdiet auctor magna, nec bibendum neque porttitor et. Duis vitae porttitor orci. Donec ut egestas metus. Ut ac purus arcu. Vestibulum laoreet purus vel libero rutrum feugiat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam varius pellentesque aliquam. Vivamus viverra tortor a purus pulvinar vehicula. In a tortor quis odio elementum egestas. Morbi dapibus sollicitudin velit, nec dapibus lorem viverra ac. Nulla libero felis, suscipit quis rutrum aliquet, egestas et nibh. Vivamus magna mauris, sagittis et mollis rutrum, gravida ac diam. Aenean at dapibus justo. Vivamus a lectus augue, non ornare tortor.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non nisi est. Donec auctor elit vitae tellus vehicula a eleifend arcu lacinia. Vivamus gravida, felis et consequat blandit, justo urna gravida risus, sit amet mollis magna est vitae nisi. Pellentesque scelerisque, sapien at mollis fringilla, libero quam accumsan urna, nec malesuada erat enim quis nibh. Suspendisse potenti. Duis tincidunt consequat dui, vel convallis nisi malesuada eget. Nullam imperdiet auctor magna, nec bibendum neque porttitor et. Duis vitae porttitor orci. Donec ut egestas metus. Ut ac purus arcu. Vestibulum laoreet purus vel libero rutrum feugiat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam varius pellentesque aliquam. Vivamus viverra tortor a purus pulvinar vehicula. In a tortor quis odio elementum egestas. Morbi dapibus sollicitudin velit, nec dapibus lorem viverra ac. Nulla libero felis, suscipit quis rutrum aliquet, egestas et nibh. Vivamus magna mauris, sagittis et mollis rutrum, gravida ac diam. Aenean at dapibus justo. Vivamus a lectus augue, non ornare tortor.</p>\r\n','test','2009-12-23 17:22:48','2009-12-23 17:22:48',12,12,0);
INSERT INTO traditions VALUES (4,'Test tradition 2','<p>\r\n	Introductions!!</p>\r\n','<p>\r\n	<img align=\"right\" alt=\"\" height=\"158\" hspace=\"\" src=\"/img/wysiwyg/images/liiv3_TherapyDetails_Final.jpg\" style=\"padding-left: 10px; padding-bottom: 10px;\" vspace=\"\" width=\"302\" />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non nisi est. Donec auctor elit vitae tellus vehicula a eleifend arcu lacinia. Vivamus gravida, felis et consequat blandit, justo urna gravida risus, sit amet mollis magna est vitae nisi. Pellentesque scelerisque, sapien at mollis fringilla, libero quam accumsan urna, nec malesuada erat enim quis nibh. Suspendisse potenti. Duis tincidunt consequat dui, vel convallis nisi malesuada eget. Nullam imperdiet auctor magna, nec bibendum neque porttitor et. Duis vitae porttitor orci. Donec ut egestas metus. Ut ac purus arcu. Vestibulum laoreet purus vel libero rutrum feugiat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam varius pellentesque aliquam. Vivamus viverra tortor a purus pulvinar vehicula. In a tortor quis odio elementum egestas. Morbi dapibus sollicitudin velit, nec dapibus lorem viverra ac. Nulla libero felis, suscipit quis rutrum aliquet, egestas et nibh. Vivamus magna mauris, sagittis et mollis rutrum, gravida ac diam. Aenean at dapibus justo. Vivamus a lectus augue, non ornare tortor.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non nisi est. Donec auctor elit vitae tellus vehicula a eleifend arcu lacinia. Vivamus gravida, felis et consequat blandit, justo urna gravida risus, sit amet mollis magna est vitae nisi. Pellentesque scelerisque, sapien at mollis fringilla, libero quam accumsan urna, nec malesuada erat enim quis nibh. Suspendisse potenti. Duis tincidunt consequat dui, vel convallis nisi malesuada eget. Nullam imperdiet auctor magna, nec bibendum neque porttitor et. Duis vitae porttitor orci. Donec ut egestas metus. Ut ac purus arcu. Vestibulum laoreet purus vel libero rutrum feugiat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam varius pellentesque aliquam. Vivamus viverra tortor a purus pulvinar vehicula. In a tortor quis odio elementum egestas. Morbi dapibus sollicitudin velit, nec dapibus lorem viverra ac. Nulla libero felis, suscipit quis rutrum aliquet, egestas et nibh. Vivamus magna mauris, sagittis et mollis rutrum, gravida ac diam. Aenean at dapibus justo. Vivamus a lectus augue, non ornare tortor.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non nisi est. Donec auctor elit vitae tellus vehicula a eleifend arcu lacinia. Vivamus gravida, felis et consequat blandit, justo urna gravida risus, sit amet mollis magna est vitae nisi. Pellentesque scelerisque, sapien at mollis fringilla, libero quam accumsan urna, nec malesuada erat enim quis nibh. Suspendisse potenti. Duis tincidunt consequat dui, vel convallis nisi malesuada eget. Nullam imperdiet auctor magna, nec bibendum neque porttitor et. Duis vitae porttitor orci. Donec ut egestas metus.</p>\r\n<ul>\r\n	<li>\r\n		Item 1</li>\r\n	<li>\r\n		Item 2</li>\r\n	<li>\r\n		Item 3</li>\r\n</ul>\r\n','test2','2009-12-23 17:23:25','2009-12-30 11:46:43',12,12,0);
INSERT INTO traditions VALUES (5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent volutpat, diam eu lobortis dignissim, nulla urna pulvinar lorem','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent volutpat, diam eu lobortis dignissim, nulla urna pulvinar lorem, sed mollis purus turpis lobortis ante. Pellentesque vestibulum, justo quis consequat cursus, felis sem pharetra neque, at semper est nibh sed dolor. Vestibulum molestie egestas elit quis faucibus. Nullam gravida pellentesque augue a sodales. Nam quis ipsum mauris, ut porta elit. Phasellus a neque lorem, id vehicula dolor. Phasellus ac lorem risus. Praesent odio lorem, bibendum in fringilla vel, faucibus eget justo. Donec lobortis urna vel quam feugiat consequat. Etiam sodales ante id metus vestibulum feugiat. Pellentesque ac quam nec erat dignissim placerat viverra quis massa. Sed augue neque, adipiscing ut luctus et, euismod a neque. Fusce dapibus tempor neque, non condimentum purus volutpat a. Donec interdum ultricies leo quis aliquet. Morbi eu nibh a massa hendrerit sollicitudin eu non arcu. Donec felis tortor, vulputate et fermentum sed, imperdiet non lorem. Duis non nibh quis sapien tristique interdum.</p>\r\n<p>\r\n	Donec accumsan dictum mauris sed pulvinar. Aenean convallis risus non lectus sollicitudin vitae aliquam orci condimentum. Pellentesque malesuada laoreet tincidunt. Vestibulum egestas libero sed dolor aliquam placerat. Vestibulum imperdiet velit a augue euismod hendrerit. Curabitur commodo tellus vitae tortor vulputate fermentum. Cras ac tincidunt ipsum. Nullam dignissim augue et massa ornare tincidunt. Nullam vitae lacus at purus semper euismod hendrerit quis est. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dui justo, pretium elementum auctor nec, dapibus et elit. Sed dolor neque, semper et sagittis facilisis, vehicula ac mauris. Aenean massa eros, pretium vitae aliquet et, luctus vel lorem.</p>\r\n<p>\r\n	Proin lobortis, purus vitae tempus elementum, arcu lectus tristique odio, a mattis dui nisi non nunc. Nam convallis hendrerit massa. Fusce congue, orci eget commodo imperdiet, urna metus fermentum purus, et lobortis dui erat ac justo. Vivamus nec sollicitudin dui. Nunc aliquam magna id quam dictum condimentum. Vivamus mauris turpis, eleifend at volutpat a, tristique et odio. Praesent fringilla consequat commodo. Vestibulum risus ipsum, laoreet eget ultricies et, vestibulum vitae ipsum. Sed pellentesque, diam nec convallis hendrerit, ante ante tristique sem, a varius quam elit a nibh. Maecenas metus massa, eleifend vel scelerisque pulvinar, lobortis at orci. Duis suscipit rhoncus scelerisque. Sed ornare lacinia libero. In vitae dignissim lacus. Cras et tellus odio. Aenean ullamcorper lobortis nisl, vitae elementum sem vehicula ut. Vestibulum tempus luctus orci, tincidunt accumsan lectus lacinia eget.</p>\r\n<p>\r\n	Donec gravida libero a nunc faucibus nec ullamcorper dolor gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam malesuada enim nec purus feugiat semper. Nam lacinia, quam ac convallis ultrices, libero metus eleifend justo, eget dictum justo nisi ullamcorper justo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed nec bibendum lacus. Mauris ultrices vulputate diam, ac tristique felis faucibus vitae. Proin aliquet volutpat risus, et facilisis magna sagittis ac. Proin varius venenatis dui, nec venenatis sapien tempor a. Duis metus metus, pulvinar vel tempor at, pulvinar non lorem. Suspendisse vel enim dui. Nam feugiat, velit at faucibus feugiat, eros augue scelerisque massa, malesuada aliquet nunc turpis ut lectus. Ut at lobortis ipsum. Duis a nunc lorem, ac pellentesque turpis. Mauris porttitor augue lectus.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent volutpat, diam eu lobortis dignissim, nulla urna pulvinar lorem, sed mollis purus turpis lobortis ante. Pellentesque vestibulum, justo quis consequat cursus, felis sem pharetra neque, at semper est nibh sed dolor. Vestibulum molestie egestas elit quis faucibus. Nullam gravida pellentesque augue a sodales. Nam quis ipsum mauris, ut porta elit. Phasellus a neque lorem, id vehicula dolor. Phasellus ac lorem risus. Praesent odio lorem, bibendum in fringilla vel, faucibus eget justo. Donec lobortis urna vel quam feugiat consequat. Etiam sodales ante id metus vestibulum feugiat. Pellentesque ac quam nec erat dignissim placerat viverra quis massa. Sed augue neque, adipiscing ut luctus et, euismod a neque. Fusce dapibus tempor neque, non condimentum purus volutpat a. Donec interdum ultricies leo quis aliquet. Morbi eu nibh a massa hendrerit sollicitudin eu non arcu. Donec felis tortor, vulputate et fermentum sed, imperdiet non lorem. Duis non nibh quis sapien tristique interdum.</p>\r\n<p>\r\n	Donec accumsan dictum mauris sed pulvinar. Aenean convallis risus non lectus sollicitudin vitae aliquam orci condimentum. Pellentesque malesuada laoreet tincidunt. Vestibulum egestas libero sed dolor aliquam placerat. Vestibulum imperdiet velit a augue euismod hendrerit. Curabitur commodo tellus vitae tortor vulputate fermentum. Cras ac tincidunt ipsum. Nullam dignissim augue et massa ornare tincidunt. Nullam vitae lacus at purus semper euismod hendrerit quis est. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dui justo, pretium elementum auctor nec, dapibus et elit. Sed dolor neque, semper et sagittis facilisis, vehicula ac mauris. Aenean massa eros, pretium vitae aliquet et, luctus vel lorem.</p>\r\n<p>\r\n	Proin lobortis, purus vitae tempus elementum, arcu lectus tristique odio, a mattis dui nisi non nunc. Nam convallis hendrerit massa. Fusce congue, orci eget commodo imperdiet, urna metus fermentum purus, et lobortis dui erat ac justo. Vivamus nec sollicitudin dui. Nunc aliquam magna id quam dictum condimentum. Vivamus mauris turpis, eleifend at volutpat a, tristique et odio. Praesent fringilla consequat commodo. Vestibulum risus ipsum, laoreet eget ultricies et, vestibulum vitae ipsum. Sed pellentesque, diam nec convallis hendrerit, ante ante tristique sem, a varius quam elit a nibh. Maecenas metus massa, eleifend vel scelerisque pulvinar, lobortis at orci. Duis suscipit rhoncus scelerisque. Sed ornare lacinia libero. In vitae dignissim lacus. Cras et tellus odio. Aenean ullamcorper lobortis nisl, vitae elementum sem vehicula ut. Vestibulum tempus luctus orci, tincidunt accumsan lectus lacinia eget.</p>\r\n<p>\r\n	Donec gravida libero a nunc faucibus nec ullamcorper dolor gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam malesuada enim nec purus feugiat semper. Nam lacinia, quam ac convallis ultrices, libero metus eleifend justo, eget dictum justo nisi ullamcorper justo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed nec bibendum lacus. Mauris ultrices vulputate diam, ac tristique felis faucibus vitae. Proin aliquet volutpat risus, et facilisis magna sagittis ac. Proin varius venenatis dui, nec venenatis sapien tempor a. Duis metus metus, pulvinar vel tempor at, pulvinar non lorem. Suspendisse vel enim dui. Nam feugiat, velit at faucibus feugiat, eros augue scelerisque massa, malesuada aliquet nunc turpis ut lectus. Ut at lobortis ipsum. Duis a nunc lorem, ac pellentesque turpis. Mauris porttitor augue lectus.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent volutpat, diam eu lobortis dignissim, nulla urna pulvinar lorem, sed mollis purus turpis lobortis ante. Pellentesque vestibulum, justo quis consequat cursus, felis sem pharetra neque, at semper est nibh sed dolor. Vestibulum molestie egestas elit quis faucibus. Nullam gravida pellentesque augue a sodales. Nam quis ipsum mauris, ut porta elit. Phasellus a neque lorem, id vehicula dolor. Phasellus ac lorem risus. Praesent odio lorem, bibendum in fringilla vel, faucibus eget justo. Donec lobortis urna vel quam feugiat consequat. Etiam sodales ante id metus vestibulum feugiat. Pellentesque ac quam nec erat dignissim placerat viverra quis massa. Sed augue neque, adipiscing ut luctus et, euismod a neque. Fusce dapibus tempor neque, non condimentum purus volutpat a. Donec interdum ultricies leo quis aliquet. Morbi eu nibh a massa hendrerit sollicitudin eu non arcu. Donec felis tortor, vulputate et fermentum sed, imperdiet non lorem. Duis non nibh quis sapien tristique interdum.</p>\r\n<p>\r\n	Donec accumsan dictum mauris sed pulvinar. Aenean convallis risus non lectus sollicitudin vitae aliquam orci condimentum. Pellentesque malesuada laoreet tincidunt. Vestibulum egestas libero sed dolor aliquam placerat. Vestibulum imperdiet velit a augue euismod hendrerit. Curabitur commodo tellus vitae tortor vulputate fermentum. Cras ac tincidunt ipsum. Nullam dignissim augue et massa ornare tincidunt. Nullam vitae lacus at purus semper euismod hendrerit quis est. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dui justo, pretium elementum auctor nec, dapibus et elit. Sed dolor neque, semper et sagittis facilisis, vehicula ac mauris. Aenean massa eros, pretium vitae aliquet et, luctus vel lorem.</p>\r\n<p>\r\n	Proin lobortis, purus vitae tempus elementum, arcu lectus tristique odio, a mattis dui nisi non nunc. Nam convallis hendrerit massa. Fusce congue, orci eget commodo imperdiet, urna metus fermentum purus, et lobortis dui erat ac justo. Vivamus nec sollicitudin dui. Nunc aliquam magna id quam dictum condimentum. Vivamus mauris turpis, eleifend at volutpat a, tristique et odio. Praesent fringilla consequat commodo. Vestibulum risus ipsum, laoreet eget ultricies et, vestibulum vitae ipsum. Sed pellentesque, diam nec convallis hendrerit, ante ante tristique sem, a varius quam elit a nibh. Maecenas metus massa, eleifend vel scelerisque pulvinar, lobortis at orci. Duis suscipit rhoncus scelerisque. Sed ornare lacinia libero. In vitae dignissim lacus. Cras et tellus odio. Aenean ullamcorper lobortis nisl, vitae elementum sem vehicula ut. Vestibulum tempus luctus orci, tincidunt accumsan lectus lacinia eget.</p>\r\n<p>\r\n	Donec gravida libero a nunc faucibus nec ullamcorper dolor gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam malesuada enim nec purus feugiat semper. Nam lacinia, quam ac convallis ultrices, libero metus eleifend justo, eget dictum justo nisi ullamcorper justo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed nec bibendum lacus. Mauris ultrices vulputate diam, ac tristique felis faucibus vitae. Proin aliquet volutpat risus, et facilisis magna sagittis ac. Proin varius venenatis dui, nec venenatis sapien tempor a. Duis metus metus, pulvinar vel tempor at, pulvinar non lorem. Suspendisse vel enim dui. Nam feugiat, velit at faucibus feugiat, eros augue scelerisque massa, malesuada aliquet nunc turpis ut lectus. Ut at lobortis ipsum. Duis a nunc lorem, ac pellentesque turpis. Mauris porttitor augue lectus.</p>\r\n','<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent volutpat, diam eu lobortis dignissim, nulla urna pulvinar lorem, sed mollis purus turpis lobortis ante. Pellentesque vestibulum, justo quis consequat cursus, felis sem pharetra neque, at semper est nibh sed dolor. Vestibulum molestie egestas elit quis faucibus. Nullam gravida pellentesque augue a sodales. Nam quis ipsum mauris, ut porta elit. Phasellus a neque lorem, id vehicula dolor. Phasellus ac lorem risus. Praesent odio lorem, bibendum in fringilla vel, faucibus eget justo. Donec lobortis urna vel quam feugiat consequat. Etiam sodales ante id metus vestibulum feugiat. Pellentesque ac quam nec erat dignissim placerat viverra quis massa. Sed augue neque, adipiscing ut luctus et, euismod a neque. Fusce dapibus tempor neque, non condimentum purus volutpat a. Donec interdum ultricies leo quis aliquet. Morbi eu nibh a massa hendrerit sollicitudin eu non arcu. Donec felis tortor, vulputate et fermentum sed, imperdiet non lorem. Duis non nibh quis sapien tristique interdum.</p>\r\n<p>\r\n	Donec accumsan dictum mauris sed pulvinar. Aenean convallis risus non lectus sollicitudin vitae aliquam orci condimentum. Pellentesque malesuada laoreet tincidunt. Vestibulum egestas libero sed dolor aliquam placerat. Vestibulum imperdiet velit a augue euismod hendrerit. Curabitur commodo tellus vitae tortor vulputate fermentum. Cras ac tincidunt ipsum. Nullam dignissim augue et massa ornare tincidunt. Nullam vitae lacus at purus semper euismod hendrerit quis est. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dui justo, pretium elementum auctor nec, dapibus et elit. Sed dolor neque, semper et sagittis facilisis, vehicula ac mauris. Aenean massa eros, pretium vitae aliquet et, luctus vel lorem.</p>\r\n<p>\r\n	Proin lobortis, purus vitae tempus elementum, arcu lectus tristique odio, a mattis dui nisi non nunc. Nam convallis hendrerit massa. Fusce congue, orci eget commodo imperdiet, urna metus fermentum purus, et lobortis dui erat ac justo. Vivamus nec sollicitudin dui. Nunc aliquam magna id quam dictum condimentum. Vivamus mauris turpis, eleifend at volutpat a, tristique et odio. Praesent fringilla consequat commodo. Vestibulum risus ipsum, laoreet eget ultricies et, vestibulum vitae ipsum. Sed pellentesque, diam nec convallis hendrerit, ante ante tristique sem, a varius quam elit a nibh. Maecenas metus massa, eleifend vel scelerisque pulvinar, lobortis at orci. Duis suscipit rhoncus scelerisque. Sed ornare lacinia libero. In vitae dignissim lacus. Cras et tellus odio. Aenean ullamcorper lobortis nisl, vitae elementum sem vehicula ut. Vestibulum tempus luctus orci, tincidunt accumsan lectus lacinia eget.</p>\r\n<p>\r\n	Donec gravida libero a nunc faucibus nec ullamcorper dolor gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam malesuada enim nec purus feugiat semper. Nam lacinia, quam ac convallis ultrices, libero metus eleifend justo, eget dictum justo nisi ullamcorper justo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed nec bibendum lacus. Mauris ultrices vulputate diam, ac tristique felis faucibus vitae. Proin aliquet volutpat risus, et facilisis magna sagittis ac. Proin varius venenatis dui, nec venenatis sapien tempor a. Duis metus metus, pulvinar vel tempor at, pulvinar non lorem. Suspendisse vel enim dui. Nam feugiat, velit at faucibus feugiat, eros augue scelerisque massa, malesuada aliquet nunc turpis ut lectus. Ut at lobortis ipsum. Duis a nunc lorem, ac pellentesque turpis. Mauris porttitor augue lectus.</p>\r\n','canyousee','2010-01-26 17:59:19','2010-02-05 16:30:38',1,12,1);

#
# Table structure for table 'traditions_articles'
#

# DROP TABLE IF EXISTS traditions_articles;
CREATE TABLE `traditions_articles` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tradition_id` int(10) unsigned NOT NULL,
  `article_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions_articles'
#


#
# Table structure for table 'traditions_audios'
#

# DROP TABLE IF EXISTS traditions_audios;
CREATE TABLE `traditions_audios` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tradition_id` int(10) unsigned NOT NULL,
  `audio_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions_audios'
#


#
# Table structure for table 'traditions_authorities'
#

# DROP TABLE IF EXISTS traditions_authorities;
CREATE TABLE `traditions_authorities` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tradition_id` int(10) unsigned NOT NULL,
  `authority_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions_authorities'
#


#
# Table structure for table 'traditions_conditions'
#

# DROP TABLE IF EXISTS traditions_conditions;
CREATE TABLE `traditions_conditions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tradition_id` int(10) unsigned NOT NULL,
  `condition_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions_conditions'
#


#
# Table structure for table 'traditions_events'
#

# DROP TABLE IF EXISTS traditions_events;
CREATE TABLE `traditions_events` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tradition_id` int(10) unsigned NOT NULL,
  `event_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions_events'
#


#
# Table structure for table 'traditions_organizations'
#

# DROP TABLE IF EXISTS traditions_organizations;
CREATE TABLE `traditions_organizations` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tradition_id` int(10) unsigned NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions_organizations'
#


#
# Table structure for table 'traditions_products'
#

# DROP TABLE IF EXISTS traditions_products;
CREATE TABLE `traditions_products` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tradition_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions_products'
#


#
# Table structure for table 'traditions_publications'
#

# DROP TABLE IF EXISTS traditions_publications;
CREATE TABLE `traditions_publications` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tradition_id` int(10) unsigned NOT NULL,
  `publication_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions_publications'
#


#
# Table structure for table 'traditions_stories'
#

# DROP TABLE IF EXISTS traditions_stories;
CREATE TABLE `traditions_stories` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tradition_id` int(10) unsigned NOT NULL,
  `story_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions_stories'
#


#
# Table structure for table 'traditions_videos'
#

# DROP TABLE IF EXISTS traditions_videos;
CREATE TABLE `traditions_videos` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tradition_id` int(10) unsigned NOT NULL,
  `video_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table 'traditions_videos'
#


#
# Table structure for table 'users'
#

# DROP TABLE IF EXISTS users;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `username` varchar(50) default NULL,
  `password` varchar(64) default NULL,
  `email` varchar(128) default NULL,
  `picture` varchar(128) default NULL,
  `link` varchar(255) default NULL,
  `group_id` int(10) unsigned NOT NULL default '1',
  `created` datetime default NULL,
  `updated` datetime default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UNIQUE` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'users'
#

INSERT INTO users VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3','nicolas@conseguir.com.ar','','',3,'2009-10-29 11:41:44','2009-12-16 12:28:26');
INSERT INTO users VALUES (5,'pam','440cbbedf1e789ad49ac0969d2d8069a','pam@eyespeak.com','','',3,'2009-11-02 16:25:58','2009-11-26 12:06:31');
INSERT INTO users VALUES (17,'benj','8449a534230bde5d4f26faa2d7cd6600','benj@eyespeak.com','','',3,'2010-01-07 02:08:22','2010-01-07 02:08:41');
INSERT INTO users VALUES (12,'rafa','8e17ee380d490b1af64e48b9af56afd4','rafael@eyespeak.com','','',3,'2009-12-09 15:58:58','2009-12-09 15:59:17');
INSERT INTO users VALUES (15,'Donna','5f4dcc3b5aa765d61d8327deb882cf99','donna@liiv.com','','',2,'2010-01-06 20:19:58','2010-01-12 15:50:57');
INSERT INTO users VALUES (10,'jess','5f4dcc3b5aa765d61d8327deb882cf99','jess@eyespeak.com','','',3,'2009-12-07 21:09:10','2010-01-07 19:23:58');
INSERT INTO users VALUES (27,'juan','a94652aa97c7211ba8954dd15a3cf838','juan@eyespeak.com','','',3,'2010-01-26 12:07:49','2010-01-29 13:41:17');
INSERT INTO users VALUES (18,'nico','e10adc3949ba59abbe56e057f20f883e','nico@eyespeak.com','','',2,'2010-01-11 15:01:44','2010-01-11 16:34:41');
INSERT INTO users VALUES (22,'Bernadette',NULL,'bernadette@liiv.com','','',2,'2010-01-12 01:18:57','2010-01-12 01:18:57');
INSERT INTO users VALUES (23,'Leesa','5f4dcc3b5aa765d61d8327deb882cf99','leesa@liiv.com','','',2,'2010-01-12 01:19:40','2010-01-12 01:19:40');
INSERT INTO users VALUES (24,'Greg','5f4dcc3b5aa765d61d8327deb882cf99','greg@liiv.com','','',2,'2010-01-12 01:20:09','2010-01-12 01:20:09');
INSERT INTO users VALUES (33,'Richa','5f4dcc3b5aa765d61d8327deb882cf99','richa@liiv.com','','',3,'2010-02-09 16:52:45','2010-02-09 16:52:45');
INSERT INTO users VALUES (26,'Slaby',NULL,'michael@liiv.com','','',3,'2010-01-12 15:50:14','2010-01-12 15:50:32');
INSERT INTO users VALUES (29,'jasonfrankmartin','440cbbedf1e789ad49ac0969d2d8069a','jasonfrankmartin@gmail.com','no se puede explorar','http://www.lanacion.com.ar/',1,'2010-01-28 16:24:23','2010-01-28 16:24:23');
INSERT INTO users VALUES (30,'testuser','5d9c68c6c50ed3d02a2fcf54f63993b6','testuser@test.com','','',1,'2010-01-29 16:46:18','2010-01-29 16:46:18');
INSERT INTO users VALUES (32,'johndoe','6579e96f76baa00787a28653876c6127','johndoe@eyespeak.com','','',3,'2010-02-08 21:09:12','2010-02-08 19:18:02');
INSERT INTO users VALUES (34,'jasonfrankmartin','8e17ee380d490b1af64e48b9af56afd4','rafaela@eyespeak.com','asdfgadsad','asdfgdasdad',1,'2010-02-11 14:48:17','2010-02-11 14:59:34');
INSERT INTO users VALUES (35,'rafaaaaa','4d186321c1a7f0f354b297e8914ab240','rafa@rafa.com','','',3,'2010-02-11 15:30:20','2010-02-11 15:30:20');
INSERT INTO users VALUES (36,'nicolas','71ae7b83d1342bd4e5be110004bdc969','dada@dada.com','','',1,'2010-02-11 18:54:24','2010-02-11 18:54:24');
INSERT INTO users VALUES (37,'sebastian','a8f5f167f44f4964e6c998dee827110c','sebastian@conseguir.com.ar','','',1,'2010-02-11 16:04:13','2010-02-12 10:49:12');
INSERT INTO users VALUES (38,'asd','7815696ecbf1c96e6894b779456d330e','asd@asd.com','','',1,'2010-02-11 16:52:29','2010-02-11 16:52:29');
INSERT INTO users VALUES (39,'testibng','a8f5f167f44f4964e6c998dee827110c','asd@asd33.com','','',1,'2010-02-12 10:58:06','2010-02-12 11:02:14');

#
# Table structure for table 'videos'
#

# DROP TABLE IF EXISTS videos;
CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(128) NOT NULL,
  `url` varchar(255) NOT NULL,
  `author` varchar(128) default NULL,
  `content` varchar(255) default NULL,
  `video_high_res` varchar(255) NOT NULL default '',
  `description_short` text,
  `area_id` int(11) unsigned default NULL,
  `videoseries_order` int(11) default '0',
  `featured` tinyint(1) default '0',
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  `videoseries_id` int(10) unsigned default NULL,
  `published` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'videos'
#

INSERT INTO videos VALUES (1,'Insomnia #1','insomnia-1','Joe Graedon, Pharamacologist, University of Michigan ','http://www.youtube.com/watch?v=5fDM43bxWks','','Joe Gradedon offers some natural alternatives to treating insomnia with products ranging from lavender to acupuncture points',6,1,0,'2009-12-07 13:14:15','2010-01-30 17:06:21',5,1,NULL,0);
INSERT INTO videos VALUES (33,'Lea Michele','lea-michele','','http://www.youtube.com/watch?v=DNFO7YQKL3I','','testing short description',1,2,0,'2009-12-09 11:58:37','2010-01-30 16:58:46',1,1,NULL,0);
INSERT INTO videos VALUES (32,'Othe','othe',NULL,'http://www.youtube.com/watch?v=yHDvTjNNPaI','',NULL,NULL,1,0,'2009-12-09 11:55:27','2010-02-17 18:18:22',1,1,NULL,0);
INSERT INTO videos VALUES (34,'Nuit Blanche','nuit-blanche','Spy Films','http://vimeo.com/9078364','','Nuit Blanche explores a fleeting moment between two strangers, revealing their brief connection in a hyper real fantasy.',1,2,0,'2009-12-09 12:55:20','2010-02-17 18:18:32',1,1,NULL,0);
INSERT INTO videos VALUES (45,'Galactic w/ Chali 2na and Boots Riley \"Immigrant Song\"','galactic-w-chali-2na-and-boots-riley-immigrant-song','Galactic','http://www.youtube.com/watch?v=YiY8dLX4tcs','','live at Fuji Rock Fest, Japan 7/25/08 on the White Stage also with Laidlaw',3,0,0,'2009-12-29 11:47:20','2010-01-30 17:06:52',1,1,NULL,0);
INSERT INTO videos VALUES (47,'Raimbeau Mars','test','me','http://www.youtube.com/watch?v=AAN21RerDJw','','Proin ipsum leo, luctus at commodo non, fringilla id turpis. In arcu metus, vehicula sit amet molestie ac, commodo eu elit. Quisque non nisl diam, scelerisque elementum tortor. Ut felis turpis, fermentum non facilisis id, ultricies nec nulla. Ut sollicitudin pretium orci scelerisque fermentum. Phasellus eget tellus at lectus accumsan luctus. Proin fringilla egestas posuere. Fusce purus magna, sodales nec ullamcorper id, cursus non dolor. Maecenas suscipit ultrices pharetra. Curabitur id leo nec velit bibendum mollis a eu ligula. Suspendisse at venenatis nisi. Nulla porta, magna id imperdiet scelerisque, urna enim auctor sapien, ut lacinia odio lorem id turpis. Phasellus egestas consequat euismod.',1,0,0,'2010-02-01 15:53:25','2010-02-01 17:20:56',NULL,NULL,NULL,0);
INSERT INTO videos VALUES (48,'Flowplayer sample video','flowplayer-sample-video','Flowplayer Ltd','/videos/669641754.flv','/videos/176817505.flv','<h2>Lorem ipsum</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultricies tortor ac ipsum dictum ut ornare diam fringilla. Aliquam eu condimentum justo. Donec semper pretium mauris ut pretium. Etiam semper dolor varius felis tempor iaculis. Mauris mi nisl, dapibus et fringilla ac, tempus ac nulla. Suspendisse nec est quis lectus vulputate vulputate. Sed pellentesque condimentum sapien, eget tristique sapien ullamcorper vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque id dui risus, eu dignissim nibh. Nunc ut bibendum.</p>',1,0,0,'2010-02-03 17:08:23','2010-02-04 15:44:52',NULL,NULL,NULL,0);
INSERT INTO videos VALUES (49,'Glee2','glee2','author','http://www.youtube.com/watch?v=zlTnt1TZLQU','','Short Description',1,0,0,'2010-02-05 14:59:33','2010-02-05 14:59:33',NULL,NULL,NULL,0);
INSERT INTO videos VALUES (50,'eTrade Babies','etrade-babies','etrade','http://www.youtube.com/watch?v=uHPg262Kr9c&feature=player_embedded','','test short description',1,2,0,'2010-02-05 15:04:54','2010-02-17 18:18:11',NULL,1,23,1);
INSERT INTO videos VALUES (51,'Sesame Street intro - Season 35','sesame-street','sesamestreet.org','/videos/152405917.flv','/videos/54486389.flv','Sesame Street intro from Season 35, episode 4061.',1,0,0,'2010-02-08 15:26:24','2010-02-08 15:26:24',NULL,NULL,NULL,0);
INSERT INTO videos VALUES (52,'Black Sabbath - Iron Man','black-sabbath-iron-man_1','','http://www.youtube.com/watch?v=GECit4oSpbk','','A bunch of awesome Iron Man pictures with Iron Man sung by Black Sabbath playing in the background.',1,0,0,'2010-02-09 12:30:04','2010-02-16 19:52:23',27,1,NULL,0);
INSERT INTO videos VALUES (53,'Codehunters','codehunters_1','Axis Animationnnnnnnnnnasdjgbh','http://vimeo.com/7432584','','Codehunters was created by the awesome Ben Hibon for MTV Asia and was broadcast as both a standalone short film and used to brand the MTV Asia Music Video Awards.',1,0,0,'2010-02-09 13:07:19','2010-02-16 19:56:22',27,1,NULL,0);
INSERT INTO videos VALUES (54,'FM','fm','Austin','http://www.youtube.com/watch?v=iFckoDkiy-8','','Christmas at eyespeak',1,0,0,'2010-02-09 23:51:39','2010-02-09 23:51:39',5,5,NULL,1);
INSERT INTO videos VALUES (55,'eTrade Outtakes','etrade-outtakes_1','etrade','http://www.youtube.com/watch?v=uHPg262Kr9c&feature=player_embedded','','test description for eTrade babies outtakes',1,0,0,'2010-02-11 03:11:55','2010-02-17 19:39:14',5,1,23,0);
INSERT INTO videos VALUES (56,'Maradó Maradó','marado-marado','god','http://www.youtube.com/watch?v=324UsTQsHAM','','The best football goal in human history.',1,0,0,'2010-02-11 19:20:10','2010-02-15 15:55:58',27,27,NULL,0);
INSERT INTO videos VALUES (57,'Soda Stereo - De Musica Ligera (El Ultimo Concierto)','soda-stereo-de-musica-ligera-el-ultimo-concierto','','http://www.youtube.com/watch?v=otLlQbDJiOs','','Soda Stereo - De Música Ligera\r\nEl Ultimo Concierto (En Vivo)\r\n20 De Septiembre de 1997\r\nEstadio de River Plate, Argentina',1,0,0,'2010-02-11 20:38:54','2010-02-16 19:24:31',NULL,1,NULL,0);
INSERT INTO videos VALUES (58,'QuickTime H.264 test','quicktime-h-264-test_1','Alex','/videos/158776260.mov','','Test of QuickTime (.mov) H.264-encoded video',1,0,0,'2010-02-12 17:45:10','2010-02-16 19:09:15',NULL,NULL,NULL,0);
INSERT INTO videos VALUES (59,'true h264 test','truetest','','/videos/31106187.mp4','','',1,0,0,'2010-02-15 18:10:10','2010-02-15 18:10:10',NULL,NULL,NULL,1);
INSERT INTO videos VALUES (60,'holistic1_2-11-10small.mp4','holistic1-2-11-10small-mp4','','/videos/353549768.mp4','','',1,0,0,'2010-02-16 01:19:42','2010-02-16 01:19:42',NULL,NULL,NULL,1);

#
# Table structure for table 'videos_audios'
#

# DROP TABLE IF EXISTS videos_audios;
CREATE TABLE `videos_audios` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `video_id` int(11) unsigned default NULL,
  `audio_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'videos_audios'
#


#
# Table structure for table 'videos_authorities'
#

# DROP TABLE IF EXISTS videos_authorities;
CREATE TABLE `videos_authorities` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `video_id` int(11) unsigned default NULL,
  `authority_id` int(11) unsigned default NULL,
  `exclusive` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'videos_authorities'
#


#
# Table structure for table 'videos_conditions'
#

# DROP TABLE IF EXISTS videos_conditions;
CREATE TABLE `videos_conditions` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `video_id` int(11) unsigned default NULL,
  `condition_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=242 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'videos_conditions'
#

INSERT INTO videos_conditions VALUES (189,1,18);
INSERT INTO videos_conditions VALUES (223,34,9);
INSERT INTO videos_conditions VALUES (222,33,9);
INSERT INTO videos_conditions VALUES (221,1,9);
INSERT INTO videos_conditions VALUES (220,54,9);
INSERT INTO videos_conditions VALUES (241,50,9);

#
# Table structure for table 'videos_events'
#

# DROP TABLE IF EXISTS videos_events;
CREATE TABLE `videos_events` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `video_id` int(11) unsigned default NULL,
  `event_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'videos_events'
#


#
# Table structure for table 'videos_organizations'
#

# DROP TABLE IF EXISTS videos_organizations;
CREATE TABLE `videos_organizations` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `video_id` int(11) unsigned default NULL,
  `organization_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'videos_organizations'
#


#
# Table structure for table 'videos_publications'
#

# DROP TABLE IF EXISTS videos_publications;
CREATE TABLE `videos_publications` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `video_id` int(11) unsigned default NULL,
  `publication_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'videos_publications'
#


#
# Table structure for table 'videos_therapies'
#

# DROP TABLE IF EXISTS videos_therapies;
CREATE TABLE `videos_therapies` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `video_id` int(11) unsigned default NULL,
  `therapy_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'videos_therapies'
#

INSERT INTO videos_therapies VALUES (25,54,12);
INSERT INTO videos_therapies VALUES (29,54,13);
INSERT INTO videos_therapies VALUES (31,50,6);
INSERT INTO videos_therapies VALUES (41,55,10);
INSERT INTO videos_therapies VALUES (40,55,8);

#
# Table structure for table 'videos_videos'
#

# DROP TABLE IF EXISTS videos_videos;
CREATE TABLE `videos_videos` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `video_id` int(11) unsigned default NULL,
  `video_sec_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'videos_videos'
#

INSERT INTO videos_videos VALUES (20,55,55);
INSERT INTO videos_videos VALUES (19,50,48);
INSERT INTO videos_videos VALUES (18,34,37);

#
# Table structure for table 'videoseries'
#

# DROP TABLE IF EXISTS videoseries;
CREATE TABLE `videoseries` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(150) NOT NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  `author` varchar(90) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'videoseries'
#

INSERT INTO videoseries VALUES (20,'New serie','2009-12-07 17:24:10','2010-02-17 18:18:11',1,5,'nico');
INSERT INTO videoseries VALUES (22,'testeadoOOOOOOUUUU','2009-12-14 14:43:56','2009-12-14 14:49:03',1,1,'jason');
INSERT INTO videoseries VALUES (23,'Test1','2010-02-17 19:39:14','2010-02-17 19:39:14',5,5,'');

#
# Table structure for table 'visits'
#

# DROP TABLE IF EXISTS visits;
CREATE TABLE `visits` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `entity_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `counter` int(10) unsigned NOT NULL default '0',
  `referer` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `Index_2` USING BTREE (`entity_id`,`item_id`,`referer`)
) ENGINE=InnoDB AUTO_INCREMENT=411 DEFAULT CHARSET=utf8;

#
# Dumping data for table 'visits'
#

INSERT INTO visits VALUES (1,18,0,1246,'/');
INSERT INTO visits VALUES (2,1,9,855,'/');
INSERT INTO visits VALUES (3,18,0,52,'/admin/login');
INSERT INTO visits VALUES (4,21,8,18,'/');
INSERT INTO visits VALUES (5,21,6,4,'/');
INSERT INTO visits VALUES (6,1,11,50,'/');
INSERT INTO visits VALUES (7,1,10,8,'/');
INSERT INTO visits VALUES (8,18,0,37,'/conditions/insomnia');
INSERT INTO visits VALUES (9,2,0,1,'/users/login');
INSERT INTO visits VALUES (10,18,0,1,'/pages/store');
INSERT INTO visits VALUES (11,18,0,57,'/store');
INSERT INTO visits VALUES (12,18,2,152,'/');
INSERT INTO visits VALUES (13,18,0,97,'/health');
INSERT INTO visits VALUES (14,1,0,884,'/');
INSERT INTO visits VALUES (15,1,0,19,'/conditions/insomnia');
INSERT INTO visits VALUES (16,1,0,15,'/conditions/test2');
INSERT INTO visits VALUES (17,1,0,34,'/conditions/test');
INSERT INTO visits VALUES (18,18,3,19,'/');
INSERT INTO visits VALUES (19,18,0,3,'/traditions/test2');
INSERT INTO visits VALUES (20,18,2,27,'/store');
INSERT INTO visits VALUES (21,1,0,1,'/admin/login');
INSERT INTO visits VALUES (22,18,0,2,'/stress');
INSERT INTO visits VALUES (23,18,0,1,'/store/detail/B002TR9DGO');
INSERT INTO visits VALUES (24,18,0,14,'/widget1.php');
INSERT INTO visits VALUES (25,18,0,3,'/store/detail/B001G6QD30');
INSERT INTO visits VALUES (26,18,2,1,'/store/detail/B000OEIS38');
INSERT INTO visits VALUES (27,18,0,2,'/store/detail/B0009R6LTS');
INSERT INTO visits VALUES (28,18,0,4,'/store_widget.php?n=1');
INSERT INTO visits VALUES (29,21,9,30,'/');
INSERT INTO visits VALUES (30,21,5,15,'/');
INSERT INTO visits VALUES (31,18,2,19,'/conditions/insomnia');
INSERT INTO visits VALUES (32,2,0,153,'/');
INSERT INTO visits VALUES (33,18,0,6,'/store_widget.php?n=4');
INSERT INTO visits VALUES (34,18,0,3,'/store/detail/B001U88QVQ');
INSERT INTO visits VALUES (35,18,2,1,'/store/detail/B001U88QVQ');
INSERT INTO visits VALUES (36,2,15,97,'/');
INSERT INTO visits VALUES (37,2,15,8,'/therapies/test');
INSERT INTO visits VALUES (38,5,0,2237,'/');
INSERT INTO visits VALUES (39,18,0,3,'/therapies/test');
INSERT INTO visits VALUES (40,18,2,10,'/videos/view/galactic-w-chali-2na-and-boots-riley-immigrant-song');
INSERT INTO visits VALUES (41,5,0,23,'/video');
INSERT INTO visits VALUES (42,18,2,1,'/therapies/test');
INSERT INTO visits VALUES (43,18,0,1,'/philosophies/test');
INSERT INTO visits VALUES (44,18,0,1,'http://webpolis.com.ar/web/projects/view/23/Backend_Management_System');
INSERT INTO visits VALUES (45,22,0,73,'/');
INSERT INTO visits VALUES (46,22,4,28,'/');
INSERT INTO visits VALUES (47,22,3,7,'/');
INSERT INTO visits VALUES (48,18,2,13,'/conditions');
INSERT INTO visits VALUES (49,18,0,35,'/conditions');
INSERT INTO visits VALUES (50,2,6,9,'/');
INSERT INTO visits VALUES (51,2,7,13,'/');
INSERT INTO visits VALUES (52,2,8,9,'/');
INSERT INTO visits VALUES (53,2,9,9,'/');
INSERT INTO visits VALUES (54,2,10,16,'/');
INSERT INTO visits VALUES (55,2,11,11,'/');
INSERT INTO visits VALUES (56,2,12,9,'/');
INSERT INTO visits VALUES (57,2,13,21,'/');
INSERT INTO visits VALUES (58,2,10,1,'/therapies/');
INSERT INTO visits VALUES (59,2,6,1,'/therapies/');
INSERT INTO visits VALUES (60,2,7,2,'/therapies/');
INSERT INTO visits VALUES (61,2,8,4,'/therapies/');
INSERT INTO visits VALUES (62,2,11,4,'/therapies/');
INSERT INTO visits VALUES (63,2,13,14,'/therapies/');
INSERT INTO visits VALUES (64,1,9,143,'/conditions');
INSERT INTO visits VALUES (65,1,9,4,'/conditions/insomnia');
INSERT INTO visits VALUES (66,18,0,15,'/admin');
INSERT INTO visits VALUES (67,2,13,34,'/therapies');
INSERT INTO visits VALUES (68,2,8,20,'/therapies');
INSERT INTO visits VALUES (69,18,0,1,'/philosophies/test2');
INSERT INTO visits VALUES (70,18,2,6,'/philosophies/test2');
INSERT INTO visits VALUES (71,18,7,16,'/');
INSERT INTO visits VALUES (72,18,8,12,'/');
INSERT INTO visits VALUES (73,18,2,1,'/privacy-policy');
INSERT INTO visits VALUES (74,18,7,10,'/privacy-policy');
INSERT INTO visits VALUES (75,18,8,8,'/terms-and-conditions');
INSERT INTO visits VALUES (76,18,7,3,'/conditions/insomnia');
INSERT INTO visits VALUES (77,2,8,1,'/therapies/test');
INSERT INTO visits VALUES (78,2,10,4,'/therapies/test');
INSERT INTO visits VALUES (79,2,10,1,'/therapies/acupuncture');
INSERT INTO visits VALUES (80,2,11,1,'/therapies/acupuncture');
INSERT INTO visits VALUES (81,18,0,3,'/therapies/acupuncture');
INSERT INTO visits VALUES (82,18,0,11,'/access');
INSERT INTO visits VALUES (83,18,0,7,'/users/login');
INSERT INTO visits VALUES (84,1,9,19,'/health');
INSERT INTO visits VALUES (85,18,7,2,'/stress');
INSERT INTO visits VALUES (86,18,0,4,'/privacy-policy');
INSERT INTO visits VALUES (87,1,0,7,'/store');
INSERT INTO visits VALUES (88,18,7,1,'/health');
INSERT INTO visits VALUES (89,2,8,7,'/therapies/herbalism');
INSERT INTO visits VALUES (90,2,9,2,'/therapies/over-the-counter-medications');
INSERT INTO visits VALUES (91,2,9,2,'/therapies');
INSERT INTO visits VALUES (92,22,4,10,'/traditions');
INSERT INTO visits VALUES (93,22,3,8,'/traditions/test2');
INSERT INTO visits VALUES (94,22,3,2,'/traditions');
INSERT INTO visits VALUES (95,2,8,16,'/conditions/insomnia');
INSERT INTO visits VALUES (96,2,15,2,'/therapies/over-the-counter-medications');
INSERT INTO visits VALUES (97,2,7,2,'/conditions/insomnia');
INSERT INTO visits VALUES (98,1,0,2,'/conditions/');
INSERT INTO visits VALUES (99,18,2,2,'/conditions/');
INSERT INTO visits VALUES (100,22,3,2,'/traditions/');
INSERT INTO visits VALUES (101,18,0,6,'/terms-and-conditions');
INSERT INTO visits VALUES (102,18,0,3,'/contact');
INSERT INTO visits VALUES (103,18,8,5,'/contact');
INSERT INTO visits VALUES (104,18,9,8,'/');
INSERT INTO visits VALUES (105,18,9,1,'/advertise-and-sponsorship');
INSERT INTO visits VALUES (106,18,0,9,'/advertise-and-sponsorship');
INSERT INTO visits VALUES (107,18,9,4,'/contact');
INSERT INTO visits VALUES (108,18,0,26,'/about');
INSERT INTO visits VALUES (109,18,1,10,'/');
INSERT INTO visits VALUES (110,18,1,2,'/terms-and-conditions');
INSERT INTO visits VALUES (111,18,9,3,'/about');
INSERT INTO visits VALUES (112,18,0,3,'/admin/liiv_todays');
INSERT INTO visits VALUES (113,18,0,4,'/admin/conditions');
INSERT INTO visits VALUES (114,2,9,4,'/conditions/insomnia');
INSERT INTO visits VALUES (115,21,14,9,'/');
INSERT INTO visits VALUES (116,2,6,7,'/therapies');
INSERT INTO visits VALUES (117,18,0,1,'/admin/users/edit/14');
INSERT INTO visits VALUES (118,18,0,6,'/admin/users');
INSERT INTO visits VALUES (119,22,3,10,'/traditions/test');
INSERT INTO visits VALUES (120,2,11,8,'/therapies/herbalism');
INSERT INTO visits VALUES (121,2,13,12,'/therapies/homeopathy');
INSERT INTO visits VALUES (122,1,12,4,'/');
INSERT INTO visits VALUES (123,1,9,2,'/conditions/sick');
INSERT INTO visits VALUES (124,1,12,14,'/conditions/insomnia');
INSERT INTO visits VALUES (125,1,12,12,'/conditions/sick');
INSERT INTO visits VALUES (126,21,15,9,'/');
INSERT INTO visits VALUES (127,2,10,13,'/therapies');
INSERT INTO visits VALUES (128,18,0,3,'/conditions/sick');
INSERT INTO visits VALUES (129,18,1,1,'/advertise-and-sponsorship');
INSERT INTO visits VALUES (130,18,8,1,'/about');
INSERT INTO visits VALUES (131,2,9,2,'http://mail.google.com/a/eyespeak.com/?ui=2&view=bsp&ver=1qygpcgurkovy');
INSERT INTO visits VALUES (132,2,0,2,'http://mail.google.com/a/eyespeak.com/?ui=2&view=bsp&ver=1qygpcgurkovy');
INSERT INTO visits VALUES (133,22,4,6,'http://pm.eyespeak.com/index.php?c=task&a=view_task&id=1014&active_project=27');
INSERT INTO visits VALUES (134,18,0,1,'/login');
INSERT INTO visits VALUES (135,18,0,1,'/admin/conditions/add');
INSERT INTO visits VALUES (136,18,0,2,'/admin/banners/view/1');
INSERT INTO visits VALUES (137,10,0,98,'/');
INSERT INTO visits VALUES (138,18,0,6,'/unauthorized');
INSERT INTO visits VALUES (139,10,1,1601,'/');
INSERT INTO visits VALUES (140,18,0,45,'/featured');
INSERT INTO visits VALUES (141,10,1,4,'/admin/login');
INSERT INTO visits VALUES (142,18,0,1,'/therapies/prescription-medications');
INSERT INTO visits VALUES (143,18,0,1,'http://pm.eyespeak.com/index.php?c=task&a=view_task&id=1073&active_project=27');
INSERT INTO visits VALUES (144,1,14,18,'/');
INSERT INTO visits VALUES (145,10,1,32,'/conditions/insomnia');
INSERT INTO visits VALUES (146,10,1,84,'/featured');
INSERT INTO visits VALUES (147,18,0,1,'http://pm.eyespeak.com/index.php?c=dashboard&a=index');
INSERT INTO visits VALUES (148,10,1,2,'/admin/liiv_todays');
INSERT INTO visits VALUES (149,10,1,162,'/health');
INSERT INTO visits VALUES (150,10,1,30,'/store');
INSERT INTO visits VALUES (151,10,1,6,'/about');
INSERT INTO visits VALUES (152,10,1,8,'/advertise-and-sponsorship');
INSERT INTO visits VALUES (153,10,1,10,'/contact');
INSERT INTO visits VALUES (154,10,1,8,'/privacy-policy');
INSERT INTO visits VALUES (155,10,1,10,'/terms-and-conditions');
INSERT INTO visits VALUES (156,10,1,8,'/access');
INSERT INTO visits VALUES (157,1,14,2,'/conditions/insomnia');
INSERT INTO visits VALUES (158,10,1,72,'/conditions');
INSERT INTO visits VALUES (159,1,14,30,'/conditions');
INSERT INTO visits VALUES (160,1,9,4,'/conditions/test');
INSERT INTO visits VALUES (161,10,1,2,'/unauthorized');
INSERT INTO visits VALUES (162,10,1,6,'/admin/therapies/add');
INSERT INTO visits VALUES (163,18,0,3,'/admin/therapies/add');
INSERT INTO visits VALUES (164,10,1,14,'/therapies');
INSERT INTO visits VALUES (165,18,0,7,'/therapies');
INSERT INTO visits VALUES (166,2,10,2,'/therapies/over-the-counter-medications');
INSERT INTO visits VALUES (167,10,1,2,'/admin/slides/add');
INSERT INTO visits VALUES (168,18,0,1,'/admin/slides/add');
INSERT INTO visits VALUES (169,10,1,4,'/therapies/acupuncture');
INSERT INTO visits VALUES (170,10,1,22,'/admin');
INSERT INTO visits VALUES (171,18,7,1,'/store');
INSERT INTO visits VALUES (172,2,11,2,'/therapies/cognitive-behaviour-therapy');
INSERT INTO visits VALUES (173,1,9,5,'/stress');
INSERT INTO visits VALUES (174,10,1,6,'http://software-web-design.net/web/projects/view/23/Backend_Management_System');
INSERT INTO visits VALUES (175,18,0,3,'http://software-web-design.net/web/projects/view/23/Backend_Management_System');
INSERT INTO visits VALUES (176,5,0,41,'/conditions/insomnia');
INSERT INTO visits VALUES (177,10,1,16,'/featured/video');
INSERT INTO visits VALUES (178,5,0,15,'/featured/video');
INSERT INTO visits VALUES (179,18,0,7,'/featured/video');
INSERT INTO visits VALUES (180,10,1,14,'/featured/story');
INSERT INTO visits VALUES (181,5,0,14,'/featured/story');
INSERT INTO visits VALUES (182,18,0,3,'/featured/story');
INSERT INTO visits VALUES (183,10,1,2,'/admin/videos/edit/45');
INSERT INTO visits VALUES (184,5,0,2,'/admin/videos/edit/45');
INSERT INTO visits VALUES (185,18,0,1,'/admin/videos/edit/45');
INSERT INTO visits VALUES (186,2,6,9,'/conditions/insomnia');
INSERT INTO visits VALUES (187,5,0,167,'/health');
INSERT INTO visits VALUES (188,10,1,2,'http://liiv.localhost/');
INSERT INTO visits VALUES (189,5,0,2,'http://liiv.localhost/');
INSERT INTO visits VALUES (190,1,0,1,'http://liiv.localhost/');
INSERT INTO visits VALUES (191,18,3,1,'http://liiv.localhost/');
INSERT INTO visits VALUES (192,10,1,2,'/admin/conditions');
INSERT INTO visits VALUES (193,5,0,2,'/admin/conditions');
INSERT INTO visits VALUES (194,5,0,21,'/admin');
INSERT INTO visits VALUES (195,10,1,8,'/item1');
INSERT INTO visits VALUES (196,5,0,8,'/item1');
INSERT INTO visits VALUES (197,18,0,4,'/item1');
INSERT INTO visits VALUES (198,5,0,18,'/therapies');
INSERT INTO visits VALUES (199,5,0,99,'/conditions');
INSERT INTO visits VALUES (200,1,0,4,'/featured/story');
INSERT INTO visits VALUES (201,18,2,3,'/featured/story');
INSERT INTO visits VALUES (202,21,18,24,'/');
INSERT INTO visits VALUES (203,21,16,2,'/');
INSERT INTO visits VALUES (204,21,19,20,'/');
INSERT INTO visits VALUES (205,21,20,1,'/');
INSERT INTO visits VALUES (206,21,17,13,'/');
INSERT INTO visits VALUES (207,5,0,9,'/contact');
INSERT INTO visits VALUES (208,1,0,2,'/contact');
INSERT INTO visits VALUES (209,18,2,2,'/contact');
INSERT INTO visits VALUES (210,5,0,7,'/terms-and-conditions');
INSERT INTO visits VALUES (211,10,1,8,'/store_widget.php?n=4');
INSERT INTO visits VALUES (212,5,0,11,'/store_widget.php?n=4');
INSERT INTO visits VALUES (213,5,0,21,'/store');
INSERT INTO visits VALUES (214,2,13,2,'/conditions/insomnia');
INSERT INTO visits VALUES (215,10,1,2,'/store_widget.php?n=1');
INSERT INTO visits VALUES (216,5,0,3,'/store_widget.php?n=1');
INSERT INTO visits VALUES (217,1,16,8,'/');
INSERT INTO visits VALUES (218,2,12,3,'/therapies');
INSERT INTO visits VALUES (219,2,16,14,'/');
INSERT INTO visits VALUES (220,1,17,4,'/');
INSERT INTO visits VALUES (221,2,12,2,'/conditions/canyousee');
INSERT INTO visits VALUES (222,22,5,5,'/');
INSERT INTO visits VALUES (223,2,13,3,'/therapies/acupuncture');
INSERT INTO visits VALUES (224,10,1,2,'/store/detail/B000OEIS38');
INSERT INTO visits VALUES (225,5,0,2,'/store/detail/B000OEIS38');
INSERT INTO visits VALUES (226,18,0,1,'/store/detail/B000OEIS38');
INSERT INTO visits VALUES (227,1,16,10,'/conditions');
INSERT INTO visits VALUES (228,10,1,2,'http://www.google.com/search?hl=en&client=safari&rls=en&q=liiv+health+site&start=10&sa=N');
INSERT INTO visits VALUES (229,5,0,2,'http://www.google.com/search?hl=en&client=safari&rls=en&q=liiv+health+site&start=10&sa=N');
INSERT INTO visits VALUES (230,18,7,1,'http://www.google.com/search?hl=en&client=safari&rls=en&q=liiv+health+site&start=10&sa=N');
INSERT INTO visits VALUES (231,5,0,6,'/about');
INSERT INTO visits VALUES (232,1,0,1,'/about');
INSERT INTO visits VALUES (233,18,2,1,'/about');
INSERT INTO visits VALUES (234,18,9,1,'/store');
INSERT INTO visits VALUES (235,5,0,6,'/advertise-and-sponsorship');
INSERT INTO visits VALUES (236,10,1,2,'http://www.google.com/search?hl=en&client=safari&rls=en&q=liiv+health&aq=f&aql=&aqi=&oq=');
INSERT INTO visits VALUES (237,5,0,2,'http://www.google.com/search?hl=en&client=safari&rls=en&q=liiv+health&aq=f&aql=&aqi=&oq=');
INSERT INTO visits VALUES (238,18,8,1,'http://www.google.com/search?hl=en&client=safari&rls=en&q=liiv+health&aq=f&aql=&aqi=&oq=');
INSERT INTO visits VALUES (239,5,0,8,'/privacy-policy');
INSERT INTO visits VALUES (240,10,1,2,'/therapies/herbalism');
INSERT INTO visits VALUES (241,5,0,6,'/therapies/herbalism');
INSERT INTO visits VALUES (242,18,0,1,'/therapies/herbalism');
INSERT INTO visits VALUES (243,18,1,2,'/conditions');
INSERT INTO visits VALUES (244,1,0,7,'/conditions');
INSERT INTO visits VALUES (245,1,0,7,'/health');
INSERT INTO visits VALUES (246,2,0,4,'/conditions');
INSERT INTO visits VALUES (247,22,0,4,'/therapies');
INSERT INTO visits VALUES (248,10,1,14,'/traditions');
INSERT INTO visits VALUES (249,5,0,17,'/traditions');
INSERT INTO visits VALUES (250,1,0,1,'/traditions');
INSERT INTO visits VALUES (251,18,3,1,'/traditions');
INSERT INTO visits VALUES (252,22,0,10,'/stress');
INSERT INTO visits VALUES (253,2,0,6,'/traditions');
INSERT INTO visits VALUES (254,1,0,4,'/therapies');
INSERT INTO visits VALUES (255,10,1,2,'/stress');
INSERT INTO visits VALUES (256,5,0,11,'/stress');
INSERT INTO visits VALUES (257,18,3,3,'/health');
INSERT INTO visits VALUES (258,18,0,6,'/traditions');
INSERT INTO visits VALUES (259,5,0,4,'/admin/login');
INSERT INTO visits VALUES (260,1,16,2,'/conditions/insomnia');
INSERT INTO visits VALUES (261,1,17,4,'/conditions/cronic-head-ache');
INSERT INTO visits VALUES (262,1,16,4,'/conditions/canyousee');
INSERT INTO visits VALUES (263,10,1,6,'/admin/users');
INSERT INTO visits VALUES (264,5,0,9,'/admin/users');
INSERT INTO visits VALUES (265,21,24,7,'/');
INSERT INTO visits VALUES (266,1,17,5,'/conditions');
INSERT INTO visits VALUES (267,1,9,2,'/conditions/canyousee');
INSERT INTO visits VALUES (268,5,0,4,'http://software-web-design.net/web/projects/view/23/Backend_Management_System');
INSERT INTO visits VALUES (269,18,3,1,'/featured/story');
INSERT INTO visits VALUES (270,4,13,4,'/');
INSERT INTO visits VALUES (271,4,14,8,'/');
INSERT INTO visits VALUES (272,4,16,7,'/');
INSERT INTO visits VALUES (273,2,6,1,'http://www.google.com/search?hl=en&newwindow=1&q=%E2%80%9CCognitive-behavioral+therapy+integrates+the+cognitive+restructuring+approach+of+cognitive+therapy+with+the+behavioral+modification+techniques+of+behavioral+therapy.+The+therapist+works+with+the+pat');
INSERT INTO visits VALUES (274,1,14,2,'/health');
INSERT INTO visits VALUES (275,1,16,3,'/conditions/test');
INSERT INTO visits VALUES (276,10,1,2,'/conditions/cronic-head-ache');
INSERT INTO visits VALUES (277,5,0,4,'/conditions/cronic-head-ache');
INSERT INTO visits VALUES (278,18,0,1,'/conditions/cronic-head-ache');
INSERT INTO visits VALUES (279,10,1,10,'/video');
INSERT INTO visits VALUES (280,18,0,2,'/video');
INSERT INTO visits VALUES (281,1,16,4,'/health');
INSERT INTO visits VALUES (282,2,0,2,'/conditions/insomnia');
INSERT INTO visits VALUES (283,5,0,3,'/therapies/acupuncture');
INSERT INTO visits VALUES (284,1,0,1,'/therapies/acupuncture');
INSERT INTO visits VALUES (285,18,2,1,'/therapies/acupuncture');
INSERT INTO visits VALUES (286,22,0,2,'/therapies/acupuncture');
INSERT INTO visits VALUES (287,18,9,1,'/conditions');
INSERT INTO visits VALUES (288,4,13,5,'/conditions/insomnia');
INSERT INTO visits VALUES (289,10,1,24,'/partnership-opportunities');
INSERT INTO visits VALUES (290,5,0,25,'/partnership-opportunities');
INSERT INTO visits VALUES (291,18,0,12,'/partnership-opportunities');
INSERT INTO visits VALUES (292,4,14,3,'/conditions/insomnia');
INSERT INTO visits VALUES (293,21,25,5,'/');
INSERT INTO visits VALUES (294,21,29,4,'/');
INSERT INTO visits VALUES (295,21,13,1,'/');
INSERT INTO visits VALUES (296,21,30,2,'/');
INSERT INTO visits VALUES (297,5,0,5,'/access');
INSERT INTO visits VALUES (298,10,1,10,'/admin/videos');
INSERT INTO visits VALUES (299,5,0,10,'/admin/videos');
INSERT INTO visits VALUES (300,18,0,5,'/admin/videos');
INSERT INTO visits VALUES (301,10,1,10,'/video/test');
INSERT INTO visits VALUES (302,5,0,10,'/video/test');
INSERT INTO visits VALUES (303,18,0,5,'/video/test');
INSERT INTO visits VALUES (304,1,0,10,'/video/test');
INSERT INTO visits VALUES (305,1,17,2,'/video/test');
INSERT INTO visits VALUES (306,10,1,2,'http://www.google.com/search?hl=en&source=hp&q=liiv+codigoaustral&aq=f&aqi=&oq=');
INSERT INTO visits VALUES (307,5,0,2,'http://www.google.com/search?hl=en&source=hp&q=liiv+codigoaustral&aq=f&aqi=&oq=');
INSERT INTO visits VALUES (308,18,0,1,'http://www.google.com/search?hl=en&source=hp&q=liiv+codigoaustral&aq=f&aqi=&oq=');
INSERT INTO visits VALUES (309,10,0,9,'/conditions');
INSERT INTO visits VALUES (310,10,0,6,'/stories/1');
INSERT INTO visits VALUES (311,5,0,6,'/stories/1');
INSERT INTO visits VALUES (312,18,0,3,'/stories/1');
INSERT INTO visits VALUES (313,10,0,3,'/health');
INSERT INTO visits VALUES (314,10,3,1,'/');
INSERT INTO visits VALUES (315,10,0,2,'http://www.google.com/search?hl=en&q=liiv+codigoaustral&aq=f&aqi=&oq=');
INSERT INTO visits VALUES (316,5,0,2,'http://www.google.com/search?hl=en&q=liiv+codigoaustral&aq=f&aqi=&oq=');
INSERT INTO visits VALUES (317,18,1,1,'http://www.google.com/search?hl=en&q=liiv+codigoaustral&aq=f&aqi=&oq=');
INSERT INTO visits VALUES (318,2,9,2,'http://www.google.com/url?sa=t&source=web&ct=res&cd=4&ved=0CBEQFjAD&url=http%3A%2F%2Fliiv.codigoaustral.com%2Ftherapies%2Ftraditional-chinese-medicine&rct=j&q=liiv+insomnia&ei=77RoS_akFtGOtgfSpbnfBg&usg=AFQjCNFuD5ehdFFCodZNcL_loxvFicWFVg');
INSERT INTO visits VALUES (319,10,0,1,'/admin');
INSERT INTO visits VALUES (320,1,9,2,'/video/flowplayer-sample-video');
INSERT INTO visits VALUES (321,1,14,32,'/conditions/test');
INSERT INTO visits VALUES (322,2,0,4,'/conditions/test');
INSERT INTO visits VALUES (323,2,16,2,'/therapies/herbalism');
INSERT INTO visits VALUES (324,2,16,6,'/therapies/canyousee');
INSERT INTO visits VALUES (325,2,16,4,'/therapies');
INSERT INTO visits VALUES (326,22,0,2,'/therapies/canyousee');
INSERT INTO visits VALUES (327,2,0,4,'/therapies');
INSERT INTO visits VALUES (328,1,0,2,'/traditions/test');
INSERT INTO visits VALUES (329,10,0,1,'/therapies/canyousee');
INSERT INTO visits VALUES (330,5,0,4,'/therapies/canyousee');
INSERT INTO visits VALUES (331,18,0,1,'/therapies/canyousee');
INSERT INTO visits VALUES (332,4,16,8,'/conditions/insomnia');
INSERT INTO visits VALUES (333,1,0,2,'http://test.liiv.com/');
INSERT INTO visits VALUES (334,2,0,2,'/health');
INSERT INTO visits VALUES (335,1,19,1,NULL);
INSERT INTO visits VALUES (336,1,19,27,'/conditions');
INSERT INTO visits VALUES (337,1,19,3,'/');
INSERT INTO visits VALUES (338,5,0,3,'/conditions/testing-visits');
INSERT INTO visits VALUES (339,1,20,1,NULL);
INSERT INTO visits VALUES (340,3,7,1,NULL);
INSERT INTO visits VALUES (341,1,16,2,'/conditions/');
INSERT INTO visits VALUES (342,1,9,16,'/conditions/cronic-head-ache');
INSERT INTO visits VALUES (343,1,21,1,NULL);
INSERT INTO visits VALUES (344,1,20,1,'/conditions');
INSERT INTO visits VALUES (345,1,21,35,'/conditions/testing-conditions-visits');
INSERT INTO visits VALUES (346,5,49,1,NULL);
INSERT INTO visits VALUES (347,5,50,1,NULL);
INSERT INTO visits VALUES (348,1,21,30,'/');
INSERT INTO visits VALUES (349,1,9,1,'/conditions/testing-fields');
INSERT INTO visits VALUES (350,2,7,3,'/therapies');
INSERT INTO visits VALUES (351,2,16,2,'/therapies/prescription-medications');
INSERT INTO visits VALUES (352,2,16,1,'/admin/therapies/view/16');
INSERT INTO visits VALUES (353,1,21,1,'/admin/conditions/view/21');
INSERT INTO visits VALUES (354,22,5,5,'/admin/traditions/view/5');
INSERT INTO visits VALUES (355,5,0,4,'http://test.liiv.com/');
INSERT INTO visits VALUES (356,1,14,1,'/stress');
INSERT INTO visits VALUES (357,1,20,1,'/conditions/test');
INSERT INTO visits VALUES (358,2,9,1,'/therapies/cognitive-behaviour-therapy');
INSERT INTO visits VALUES (359,1,16,1,'/stress');
INSERT INTO visits VALUES (360,5,51,1,NULL);
INSERT INTO visits VALUES (361,5,0,1,'/admin/users/add');
INSERT INTO visits VALUES (362,5,0,1,'/admin/');
INSERT INTO visits VALUES (363,5,52,1,NULL);
INSERT INTO visits VALUES (364,5,53,1,NULL);
INSERT INTO visits VALUES (365,5,54,1,NULL);
INSERT INTO visits VALUES (366,5,0,1,'/videos/view/galactic-w-chali-2na-and-boots-riley-immigrant-song');
INSERT INTO visits VALUES (367,5,55,1,NULL);
INSERT INTO visits VALUES (368,5,0,1,'/video/etrade-outtakes');
INSERT INTO visits VALUES (369,5,0,1,'/video/etrade-babies');
INSERT INTO visits VALUES (370,2,17,1,NULL);
INSERT INTO visits VALUES (371,2,17,8,'/therapies');
INSERT INTO visits VALUES (372,2,17,10,'/');
INSERT INTO visits VALUES (373,22,5,2,'/traditions');
INSERT INTO visits VALUES (374,2,11,1,'/therapies');
INSERT INTO visits VALUES (375,5,0,1,'/traditions/canyousee');
INSERT INTO visits VALUES (376,1,16,1,'/video/insomnia-1');
INSERT INTO visits VALUES (377,1,21,1,'/conditions/cronic-head-ache');
INSERT INTO visits VALUES (378,1,21,1,'/conditions/testing-fields');
INSERT INTO visits VALUES (379,1,16,1,'/conditions/testing-fields');
INSERT INTO visits VALUES (380,1,14,2,'/conditions/cronic-head-ache');
INSERT INTO visits VALUES (381,2,6,1,'/conditions/test');
INSERT INTO visits VALUES (382,2,10,1,'/therapies/cognitive-behaviour-therapy');
INSERT INTO visits VALUES (383,5,0,1,'/therapies/over-the-counter-medications');
INSERT INTO visits VALUES (384,1,19,1,'/health');
INSERT INTO visits VALUES (385,5,0,1,'/therapies/yoga');
INSERT INTO visits VALUES (386,1,14,1,'/video/fm');
INSERT INTO visits VALUES (387,5,0,1,'/conditions/test');
INSERT INTO visits VALUES (388,5,56,1,NULL);
INSERT INTO visits VALUES (389,5,57,1,NULL);
INSERT INTO visits VALUES (390,1,22,1,NULL);
INSERT INTO visits VALUES (391,1,22,2,'/conditions/insomnia');
INSERT INTO visits VALUES (392,1,9,2,'/conditions/testing-reference');
INSERT INTO visits VALUES (393,21,31,1,NULL);
INSERT INTO visits VALUES (394,21,10,5,'/');
INSERT INTO visits VALUES (395,21,31,1,'/');
INSERT INTO visits VALUES (396,5,58,1,NULL);
INSERT INTO visits VALUES (397,1,22,21,'/');
INSERT INTO visits VALUES (398,1,23,1,NULL);
INSERT INTO visits VALUES (399,1,23,10,'/conditions');
INSERT INTO visits VALUES (400,1,23,6,'/');
INSERT INTO visits VALUES (401,5,0,3,'/conditions/asd');
INSERT INTO visits VALUES (402,4,14,1,'/video/test');
INSERT INTO visits VALUES (403,2,18,1,NULL);
INSERT INTO visits VALUES (404,2,18,6,'/');
INSERT INTO visits VALUES (405,18,10,1,NULL);
INSERT INTO visits VALUES (406,5,0,2,'/users/login');
INSERT INTO visits VALUES (407,5,59,1,NULL);
INSERT INTO visits VALUES (408,5,0,3,'/video/black-sabbath-iron-man');
INSERT INTO visits VALUES (409,5,0,1,'http://www.google.com/search?hl=en&source=hp&q=PLANETHOPE+LLC&aq=f&aqi=&oq=');
INSERT INTO visits VALUES (410,5,0,1,'/admin/videos/edit/50');

#
# Table structure for table 'workshops'
#

# DROP TABLE IF EXISTS workshops;
CREATE TABLE `workshops` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(45) NOT NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `createdby` int(10) unsigned default NULL,
  `updatedby` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Dumping data for table 'workshops'
#


